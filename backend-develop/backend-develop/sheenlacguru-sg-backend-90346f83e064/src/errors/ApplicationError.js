class ApplicationError extends Error {
    constructor({name = "ApplicationError", message = "Oops! something went wrong.", status = 500, info = {}} = {}) {
        super(message);
        this.name = name;
        this.status = status;
        this.info = info;
    }
}

module.exports = ApplicationError;