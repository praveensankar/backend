const multer = require('multer');
const aws = require('aws-sdk');
const multerS3 = require('multer-s3');
const uniqid = require('uniqid');
const lodash = require('lodash');
const nconf = require('nconf');

const ApplicationError = require('@src/errors/ApplicationError');

class Uploader {

    static uploader(options = {}) {
        const defaultOptions = {
            accept: ["image/png", "image/jpeg", "image/jpg", "application/pdf"],
            bucket: nconf.get("STORAGE:BUCKET"),
            dest: ''
        };
        options = lodash.defaultsDeep(options, defaultOptions);
        const fileFilter = (req, file, cb) => {
            if (lodash.includes(options.accept, file.mimetype)) {
                return cb(null, true)
            }
            return cb(new ApplicationError({status: 400, name: 'ImageFormatError', message: `${file.fieldname} format not accepted.`}));
        };
        const storage = multerS3({
            s3: new aws.S3({
                accessKeyId : nconf.get("STORAGE:ACCESSKEYID"),
                secretAccessKey : nconf.get("STORAGE:SECRECTACCESSKEY")
            }),
            acl: 'public-read',
            bucket: options.bucket,
            contentType: multerS3.AUTO_CONTENT_TYPE,
            metadata: function (req, file, cb) {
                console.log(file)
                cb(null, { fieldname: file.fieldname });
            },
            key: function (req, file, cb) {
                const extArr = file.mimetype.split('/');
                const ext = (extArr.length > 1) ? `.${extArr[extArr.length - 1]}` : '';
                cb(null, `${options.dest}${uniqid()}${ext}`);
            }
        });
        /* const storage = multer.diskStorage({
            destination: options.dest,
            filename: function (req, file, cb) {
                const extArr = file.mimetype.split('/');
                const ext = (extArr.length > 1) ? `.${extArr[extArr.length - 1]}` : '';
                cb(null, `${uniqid()}${ext}`);
            }
        }) */
        return multer({
            storage: storage,
            fileFilter: fileFilter
        });
    }

}

module.exports = Uploader.uploader;