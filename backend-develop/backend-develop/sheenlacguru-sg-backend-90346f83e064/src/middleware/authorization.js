const lodash = require("lodash");

const ApplicationError = require('@src/errors/ApplicationError');

class Authorization {

    static authorize(roles) {
        return  (req, res, next) => {
            roles = lodash.compact(roles);
            let oldUserRoles = lodash.get(req, "locals.claims.role", []);
            let newUserRoles = lodash.get(req,"locals.claims.access_previledges",[]);
            let userRoles = lodash.concat(oldUserRoles, newUserRoles);
            let isAuthorized = false;
            let permissions = [];
            console.log(userRoles)

            roles.forEach(element => {
                if (lodash.includes(userRoles, element)) {
                    permissions.push(element);
                }
            });
            if (permissions.length > 0 ) {
                isAuthorized = true;
                next();
            }
            console.log(permissions);
            return !isAuthorized && next(
                new ApplicationError({
                    status: 401, 
                    name: 'Unauthorized Request', 
                    message: 'You are not authorized to perform this action.'
                })
            );
        };
    }

}

module.exports = Authorization;