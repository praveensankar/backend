const Promise = require('bluebird');
const _ = require('lodash');
const jwt = Promise.promisifyAll(require('jsonwebtoken'));
const nconf = require('nconf');

const ApplicationError = require('@src/errors/ApplicationError');

class Token {

    static generate(options = {}) {
        options = _.defaultsDeep(options, { for: 'admin' });
        return (req, res, next) => {
            if (!req.locals.claims)
                return next(new ApplicationError({message: 'Token claims are required.'}));
            
            const token_claims = {
                sub: req.locals.claims.id,
                iss: nconf.get("APPLICATION:NAME"),
                aud: nconf.get("APPLICATION:NAME"),
                exp: Math.floor(Date.now()/1000) + nconf.get(`JWT:${options.for.toUpperCase()}:EXPIRATION`),
                claims: req.locals.claims
            }

            return jwt.signAsync(token_claims, nconf.get(`JWT:${options.for.toUpperCase()}:SECRET`))
                .then( (access_token) => {
                    let response_data = {
                        access_token: access_token,
                        token_type: "Bearer",
                        expires_in: nconf.get(`JWT:${options.for.toUpperCase()}:EXPIRATION`)
                    };
                    if (req.locals.user_status) response_data["user_status"] = req.locals.user_status;
                    response_data["sp_verified"] = req.locals.sp_verified;
                    return res.status(200).json(
                        new res.app.locals.Success({data: response_data})
                    );
                })
                .catch( (err) => {
                    return next(err);
                });
        };
    }

    static verify(options = {}) {
        options = _.defaultsDeep(options, { for: 'admin' });
        return (req, res, next) => {
            const authorization = req.get('Authorization') || "";
            const token = authorization.replace(/Bearer /g, '');

            if (!token)
                return next(new ApplicationError({name: 'BadAccessToken', status: 401, message: 'Missing/Bad Access token.'}));

            const jwt_verify_options = {
                issuer: nconf.get("APPLICATION:NAME"),
                audience: nconf.get("APPLICATION:NAME"),
                ignoreExpiration: nconf.get(`JWT:${options.for.toUpperCase()}:IGNORE_EXPIRATION`)
            }

            return jwt.verifyAsync(token, nconf.get(`JWT:${options.for.toUpperCase()}:SECRET`), jwt_verify_options)
                .then( (decoded_token) => {
                    req.locals = req.locals || {};
                    req.locals.user_id = decoded_token.sub;
                    req.locals.empId = decoded_token.claims.empId || '';
                    req.locals.claims = decoded_token.claims;
                    return next();
                })
                .catch( (err) => {
                    return next(err);
                });

        };
    }

}

module.exports = Token;