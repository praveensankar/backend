const otpConfig = require('../configs/OtpServiceConfig.json')
const axios = require('axios');
const ApplicationError = require('@src/errors/ApplicationError');
class OneTimePassword {

    static generate(length = 6) {
        let digits = "0123456789"
        let otp = '';
        for (let i = 1; i <= length; i++) {
            let index = Math.floor(Math.random() * (digits.length));
            otp = `${otp}${digits[index]}`;
        }
        return otp;
    }

    static sendByMSG91(options = {}) {
        const template_no = options.template_no - 1;
        let url = `${otpConfig.MSG91.baseUrl}/otp?template_id=${otpConfig.MSG91.templates[template_no].templateId}&authkey=${otpConfig.MSG91.authKey}&mobile=91${options.mobile_number}&otp=${options.otp}`;
        var config = {
            url: url,
            method: 'POST',
            headers: {
                "Content-Type": 'application/json'
            }
        }
        if (options.is_extra_params) {
            config.data = options.extra_params;
        }
        
        return axios.request(config).then(response => {
            const msg91Response = response.data;
            console.log(msg91Response);
            if (msg91Response.type === "error")
                throw new ApplicationError({
                    name: "MSG91SendOTPError",
                    message: msg91Response.message
                });

            return {
                ...msg91Response,
                message: "OTP send Successfully"
            }
        });
    }
}

module.exports = OneTimePassword;