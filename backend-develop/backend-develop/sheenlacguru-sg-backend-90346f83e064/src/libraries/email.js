const nodemailer = require("nodemailer");
const nconf = require("nconf");

var transporter = nodemailer.createTransport({
    pool: true,
    host: nconf.get("MAIL:HOST"),
    port: nconf.get("MAIL:PORT"),
    secure: nconf.get("MAIL:SECURE"),
    auth: {
        user: nconf.get("MAIL:USER"),
        pass: nconf.get("MAIL:PASSWORD")
    }
}, { from: nconf.get("MAIL:ADDRESS")});

module.exports = transporter;