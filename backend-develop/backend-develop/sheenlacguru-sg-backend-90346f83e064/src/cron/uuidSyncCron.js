const cron = require('node-cron');
const { fetchUuid } = require('../app/coupons/fetch_uuid/fetch_uuid.task');

const getCurrentDate = () => {
    const date = new Date().toISOString().replace(/T.*/, '');
    return date;
}

const getSpecificDate = (day,month,year) => {
    const date = new Date(Date.UTC(year,month,day)).toISOString().replace(/T.*/, '');
    return date;
}

function daysInMonth(month, year) { // Use 1 for January, 2 for February, etc.
    return new Date(year, month, 0).getDate();
}

const syncCouponScheduler = async () => {
    return cron.schedule('*/2 * * * * *', async () => {
        const currentDate = getCurrentDate();
        console.log(currentDate);
        const uuids = fetchUuid({
            fromdate: currentDate,
            todate: currentDate
        });
    }, {
        scheduled: true,
        timezone: "Asia/Kolkata"
    });
}

const splitNumber = (num = 1, parts = 1) => {
    let n = Math.floor(num / parts);
    const arr = [];

    for (let i = 0; i < parts; i++) {
        arr.push(n)
    };
    if (arr.reduce((a, b) => a + b, 0) === num) {
        return arr;
    };
    for (let i = 0; i < parts; i++) {
        arr[i]++;
        if (arr.reduce((a, b) => a + b, 0) === num) {
            return arr;
        };
    };
};


module.exports = {
    getCurrentDate,
    getSpecificDate,
    daysInMonth,
    splitNumber,
    syncCouponScheduler
};