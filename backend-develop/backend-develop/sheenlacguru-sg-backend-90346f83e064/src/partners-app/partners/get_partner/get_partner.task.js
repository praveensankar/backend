const UserModel = require("../../../app/users/user.model");
const ApplicationError = require("@src/errors/ApplicationError");

class GetPartner {
    static async getPartner(partnerId) {
        try {
            const PartnersQuery = UserModel.findOne({
                _id: partnerId,
                status: "APPROVED"
            });
            const partner = await PartnersQuery;
            if (!partner) {
                throw new ApplicationError({
                    name: "GetPartnersError",
                    message: "No Partner found"
                })
            }

            return partner;
        } catch (error) {
            throw new ApplicationError({
                name: error.name,
                message: error.message
            })
        }
    }
}

module.exports = GetPartner;