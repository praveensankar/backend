const Ajv = require('ajv');
const _ = require("lodash");

const ApplicationError = require("@src/errors/ApplicationError");
const validations = require('@app/validations');
const ajv = new Ajv(validations.ajv_options);
const GetPartners = require('./get_partners/get_partners.task');
const GetPartner = require('./get_partner/get_partner.task');


class PartnersController {

    static getPartners(req, res, next) {
         if (!ajv.validate(GetPartners.validation(), req.query)) {
             let errors = ajv.errors;
             return next(
                 new ApplicationError({
                     status: 400,
                     name: "ValidationError",
                     message: "Bad request parameters.",
                     info: errors
                 })
             );
         }
        return GetPartners.getPartners(req.query)
            .then((partners) => {
                console.log(partners)
                return res.status(200).json(
                    new res.app.locals.Success({
                        data: partners
                    })
                );
            })
            .catch((err) => {
                return next(err);
            });
    }

    static getPartner(req, res, next) {
        return GetPartner.getPartner(req.params.id)
            .then((partner) => {
                console.log(partner)
                return res.status(200).json(
                    new res.app.locals.Success({
                        data: partner
                    })
                );
            })
            .catch((err) => {
                return next(err);
            });
    }
}

module.exports = PartnersController;