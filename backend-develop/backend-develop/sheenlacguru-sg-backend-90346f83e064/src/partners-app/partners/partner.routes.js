const express = require('express');
const router = express.Router();

const Token = require('@src/middleware/token');
const PartnersController = require('./partner.controller');

router.get('/', PartnersController.getPartners);
router.get('/getPartner/:id', PartnersController.getPartner);

module.exports = router;