const UserModel = require("../../../app/users/user.model");
const ApplicationError = require("@src/errors/ApplicationError");
const mongoose = require("mongoose");

class GetPartners {
    static async getPartners(options = {}) {
        console.log(options)
        const skip = options.page > 0 ? (options.page - 1) * options.size : 0;
        try {
            const PartnersQuery = UserModel.find({
                status: "APPROVED"
            });
            if (options.partnerId !== "") PartnersQuery.where('partnerId').equals(mongoose.Types.ObjectId(options.partnerId));
            if (options.serviceId !== "") PartnersQuery.where('serviceId').equals(mongoose.Types.ObjectId(options.serviceId));
            if (options.pincode !== "") PartnersQuery.where('pincode').equals(options.pincode);
            if (options.size) PartnersQuery.skip(skip).limit(options.size);
            const partners = await PartnersQuery;

             const PartnersCountQuery = UserModel.find({
                 status: "APPROVED"
             });
             if (options.partnerId !== "") PartnersCountQuery.where('partnerId').equals(mongoose.Types.ObjectId(options.partnerId));
             if (options.serviceId !== "") PartnersCountQuery.where('serviceId').equals(mongoose.Types.ObjectId(options.serviceId));
             if (options.pincode !== "") PartnersCountQuery.where('pincode').equals(options.pincode);
            //  if (options.size) PartnersCountQuery.skip(skip).limit(options.size);
             const count = await PartnersCountQuery.countDocuments();
            // if (partners.length === 0) {
            //     throw new ApplicationError({
            //         name: "GetPartnersError",
            //         message: "No Partners found"
            //     })
            // }

            return {
                items: partners,
                totalCount: count
            };
        } catch (error) {
            throw new ApplicationError({
                name: error.name,
                message: error.message
            })
        }
    }
    static validation() {
        return {
            "$id": "http://guru.sheenlac.com/schemas/admins/get_partners.json",
            "$async": true,
            "type": "object",
            "properties": {
                "page": {
                    "type": "integer",
                    "default": 1
                },
                "size": {
                    "type": "integer",
                    "default": 10
                },
                "status": {
                    "type": "string"
                },
                "searchText": {
                    "type": "string",
                    "default": ""
                },
                "sort": {
                    "type": "integer",
                    "default": -1
                },
                "pincode": {
                    "type": "integer",
                    "default" : ""
                },
                "partnerId": {
                    "type": "string",
                    "default" : ""
                },
                "serviceId": {
                    "type": "string",
                    "default" : ""
                }
            },
            "required": [],
            "additionalProperties": false
        };
    }
}

module.exports = GetPartners;