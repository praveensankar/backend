var express = require('express');
var router = express.Router();

const partnerCategoriesRoutes = require('@partnersapp/partner_categories/partners_categories.routes');
const adminPartnerCategoriesRoutes = require('@partnersapp/partner_categories/partners_categories_admin.routes');
const adminServiceCategoriesRoutes = require('@partnersapp/service_categories/service_categories_admin.routes');
const serviceCategoriesRoutes = require('@partnersapp/service_categories/service_categories.routes');
const customerRoutes = require('@partnersapp/customers/customer.routes');
const partnersRoutes = require('@partnersapp/partners/partner.routes');
const serviceRequestRoutes = require('@partnersapp/service_requests/service_requests.routes');

router.use('/admin/partnercategories', adminPartnerCategoriesRoutes);
router.use('/admin/servicecategories', adminServiceCategoriesRoutes);

router.use('/partnercategories', partnerCategoriesRoutes);
router.use('/servicecategories', serviceCategoriesRoutes);
router.use('/customer', customerRoutes);
router.use('/', partnersRoutes);
router.use('/serviceRequest', serviceRequestRoutes);

module.exports = router;