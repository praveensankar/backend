const CustomerModel = require("../customer.model");


class GetCustomerTask {

    static async getCustomerDetail(customerId) {
        return await CustomerModel.findOne({
            _id: customerId
        });
    }
}

module.exports = GetCustomerTask;