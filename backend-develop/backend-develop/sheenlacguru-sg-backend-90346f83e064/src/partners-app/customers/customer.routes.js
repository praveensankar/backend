const express = require('express');
const router = express.Router();

const CustomerController = require('./customer.controller');
const Token = require('@src/middleware/token');

router.post('/registerOrLoginCustomer', CustomerController.registerOrLoginCustomer, Token.generate({
    for: 'Customer'
}));
router.post('/sudoRegisterOrLoginCustomer', CustomerController.sudoRegisterOrLoginCustomer, Token.generate({
    for: 'Customer'
}));

router.get('/detail', Token.verify({
    for: "Customer"
}), CustomerController.getCustomerDetail);

router.get('/identity/:mobile_number', CustomerController.checkIdentity);
router.get('/verify_missedcall', CustomerController.verify_missedcall);
module.exports = router;