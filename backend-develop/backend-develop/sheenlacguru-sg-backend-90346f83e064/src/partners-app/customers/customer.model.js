const mongoose = require('mongoose');
// const nconf = require('nconf');

const Schema = mongoose.Schema;

// const SERVER_HOST = nconf.get('APPLICATION:S3');

const CustomersSchema = new Schema({
    customerId: {
        type: String,
        trim: true
    },
    firstname: {
        type: String,
        required: [true, 'First Name is required.'],
        trim: true
    },
    lastname: {
        type: String,
        required: [true, 'Last Name is required.'],
        trim: true
    },
    mobile_number: {
        type: String,
        trim: true,
        unique: true,
        sparse: true,
        required: [true, 'Please enter the mobile number.']
    },
    email: {
        type: String,
        trim: true,
        lowercase: true
    },
    address: {
        type: String
    },
    pincode: {
        type: String,
        trim: true
    },
    status: {
        type: String,
        enum: ['NEW'],
        default: "NEW",
        required: [true, 'Status is required.']
    },
    isMobileNumberVerified: {
        type: Boolean,
        default: false
    },
    isAuthorisedToContact: {
        type: Boolean,
        default: false

    },
    mobileNumberVerifiedDate: {
        type: Date
    }
}, {
    timestamps: true
});

CustomersSchema.virtual('fullname').get(function () {
    return this.firstname + " " + this.lastname;
})

CustomersSchema.set('toJSON', {
    virtuals: true,
    getters: true
});

module.exports = mongoose.model('Customers', CustomersSchema);