const CustomerModel = require("../customer.model");
const ApplicationError = require("@src/errors/ApplicationError");

class SudoRegisterOrLoginCustomerTask {

    static async sudoRegisterOrLoginCustomer(request) {
        console.log(request)
        try {
            const customerExist = await CustomerModel.findOne({
                mobile_number: request.mobile_number
            });
            if (customerExist) {
                console.log(customerExist);
                customerExist.login = true;
                return Promise.resolve(customerExist);
            }
            return new CustomerModel(request).save()
                .then((customer) => {
                    if (!customer)
                        throw new ApplicationError({
                            name: 'CreateCustomerError',
                            message: 'Customer not created.'
                        });
                    customer.login = false;
                    return customer;
                });
        } catch (error) {
            throw new ApplicationError({
                name: error.name,
                message: error.message
            });
        }
    }

    static validation() {
        return {
            "$id": "http://guru.sheenlac.com/schemas/sudo_register_customer.json",
            "type": "object",
            "properties": {
                "firstname": {
                    "type": "string"
                },
                "lastname": {
                    "type": "string"
                },
                "mobile_number": {
                    "type": "string"
                },
                "pincode": {
                    "type": "integer"
                },
                "isAuthorisedToContact": {
                    "type": "boolean",
                    "default": true
                }
            },
            "required": ["firstname", "lastname", "mobile_number", "pincode", "isAuthorisedToContact"],
            "additionalProperties": false
        };
    }
}

module.exports = SudoRegisterOrLoginCustomerTask;