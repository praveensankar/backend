const CustomerModel = require("../customer.model");


class CheckIdentityTask {

    static async checkIdentity(mobile_number) {
        const customer = await CustomerModel.findOne({
            mobile_number: mobile_number
        });
        if (!customer) {
            return false
        }
        return customer;
    }
}

module.exports = CheckIdentityTask;