const Ajv = require('ajv');
const _ = require("lodash");

const ApplicationError = require("@src/errors/ApplicationError");
const validations = require('@app/validations');
const ajv = new Ajv(validations.ajv_options);
const RegisterCustomerTask = require("./registerOrLoginCustomer/registerOrLoginCustomer.task");
const GetCustomerTask = require('./get_customer/get_customer.task');
const CheckIdentityTask = require('./check_identity/check_identity.task');
const SudoRegisterOrLoginCustomerTask = require('./sudoRegisterOrLogin/sudoRegisterOrLoginCustomer.task');
const VerifyCustomerMissedCall = require('./verify_missedcall/verify_missedcall.task');

class CustomerController {

    static registerOrLoginCustomer(req, res, next) {
        if (!ajv.validate(RegisterCustomerTask.validation(), req.body)) {
            let errors = ajv.errors;
            return next(
                new ApplicationError({
                    status: 400,
                    name: "ValidationError",
                    message: "Bad request parameters.",
                    info: errors
                })
            );
        }

        return RegisterCustomerTask.registerOrLoginCustomer(req.body)
            .then((user) => {
                req.locals = req.locals || {};
                req.locals.claims = {
                    id: user._id,
                    isSudoUser: false,
                    isMobileNumberVerified: user.isMobileNumberVerified
                }
                req.locals.status = user.status;
                req.locals.isMobileNumberVerified = user.isMobileNumberVerified;
                return next();
            })
            .catch((err) => {
                return next(err);
            });
    }

    static sudoRegisterOrLoginCustomer(req, res, next) {
        if (!ajv.validate(SudoRegisterOrLoginCustomerTask.validation(), req.body)) {
            let errors = ajv.errors;
            return next(
                new ApplicationError({
                    status: 400,
                    name: "ValidationError",
                    message: "Bad request parameters.",
                    info: errors
                })
            );
        }

        return SudoRegisterOrLoginCustomerTask.sudoRegisterOrLoginCustomer(req.body)
            .then((user) => {
                req.locals = req.locals || {};
                req.locals.claims = {
                    id: user._id,
                    isSudoUser: true,
                    isMobileNumberVerified: user.isMobileNumberVerified
                }
                req.locals.status = user.status;
                req.locals.isMobileNumberVerified = user.isMobileNumberVerified;
                return next();
            })
            .catch((err) => {
                return next(err);
            });
    }

    static getCustomerDetail(req, res, next) {
        return GetCustomerTask.getCustomerDetail(req.locals.user_id)
            .then((user) => {
                console.log(user)
                return res.status(200).json(
                    new res.app.locals.Success({
                        data: user
                    })
                );
            })
            .catch((err) => {
                return next(err);
            });
    }

    static checkIdentity(req, res, next) {
        return CheckIdentityTask.checkIdentity(req.params.mobile_number)
            .then((user) => {
                console.log(user)
                return res.status(200).json(
                    new res.app.locals.Success({
                        data: user
                    })
                );
            })
            .catch((err) => {
                return next(err);
            });
    }
    static verify_missedcall(req, res, next) {
        if (!ajv.validate(VerifyCustomerMissedCall.validation(), req.query)) {
            let errors = ajv.errors;
            return next(
                new ApplicationError({
                    status: "400",
                    name: "ValidationError",
                    message: "Bad request parameters.",
                    info: errors
                })
            );
        }
        return VerifyCustomerMissedCall.exec(req.query)
            .then((otp) => {
                return res.status(200).json(
                    new res.app.locals.Success({
                        data: otp
                    })
                );
            })
            .catch((err) => {
                return next(err);
            });
    }
}

module.exports = CustomerController;