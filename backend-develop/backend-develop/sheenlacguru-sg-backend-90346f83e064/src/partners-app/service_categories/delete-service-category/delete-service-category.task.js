const ServicesCategoriesModel = require("../service_categories.model");
const ApplicationError = require("@src/errors/ApplicationError");

class DeleteServiceCategoryTask {

    /**
     * Update Service Category
     * @param {string} serviceCategoryId service category id
     * @param {object} document document object
     * @param {session instance} session session instance
     * @return return service category
     */
    static exec(serviceCategoryId, session) {
        if (session) {
            return ServicesCategoriesModel.deleteOne()
                .where('_id').equals(serviceCategoryId).session(session).then((serviceCategory) => {
                    if (!serviceCategory)
                        throw new ApplicationError({
                            name: 'UpdateServiceCategoryError',
                            message: 'Service Category not updated.'
                        });
                    return serviceCategory;
                });
        } else {
            return ServicesCategoriesModel.deleteOne()
                .where('_id').equals(serviceCategoryId)
                .then((serviceCategory) => {
                    if (!serviceCategory)
                        throw new ApplicationError({
                            name: 'UpdateServiceCategoryError',
                            message: 'Service Category not updated.'
                        });
                    return serviceCategory;
                });
        }
    }
}

module.exports = DeleteServiceCategoryTask;