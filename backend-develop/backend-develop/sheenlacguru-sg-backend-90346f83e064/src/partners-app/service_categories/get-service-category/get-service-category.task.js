const ServicesCategoriesModel = require("../service_categories.model");
const ApplicationError = require("@src/errors/ApplicationError");
const _ = require("lodash");

class GetServiceCategoryTask {

    /**
     * Get Service Category
     * @param {string} serviceCategoryId service category id
     * @param {session instance} session session instance 
     * @return return service category
     */
    static exec(serviceCategoryId, session) {
        console.log(serviceCategoryId)
        if (session) {
            return ServicesCategoriesModel.findById(serviceCategoryId).session(session).then((serviceCategory) => {
                if (!serviceCategory)
                    throw new ApplicationError({
                        name: 'GetServiceCategoryError',
                        message: 'Service Category not found.'
                    });
                return serviceCategory;
            });
        } else {
            return ServicesCategoriesModel.findById(serviceCategoryId)
                .then((serviceCategory) => {
                    if (!serviceCategory)
                        throw new ApplicationError({
                            name: 'GetServiceCategoryError',
                            message: 'Service Category not found.'
                        });
                    return serviceCategory;
                });
        }
    }
}

module.exports = GetServiceCategoryTask;