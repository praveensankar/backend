const mongoose = require('mongoose');
const nconf = require('nconf');

const Schema = mongoose.Schema;

const SERVER_HOST = nconf.get('APPLICATION:S3');

const PartnerCategoriesSchema = new Schema({
    title: {
        type: String,
        select: true,
        trim: true
    },
    description: {
        type: String,
        trim: true
    },
    image: {
        type: String,
        trim: true
    },
    displayOrder: {
        type: Number
    },
    status: {
        type: String,
        enum: ['ACTIVE', 'INACTIVE'],
        default: "INACTIVE"
    }
});

const ServiceCategories = new Schema({
    title: {
        type: String,
        required: [true, 'Service Category title is required.'],
        select: true,
        trim: true
    },
    description: {
        type: String,
        required: [true, 'Service Category description is required.'],
        trim: true
    },
    image: {
        type: String,
        get: (value) => `${SERVER_HOST}${value}`,
        // required: [true, 'Service Category image is required.'],
        trim: true
    },
    partnerCategories: {
        type: [PartnerCategoriesSchema]
    },
    displayOrder: {
        type: Number,
        required: [true, 'Service Category display order is required.']
    },
    status: {
        type: String,
        enum: ['ACTIVE', 'INACTIVE'],
        default: "INACTIVE",
        required: [true, 'Service Category status is required.']
    }
}, {
    timestamps: true
});

ServiceCategories.set('toJSON', {
    virtuals: true,
    getters: true
});

module.exports = mongoose.model('ServiceCategories', ServiceCategories);