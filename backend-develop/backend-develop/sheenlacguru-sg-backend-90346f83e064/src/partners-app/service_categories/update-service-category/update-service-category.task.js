const ServiceCategoriesModel = require("../service_categories.model");
const ApplicationError = require("@src/errors/ApplicationError");
const PartnersCategoriesModel = require("../../partner_categories/partners_categories.model");
const _ = require('lodash');
class UpdateServiceCategoryTask {

    /**
     * Update Service Category
     * @param {string} serviceCategoryId service category id
     * @param {object} document document object
     * @param {session instance} session session instance
     * @return return service category
     */
    static async exec(serviceCategoryId, document, session) {

        try {
            // Get Partner Categories for id's
            const getPartners = await PartnersCategoriesModel.find({
                _id: {
                    $in: document.partnerCategories
                }
            });
            if (!getPartners) {
                throw new ApplicationError({
                    name: 'UpdateServiceCategoryError',
                    message: 'Service Category not updated.'
                });
            }
            document = _.omit(document, ['partnerCategories']);
            document = _.defaultsDeep(_.cloneDeep(document), {
                partnerCategories: getPartners
            });

            if (session) {
                return ServiceCategoriesModel.where('_id').equals(serviceCategoryId).updateOne({}, {
                        omitUndefined: true
                    }).set(document).session(session)
                    .then((serviceCategory) => {
                        if (serviceCategory.n < 1 || serviceCategory.nModified < 1)
                            throw new ApplicationError({
                                name: 'UpdateServiceCategoryError',
                                message: 'Service Category not updated.'
                            });
                        return serviceCategory;
                    });
            } else {
                return ServiceCategoriesModel.where('_id').equals(serviceCategoryId).updateOne({}, {
                        omitUndefined: true
                    }).set(document)
                    .then((serviceCategory) => {
                        if (serviceCategory.n < 1 || serviceCategory.nModified < 1)
                            throw new ApplicationError({
                                name: 'UpdateServiceCategoryError',
                                message: 'Service Category not updated.'
                            });
                        return serviceCategory;
                    });
            }
        } catch (error) {
            throw new ApplicationError({
                name: error.name,
                message: error.message
            });
        }
    }

    static validation() {
        return {
            "$id": "http://guru.sheenlac.com/schemas/update_service_category.json",
            "type": "object",
            "properties": {
                "title": {
                    "type": "string",
                    "minLength": 2
                },
                "description": {
                    "type": "string",
                    "minLength": 2
                },
                "image": {
                    "type": "string"
                },
                "displayOrder": {
                    "type": "integer"
                },
                "partnerCategories": {
                    "type": "array",
                    "items": {
                        "type": "string"
                    }
                },
                "status": {
                    "type": "string",
                    "enum": ["ACTIVE", "INACTIVE"]
                }
            },
            "additionalProperties": false
        };
    }
}

module.exports = UpdateServiceCategoryTask;