const express = require('express');
const router = express.Router();

const ServiceCategoriesController = require('./service_categories.controller');

router.get('/', ServiceCategoriesController.getServices);

module.exports = router;