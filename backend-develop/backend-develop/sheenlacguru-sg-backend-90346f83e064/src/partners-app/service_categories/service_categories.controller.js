const Ajv = require('ajv');
const _ = require("lodash");

const ApplicationError = require("@src/errors/ApplicationError");
const validations = require('@app/validations');
const AddServiceCategoryTask = require('./add-service-category/add-service-category.task');
const UpdateServiceCategoryTask = require('./update-service-category/update-service-category.task');
const GetServiceCategoryTask = require('./get-service-category/get-service-category.task');
const ListServiceCategoryTask = require('./list-service-category/list-service-category.task');
const DeleteServiceCategoryTask = require('./delete-service-category/delete-service-category.task');
const ajv = new Ajv(validations.ajv_options);

class ServiceCategoriesController {

    /**
     * Create Service Category
     * @param {object} req request object
     * @param {object} res response object
     * @param {function} next next function
     * @return return
     */
    static create(req, res, next) {
        if (req.file) req.body["image"] = req.file.key;
        if (!ajv.validate(AddServiceCategoryTask.validation(), req.body)) {
            let errors = ajv.errors;
            return next(
                new ApplicationError({
                    status: 400,
                    name: "ValidationError",
                    message: "Bad request parameters.",
                    info: errors
                })
            );
        }

        return AddServiceCategoryTask.exec(req.body)
            .then((response) => {
                return res.status(200).json(
                    new res.app.locals.Success({
                        data: response
                    })
                );
            })
            .catch((err) => {
                return next(err);
            });
    }

    /**
     * Get Service Category
     * @param {object} req request object
     * @param {object} res response object
     * @param {function} next next function
     * @return return
     */
    static show(req, res, next) {
        return GetServiceCategoryTask.exec(req.params.id)
            .then((response) => {
                return res.status(200).json(
                    new res.app.locals.Success({
                        data: response
                    })
                );
            })
            .catch((err) => {
                return next(err);
            });
    }

    /**
     * Get Service Category with query parameters
     * @param {object} req request object
     * @param {object} res response object
     * @param {function} next next function
     * @return return
     */
    static getServices(req, res, next) {
        if (!ajv.validate(ListServiceCategoryTask.validation(), req.query)) {
            let errors = ajv.errors;
            return next(
                new ApplicationError({
                    status: 400,
                    name: "ValidationError",
                    message: "Bad request parameters.",
                    info: errors
                })
            );
        }
        return ListServiceCategoryTask.getServiceCategoriesByParams(req.query)
            .then((response) => {
                return res.status(200).json(
                    new res.app.locals.Success({
                        data: response
                    })
                );
            })
            .catch((err) => {
                return next(err);
            });
    }

    /**
     * List Service Categories
     * @param {object} req request object
     * @param {object} res response object
     * @param {function} next next function 
     * @return return partner categories
     */
    static list(req, res, next) {
        if (!ajv.validate(ListServiceCategoryTask.validation(), req.query)) {
            let errors = ajv.errors;
            return next(
                new ApplicationError({
                    status: 400,
                    name: "ValidationError",
                    message: "Bad request parameters.",
                    info: errors
                })
            );
        }

        let query = _.pick(req.query, ['status']);
        return ListServiceCategoryTask.list(query, req.query)
            .then((resp) => {
                return res.status(200).json(
                    new res.app.locals.Success({
                        data: resp
                    })
                );
            })
            .catch((err) => {
                return next(err);
            });
    };

    /**
     * Update Service Category
     * @param {object} req request object
     * @param {object} res response object
     * @param {function} next next function
     * @return return
     */
    static update(req, res, next) {
        console.log(req.body);
        if (req.file) req.body["image"] = req.file.key;
        if (!ajv.validate(UpdateServiceCategoryTask.validation(), req.body)) {
            let errors = ajv.errors;
            return next(
                new ApplicationError({
                    status: 400,
                    name: "ValidationError",
                    message: "Bad request parameters.",
                    info: errors
                })
            );
        }

        return UpdateServiceCategoryTask.exec(req.params.id, req.body)
            .then((response) => {
                return res.status(200).json(
                    new res.app.locals.Success({
                        data: response
                    })
                );
            })
            .catch((err) => {
                return next(err);
            });
    };

    /**
     * Delete Service Category
     * @param {object} req request object
     * @param {object} res response object
     * @param {function} next next function
     * @return return
     */
    static delete(req, res, next) {
        return DeleteServiceCategoryTask.exec(req.params.id)
            .then((response) => {
                return res.status(200).json(
                    new res.app.locals.Success({
                        data: response
                    })
                );
            })
            .catch((err) => {
                return next(err);
            });
    }
}

module.exports = ServiceCategoriesController;