const express = require('express');
const router = express.Router();
const nconf = require('nconf');

const Token = require('@src/middleware/token');
const Authorization = require('@src/middleware/authorization');
const ServiceCategoriesController = require('./service_categories.controller');
const Uploader = require('@src/middleware/uploader.middleware');

router.use(Token.verify({
    for: 'admin'
}));

router.post('/', Authorization.authorize(["partnerManagement"]), Uploader({
    dest: nconf.get("STORAGE:PARTNERCATEGORIES")
}).single('image'), ServiceCategoriesController.create);
router.get('/', Authorization.authorize(["partnerManagement"]), ServiceCategoriesController.list);
router.get('/:id', Authorization.authorize(["partnerManagement"]), ServiceCategoriesController.show);
router.patch('/:id', Authorization.authorize(["partnerManagement"]), Uploader({
    dest: nconf.get("STORAGE:PARTNERCATEGORIES")
}).single('image'), ServiceCategoriesController.update);
router.delete('/:id', Authorization.authorize(["partnerManagement"]), ServiceCategoriesController.delete);

module.exports = router;