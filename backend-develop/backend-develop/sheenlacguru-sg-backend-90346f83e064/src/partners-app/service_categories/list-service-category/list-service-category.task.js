const _ = require('lodash');
const ServiceCategoriesModel = require('../service_categories.model');
const ApplicationError = require("@src/errors/ApplicationError");

class ListServiceCategoryTask {

    static async list(query = {}, options = {}) {
        const skip = options.page > 0 ? (options.page - 1) * options.size : 0;
        let serviceCategories = [];
        let serviceCategoriesCount = 0;
        // search when search key exist in options
        if (options.searchText !== "") {
            /* Start of Search */

            /* ------- Search with exact match using text based search */
            const serviceCategoriesWithExactMatch = await ServiceCategoriesModel.find({
                $text: {
                    $search: options.searchText
                }
            });
            /* -------- Search with start (title,status,mobile_number,email,empId) */
            const rgxForStartMatch = (pattern) => new RegExp(`^${pattern}`); // pattern for matching start of searchText
            const searchTextForStartMatch = rgxForStartMatch(options.searchText);
            const serviceCategoriesWithStartMatch = await ServiceCategoriesModel.find({
                $or: [{
                        title: {
                            $regex: searchTextForStartMatch,
                            $options: 'i'
                        }
                    },
                    {
                        status: {
                            $regex: searchTextForStartMatch,
                            $options: 'i'
                        }
                    }
                ]
            });

            /* -------- Fuzzy search with (title,status,mobile_number,email) */
            const rgxForFuzzyMatch = (pattern) => new RegExp(`${pattern}`);
            const searchTextForFuzzyMatch = rgxForFuzzyMatch(options.searchText);
            const serviceCategoriesWithFuzzyMatch = await ServiceCategoriesModel.find({
                $or: [{
                        title: {
                            $regex: searchTextForFuzzyMatch,
                            $options: 'i'
                        }
                    },
                    {
                        status: {
                            $regex: searchTextForFuzzyMatch,
                            $options: 'i'
                        }
                    }
                ]
            });

            const serviceList = _.concat(serviceCategoriesWithExactMatch, serviceCategoriesWithStartMatch, serviceCategoriesWithFuzzyMatch);

            /* generate list of distinct serviceCategories if serviceCategories got repeated */
            serviceCategories = _.concat([], _.uniqBy(serviceList, '_id'));
            serviceCategoriesCount = serviceCategories.length;
            serviceCategories = serviceCategories.slice(skip).slice(0, options.size)

            /* End of Search */
        } else {
            /* fetch serviceCategories if no search key is present on options */
            serviceCategories = await ServiceCategoriesModel.find().skip(skip).limit(options.size).exec();
            serviceCategoriesCount = await ServiceCategoriesModel.countDocuments().exec();
        }

        return Promise.all([
            serviceCategories,
            serviceCategoriesCount
        ]).then(([serviceCategories, count]) => {
            return {
                items: serviceCategories,
                totalCount: count
            };
        });
    }

    static async getServiceCategoriesByParams(query, session) {
        console.log(query.partnerCategoryId)
        const ServicesCategoriesQuery = ServiceCategoriesModel.find();
        if (query.partnerCategoryId) ServicesCategoriesQuery.where("partnerCategories._id").equals(query.partnerCategoryId);
        const count = await ServiceCategoriesModel.countDocuments().exec();
        if (session) {
            return ServicesCategoriesQuery.session(session).then((serviceCategory) => {
                if (!serviceCategory)
                    throw new ApplicationError({
                        name: 'GetServiceCategoryError',
                        message: 'Service Category not found.'
                    });
                return serviceCategory;
            });
        } else {
            return ServicesCategoriesQuery
                .then((serviceCategory) => {
                    console.log(serviceCategory)
                    if (!serviceCategory || _.isEmpty(serviceCategory))
                        throw new ApplicationError({
                            name: 'GetServiceCategoryError',
                            message: 'Service Category not found.'
                        });
                    return {
                        items: serviceCategory,
                        totalCount: count
                    };
                });
        }
    }

    static validation() {
        return {
            "$id": "http://guru.sheenlac.com/schemas/amdins/list_services_category.json",
            "$async": true,
            "type": "object",
            "properties": {
                "page": {
                    "type": "integer",
                    "default": 1
                },
                "size": {
                    "type": "integer",
                    "default": 10
                },
                "status": {
                    "type": "string"
                },
                "searchText": {
                    "type": "string",
                    "default" : ""
                },
                "partnerCategoryId": {
                    "type": "string",
                }
            },
            "required": [],
            "additionalProperties": false
        };
    }

}

module.exports = ListServiceCategoryTask;