const ServicesCategoriesModel = require("../service_categories.model");
const ApplicationError = require("@src/errors/ApplicationError");
const PartnersCategoriesModel = require("../../partner_categories/partners_categories.model");
const _ = require("lodash");

class AddServiceCategoryTask {

    /**
     * Create Service Category
     * @param {object} document document object
     * @param {session instance} session session instance
     * @return return service category
     */
    static async exec(document, session) {

        try {

            const getPartners = await PartnersCategoriesModel.find({ _id: { $in: document.partnerCategories } });
            if (!getPartners) {
                throw new ApplicationError({
                    name: 'CreateServiceCategoryError',
                    message: 'Service Category not created.'
                });
            }
            document = _.omit(document, ['partnerCategories']);
            document = _.defaultsDeep(_.cloneDeep(document), {
                partnerCategories : getPartners
            });

            // // Generate services array for partnerCategoryId's
            // const services = AddServiceCategoryTask.createServiceObject(document);
            if (session) {
                return ServicesCategoriesModel.create([document], session).then((serviceCategory) => {
                    if (!serviceCategory)
                        throw new ApplicationError({
                            name: 'CreateServiceCategoryError',
                            message: 'Service Category not created.'
                        });
                    return serviceCategory;
                });
            } else {
                return ServicesCategoriesModel.insertMany(document)
                    .then((serviceCategory) => {
                        if (!serviceCategory)
                            throw new ApplicationError({
                                name: 'CreateServiceCategoryError',
                                message: 'Service Category not created.'
                            });
                        return serviceCategory;
                    });
            }
        } catch (error) {
            throw new ApplicationError({
                name: error.name,
                message: error.message
            });
        }
    }

    /**
     * Generate services array by partner category id's
     * @param {object} document document object
     * @returns returns services
     */
    // static createServiceObject(document) {
    //     let services = [];
    //     document.partnerCategories.map(categoryId => {
    //         let service = {
    //             title: document.title,
    //             description: document.description,
    //             image: document.image,
    //             partnerCategoryId: categoryId,
    //             displayOrder: document.displayOrder,
    //             status: document.status
    //         }
    //         services.push(service);
    //     })

    //     return services;
    // }

    static validation() {
        return {
            "$id": "http://guru.sheenlac.com/schemas/create_service_category.json",
            "type": "object",
            "properties": {
                "title": {
                    "type": "string",
                    "minLength": 2
                },
                "description": {
                    "type": "string",
                    "minLength": 2
                },
                "image": {
                    "type": "string"
                },
                "partnerCategories": {
                    "type": "array",
                    "items": {
                        "type": "string"
                    }
                },
                "displayOrder": {
                    "type": "integer"
                },
                "status": {
                    "type": "string",
                    "enum": ["ACTIVE", "INACTIVE"]
                }
            },
            "required": ["title", "description", "partnerCategories", "displayOrder", "status"],
            "additionalProperties": false
        };
    }
}

module.exports = AddServiceCategoryTask;