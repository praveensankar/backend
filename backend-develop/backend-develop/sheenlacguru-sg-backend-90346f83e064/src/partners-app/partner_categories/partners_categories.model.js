const mongoose = require('mongoose');
const nconf = require('nconf');

const Schema = mongoose.Schema;

const SERVER_HOST = nconf.get('APPLICATION:S3');

const PartnerCategoriesSchema = new Schema({
    title: {
        type: String,
        required: [true, 'Partner Category title is required.'],
        select: true,
        trim: true
    },
    description: {
        type: String,
        required: [true, 'Partner Category description is required.'],
        trim: true
    },
    image: {
        type: String,
        get: (value) => `${SERVER_HOST}${value}`,
        required: [true, 'Partner Category image is required.'],
        trim: true
    },
    displayOrder: {
        type: Number,
        required: [true, 'Partner Category display order is required.']
    },
    status: {
        type: String,
        enum: ['ACTIVE', 'INACTIVE'],
        default: "INACTIVE",
        required: [true, 'Partner Category status is required.']
    }
}, {
    timestamps: true
});

PartnerCategoriesSchema.set('toJSON', {
    virtuals: true,
    getters: true
});

module.exports = mongoose.model('PartnerCategories', PartnerCategoriesSchema);
