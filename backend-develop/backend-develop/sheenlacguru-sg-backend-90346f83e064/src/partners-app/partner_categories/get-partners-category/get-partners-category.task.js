const PartnersCategoriesModel = require("../partners_categories.model");
const ApplicationError = require("@src/errors/ApplicationError");

class GetPartnerCategoryTask {

    /**
     * Get Partner Category
     * @param {string} partnerCategoryId partner category id
     * @param {session instance} session session instance 
     * @return return partner category
     */
    static exec(partnerCategoryId, session) {
        console.log(partnerCategoryId)
        if (session) {
            return PartnersCategoriesModel.findById(partnerCategoryId).session(session).then((partnerCategory) => {
                if (!partnerCategory)
                    throw new ApplicationError({
                        name: 'GetPartnerCategoryError',
                        message: 'Partner Category not found.'
                    });
                return partnerCategory;
            });
        } else {
            return PartnersCategoriesModel.findById(partnerCategoryId)
                .then((partnerCategory) => {
                    if (!partnerCategory)
                        throw new ApplicationError({
                            name: 'GetPartnerCategoryError',
                            message: 'Partner Category not created.'
                        });
                    return partnerCategory;
                });
        }
    }
}

module.exports = GetPartnerCategoryTask;