const Ajv = require('ajv');
const _ = require("lodash");

const ApplicationError = require("@src/errors/ApplicationError");
const validations = require('@app/validations');
const AddPartnerCategoryTask = require('./add-partners-category/add-partners-category.task');
const UpdatePartnerCategoryTask = require('./update-partners-category/update-partners-category.task');
const GetPartnerCategoryTask = require('./get-partners-category/get-partners-category.task');
const ListPartnerCategoryTask = require('./list-partners-category/list-partners-category.task');
const DeletePartnerCategoryTask = require('./delete-partners-category/delete-partners-category.task');
const ajv = new Ajv(validations.ajv_options);

class PartnerCategoriesController {

    /**
     * Create Partner Category
     * @param {object} req request object
     * @param {object} res response object
     * @param {function} next next function
     * @return return
     */
    static create(req, res, next) {
        if (req.file) req.body["image"] = req.file.key;
        if (!ajv.validate(AddPartnerCategoryTask.validation(), req.body)) {
            let errors = ajv.errors;
            return next(
                new ApplicationError({
                    status: 400,
                    name: "ValidationError",
                    message: "Bad request parameters.",
                    info: errors
                })
            );
        }

        return AddPartnerCategoryTask.exec(req.body)
            .then((response) => {
                return res.status(200).json(
                    new res.app.locals.Success({
                        data: response
                    })
                );
            })
            .catch((err) => {
                return next(err);
            });
    }

    /**
     * Get Partner Category
     * @param {object} req request object
     * @param {object} res response object
     * @param {function} next next function
     * @return return
     */
    static show(req, res, next) {
        return GetPartnerCategoryTask.exec(req.params.id)
            .then((response) => {
                return res.status(200).json(
                    new res.app.locals.Success({
                        data: response
                    })
                );
            })
            .catch((err) => {
                return next(err);
            });
    }

    /**
     * List Partner Categories
     * @param {object} req request object
     * @param {object} res response object
     * @param {function} next next function 
     * @return return partner categories
     */
    static list(req, res, next) {
        if (!ajv.validate(ListPartnerCategoryTask.validation(), req.query)) {
            let errors = ajv.errors;
            return next(
                new ApplicationError({
                    status: 400,
                    name: "ValidationError",
                    message: "Bad request parameters.",
                    info: errors
                })
            );
        }

        let query = _.pick(req.query, ['status']);
        return ListPartnerCategoryTask.list(query, req.query)
            .then((resp) => {
                return res.status(200).json(
                    new res.app.locals.Success({
                        data: resp
                    })
                );
            })
            .catch((err) => {
                return next(err);
            });
    };

    /**
     * Update Partner Category
     * @param {object} req request object
     * @param {object} res response object
     * @param {function} next next function
     * @return return
     */
    static update(req, res, next) {
        console.log(req.body);
        if (req.file) req.body["image"] = req.file.key;
        if (!ajv.validate(UpdatePartnerCategoryTask.validation(), req.body)) {
            let errors = ajv.errors;
            return next(
                new ApplicationError({
                    status: 400,
                    name: "ValidationError",
                    message: "Bad request parameters.",
                    info: errors
                })
            );
        }

        return UpdatePartnerCategoryTask.exec(req.params.id,req.body)
            .then((response) => {
                return res.status(200).json(
                    new res.app.locals.Success({
                        data: response
                    })
                );
            })
            .catch((err) => {
                return next(err);
            });
    };

    /**
     * Delete Partner Category
     * @param {object} req request object
     * @param {object} res response object
     * @param {function} next next function
     * @return return
     */
    static delete(req, res, next) {
        return DeletePartnerCategoryTask.exec(req.params.id)
            .then((response) => {
                return res.status(200).json(
                    new res.app.locals.Success({
                        data: response
                    })
                );
            })
            .catch((err) => {
                return next(err);
            });
    }
}

module.exports = PartnerCategoriesController;