const PartnersCategoriesModel = require("../partners_categories.model");
const ApplicationError = require("@src/errors/ApplicationError");

class UpdatePartnerCategoryTask {

    /**
     * Update Partner Category
     * @param {string} partnerCategoryId partner category id
     * @param {object} document document object
     * @param {session instance} session session instance
     * @return return partner category
     */
    static exec(partnerCategoryId,document, session) {
        if (session) {
            return PartnersCategoriesModel.updateOne({}, {
                    omitUndefined: true
                })
                .where('_id').equals(partnerCategoryId)
                .set(document).session(session).then((partnerCategory) => {
                if (partnerCategory.n < 1 || partnerCategory.nModified < 1)
                    throw new ApplicationError({
                        name: 'UpdatePartnerCategoryError',
                        message: 'Partner Category not updated.'
                    });
                return partnerCategory;
            });
        } else {
            return PartnersCategoriesModel.updateOne({}, {
                    omitUndefined: true
                })
                .where('_id').equals(partnerCategoryId)
                .set(document)
                .then((partnerCategory) => {
                    if (partnerCategory.n < 1 || partnerCategory.nModified < 1)
                        throw new ApplicationError({
                            name: 'UpdatePartnerCategoryError',
                            message: 'Partner Category not updated.'
                        });
                    return partnerCategory;
                });
        }
    }

    static validation() {
        return {
            "$id": "http://guru.sheenlac.com/schemas/update_partner_category.json",
            "type": "object",
            "properties": {
                "title": {
                    "type": "string"
                },
                "description": {
                    "type": "string"
                },
                "image": {
                    "type": "string"
                },
                "displayOrder": {
                    "type": "integer"
                },
                "status": {
                    "type": "string",
                    "enum": ["ACTIVE", "INACTIVE"]
                }
            },
            "additionalProperties": false
        };
    }
}

module.exports = UpdatePartnerCategoryTask;