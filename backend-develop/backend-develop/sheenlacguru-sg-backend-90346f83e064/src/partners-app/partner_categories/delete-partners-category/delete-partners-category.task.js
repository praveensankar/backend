const PartnersCategoriesModel = require("../partners_categories.model");
const ApplicationError = require("@src/errors/ApplicationError");

class DeletePartnerCategoryTask {

    /**
     * Update Partner Category
     * @param {string} partnerCategoryId partner category id
     * @param {object} document document object
     * @param {session instance} session session instance
     * @return return partner category
     */
    static exec(partnerCategoryId, session) {
        if (session) {
            return PartnersCategoriesModel.deleteOne()
                .where('_id').equals(partnerCategoryId).session(session).then((partnerCategory) => {
                    if (!partnerCategory)
                        throw new ApplicationError({
                            name: 'UpdatePartnerCategoryError',
                            message: 'Partner Category not updated.'
                        });
                    return partnerCategory;
                });
        } else {
            return PartnersCategoriesModel.deleteOne()
                .where('_id').equals(partnerCategoryId)
                .then((partnerCategory) => {
                    if (!partnerCategory)
                        throw new ApplicationError({
                            name: 'UpdatePartnerCategoryError',
                            message: 'Partner Category not updated.'
                        });
                    return partnerCategory;
                });
        }
    }
}

module.exports = DeletePartnerCategoryTask;