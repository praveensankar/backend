const express = require('express');
const router = express.Router();
const nconf = require('nconf');

const Token = require('@src/middleware/token');
const Authorization = require('@src/middleware/authorization');
const PartnerCategoriesController = require('./partners_categories.controller');
const Uploader = require('@src/middleware/uploader.middleware');

router.use(Token.verify({
    for: 'admin'
}));

router.post('/', Authorization.authorize(["partnerManagement"]), Uploader({
    dest: nconf.get("STORAGE:PARTNERCATEGORIES")
}).single('image'), PartnerCategoriesController.create);
router.get('/', Authorization.authorize(["partnerManagement"]), PartnerCategoriesController.list);
router.get('/:id', Authorization.authorize(["partnerManagement"]), PartnerCategoriesController.show);
router.patch('/:id', Authorization.authorize(["partnerManagement"]), Uploader({
    dest: nconf.get("STORAGE:PARTNERCATEGORIES")
}).single('image'), PartnerCategoriesController.update);
router.delete('/:id', Authorization.authorize(["partnerManagement"]), PartnerCategoriesController.delete);

module.exports = router;