const _ = require('lodash');
const PartnerCategoriesModel = require('../partners_categories.model');
const ApplicationError = require("@src/errors/ApplicationError");

class ListPartnerCategoryTask {

    static async list(query = {}, options = {}) {
        const skip = options.page > 0 ? (options.page - 1) * options.size : 0;
        let partnerCategories = [];
        let partnerCategoriesCount = 0;

        // search when search key exist in options
        if (options.searchText !== "") {
            /* Start of Search */

            /* ------- Search with exact match using text based search */
            const partnerCategoriesWithExactMatch = await PartnerCategoriesModel.find({
                $text: {
                    $search: options.searchText
                }
            });
            /* -------- Search with start (title,status,mobile_number,email,empId) */
            const rgxForStartMatch = (pattern) => new RegExp(`^${pattern}`); // pattern for matching start of searchText
            const searchTextForStartMatch = rgxForStartMatch(options.searchText);
            const partnerCategoriesWithStartMatch = await PartnerCategoriesModel.find({
                $or: [{
                        title: {
                            $regex: searchTextForStartMatch,
                            $options: 'i'
                        }
                    },
                    {
                        status: {
                            $regex: searchTextForStartMatch,
                            $options: 'i'
                        }
                    },
                    {
                        description: {
                            $regex: searchTextForStartMatch,
                            $options: 'i'
                        }
                    }
                ]
            });

            /* -------- Fuzzy search with (title,status,mobile_number,email) */
            const rgxForFuzzyMatch = (pattern) => new RegExp(`${pattern}`);
            const searchTextForFuzzyMatch = rgxForFuzzyMatch(options.searchText);
            const partnerCategoriesWithFuzzyMatch = await PartnerCategoriesModel.find({
                $or: [{
                        title: {
                            $regex: searchTextForFuzzyMatch,
                            $options: 'i'
                        }
                    },
                    {
                        status: {
                            $regex: searchTextForFuzzyMatch,
                            $options: 'i'
                        }
                    },
                    {
                        description: {
                            $regex: searchTextForStartMatch,
                            $options: 'i'
                        }
                    }
                ]
            });

            const partnerList = _.concat(partnerCategoriesWithExactMatch, partnerCategoriesWithStartMatch, partnerCategoriesWithFuzzyMatch);

            /* generate list of distinct partnerCategories if partnerCategories got repeated */
            partnerCategories = _.concat([], _.uniqBy(partnerList, '_id'));
            partnerCategoriesCount = partnerCategories.length;
            partnerCategories = partnerCategories.slice(skip).slice(0, options.size)

            /* End of Search */
        } else {
            /* fetch partnerCategories if no search key is present on options */
            const PartnerCategoriesQuery = PartnerCategoriesModel.find();
            if (options.status) PartnerCategoriesQuery.where("status").equals(options.status);
            partnerCategories = await PartnerCategoriesQuery.find().skip(skip).limit(options.size).exec();
            partnerCategoriesCount = await PartnerCategoriesModel.countDocuments().exec();
        }

        return Promise.all([
            partnerCategories,
            partnerCategoriesCount
        ]).then(([partnerCategories, count]) => {
            return {
                items: partnerCategories,
                totalCount: count
            };
        });
    }

    static validation() {
        return {
            "$id": "http://guru.sheenlac.com/schemas/amdins/list_partners_category.json",
            "$async": true,
            "type": "object",
            "properties": {
                "page": {
                    "type": "integer",
                    "default": 1
                },
                "size": {
                    "type": "integer",
                    "default": 10
                },
                "status": {
                    "type": "string"
                },
                "searchText": {
                    "type": "string",
                    "default": ""
                }
            },
            "required": [],
            "additionalProperties": false
        };
    }

}

module.exports = ListPartnerCategoryTask;