const express = require('express');
const router = express.Router();

const PartnerCategoriesController = require('./partners_categories.controller');

router.get('/', PartnerCategoriesController.list);
router.get('/:id', PartnerCategoriesController.show);

module.exports = router;