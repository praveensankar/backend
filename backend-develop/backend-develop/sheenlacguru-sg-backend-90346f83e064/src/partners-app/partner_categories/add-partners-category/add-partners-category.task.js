const PartnersCategoriesModel = require("../partners_categories.model");
const ApplicationError = require("@src/errors/ApplicationError");

class AddPartnerCategoryTask {

    /**
     * Create Partner Category
     * @param {object} document document object
     * @param {session instance} session session instance
     * @return return partner category
     */
    static exec(document, session) {
        if (session) {
            return PartnersCategoriesModel.create([document], session).then((partnerCategory) => {
                if (!partnerCategory)
                    throw new ApplicationError({
                        name: 'CreatePartnerCategoryError',
                        message: 'Partner Category not created.'
                    });
                return partnerCategory;
            });
        } else {
            return new PartnersCategoriesModel(document).save()
                .then((partnerCategory) => {
                    if (!partnerCategory)
                        throw new ApplicationError({
                            name: 'CreatePartnerCategoryError',
                            message: 'Partner Category not created.'
                        });
                    return partnerCategory;
                });
        }
    }

    static validation() {
        return {
            "$id": "http://guru.sheenlac.com/schemas/create_partner_category.json",
            "type": "object",
            "properties": {
                "title": {
                    "type": "string"
                },
                "description": {
                    "type": "string"
                },
                "image": {
                    "type": "string"
                },
                "displayOrder": {
                    "type": "integer"
                },
                "status": {
                    "type": "string",
                    "enum": ["ACTIVE", "INACTIVE"]
                }
            },
            "required": ["title", "description", "image", "displayOrder", "status"],
            "additionalProperties": false
        };
    }
}

module.exports = AddPartnerCategoryTask;