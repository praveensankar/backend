const ServiceRequestModel = require("../service_requests.model");
const ApplicationError = require("@src/errors/ApplicationError");
const uniqId = require('uniqid');
const CustomerModel = require("../../customers/customer.model");
const _ = require("lodash");

class ServiceRequestTask {

    static async create(request) {
        request['serviceRequestId'] = uniqId();
        console.log(request);
        try {
            const updateCustomer = await CustomerModel.updateOne().where("_id").equals(request.customer).set({
                address : request.address ? request.address : "",
                email : request.email ? request.email : ""
            }).exec();
   
            if(updateCustomer.n === 1 || updateCustomer.nModified === 1 ){
                request = _.omit(request,["address","email"]);
                return new ServiceRequestModel(request).save()
                    .then((created_request) => {
                        if (!created_request)
                            throw new ApplicationError({
                                name: 'CreateServiceRequestError',
                                message: 'Service Request not created.'
                            });
                        return created_request;
                    });
            }
        } catch (error) {
            throw new ApplicationError({
                name: error.name,
                message: error.message
            });
        }
    }

    static validation() {
        return {
            "$id": "http://guru.sheenlac.com/schemas/service_request.json",
            "type": "object",
            "properties": {
                "customer": {
                    "type": "string"
                },
                "partner": {
                    "type": "string"
                },
                "service": {
                    "type": "string"
                },
                "painter": {
                    "type": "string"
                },
                "email": {
                    "type": "string"
                },
                "address": {
                    "type": "string"
                },
                "jobDescription": {
                    "type": "string"
                },
                "preferredDate": {
                    "type": "string",
                },
                "preferredTimeSlot": {
                    "type": "string"
                },
                "isAuthorisedtoContact": {
                    "type": "boolean",
                    "default": true
                }
            },
            "required": ["customer", "partner", "service", "painter", "jobDescription", "preferredDate", "preferredTimeSlot", "isAuthorisedtoContact"],
            "additionalProperties": false
        };
    }
}

module.exports = ServiceRequestTask;