const _ = require('lodash');
const serviceRequestsModel = require('../service_requests.model');
const ApplicationError = require("@src/errors/ApplicationError");

class ListServiceRequestsTask {

    static async list(options = {}) {
        const skip = options.page > 0 ? (options.page - 1) * options.size : 0;
        let serviceRequests = [];
        let serviceRequestsCount = 0;

        // search when search key exist in options
        if (options.searchText !== "") {
            /* Start of Search */

            /* ------- Search with exact match using text based search */
            const serviceRequestsWithExactMatch = await serviceRequestsModel.find({
                $text: {
                    $search: options.searchText
                }
            }).populate('customer').populate('painter','first_name last_name mobile_number').populate('service').populate('partner');
            /* -------- Search with start (title,status,mobile_number,email,empId) */
            const rgxForStartMatch = (pattern) => new RegExp(`^${pattern}`); // pattern for matching start of searchText
            const searchTextForStartMatch = rgxForStartMatch(options.searchText);
            const serviceRequestsWithStartMatch = await serviceRequestsModel.find({
                $or: [{
                    serviceRequestId: {
                            $regex: searchTextForStartMatch,
                            $options: 'i'
                        }
                    }
                ]
            }).populate('customer').populate('painter','first_name last_name mobile_number').populate('service').populate('partner');

            /* -------- Fuzzy search with (title,status,mobile_number,email) */
            const rgxForFuzzyMatch = (pattern) => new RegExp(`${pattern}`);
            const searchTextForFuzzyMatch = rgxForFuzzyMatch(options.searchText);
            const serviceRequestsWithFuzzyMatch = await serviceRequestsModel.find({
                $or: [{
                    serviceRequestId: {
                            $regex: searchTextForFuzzyMatch,
                            $options: 'i'
                        }
                    }
                ]
            }).populate('customer').populate('painter','first_name last_name mobile_number').populate('service').populate('partner');

            const partnerList = _.concat(serviceRequestsWithExactMatch, serviceRequestsWithStartMatch, serviceRequestsWithFuzzyMatch);

            /* generate list of distinct serviceRequests if serviceRequests got repeated */
            serviceRequests = _.concat([], _.uniqBy(partnerList, '_id'));
            serviceRequestsCount = serviceRequests.length;
            serviceRequests = serviceRequests.slice(skip).slice(0, options.size)

            /* End of Search */
        } else {
            /* fetch serviceRequests if no search key is present on options */
            const serviceRequestsQuery = serviceRequestsModel.find().populate('customer').populate('painter','first_name last_name mobile_number').populate('service').populate('partner');
            if (options.status) serviceRequestsQuery.where("status").equals(options.status);
            serviceRequests = await serviceRequestsQuery.find().skip(skip).limit(options.size).exec();
            serviceRequestsCount = await serviceRequestsModel.countDocuments().exec();
        }

        return Promise.all([
            serviceRequests,
            serviceRequestsCount
        ]).then(([serviceRequests, count]) => {
            return {
                items: serviceRequests,
                totalCount: count
            };
        });
    }

    static validation() {
        return {
            "$id": "http://guru.sheenlac.com/schemas/amdins/list_service_request.json",
            "$async": true,
            "type": "object",
            "properties": {
                "page": {
                    "type": "integer",
                    "default": 1
                },
                "size": {
                    "type": "integer",
                    "default": 10
                },
                "status": {
                    "type": "string"
                },
                "searchText": {
                    "type": "string",
                    "default": ""
                }
            },
            "required": [],
            "additionalProperties": false
        };
    }

}

module.exports = ListServiceRequestsTask;