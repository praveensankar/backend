const _ = require('lodash');
const serviceRequestsModel = require('../service_requests.model');
const ApplicationError = require("@src/errors/ApplicationError");

class ListCustomerServiceRequestsTask {

    static async listCustomerServiceRequests(options) {
        // const skip = options.page > 0 ? (options.page - 1) * options.size : 0;

        /* fetch serviceRequests if no search key is present on options */
        const serviceRequestsQuery = await serviceRequestsModel.find().where("customer").equals(options.customerId).populate('customer').populate('painter', 'first_name last_name mobile_number').populate('service').populate('partner').exec();  
        const serviceRequestsCount = await serviceRequestsModel.find().where("customer").equals(options.customerId).countDocuments().exec();

        return Promise.all([    
            serviceRequestsQuery,
            serviceRequestsCount
        ]).then(([serviceRequests, count]) => {
            return {
                items: serviceRequests,
                totalCount: count
            };
        });
    }

    // static validation() {
    //     return {
    //         "$id": "http://guru.sheenlac.com/schemas/amdins/list_service_request.json",
    //         "$async": true,
    //         "type": "object",
    //         "properties": {
    //             "page": {
    //                 "type": "integer",
    //                 "default": 1
    //             },
    //             "size": {
    //                 "type": "integer",
    //                 "default": 10
    //             },
    //             "status": {
    //                 "type": "string"
    //             },
    //             "searchText": {
    //                 "type": "string",
    //                 "default": ""
    //             }
    //         },
    //         "required": [],
    //         "additionalProperties": false
    //     };
    // }

}

module.exports = ListCustomerServiceRequestsTask;