const mongoose = require('mongoose');
const nconf = require('nconf');

const Schema = mongoose.Schema;

// const SERVER_HOST = nconf.get('APPLICATION:S3');

const ServiceRequests = new Schema({
    serviceRequestId: {
        type: String,
        trim: true
    },
    customer: {
        type: Schema.Types.ObjectId,
        ref: "Customers"
    },
    partner: {
        type: Schema.Types.ObjectId,
        ref: "PartnerCategories"
    },
    service: {
        type: Schema.Types.ObjectId,
        ref: "ServiceCategories"
    },
    painter: {
        type: Schema.Types.ObjectId,
        ref: "User"
    },
    jobDescription: {
        type: String,
        trim: true
    },
    preferredDate: {
        type: String
    },
    preferredTimeSlot: {
        type: String
    },
    isAuthorisedtoContact: {
        type: Boolean,
        default: false
    },
    status: {
        type: String,
        enum: ['NEW', 'INACTIVE'],
        default: "NEW"
    }
}, {
    timestamps: true
});

ServiceRequests.set('toJSON', {
    virtuals: true,
    getters: true
});

module.exports = mongoose.model('ServiceRequests', ServiceRequests);