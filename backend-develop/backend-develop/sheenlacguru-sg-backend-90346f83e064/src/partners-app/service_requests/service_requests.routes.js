const express = require('express');
const router = express.Router();

const Token = require('@src/middleware/token');
const ServiceRequestController = require('./service_requests.controller');
const Authorization = require('@src/middleware/authorization');


router.post('/createRequest', ServiceRequestController.createRequest);
router.get('/', Token.verify({
    for: 'admin'
}), Authorization.authorize(["serviceManagement"]), ServiceRequestController.index);
router.get('/listCustomerServiceRequest', Token.verify({
    for: 'customer'
}), ServiceRequestController.listCustomerServiceRequests);

module.exports = router;