const Ajv = require('ajv');
const _ = require("lodash");

const ApplicationError = require("@src/errors/ApplicationError");
const validations = require('@app/validations');
const ajv = new Ajv(validations.ajv_options);
const ServiceRequestTask = require("./create_service_request/create_service_request.task");
const ListServiceRequestsTask = require('./list_service_request/list_service_request.task');
const ListCustomerServiceRequestsTask = require('./list_customer_service_request/list_customer_service_request.task');
class ServiceRequestController {

    static createRequest(req, res, next) {
        if (!ajv.validate(ServiceRequestTask.validation(), req.body)) {
            let errors = ajv.errors;
            return next(
                new ApplicationError({
                    status: 400,
                    name: "ValidationError",
                    message: "Bad request parameters.",
                    info: errors
                })
            );
        }

        return ServiceRequestTask.create(req.body)
            .then((request) => {
                console.log(request)
                return res.status(200).json(
                    new res.app.locals.Success({
                        data: request
                    })
                );
            })
            .catch((err) => {
                return next(err);
            });
    }

    static index(req, res, next) {
        if (!ajv.validate(ListServiceRequestsTask.validation(), req.query)) {
            let errors = ajv.errors;
            return next(
                new ApplicationError({
                    status: 400,
                    name: "ValidationError",
                    message: "Bad request parameters.",
                    info: errors
                })
            );
        }

        let options = _.omit(req.query, ['status']);
        return ListServiceRequestsTask.list(req.query)
            .then((serviceRequests) => {
                console.log(serviceRequests)
                return res.status(200).json(
                    new res.app.locals.Success({
                        data: serviceRequests
                    })
                );
            })
            .catch((err) => {
                return next(err);
            });
    };

    static listCustomerServiceRequests(req, res, next) {
        return ListCustomerServiceRequestsTask.listCustomerServiceRequests(req.query)
            .then((serviceRequests) => {
                return res.status(200).json(
                    new res.app.locals.Success({
                        data: serviceRequests
                    })
                );
            })
            .catch((err) => {
                return next(err);
            });
    };
}

module.exports = ServiceRequestController;