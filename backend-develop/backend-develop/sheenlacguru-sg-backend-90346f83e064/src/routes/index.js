var express = require('express');
var router = express.Router();

router.use('/api/v1', require('../app/routes/v1'));
router.use('/api/v2', require('../app/routes/v2'));

// Partner APP Routes
router.use('/api/v2/partners', require('../partners-app/routes/v2'));

module.exports = router;