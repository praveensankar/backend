const lodash = require('lodash');

class Success {
    constructor({data = {}, message="Success"} = {}) {
        this.status = 1;
        this.data = data;
        this.message = data.message || message;
    }
}

class Error { 
    constructor ({error = {},  message = "Oops! something went wrong."} = {}) {
        this.status = 0;
        this.error = lodash.assign({name: "InternalServerError", message: error.message || message}, error);
        this.message = message;
    }
}

module.exports.Success = Success;
module.exports.Error = Error;