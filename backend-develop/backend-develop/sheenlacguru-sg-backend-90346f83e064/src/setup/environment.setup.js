var path = require('path');
var nconf = require('nconf');

nconf
.overrides({ "APP_NAME": "SHEENLAC GURU" })
.argv()
.env()
.file(nconf.get("NODE_ENV"), path.resolve(__dirname, '..', 'environments', `environment.${nconf.get('NODE_ENV')}.json`))
.file("default", path.resolve(__dirname, '..', 'environments', `environment.json`));

console.log(`********** ${nconf.get("APP_NAME")} ${nconf.get("NODE_ENV")}**********`)

module.exports = nconf;