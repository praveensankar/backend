const mongoose = require('mongoose');
const bluebird = require('bluebird');
const nconf = require('nconf');

const dbHost = nconf.get("DB:HOST");
const dbUser = nconf.get("DB:USER");
const dbPass = nconf.get("DB:PASSWORD");
const authDB = nconf.get("DB:AUTHDB");
const dbName = nconf.get("DB:NAME");

const dbUrl = `mongodb+srv://${encodeURIComponent(dbUser)}:${encodeURIComponent(dbPass)}@${dbHost}/${dbName}?authSource=${authDB}&retryWrites=true&w=majority`;

const dbOptions = {
  useNewUrlParser: true,
  useCreateIndex: true,
  useUnifiedTopology: true,
  // autoReconnect: true,
  // reconnectTries: 10,
  // reconnectInterval: 60000,
  promiseLibrary: bluebird
}

// Create the database connection
mongoose.connect(dbUrl, dbOptions);

/* CONNECTION EVENTS */

mongoose.connection.on('connecting', function () {
  console.log(`connecting to ${dbName}`);
});

mongoose.connection.on('connected', function () {
  console.log(`connected to ${dbName}`);
});

// works fine, but not fired when I kill **mongod** process
mongoose.connection.on('error', function (error) {
  // error log to a file
  console.log(`error in connection to ${dbName}`);
});

mongoose.connection.once('open', function () {
  console.log(`MongoDB connection opened to ${dbName}!`);
});

mongoose.connection.on('reconnected', function () {
  console.log(`MongoDB reconnected to ${dbName}!`);
});

mongoose.connection.on('disconnected', function () {
  console.log(`MongoDB disconnected from ${dbName}!`);
  // Below line is not working as expected
  mongoose.connection = mongoose.createConnection(dbUrl, dbOptions);
});


function gracefulExit() {
  mongoose.connection.close(function () {
    console.log(`Mongoose connection to ${dbName} disconnected through app termination`);
    process.exit(0);
  });
};

// If the Node process ends, close the Mongoose connection
process.on('SIGINT', gracefulExit).on('SIGTERM', gracefulExit);