var path = require('path');

var express = require('express');
var logger = require('morgan');
var cors = require('cors');

var response = require('./response');
var transporter = require('@src/libraries/email');
var routes = require('./routes/index');

var app = express();

// Disable cross origin resource sharing when development was done.
app.use(cors());

app.locals.transporter = transporter;
app.locals.Success = response.Success;
app.locals.Error = response.Error;

// Add response compression logic to the app.
app.use(logger('dev'));
app.use(express.json({limit: '50mb'}));
app.use(express.urlencoded({ limit: '50mb', extended: false }));
app.use('/media', express.static(path.join(__dirname, '..', 'media')));

app.use('/', routes);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  return res.status(404).json(
    new res.app.locals.Error({message: "Service not found."})
  );
});

// error handler
app.use(function(err, req, res, next) {
  return res.status(err.status || 500).json(
    new res.app.locals.Error({error: err})
  );
});

module.exports = app;
