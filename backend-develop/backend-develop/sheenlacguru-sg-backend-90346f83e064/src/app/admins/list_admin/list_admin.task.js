const Promise = require("bluebird");
const _ = require('lodash');
const AdminModel = require('../admin.model');

class ListAdminTask {

    static async list(query = {}, options = {}) {
        const skip = options.page > 0 ? (options.page - 1) * options.size : 0;
        let admins = [];
        let adminCount = 0;

        // search when search key exist in options
        if (options.searchText !== "") {
            /* Start of Search */

            /* ------- Search with exact match using text based search */
            const adminsWithExactMatch = await AdminModel.find({
                $text: {
                    $search: options.searchText
                }
            });
            /* -------- Search with start (first_name,last_name,mobile_number,email,empId) */
            const rgxForStartMatch = (pattern) => new RegExp(`^${pattern}`); // pattern for matching start of searchText
            const searchTextForStartMatch = rgxForStartMatch(options.searchText);
            const adminsWithStartMatch = await AdminModel.find({
                $or: [{
                        first_name: {
                            $regex: searchTextForStartMatch,
                            $options: 'i'
                        }
                    },
                    {
                        last_name: {
                            $regex: searchTextForStartMatch,
                            $options: 'i'
                        }
                    },
                    {
                        mobile_number: {
                            $regex: searchTextForStartMatch,
                            $options: 'i'
                        }
                    },
                    {
                        email: {
                            $regex: searchTextForStartMatch,
                            $options: 'i'
                        }
                    },
                    {
                        empId: {
                            $regex: searchTextForStartMatch,
                            $options: 'i'
                        }
                    }
                ]
            });

            /* -------- Fuzzy search with (first_name,last_name,mobile_number,email) */
            const rgxForFuzzyMatch = (pattern) => new RegExp(`${pattern}`);
            const searchTextForFuzzyMatch = rgxForFuzzyMatch(options.searchText);
            const adminsWithFuzzyMatch = await AdminModel.find({
                $or: [{
                        first_name: {
                            $regex: searchTextForFuzzyMatch,
                            $options: 'i'
                        }
                    },
                    {
                        last_name: {
                            $regex: searchTextForFuzzyMatch,
                            $options: 'i'
                        }
                    },
                    {
                        mobile_number: {
                            $regex: searchTextForFuzzyMatch,
                            $options: 'i'
                        }
                    },
                    {
                        email: {
                            $regex: searchTextForFuzzyMatch,
                            $options: 'i'
                        }
                    },
                    {
                        empId: {
                            $regex: searchTextForStartMatch,
                            $options: 'i'
                        }
                    }
                ]
            });

            const adminlist = _.concat(adminsWithExactMatch, adminsWithStartMatch, adminsWithFuzzyMatch);

            /* generate list of distinct admins if admins got repeated */
            admins = _.concat([], _.uniqBy(adminlist, 'id'));
            adminCount = admins.length;
            admins = admins.slice(skip).slice(0, options.size)

            /* End of Search */
        } else {
            /* fetch admins if no search key is present on options */
            admins = await AdminModel.find().select('-password').populate('access_previledges').skip(skip).limit(options.size).exec();
            adminCount = await AdminModel.countDocuments().exec();
        }

        return Promise.all([
            admins,
            adminCount
        ]).then(([admins, count]) => {
            return {
                items: admins,
                totalCount: count
            };
        });
    }

    static validation() {
        return {
            "$id": "http://guru.sheenlac.com/schemas/amdins/list_admins.json",
            "$async": true,
            "type": "object",
            "properties": {
                "page": {
                    "type": "integer",
                    "default": 1
                },
                "size": {
                    "type": "integer",
                    "default": 10
                },
                "status": {
                    "type": "string"
                },
                "searchText": {
                    "type": "string",
                    "default": ""
                }
            },
            "required": [],
            "additionalProperties": false
        };
    }

}

module.exports = ListAdminTask;