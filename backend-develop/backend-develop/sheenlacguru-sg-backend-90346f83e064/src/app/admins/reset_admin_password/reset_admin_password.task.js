const Promise = require('bluebird');

const AdminModel = require('../admin.model');
const ApplicationError = require("@src/errors/ApplicationError");

class ResetAdminPasswordTask {

    static exec(user_id, credentials) {
        return AdminModel.findById(user_id)
                    .then( (admin) => {
                        if (!admin)
                            throw new ApplicationError({status: 400, name: "ResourceNotFound", message: "Admin does not exists."});
                        return Promise.all([admin]);
                    })
                    .then( ([admin]) => {
                        return admin.set_password(credentials.password);
                    })
                    .then( (admin) => {
                        return AdminModel.updateOne().where("_id").equals(admin._id).set({password: admin.password}).exec();
                    })
                    .then( (reset_ops) => {
                        if (reset_ops.n < 1 || reset_ops.nModified < 1)
                            throw new ApplicationError({name: "ResetPasswordError", message: "Password is not updated."});
                        return { matched: reset_ops.n, updated: reset_ops.nModified, message: "Password updated." };
                    });
    }

    static validation() {
        return {
            "$id": "http://guru.sheenlac.com/schemas/reset_admin_password.json",
            "type": "object",
            "properties": {
                "password": { "type": "string", "minLength": 8 },
            },
            "required": ["password"],
            "additionalProperties": false
        };
    }

}

module.exports = ResetAdminPasswordTask;