const express = require('express');
const router = express.Router();

const Token = require('@src/middleware/token');
const Authorization = require('@src/middleware/authorization');
const AdminController = require('./admin.controller')

router.use(Token.verify({ for: 'admin' }));

router.post('/reset_password', AdminController.reset_password);
router.get('/', Authorization.authorize(["super"]), AdminController.index);
router.put('/', Authorization.authorize(["super"]), AdminController.create);
router.get('/:id', AdminController.show);
router.delete('/:id', Authorization.authorize(["super"]), AdminController.delete);
router.post('/:id', AdminController.update);

module.exports = router;