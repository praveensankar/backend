const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const nconf = require('nconf');

const PASSWORD_SALT_FACTOR = nconf.get("USER_PASSWORD_SALT_FACTOR");

const Schema = mongoose.Schema;

const AdminSchema = new Schema({
    empId: {
        type: String,
        trim: true
    },
    first_name: {
        type: String,
        trim: true,
        minlength: [3, 'First name must be three or more characters long.'],
        required: [true, 'First name is required.']
    },
    last_name: {
        type: String,
        trim: true,
        required: [true, 'Last name is required.']
    },
    designation: {
        type : String,
        trim : true
    },
    location: {
        type : String,
        trim : true
    },
    email: {
        type: String,
        trim: true,
        lowercase: true,
        unique: true,
        match: [/^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/, '`{VALUE}` is not a valid email id.'],
        required: [true, 'Email Id is required.']
    },
    mobile_no: {
        type : String,
        trim : true,
        required: [true, 'Mobile No is required.']
    },
    status : {
        type : String,
        enum : ['ACTIVE','INACTIVE'],
        default : 'INACTIVE',
        required: [true, 'Status is required.']
    },
    password: {
        type: String,
        trim: true,
        minlength: 8
    },
    role_id: {
        type: Schema.Types.ObjectId,
        ref: 'AdminRoles',
    }
}, { 
    timestamps: true
});

AdminSchema.statics.HASH_PASSWORD = function(password) {
    return bcrypt.hash(password, PASSWORD_SALT_FACTOR);
}

AdminSchema.methods.set_password = function (password) {
    return bcrypt.hash(password, PASSWORD_SALT_FACTOR)
                .then( (hash) => {
                    this.password = hash;
                    return this;
                });
}

AdminSchema.methods.compare_password = function (admin_password) {
    return bcrypt.compare(admin_password, this.password);
}

AdminSchema.methods.check_status = function (status) {
    return this.status === status ? true : false;
}

AdminSchema.pre('save', function(next) {
    if (!this.isModified('password')) return next();

    return bcrypt.hash(this.password, PASSWORD_SALT_FACTOR).then( (hash) => this.password = hash );
});

AdminSchema.virtual('id').get(function() {
    return this._id.toHexString();
});

AdminSchema.virtual('access_previledges', {
    ref: 'AdminRoles',
    localField: 'role_id',
    foreignField: '_id',
    justOne: true
})

AdminSchema.set('toJSON', { virtuals: true, getters: true });


module.exports = mongoose.model("Admin", AdminSchema);