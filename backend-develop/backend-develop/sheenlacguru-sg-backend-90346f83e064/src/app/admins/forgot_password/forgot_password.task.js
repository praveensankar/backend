const moment = require('moment');

const ApplicationError = require('@src/errors/ApplicationError');
const OneTimePassword = require('@src/libraries/otp');
const EmailOTPModel = require('../email_otp.model');
const AdminModel = require('../admin.model');

class ForgotPasswordTask {

    static initiate(email) {
        return AdminModel.findOne().where('email').equals(email).exec()
                   .then( (admin) => {
                       if (!admin) 
                        throw new ApplicationError({status: 400, name: "ResourceNotFound", message: "admin does not exists."});

                       return EmailOTPModel.findOne()
                                           .where('email').equals(email)
                                           .where('is_verified').equals(false)
                                           .where('expires_at').gte(moment.utc().add(5, 'minutes'))
                                           .sort('-createdAt').exec()
                   })
                   .then( (email_otp_doc) => {
                       if (email_otp_doc) return email_otp_doc;

                       return new EmailOTPModel({
                           email: email,
                           otp: OneTimePassword.generate(),
                           expires_at: moment.utc().add(30, 'minutes')
                       }).save();
                   });
    }

    static update(document) {
        return EmailOTPModel.findOne()
                            .where('email').equals(document.email)
                            .where('otp').equals(document.uuid)
                            .where('is_verified').equals(false)
                            .where('expires_at').gte(new Date())
                            .sort('-createdAt').exec()
                            .then( (email_otp_doc) => {
                                if (!email_otp_doc) 
                                    throw new ApplicationError({status: 400, name: "ResourceNotFound", message: "Invalid one time password."});

                                email_otp_doc.is_verified = true;
                                return email_otp_doc.save();
                            })
                            .then( (email_otp_doc) => {
                                return AdminModel.HASH_PASSWORD(document.password);
                            })
                            .then( (hash) => {
                                return AdminModel.updateOne()
                                                 .where('email').equals(document.email)
                                                 .set({password: hash}).exec();
                            })
                            .then( (response) => {
                                if (response.n < 1 || response.nModified < 1)
                                    throw new ApplicationError({name: "UpdateError", message: "Password is not updated."});
                                return { matched: response.n, updated: response.nModified, message: "Password updated." };
                            });
    }

    static validation() {
        return {
            "$id": "http://guru.sheenlac.com/schemas/forgot_password.json",
            "type": "object",
            "properties": {
                "email": { "type": "string", "format": "email" },
                "uuid": { "type": "string", "minLength": 6, "maxLength": 6},
                "password": { "type": "string", "minLength": 8 }
            },
            "required": ["email", "uuid", "password"],
            "additionalProperties": false
        };
    }

}

module.exports = ForgotPasswordTask;