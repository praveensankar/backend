const moment = require('moment');

const AdminModel = require('../admin.model');
const EmailOTPModel = require('../email_otp.model');

const ApplicationError = require("@src/errors/ApplicationError");
const OneTimePassword = require("@src/libraries/otp");

class CreateAdminTask {

    static exec(document) {
      return new AdminModel(document).save()
                        .then( (admin) => {
                            if (!admin)
                                throw new ApplicationError({name: "CreateAdminError", message: "Admin is not created."});
                            
                            return Promise.all([
                                EmailOTPModel.findOne()
                                             .where('email').equals(admin.email)
                                             .where('is_verified').equals(false)
                                             .where('expires_at').gte(moment.utc().add(5, "minutes"))
                                             .sort('-createdAt').exec(),
                                admin
                            ]);

                        })
                        .then( ([email_otp_doc, admin]) => {
                            if (email_otp_doc) return email_otp_doc;

                            return new EmailOTPModel({
                                email: admin.email,
                                otp: OneTimePassword.generate(),
                                expires_at: moment.utc().add(30, "minutes")
                            }).save();
                        });
    }

    static validation() {
        return {
            "$id": "http://guru.sheenlac.com/schemas/create_admin.json",
            "type": "object",
            "properties": {
                "first_name": { "type": "string", "minLength": 3 },
                "last_name": { "type": "string", "minLength": 1 },
                "email": { "type": "string", "format": "email" },
                "password": { "type": "string", "minLength": 8 },
                "empId" : { "type" : "string" },
                "location" : { "type" : "string" },
                "designation" : { "type" : "string" },
                "mobile_no" : { "type" : "string" },
                "status" : { "type" : "string", "enum": ['ACTIVE', 'INACTIVE'] },
                "role_id": { "type" : "string" }
            },
            "required": ["first_name", "last_name", "email","mobile_no","role_id","status","password"],
            "additionalProperties": false
        };
    }

}

module.exports = CreateAdminTask;