const Promise = require('bluebird');

const AdminModel = require('../admin.model');
const ApplicationError = require("@src/errors/ApplicationError");

class ResetPasswordTask {

    static exec(user_id, credentials) {
        return AdminModel.findById(user_id)
                    .then( (admin) => {
                        if (!admin)
                            throw new ApplicationError({status: 400, name: "ResourceNotFound", message: "Admin does not exists."});
                        return Promise.all([admin, admin.compare_password(credentials.old_password)]);
                    })
                    .then( ([admin, isValidPassword]) => {
                        if (!isValidPassword)
                            throw new ApplicationError({status: 400, name: "Invalid password.", message: "please provide valid password."});
                        return admin.set_password(credentials.password);
                    })
                    .then( (admin) => {
                        return AdminModel.updateOne().where("_id").equals(admin._id).set({password: admin.password}).exec();
                    })
                    .then( (reset_ops) => {
                        if (reset_ops.n < 1 || reset_ops.nModified < 1)
                            throw new ApplicationError({name: "ResetPasswordError", message: "Password is not updated."});
                        return { matched: reset_ops.n, updated: reset_ops.nModified, message: "Password updated." };
                    });
    }

    static validation() {
        return {
            "$id": "http://guru.sheenlac.com/schemas/reset_password.json",
            "type": "object",
            "properties": {
                "old_password": { "type": "string", "minLength": 8 },
                "password": { "type": "string", "minLength": 8 },
            },
            "required": ["old_password", "password"],
            "additionalProperties": false
        };
    }

}

module.exports = ResetPasswordTask;