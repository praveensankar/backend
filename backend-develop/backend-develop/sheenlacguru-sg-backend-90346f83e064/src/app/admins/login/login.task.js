const Promise = require('bluebird');

const AdminModel = require('../admin.model');
const ApplicationError = require("@src/errors/ApplicationError");

class LoginTask {

    static exec(credentials) {
        return AdminModel.findOne().where("email").equals(credentials.username)
            .populate('access_previledges')
            .exec()
            .then( (admin) => {
                if (!admin)
                    throw new ApplicationError({status: 400, name: "Bad Request", message: "Invalid username / password."});
                return Promise.all([admin, admin.compare_password(credentials.password)]);
            })
            .then( ([admin, isValidPassword]) => {
                if (!isValidPassword)
                    throw new ApplicationError({status: 400, name: "Bad Request", message: "Invalid username / password."});
                    console.log(admin.access_previledges);
                if (admin.status === 'INACTIVE' || admin.access_previledges === null)
                    throw new ApplicationError({status: 400, name: "Bad Request", message: "You do not have access to the Sheenlac Guru Administration Portal, please contact your administrator."});
                return admin;
            });
    }

    static validation() {
        return {
            "$id": "http://guru.sheenlac.com/schemas/login.json",
            "type": "object",
            "properties": {
                "username": { "type": "string", "format": "email" },
                "password": { "type": "string", "minLength": 8 }
            },
            "required": ["username", "password"],
            "additionalProperties": false
        };
    }

}

module.exports = LoginTask;