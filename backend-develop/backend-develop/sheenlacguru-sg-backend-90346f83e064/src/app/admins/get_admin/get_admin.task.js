const AdminModel = require('../admin.model');

class GetAdminTask {

    static exec(id) {
        return AdminModel.findById(id).select('-password').populate('access_previledges').exec();
    }

}

module.exports = GetAdminTask;