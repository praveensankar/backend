const AdminModel = require('../admin.model');

const ApplicationError = require("@src/errors/ApplicationError");

class UpdateAdminTask {

    static exec(id, document) {
        return AdminModel.where('_id').equals(id).updateOne({}, {omitUndefined: true}).set(document)
                         .exec()
                         .then( (update_resp) => {
                             if (update_resp.n < 1 || update_resp.nModified < 1 )
                                throw new ApplicationError({name: "UpdateError", message: "Updation of admin failed."});

                            return { matched: update_resp.n, updated: update_resp.nModified, message: "Admin updated." };
                         });
    }

    static validation() {
        return {
            "$id": "http://guru.sheenlac.com/schemas/update_admin.json",
            "type": "object",
            "properties": {
                "first_name": { "type": "string", "minLength": 3 },
                "last_name": { "type": "string", "minLength": 1 },
                "email": { "type": "string", "format": "email" },
                "password": { "type": "string", "minLength": 8 },
                "empId" : { "type" : "string" },
                "location" : { "type" : "string" },
                "designation" : { "type" : "string" },
                "mobile_no" : { "type" : "string" },
                "status" : { "type" : "string", "enum": ['ACTIVE', 'INACTIVE'] },
                "role_id": { "type" : "string" }
            },
            "additionalProperties": false
        };
    }

}

module.exports = UpdateAdminTask;