const AdminModel = require('../admin.model')

const ApplicationError = require("@src/errors/ApplicationError");

class DeleteAdminTask {

    static exec(id) {
        return AdminModel.deleteOne().where('_id').equals(id)
                         .exec()
                         .then( (op_resp) => {
                             if (op_resp.ok != 1 || op_resp.deletedCount < 1) {
                                 throw new ApplicationError({name: "DeleteError", message: "Document deletion failed."});
                             }

                             return { deleted: op_resp.deletedCount, message: "Document deleted." };
                         });
    }

}

module.exports = DeleteAdminTask;