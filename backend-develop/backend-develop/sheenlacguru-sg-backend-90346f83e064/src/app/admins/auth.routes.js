const express = require('express');
const router = express.Router();

const Token = require('@src/middleware/token');
const AuthController = require('./auth.controller');

router.post('/', AuthController.login, Token.generate({ for: 'admin' }));
router.post('/forgot_password', AuthController.initiate_forgot_password);
router.patch('/forgot_password', AuthController.forgot_password)

module.exports = router;