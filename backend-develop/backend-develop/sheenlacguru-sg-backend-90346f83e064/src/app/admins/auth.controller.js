const Ajv = require('ajv');
const nconf = require('nconf');
const _ = require("lodash");

const ApplicationError = require("@src/errors/ApplicationError");
const validations = require('@app/validations');
const ajv = new Ajv(validations.ajv_options);

const LoginTask = require('./login/login.task');
const ForgotPasswordTask = require('./forgot_password/forgot_password.task');

class AuthController {

    static login(req, res, next) {
        if (!ajv.validate(LoginTask.validation(), req.body)) {
            let errors = ajv.errors;
            return next(
                new ApplicationError({status: 400, name: "ValidationError", message: "Bad request parameters", info: errors})
            );
        }

        return LoginTask.exec(req.body)
                    .then( (admin) => {
                        req.locals = req.locals || {};
                        req.locals.claims = {
                            id: admin._id,
                            empId : admin.empId || '',
                            role: admin.role,
                            role_id : admin.role_id,
                            access_previledges: AuthController.getAccessPreviledges(admin),
                            email: admin.email,
                            first_name: admin.first_name,
                            last_name: admin.last_name
                        }
                        return next();
                    })
                    .catch( (err) => {
                        return next(err);
                    });
    }

    static getAccessPreviledges(admin) {
        return _.flatten([admin.access_previledges.role_access.generalAccessPreviledges,admin.access_previledges.role_access.superAccessPreviledges]);
    }

    static initiate_forgot_password(req, res, next) {
        if (!req.body.email)
            return next(
                new ApplicationError({status: 400, name: "ValidationError", message: "Email is required."})
            );
        
        return ForgotPasswordTask.initiate(req.body.email)
                        .then( (email_otp_resp) => {
                            res.app.locals.transporter.sendMail({
                                    "to": email_otp_resp.email,
                                    "subject": "Forgot Password: Sheenlac Guru application.",
                                    "text": `One time password for resetting you password is ${email_otp_resp.otp}`,
                                    "html": `<p>Sheenlac Guru application one time password (OTP) for resetting password: ${email_otp_resp.otp}<br /><a href='${nconf.get('APPLICATION:DOMAIN')}admin/auth/set-password?email=${email_otp_resp.email}' target='_blank'>Set Password</a></p>`
                                }).then( (resp) => {
                                    console.log(resp);
                                })
                                .catch( (err) => {
                                    console.log(err);
                                });
                            return res.status(200).json(
                                new res.app.locals.Success({data: {message: 'Email sent.'}})
                            );
                        })
                        .catch( (err) => {
                            return next(err);
                        })
    }

    static forgot_password(req, res, next) {
        if (!ajv.validate(ForgotPasswordTask.validation(), req.body)) {
            let errors = ajv.errors;
            return next(
                new ApplicationError({status: 400, name: "ValidationError", message: "Bad request parameters", info: errors})
            );
        }

        return ForgotPasswordTask.update(req.body)
                    .then( (response_data) => {
                        return res.status(200).json(
                            new res.app.locals.Success({data: response_data})
                        );
                    })
                    .catch( (err) => {
                        return next(err);
                    });
    }
}

module.exports = AuthController;