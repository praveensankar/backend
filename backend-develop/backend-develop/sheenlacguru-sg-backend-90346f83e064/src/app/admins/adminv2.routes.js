const express = require('express');
const router = express.Router();

const Token = require('@src/middleware/token');
const Authorization = require('@src/middleware/authorization');
const AdminController = require('./admin.controller')

router.use(Token.verify({ for: 'admin' }));

router.post('/reset_password', AdminController.reset_password);
router.post('/reset_admin_password/:id', Authorization.authorize(["adminUserManagement"]), AdminController.reset_admin_password);
router.get('/', Authorization.authorize(["adminUserManagement"]), AdminController.index);
router.post('/', Authorization.authorize(["adminUserManagement"]), AdminController.create);
router.get('/:id', AdminController.show);
router.delete('/:id', Authorization.authorize(["adminUserManagement"]), AdminController.delete);
router.patch('/:id', AdminController.update);

module.exports = router;