const Ajv = require('ajv');
const nconf = require('nconf');
const _ = require("lodash");
const ApplicationError = require("@src/errors/ApplicationError");
const validations = require('@app/validations');
const ajv = new Ajv(validations.ajv_options);

const CreateAdminTask = require('./create_admin/create_admin.task');
const ListAdminTask = require('./list_admin/list_admin.task');
const GetAdminTask = require('./get_admin/get_admin.task');
const UpdateAdminTask = require('./update_admin/update_admin.task');
const ResetPasswordTask = require('./reset_password/reset_password.task');
const DeleteAdminTask = require('./delete_admin/delete_admin.task');
const ResetAdminPasswordTask = require('./reset_admin_password/reset_admin_password.task');

class AdminController {

    static index(req, res, next) {
        if (!ajv.validate(ListAdminTask.validation(), req.query)) {
            let errors = ajv.errors;
            return next(
                new ApplicationError({status: 400, name: "ValidationError", message: "Bad request parameters.", info: errors})
            );
        }

        let query = _.pick(req.query, ['status']);
        let options = _.omit(req.query, ['status']);
        return ListAdminTask.list(query,options)
            .then( (admins_data) => {
                return res.status(200).json(
                    new res.app.locals.Success({data: admins_data})
                );
            })
            .catch( (err) => {
                return next(err);
            });
    };

    static show(req, res, next) {
        return GetAdminTask.exec(req.params.id)
            .then( (admin) => {
                return res.status(200).json(
                    new res.app.locals.Success({data: {admin: admin}})
                );
            })
            .catch( (err) => {
                return next(err);
            });
    }

    static create(req, res, next) {
        if (!ajv.validate(CreateAdminTask.validation(), req.body)) {
            let errors = ajv.errors;
            return next(
                new ApplicationError({status: 400, name: "ValidationError", message: "Bad request parameters.", info: errors})
            );
        }
        
        return CreateAdminTask.exec(req.body)
                .then( (email_otp_resp) => {
                    res.app.locals.transporter.sendMail({
                        "to": email_otp_resp.email,
                        "subject": "Admin invitation to Sheenlac",
                        "html": `<p>Sheenlac Guru application one time password (OTP) for setting password: ${email_otp_resp.otp}<br /><a href='${nconf.get('APPLICATION:DOMAIN')}admin/auth/set-password?email=${email_otp_resp.email}' target='_blank'>Set Password</a></p>`
                    }).then( (res) => {
                        console.log(res);
                    }).catch( (err) => {
                        console.log(err);
                    })
                })
                .then( () => {
                    return res.status(200).json(
                        new res.app.locals.Success({data: {"message": "Admin Created."}})
                    );
                })
                .catch( (err) => {
                    return next(err);
                });
    }

    static update(req, res, next) {
        if (!ajv.validate(UpdateAdminTask.validation(), req.body)) {
            let errors = ajv.errors;
            return next(
                new ApplicationError({status: 400, name: "ValidationError", message: "Bad request parameters", info: errors})
            );
        }

        UpdateAdminTask.exec(req.params.id, req.body)
            .then( (response) => {
                return res.status(200).json(
                    new res.app.locals.Success({data: response})
                );
            })
            .catch( (err) => {
                return next(err);
            });
    }

    static reset_password(req, res, next) {
        if (!ajv.validate(ResetPasswordTask.validation(), req.body)) {
            let errors = ajv.errors;
            return next(
                new ApplicationError({status: 400, name: "ValidationError", message: "Bad request parameters.", info: errors})
            );
        }

        return ResetPasswordTask.exec(req.locals.user_id, req.body)
                    .then( (response) => {
                        return res.status(200).json(
                            new res.app.locals.Success({data: response})
                        );
                    })
                    .catch( (err) => {
                        return next(err);
                    });
    }

    static reset_admin_password(req, res, next) {
        if (!ajv.validate(ResetAdminPasswordTask.validation(), req.body)) {
            let errors = ajv.errors;
            return next(
                new ApplicationError({status: 400, name: "ValidationError", message: "Bad request parameters.", info: errors})
            );
        }

        return ResetAdminPasswordTask.exec(req.params.id, req.body)
                    .then( (response) => {
                        return res.status(200).json(
                            new res.app.locals.Success({data: response})
                        );
                    })
                    .catch( (err) => {
                        return next(err);
                    });
    }

    static delete(req, res, next) {
        DeleteAdminTask.exec(req.params.id)
            .then( (response) => {
                return res.status(200).json(
                    new res.app.locals.Success({data: response})
                );
            })
            .catch( (err) => {
                return next(err);
            });
    }


}

module.exports = AdminController;