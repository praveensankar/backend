const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const email_otp_schema = new Schema({
    email: {
        type: String,
        trim: true,
        index: true,
        required: [true, '{PATH} is required']
    },
    otp: {
        type: String,
        trim: true,
        match: [/^\d{4,10}$/, '{VALUE} does not comply with {PATH} format'],
        required: [true, '{PATH} is required']
    },
    is_verified: {
        type: Boolean,
        default: false
    },
    expires_at: {
        type: Date,
        required: [true, '{PATH} is required']
    }
}, {
    timestamps: true
});

module.exports = mongoose.model("emailotp", email_otp_schema);