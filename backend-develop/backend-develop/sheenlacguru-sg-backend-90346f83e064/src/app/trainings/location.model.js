const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const trainingLocationSchema = new Schema(
    {
        name: {
            type: String,
            trim: true,
            required: [true, 'Name is required.']
        },
        address: {
            type: String,
            trim: true,
            required: [true, 'Address is required.']
        },
        locality: {
            type: String,
            trim: true,
            required: [true, 'Locality is required.']
        },
        city: {
            type: String,
            trim: true,
            required: [true, 'City is required.']
        },
        state: {
            type: String,
            trim: true,
            required: [true, 'State is required.']
        },
        pincode: {
            type: String,
            trim: true,
            required: [true, 'Pincode is required.']
        },
        country: {
            type: String,
            trim: true,
            required: [true, 'Country is required.']
        },
        landmark: {
            type: String,
            trim: true
        },
        createdBy: {
            type: Schema.Types.ObjectId,
            ref: 'Admin',
            required: [true, 'Create info is required.']
        },
        updatedBy: {
            type: Schema.Types.ObjectId,
            ref: 'Admin',
            required: [true, 'Update info is required.']
        }
    },
    {
        timestamps: true
    }
);

trainingLocationSchema.set('toJSON', { virtuals: true, getters: true });

module.exports = mongoose.model("TrainingLocation", trainingLocationSchema);