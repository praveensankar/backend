const UserModel = require('../../users/user.model');

class GetTrainerPainterTask {

    static exec(user_id) {
        return UserModel.where('_id').equals(user_id)
            .populate('sp_user')
            .select('-__v')
            .findOne()
            .exec().then(user => {
                return Object.assign(user, {
                    sp_user: {
                        _id: user.sp_user._id,
                        id: user.sp_user._id,
                        sp_name: `${user.sp_user.first_name} ${user.sp_user.last_name}`,
                        sp_id: user.sp_user.empId,
                        sp_designation: user.sp_user.designation ? user.sp_user.designation : '',
                        sp_location: user.sp_user.location ? user.sp_user.location : '',
                        sp_mobile: user.sp_user.mobile_no ? user.sp_user.mobile_no : ''
                    }
                });
            })
    }

    static getAdminPainter(user_id) {
        return UserModel.where('_id').equals(user_id)
            .populate('sp_user')
            .select('-__v')
            .findOne()
            .exec().then(user => {
                return Object.assign(user, {
                    sp_user: {
                        _id: user.sp_user._id,
                        id: user.sp_user._id,
                        sp_name: `${user.sp_user.first_name} ${user.sp_user.last_name}`,
                        sp_id: user.sp_user.empId,
                        sp_designation: user.sp_user.designation ? user.sp_user.designation : '',
                        sp_location: user.sp_user.location ? user.sp_user.location : '',
                        sp_mobile: user.sp_user.mobile_no ? user.sp_user.mobile_no : ''
                    }
                });
            })
    }

}

module.exports = GetTrainerPainterTask;