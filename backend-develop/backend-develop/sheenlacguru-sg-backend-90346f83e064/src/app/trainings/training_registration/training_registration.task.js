const TrainingRegistrationModel = require('../registration.model');
const ApplicationError = require('@src/errors/ApplicationError');

class TrainingRegistrationTask {

    static exec(document) {
        return TrainingRegistrationModel.findOne()
            .where("painter").equals(document.painter)
            .where("training").equals(document.training)
            .exec()
            .then( registration => {
                if (registration)
                    throw new ApplicationError({name: 'TrainingRegistrationError', message: 'You have already registered for this training.'});
                return new TrainingRegistrationModel(document).save();
            })
            .then( registration => {
                if (!registration)
                    throw new ApplicationError({name: 'TrainingRegistrationError', message: 'Registration failed.'});
                return registration;
            });
    }

}

module.exports = TrainingRegistrationTask;