const UserModel = require("../../users/user.model");
const _ = require('lodash');
const ApplicationError = require('@src/errors/ApplicationError');
const UserStatusModel = require('../../users/user_status.model');
class UpdatePainterTask {
    static exec(user_id, document) {
        const FETCH_USER = UserModel.where('_id').equals(user_id)
            .findOne()
            .exec();

        return Promise.all([FETCH_USER])
            .then(([user]) => {
                if (!user)
                    throw new ApplicationError({
                        name: 'PainterError',
                        message: 'Painter not registered.'
                    });
                switch (user.status) {
                    case 'NEW':
                        document = _.defaultsDeep(_.cloneDeep(document), {
                            status: 'PROFILE_ADDED',
                            sp_verified: true,
                            profile_added: true,
                            sp_id: document.empId,
                            sp_verifiedAt: new Date().toISOString(),
                            profile_addedAt: new Date().toISOString(),
                            profile_updatedAt: new Date().toISOString(),
                            updatedAt: new Date().toISOString()
                        });
                        new UserStatusModel({
                            status: "PROFILE_ADDED",
                            remarks: 'ADMIN ADDED RECORD',
                            user: user._id,
                            admin: document.addedById
                        }).save();
                        break;
                    case 'SP_VERIFIED':
                        document = _.defaultsDeep(_.cloneDeep(document), {
                            status: 'PROFILE_ADDED',
                            profile_added: true,
                            profile_addedAt: new Date().toISOString(),
                            profile_updatedAt: new Date().toISOString(),
                            updatedAt: new Date().toISOString()
                        });
                        new UserStatusModel({
                            status: "PROFILE_ADDED",
                            remarks: 'ADMIN ADDED RECORD',
                            user: user._id,
                            admin: document.addedById
                        }).save();
                        break;
                    case 'PROFILE_ADDED':
                        document = _.defaultsDeep(_.cloneDeep(document), {
                            profile_updatedAt: new Date().toISOString(),
                            updatedAt: new Date().toISOString(),
                        });
                        new UserStatusModel({
                            status: "PROFILE_ADDED",
                            remarks: 'ADMIN ADDED RECORD',
                            user: user._id,
                            admin: document.addedById
                        }).save();
                        break;
                    case 'ADDRESS_ADDED':
                        if (!user.profile_added) {
                            document = _.defaultsDeep(_.cloneDeep(document), {
                                profile_added: true,
                                profile_addedAt: new Date().toISOString(),
                                profile_updatedAt: new Date().toISOString(),
                                updatedAt: new Date().toISOString(),
                            });
                            new UserStatusModel({
                                status: "PROFILE_ADDED",
                                remarks: 'ADMIN ADDED RECORD',
                                user: user._id,
                                admin: document.addedById
                            }).save();
                        } else {
                            document = _.defaultsDeep(_.cloneDeep(document), {
                                // profile_added: true,
                                profile_updatedAt: new Date().toISOString(),
                                updatedAt: new Date().toISOString(),
                            });
                        }
                        break;
                    case 'BANK_INFO_ADDED':
                        if (!user.profile_added) {
                            document = _.defaultsDeep(_.cloneDeep(document), {
                                profile_added: true,
                                profile_addedAt: new Date().toISOString(),
                                profile_updatedAt: new Date().toISOString(),
                                updatedAt: new Date().toISOString(),
                            });
                            new UserStatusModel({
                                status: "PROFILE_ADDED",
                                remarks: 'ADMIN ADDED RECORD',
                                user: user._id,
                                admin: document.addedById
                            }).save();
                        } else {
                            document = _.defaultsDeep(_.cloneDeep(document), {
                                // profile_added: true,
                                profile_updatedAt: new Date().toISOString(),
                                updatedAt: new Date().toISOString(),
                            });
                        }
                        break;
                    default:
                        document = _.omit(document, ['status']);
                        document = _.defaultsDeep(_.cloneDeep(document), {
                            status: user.status,
                            profile_updatedAt: new Date().toISOString(),
                            updatedAt: new Date().toISOString()
                        });
                        break;
                }
                return UserModel.where('_id').equals(user_id)
                    .updateOne({}, {
                        omitUndefined: true
                    })
                    .set(document)
                    .exec()
                    .then(op_resp => {
                        if (op_resp.n < 1 || op_resp.nModified < 1)
                            throw new ApplicationError({
                                name: "UpdateProfileError",
                                message: "Profile was not updated."
                            });
                        return {
                            matched: op_resp.n,
                            updated: op_resp.nModified,
                            message: "Profile updated.",
                            isAdded: false
                        };
                    });
            });
    }

    static validation() {
        return {
            "$id": "http://guru.sheenlac.com/schemas/training/update_painter.json",
            "type": "object",
            "properties": {
                "photo": {
                    "type": "string"
                },
                "sp_id": {
                    "type": "string"
                },
                "first_name": {
                    "type": "string"
                },
                "last_name": {
                    "type": "string"
                },
                "alternate_mobile_number": {
                    "type": "string"
                },
                "email": {
                    "type": "string",
                    "format": "email"
                },
                "gender": {
                    "type": "string",
                    "enum": ["MALE", "FEMALE"]
                },
                "birth_date": {
                    "type": "string"
                },
                "pincode": {
                    "type": "string",
                    "minLength": 6
                },
                "dealer_code": {
                    "type": "string"
                },
                "registration_type": {
                    "type": "string",
                    "enum": ['PAINTER', 'DEALER', 'CONTRACTOR']
                },
                "proof_type": {
                    "type": "string",
                    "enum": ['AADHAAR', 'PAN CARD']
                },
                "proof": {
                    "type": "array",
                    "minItems": 2,
                    "uniqueItems": true,
                    "items": {
                        "type": "string"
                    }
                },
                "status": {
                    "type": "string",
                    "enum": ['PROFILE_ADDED', 'UNDER_REVIEW', 'ON_HOLD', 'APPROVED', 'REJECTED', 'BLACKLISTED']
                },
                "painter_access": {
                    "type": "string",
                    "enum": ["No_Scan_Access", "Painter_Under_Contractor", "Individual_Painter", "Contractor"],
                }
            },
            "additionalProperties": false
        };
    }

}

module.exports = UpdatePainterTask;