const Ajv = require('ajv');
const _ = require('lodash');

const ApplicationError = require('@src/errors/ApplicationError');
const validations = require('@app/validations');
const ajv = new Ajv(validations.ajv_options);

const CreateTrainingLocationTask = require('./create_location/create_location.task');
const GetTrainingLocationsTask = require('./get_locations/get_locations.task');
const GetTrainingLocationTask = require('./get_location/get_location.task');
const UpdateTrainingLocationTask = require('./update_location/update_location.task');
const DeleteTrainingLocationTask = require('./delete_location/delete_location.task');
const CreateTrainingCalendarTask = require('./create_calendar/create_calendar.task');
const GetTrainingCalendarsTask = require('./get_calendars/get_calendars.task');
const GetTrainingCalendarTask = require('./get_calendar/get_calendar.task');
const UpdateTrainingCalendarTask = require('./update_calendar/update_calendar.task');
const DeleteTrainingCalendarTask = require('./delete_calendar/delete_calendar.task');
const TrainingRegistrationTask = require('./training_registration/training_registration.task');
const GetTrainingRegistrationsTask = require('./get_training_registrations/get_training_registrations.task');
const UpdateTrainingRegistrationsAttendanceTask = require('./update_training_registrations_attendance/update_training_registrations_attendance.task');

class TrainingController {

    static locationIndex(req, res, next) {
        if (!ajv.validate(GetTrainingLocationsTask.validation(), req.query)) {
            let errors = ajv.errors;
            return next(
                new ApplicationError({status: 400, name: "ValidationError", message: "Bad request parameters.", info: errors})
            );
        }

        return GetTrainingLocationsTask.exec({}, req.query)
            .then(data => {
                return res.status(200).json(
                    new res.app.locals.Success({ data: data })
                );
            })
            .catch(err => next(err));
    }

    static locationShow(req, res, next) {
        return GetTrainingLocationTask.exec(req.params.id)
            .then( location => {
                return res.status(200).json(
                    new res.app.locals.Success({ data: { location }})
                );
            })
            .catch(err => next(err));
    }

    static locationCreate(req, res, next) {
        if (!ajv.validate(CreateTrainingLocationTask.validation(), req.body)) {
            let errors = ajv.errors;
            return next(
                new ApplicationError({status: 400, name: "ValidationError", message: "Bad request parameters.", info: errors})
            );
        }

        let adminId = req.locals["user_id"];
        let document = _.defaultsDeep(_.cloneDeep(req.body), { createdBy: adminId, updatedBy: adminId });
        return CreateTrainingLocationTask.exec(document)
            .then(location => {
                return res.status(200).json(
                    new res.app.locals.Success({ data: location })
                );
            })
            .catch(err => next(err));
    }

    static locationUpdate(req, res, next) {
        if (!ajv.validate(UpdateTrainingLocationTask.validation(), req.body)) {
            let errors = ajv.errors;
            return next(
                new ApplicationError({status: 400, name: "ValidationError", message: "Bad request parameters.", info: errors})
            );
        }

        let adminId = req.locals["user_id"];
        let document = _.defaultsDeep(_.cloneDeep(req.body), { updatedBy: adminId });
        return UpdateTrainingLocationTask.exec(req.params.id, document)
            .then(data => {
                return res.status(200).json(
                    new res.app.locals.Success({ data: data })
                );
            })
            .catch(err => next(err));
    }

    static locationDelete(req, res, next) {
        return DeleteTrainingLocationTask.exec(req.params.id)
            .then(data => {
                return res.status(200).json(
                    new res.app.locals.Success({ data: data })
                );
            })
            .catch(err => next(err));
    }

    static calendarIndex(req, res, next) {
        if (!ajv.validate(GetTrainingCalendarsTask.validation(), req.query)) {
            let errors = ajv.errors;
            return next(
                new ApplicationError({status: 400, name: "ValidationError", message: "Bad request parameters.", info: errors})
            );
        }

        return GetTrainingCalendarsTask.exec({}, req.query)
            .then(data => {
                return res.status(200).json(
                    new res.app.locals.Success({ data: data })
                );
            })
            .catch(err => next(err));
    }

    static calendarShow(req, res, next) {
        return GetTrainingCalendarTask.exec(req.params.id)
            .then( calendar => {
                return res.status(200).json(
                    new res.app.locals.Success({ data: { calendar }})
                );
            })
            .catch(err => next(err));
    }

    static calendarCreate(req, res, next) {
        if (req.file) req.body["agenda"] = req.file.key;
        if (!ajv.validate(CreateTrainingCalendarTask.validation(), req.body)) {
            let errors = ajv.errors;
            return next(
                new ApplicationError({status: 400, name: "ValidationError", message: "Bad request parameters.", info: errors})
            );
        }

        let adminId = req.locals["user_id"];
        let document = _.defaultsDeep(_.cloneDeep(req.body), { createdBy: adminId, updatedBy: adminId });
        return CreateTrainingCalendarTask.exec(document)
            .then(calendar => {
                return res.status(200).json(
                    new res.app.locals.Success({ data: calendar })
                );
            })
            .catch(err => next(err));
    }

    static calendarUpdate(req, res, next) {
        if (req.file) req.body["agenda"] = req.file.key;
        if (!ajv.validate(UpdateTrainingCalendarTask.validation(), req.body)) {
            let errors = ajv.errors;
            return next(
                new ApplicationError({status: 400, name: "ValidationError", message: "Bad request parameters.", info: errors})
            );
        }

        let adminId = req.locals["user_id"];
        let document = _.defaultsDeep(_.cloneDeep(req.body), { updatedBy: adminId });
        return UpdateTrainingCalendarTask.exec(req.params.id, document)
            .then(data => {
                return res.status(200).json(
                    new res.app.locals.Success({ data: data })
                );
            })
            .catch(err => next(err));
    }

    static calendarDelete(req, res, next) {
        return DeleteTrainingCalendarTask.exec(req.params.id)
            .then(data => {
                return res.status(200).json(
                    new res.app.locals.Success({ data: data })
                );
            })
            .catch(err => next(err));
    }

    static trainingRegistration(req, res, next) {
        let document = _.defaultsDeep({ training: req.params["id"] }, { painter: req.locals["user_id"] });
        return TrainingRegistrationTask.exec(document)
            .then(registration => {
                return res.status(200).json(
                    new res.app.locals.Success({ data: registration })
                );
            })
            .catch(err => next(err));
    }

    static updateTrainingRegistrationsAttendance(req, res, next) {
        if (!ajv.validate(UpdateTrainingRegistrationsAttendanceTask.validation(), req.body)) {
            let errors = ajv.errors;
            return next(
                new ApplicationError({status: 400, name: "ValidationError", message: "Bad request parameters.", info: errors})
            );
        }

        return UpdateTrainingRegistrationsAttendanceTask.exec(req.params["id"], req.body)
            .then(data => {
                return res.status(200).json(
                    new res.app.locals.Success({ data: data })
                );
            })
            .catch(err => next(err));
    }

    static trainingRegistrationIndex(req, res, next) {
        if (!ajv.validate(GetTrainingRegistrationsTask.validation(), req.query)) {
            let errors = ajv.errors;
            return next(
                new ApplicationError({status: 400, name: "ValidationError", message: "Bad request parameters.", info: errors})
            );
        }

        return GetTrainingRegistrationsTask.exec(req.params["id"], req.query)
            .then(data => {
                return res.status(200).json(
                    new res.app.locals.Success({ data: data })
                );
            })
            .catch(err => next(err));
    }

}

module.exports = TrainingController;