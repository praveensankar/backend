const TrainingLocationModel = require('../location.model');
const ApplicationError = require('@src/errors/ApplicationError');

class DeleteTrainingLocationTask {

    static exec(locationId) {
        return TrainingLocationModel.deleteOne()
            .where('_id').equals(locationId)
            .exec().then(op_resp => {
                if (op_resp.ok != 1 || op_resp.deletedCount < 1)
                    throw new ApplicationError({name: 'DeleteLocationError', message: 'Location not deleted.'});

                return { deleted: op_resp.deletedCount, message: 'Training location deleted.'};
            });
    }

}

module.exports = DeleteTrainingLocationTask;