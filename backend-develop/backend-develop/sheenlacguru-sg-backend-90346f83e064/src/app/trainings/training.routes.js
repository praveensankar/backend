const express = require('express');
const nconf = require('nconf');
const router = express.Router();

const Token = require('@src/middleware/token');
const Authorization = require('@src/middleware/authorization');
const Uploader = require('@src/middleware/uploader.middleware');
const TrainingController = require('./training.controller');

router.use(Token.verify({ for: 'admin' }))

router.post('/locations/', Authorization.authorize(["training"]), Uploader({}).none(),TrainingController.locationCreate);
router.get('/locations/', Authorization.authorize(["training", "painter", "contractor"]), TrainingController.locationIndex);
router.get('/locations/:id', Authorization.authorize(["training", "painter", "contractor"]), TrainingController.locationShow);
router.post('/locations/:id', Authorization.authorize(["training"]), Uploader({}).none(), TrainingController.locationUpdate);
router.delete('/locations/:id', Authorization.authorize(["training"]), TrainingController.locationDelete);

router.post('/', Authorization.authorize(["training"]), Uploader({ dest: nconf.get("STORAGE:AGENDAS") }).single("agenda"), TrainingController.calendarCreate);
router.get('/', Authorization.authorize(["training", "painter", "contractor"]), TrainingController.calendarIndex);
router.get('/:id', Authorization.authorize(["training", "painter", "contractor"]), TrainingController.calendarShow);
router.post('/:id', Authorization.authorize(["training"]), Uploader({ dest: nconf.get("STORAGE:AGENDAS") }).single("agenda"), TrainingController.calendarUpdate);
router.delete('/:id', Authorization.authorize(["training"]), TrainingController.calendarDelete);

router.get('/:id/registrations', Authorization.authorize(["training"]), TrainingController.trainingRegistrationIndex);
router.patch('/:id/registrations', Authorization.authorize(["training"]), TrainingController.updateTrainingRegistrationsAttendance);
router.post('/:id/register', Authorization.authorize(["painter", "contractor"]), TrainingController.trainingRegistration);


module.exports = router;