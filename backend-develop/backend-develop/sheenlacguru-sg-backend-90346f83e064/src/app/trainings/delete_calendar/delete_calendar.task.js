const TrainingCalendarModel = require('../calendar.model');
const ApplicationError = require('@src/errors/ApplicationError');

class DeleteTrainingCalendarTask {

    static exec(calendarId) {
        return TrainingCalendarModel.deleteOne()
            .where('_id').equals(calendarId)
            .exec().then( op_resp => {
                if (op_resp.ok != 1 || op_resp.deletedCount < 1)
                    throw new ApplicationError({name: 'DeleteCalendarError', message: 'Calendar not deleted.'});
                return { deleted: op_resp.deletedCount, message: 'Calendar deleted.' };
            });
    }

}

module.exports = DeleteTrainingCalendarTask;