const TrainingRegistrationModel = require('../registration.model');

class GetTrainingRegistrationTask {

    static exec(trainingId, options = {}) {
        const skip = options.page > 0 ? (options.page - 1) * options.size : 0;
        let GetTrainingRegistrationsQuery = TrainingRegistrationModel.find().where("training").equals(trainingId).select('-__v').populate('painter').skip(skip);
        GetTrainingRegistrationsQuery = (options.size > 0) ? GetTrainingRegistrationsQuery.limit(options.size) : GetTrainingRegistrationsQuery;
        return Promise.all([
            GetTrainingRegistrationsQuery.exec(),
            TrainingRegistrationModel.find().where("training").equals(trainingId).countDocuments()
        ]).then(([registrations, count]) => {
            return {
                items: registrations,
                totalCount: count
            };
        })
    }

    static validation() {
        return {
            "$id": "http://guru.sheenlac.com/schemas/get_training_registrations_query_params.json",
            "$async": true,
            "type": "object",
            "properties": {
                "page": { "type": "integer", "default": 0 },
                "size": { "type": "integer", "default": 0 },
            },
            "required": [],
            "additionalProperties": false
        };
    }
}

module.exports = GetTrainingRegistrationTask;