const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const trainingRegistrationSchema = new Schema({
    painter: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: [true, 'Painter is required.']
    },
    training: {
        type: Schema.Types.ObjectId,
        ref: 'TrainingCalendar',
        required: [true, 'Training is required.']
    },
    attended: {
        type: Boolean,
        default: false
    }
},
{
    timestamps: true
});

trainingRegistrationSchema.set('toJSON', { virtuals: true, getters: true });

module.exports = mongoose.model("TrainingRegistration", trainingRegistrationSchema);