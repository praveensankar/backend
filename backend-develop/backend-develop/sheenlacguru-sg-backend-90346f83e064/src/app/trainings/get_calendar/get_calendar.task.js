const TrainingCalendarModel = require('../calendar.model');

class GetTrainingCalendarTask {

    static exec(calendarId) {
        return TrainingCalendarModel
            .where('_id').equals(calendarId)
            .populate('location')
            .select('-__v')
            .findOne().exec();
    }

}

module.exports = GetTrainingCalendarTask;