const express = require('express');
const nconf = require('nconf');
const router = express.Router();

const Token = require('@src/middleware/token');
const Authorization = require('@src/middleware/authorization');
const Uploader = require('@src/middleware/uploader.middleware');
const TrainingController = require('./training.controller');
const TrainerController = require('./trainer.controller');
const SalesRepController = require('../salesrep/salesrep.controller');

router.use(Token.verify({ for: 'admin' }))

router.post('/locations/', Authorization.authorize(["trainingManagement"]), Uploader({}).none(),TrainingController.locationCreate);
router.get('/locations/', Authorization.authorize(["trainingManagement", "painter", "contractor"]), TrainingController.locationIndex);
router.get('/locations/:id', Authorization.authorize(["trainingManagement", "painter", "contractor"]), TrainingController.locationShow);
router.post('/locations/:id', Authorization.authorize(["trainingManagement"]), Uploader({}).none(), TrainingController.locationUpdate);
router.delete('/locations/:id', Authorization.authorize(["trainingManagement"]), TrainingController.locationDelete);

router.post('/', Authorization.authorize(["trainingManagement"]), Uploader({ dest: nconf.get("STORAGE:AGENDAS") }).single("agenda"), TrainingController.calendarCreate);
router.get('/', Authorization.authorize(["trainingManagement", "painter", "contractor"]), TrainingController.calendarIndex);
router.get('/:id', Authorization.authorize(["trainingManagement", "painter", "contractor"]), TrainingController.calendarShow);
router.post('/:id', Authorization.authorize(["trainingManagement"]), Uploader({ dest: nconf.get("STORAGE:AGENDAS") }).single("agenda"), TrainingController.calendarUpdate);
router.delete('/:id', Authorization.authorize(["trainingManagement"]), TrainingController.calendarDelete);

router.get('/:id/registrations', Authorization.authorize(["trainingManagement"]), TrainingController.trainingRegistrationIndex);
router.patch('/:id/registrations', Authorization.authorize(["trainingManagement"]), TrainingController.updateTrainingRegistrationsAttendance);
router.post('/:id/register', Authorization.authorize(["painter", "contractor"]), TrainingController.trainingRegistration);

// Trainer API
router.get('/trainer/painter',Authorization.authorize(["trainingManagement"]),TrainerController.getTrainerPainters);
router.get('/trainer/painter/:id',Authorization.authorize(["trainingManagement"]),TrainerController.getTrainerPainter);
router.post('/trainer/painter', Authorization.authorize(["trainingManagement"]), Uploader({
    dest: nconf.get("STORAGE:KYC_DOCS")
}).fields([{
    name: 'photo',
    maxCount: 1
}, {
    name: 'proof',
    maxCount: 5
}]), TrainerController.createPainterProfile);
router.patch('/trainer/painter/:id', Authorization.authorize([ "trainingManagement","canAddPainter"]), Uploader({
    dest: nconf.get("STORAGE:KYC_DOCS")
}).fields([{
    name: 'photo',
    maxCount: 1
}, {
    name: 'proof',
    maxCount: 5
}]), TrainerController.updatePainter, TrainerController.makeUnderReview);
router.post('/trainer/painter/:id/address', Authorization.authorize([ "trainingManagement","canAddPainter"]), TrainerController.savePainterAddress, TrainerController.makeUnderReview);

router.post('/trainer/painter/:id/bank_details', Authorization.authorize(["trainingManagement","canAddPainter"]), Uploader({dest: nconf.get("STORAGE:KYC_DOCS")}).single('passbookPhoto'), TrainerController.savePainterBankDetails,TrainerController.makeUnderReview);
router.get('/trainer/painter/identity-check/:mobile_number', Authorization.authorize(["trainingManagement", "canAddPainter"]), TrainerController.check_mobile_number);

// Added By Admin for Trainer
router.get('/admin/painter/', Authorization.authorize([ "trainingManagement","canAddPainter"]), TrainerController.getAdminPainters);
router.post('/admin/painter/', Authorization.authorize([ "trainingManagement","canAddPainter"]), Uploader({
    dest: nconf.get("STORAGE:KYC_DOCS")
}).fields([{
    name: 'photo',
    maxCount: 1
}, {
    name: 'proof',
    maxCount: 5
    }]), TrainerController.createPainterProfile);
router.patch('/admin/painter/:id', Authorization.authorize(["trainingManagement", "canAddPainter"]), Uploader({
    dest: nconf.get("STORAGE:KYC_DOCS")
}).fields([{
    name: 'photo',
    maxCount: 1
}, {
    name: 'proof',
    maxCount: 5
}]), TrainerController.updatePainter, TrainerController.makeUnderReview);
router.post('/admin/painter/:id/address', Authorization.authorize(["trainingManagement", "canAddPainter", "dropoffPainterManagement"]), TrainerController.savePainterAddress, TrainerController.makeUnderReview);

router.post('/admin/painter/:id/bank_details', Authorization.authorize(["trainingManagement", "canAddPainter"]), Uploader({
    dest: nconf.get("STORAGE:KYC_DOCS")
}).single('passbookPhoto'), TrainerController.savePainterBankDetails, TrainerController.makeUnderReview);
router.get('/admin/painter/:id', Authorization.authorize(["trainingManagement", "canAddPainter", "dropoffPainterManagement"]), TrainerController.getAdminPainter);
router.get('/admin/painter/empId/:id', Authorization.authorize(["trainingManagement", "canAddPainter", "dropoffPainterManagement","painterRemapping"]), SalesRepController.fetchSalesRep);
router.get('/admin/painter/identity-check/:mobile_number', Authorization.authorize(["trainingManagement", "canAddPainter", "dropoffPainterManagement"]), TrainerController.check_mobile_number);

module.exports = router;