const TrainingCalendarModel = require('../calendar.model');
const ApplicationError = require('@src/errors/ApplicationError');

class UpdateTrainingCalendarTask {

    static exec(calendarId, calendar) {
        return TrainingCalendarModel
            .where('_id').equals(calendarId)
            .updateOne({}, {omitUndefined: true})
            .set(calendar).exec()
            .then( op_resp => {
                if (op_resp.n < 1 || op_resp.nModified < 1)
                    throw new ApplicationError({name: 'UpdateCalendarError', message: 'Calendar not updated.'});

                return { matched: op_resp.n, updated: op_resp.nModified, message: 'Calendar updated.' };
            });
    }

    static validation() {
        return {
            "$id": "http://guru.sheenlac.com/schemas/update_training_calendar.json",
            "type": "object",
            "properties": {
                "title": { "type": "string" },
                "description": { "type": "string" },
                "agenda": { "type": "string" },
                "start": { "type": "string", "format": "date-time" },
                "startTime": { "type": "string" },
                "duration": { "type": "string" },
                "location": { "type": "string" }
            },
            "additionalProperties": false
        };
    }

}

module.exports = UpdateTrainingCalendarTask;