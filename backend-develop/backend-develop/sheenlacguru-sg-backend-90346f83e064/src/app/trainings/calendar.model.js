const mongoose = require('mongoose');
const nconf = require('nconf');

const Schema = mongoose.Schema;

const SERVER_HOST = nconf.get('APPLICATION:S3');

const trainingCalendarSchema = new Schema(
    {
        title: {
            type: String,
            trim: true,
            required: [true, 'Title is required.']
        },
        description: {
            type: String,
            trim: true,
            required: [true, 'Description is required.']
        },
        agenda: {
            type: String,
            trim: true,
            get: (value) => (value) ? `${SERVER_HOST}${value}` : ''
        },
        start: {
            type: Date,
            required: [true, 'Start date is required.']
        },
        startTime: {
            type: String,
            required: [true, 'Start time is required.']
        },
        duration: {
            type: String,
            required: [true, 'Duration is required.']
        },
        location: {
            type: Schema.Types.ObjectId,
            ref: 'TrainingLocation',
            required: [true, 'Location is required.']
        },
        createdBy: {
            type: Schema.Types.ObjectId,
            ref: 'Admin',
            required: [true, 'Create info is required.']
        },
        updatedBy: {
            type: Schema.Types.ObjectId,
            ref: 'Admin',
            required: [true, 'Update info is required.']
        }
    },
    {
        timestamps: true
    }
);

trainingCalendarSchema.set('toJSON', { virtuals: true, getters: true });

module.exports = mongoose.model("TrainingCalendar", trainingCalendarSchema);