const TrainingLocationModel = require('../location.model');
const ApplicationError = require('@src/errors/ApplicationError');

class CreateTrainingLocationTask {

    static exec(location) {
        return new TrainingLocationModel(location).save()
            .then(location => {
                if (!location)
                    throw new ApplicationError({name: 'CreateLocationError', message: 'Location not created.'});

                return location;
            })
    }

    static validation() {
        return {
            "$id": "http://guru.sheenlac.com/schemas/create_training_location.json",
            "type": "object",
            "properties": {
                "name": { "type": "string" },
                "address": { "type": "string" },
                "locality": { "type": "string" },
                "city": { "type": "string" },
                "state": { "type": "string" },
                "pincode": { "type": "string" },
                "country": { "type": "string", "enum": ["India"] },
                "landmark": { "type": "string" }
            },
            "required": ["name", "address", "locality", "city", "state", "pincode", "country"],
            "additionalProperties": false
        };
    }

}

module.exports = CreateTrainingLocationTask;