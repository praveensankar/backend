const TrainingRegistrationModel = require('../registration.model');
const ApplicationError = require('@src/errors/ApplicationError');

class UpdateTrainingRegistrationsAttendanceTask {

    static exec(trainingId = '', document = {}) {
        return TrainingRegistrationModel.updateMany()
            .where("_id").in(document.registrations)
            .where("training").equals(trainingId)
            .set("attended", document.attendance)
            .exec()
            .then(op_resp => {
                if (op_resp.n < 1 || op_resp.nModified < 1)
                    throw new ApplicationError({ name: 'UpdateTrainingRegistrationsAttendanceError', message: 'Training registrations attendance not updated.' });

                return { matched: op_resp.n, updated: op_resp.nModified, message: 'Training registrations attendance updated.' };
            });
    }

    static validation() {
        return {
            "$id": "http://guru.sheenlac.com/schemas/update_training_registrations_attendance.json",
            "type": "object",
            "properties": {
                "registrations": {
                    "type": "array",
                    "minItems": 1,
                    "uniqueItems": true,
                    "items": { "type": "string", "minLength": 24, "maxLength": 24 }
                },
                "attendance": {
                    "type": "boolean"
                }
            },
            "required": ["registrations", "attendance"],
            "additionalProperties": false
        };
    }

}

module.exports = UpdateTrainingRegistrationsAttendanceTask;