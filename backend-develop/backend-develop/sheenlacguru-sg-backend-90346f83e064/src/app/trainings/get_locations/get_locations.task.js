const Promise = require('bluebird');
const TrainingLocationModel = require('../location.model');

class GetTrainingLocationsTask {

    static exec(query = {}, options = {}) {
        const skip = options.page > 0 ? (options.page - 1) * options.size : 0;
        let GetTrainingLocationsQuery = TrainingLocationModel.find(query).select('-__v').skip(skip);
        GetTrainingLocationsQuery = (options.size > 0) ? GetTrainingLocationsQuery.limit(options.size) : GetTrainingLocationsQuery;
        GetTrainingLocationsQuery = (options.location) ? GetTrainingLocationsQuery.where('name').equals(options.location) : GetTrainingLocationsQuery;
        GetTrainingLocationsQuery = (options.city) ? GetTrainingLocationsQuery.where('city').equals(options.city) : GetTrainingLocationsQuery;
        GetTrainingLocationsQuery = (options.state) ? GetTrainingLocationsQuery.where('state').equals(options.state) : GetTrainingLocationsQuery;
        return Promise.all([
            GetTrainingLocationsQuery.exec(),
            TrainingLocationModel.estimatedDocumentCount().exec()
        ]).then(([locations, count]) => {
            return {
                items: locations,
                totalCount: count
            };
        });
    }

    static validation() {
        return {
            "$id": "http://guru.sheenlac.com/schemas/list_locations_query_params.json",
            "$async": true,
            "type": "object",
            "properties": {
                "page": { "type": "integer", "default": 0 },
                "size": { "type": "integer", "default": 0 },
                "location": { "type": "string" },
                "city": { "type": "string" },
                "state": { "type": "string" }
            },
            "required": [],
            "additionalProperties": false
        };
    }

}

module.exports = GetTrainingLocationsTask;