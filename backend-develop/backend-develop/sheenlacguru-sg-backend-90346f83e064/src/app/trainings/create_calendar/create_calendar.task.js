const TrainingCalendarModel = require('../calendar.model');
const ApplicationError = require('@src/errors/ApplicationError');

class CreateTrainingCalendarTask {

    static exec(calendar) {
        return new TrainingCalendarModel(calendar).save()
            .then(calendar => {
                if (!calendar)
                    throw new ApplicationError({ name: 'CreateCalendarError', message: 'Calendar not created.' });

                return calendar;
            });
    }

    static validation() {
        return {
            "$id": "http://guru.sheenlac.com/schemas/create_training_calendar.json",
            "type": "object",
            "properties": {
                "title": { "type": "string" },
                "description": { "type": "string" },
                "agenda": { "type": "string" },
                "start": { "type": "string", "format": "date-time" },
                "startTime": { "type": "string" },
                "duration": { "type": "string" },
                "location": { "type": "string" }
            },
            "required": ["title", "description", "start", "startTime", "duration", "location"],
            "additionalProperties": false
        };
    }

}

module.exports = CreateTrainingCalendarTask;