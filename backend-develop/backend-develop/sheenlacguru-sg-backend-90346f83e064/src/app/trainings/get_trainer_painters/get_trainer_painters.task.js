const UserModel = require('../../users/user.model');
const Promise = require('bluebird');
const _ = require("lodash");

class GetTrainerPaintersTask {

    static async exec(trainerId, options = {}) {

        const skip = options.page > 0 ? (options.page - 1) * options.size : 0;
        let painters = [];
        let painterCount = 0;

        // search when search key exist in options
        if (options.searchText !== "") {
            /* Start of Search */

            /* ------- Search with exact match using text based search */
            const paintersWithExactMatch = await UserModel.find({
                $text: {
                    $search: options.searchText
                }
            }).select('first_name bp_number mobile_number total used balance underReview last_name status milestone remark createdAt registeredAt updatedAt').where('sp_verified').equals(true).where('sp_id').equals(trainerId);
            /* -------- Search with start (first_name,last_name,mobile_number,email,empId) */
            const rgxForStartMatch = (pattern) => new RegExp(`^${pattern}`); // pattern for matching start of searchText
            const searchTextForStartMatch = rgxForStartMatch(options.searchText);
            const paintersWithStartMatch = await UserModel.find({
                $or: [{
                        first_name: {
                            $regex: searchTextForStartMatch,
                            $options: 'i'
                        }
                    },
                    {
                        last_name: {
                            $regex: searchTextForStartMatch,
                            $options: 'i'
                        }
                    },
                    {
                        mobile_number: {
                            $regex: searchTextForStartMatch,
                            $options: 'i'
                        }
                    },
                    {
                        email: {
                            $regex: searchTextForStartMatch,
                            $options: 'i'
                        }
                    }
                ]
            }).select('first_name bp_number mobile_number total used balance underReview last_name status milestone remark createdAt registeredAt updatedAt').where('sp_verified').equals(true).where('sp_id').equals(trainerId);

            /* -------- Fuzzy search with (first_name,last_name,mobile_number,email) */
            const rgxForFuzzyMatch = (pattern) => new RegExp(`${pattern}`);
            const searchTextForFuzzyMatch = rgxForFuzzyMatch(options.searchText);
            const paintersWithFuzzyMatch = await UserModel.find({
                $or: [{
                        first_name: {
                            $regex: searchTextForFuzzyMatch,
                            $options: 'i'
                        }
                    },
                    {
                        last_name: {
                            $regex: searchTextForFuzzyMatch,
                            $options: 'i'
                        }
                    },
                    {
                        mobile_number: {
                            $regex: searchTextForFuzzyMatch,
                            $options: 'i'
                        }
                    },
                    {
                        email: {
                            $regex: searchTextForFuzzyMatch,
                            $options: 'i'
                        }
                    }
                ]
            }).select('first_name bp_number mobile_number total used balance underReview last_name status milestone remark createdAt registeredAt updatedAt').where('sp_verified').equals(true).where('sp_id').equals(trainerId);

            const adminlist = _.concat(paintersWithExactMatch, paintersWithStartMatch, paintersWithFuzzyMatch);

            /* generate list of distinct painters if painters got repeated */
            const map = new Map();
            for (const item of adminlist) {
                if (!map.has(item.id)) {
                    map.set(item.id, true); // set any value to Map
                    painters.push(item);
                }
            }
            painterCount = painters.length;
            // painters = options.size > 0 ? painters.slice(skip).slice(0, options.size) : painters;

            /* End of Search */
        } else {
            /* fetch painters if no search key is present on options */
            const FetchAddedByAdminPainters = UserModel.find({}).select('first_name bp_number mobile_number total used balance underReview last_name status milestone remark createdAt registeredAt updatedAt').where('sp_verified').equals(true).where('sp_id').equals(trainerId);
            if (options.size > 0) FetchAddedByAdminPainters.limit(options.size).skip(skip);
            painters = FetchAddedByAdminPainters.populate('sp_user').sort({
                createdAt: -1
            }).exec();

            const CountQuery = UserModel.find().where('sp_verified').equals(true).where('sp_id').equals(trainerId);
            if (options.size > 0) CountQuery.limit(options.size).skip(skip);
            painterCount = CountQuery.countDocuments();
        }

        return Promise.all([
            painters,
            painterCount
        ]).then(([painters, count]) => {
            return {
                items: painters,
                totalCount: count
            }
        });
    }

    static async getPainterByAdmin(adminId, options = {}) {

        const skip = options.page > 0 ? (options.page - 1) * options.size : 0;
        let painters = [];
        let painterCount = 0;

        // search when search key exist in options
        if (options.searchText !== "") {
            /* Start of Search */

            /* ------- Search with exact match using text based search */
            const paintersWithExactMatch = await UserModel.find({
                $text: {
                    $search: options.searchText
                }
            }).where('addedBy').equals(adminId).populate('sp_user');
            /* -------- Search with start (first_name,last_name,mobile_number,email,empId) */
            const rgxForStartMatch = (pattern) => new RegExp(`^${pattern}`); // pattern for matching start of searchText
            const searchTextForStartMatch = rgxForStartMatch(options.searchText);
            const paintersWithStartMatch = await UserModel.find({
                $or: [{
                        first_name: {
                            $regex: searchTextForStartMatch,
                            $options: 'i'
                        }
                    },
                    {
                        last_name: {
                            $regex: searchTextForStartMatch,
                            $options: 'i'
                        }
                    },
                    {
                        mobile_number: {
                            $regex: searchTextForStartMatch,
                            $options: 'i'
                        }
                    },
                    {
                        email: {
                            $regex: searchTextForStartMatch,
                            $options: 'i'
                        }
                    }
                ]
            }).where('addedBy').equals(adminId).populate('sp_user');

            /* -------- Fuzzy search with (first_name,last_name,mobile_number,email) */
            const rgxForFuzzyMatch = (pattern) => new RegExp(`${pattern}`);
            const searchTextForFuzzyMatch = rgxForFuzzyMatch(options.searchText);
            const paintersWithFuzzyMatch = await UserModel.find({
                $or: [{
                        first_name: {
                            $regex: searchTextForFuzzyMatch,
                            $options: 'i'
                        }
                    },
                    {
                        last_name: {
                            $regex: searchTextForFuzzyMatch,
                            $options: 'i'
                        }
                    },
                    {
                        mobile_number: {
                            $regex: searchTextForFuzzyMatch,
                            $options: 'i'
                        }
                    },
                    {
                        email: {
                            $regex: searchTextForFuzzyMatch,
                            $options: 'i'
                        }
                    }
                ]
            }).where('addedBy').equals(adminId).populate('sp_user');

            const adminlist = _.concat(paintersWithExactMatch, paintersWithStartMatch, paintersWithFuzzyMatch);

            /* generate list of distinct painters if painters got repeated */
            painters = _.concat([], _.uniqBy(adminlist, 'id'));
            painterCount = painters.length;
            painters = options.size > 0 ? painters.slice(skip).slice(0, options.size) : painters;

            /* End of Search */
        } else {
            /* fetch painters if no search key is present on options */
            const FetchAddedByAdminPainters = UserModel.find({}).where('addedBy').equals(adminId);
            if (options.size > 0) FetchAddedByAdminPainters.limit(options.size).skip(skip);
            painters = FetchAddedByAdminPainters.populate('sp_user').sort({
                createdAt: -1
            }).exec();

            const CountQuery = UserModel.find().where('addedBy').equals(adminId);
            if (options.size > 0) CountQuery.limit(options.size).skip(skip);
            painterCount = CountQuery.countDocuments();
        }

        // const FetchAddedByAdminPainters = UserModel.find({}).where('addedBy').equals(adminId);
        // if (options.size > 0) FetchAddedByAdminPainters.limit(options.size).skip(skip);

        // const CountQuery = UserModel.find().where('addedBy').equals(adminId);
        // if (options.size > 0) CountQuery.limit(options.size).skip(skip);


        return Promise.all([
            painters,
            painterCount
        ]).then(([painters, count]) => {
            return {
                items: painters,
                totalCount: count
            }
        });

    }

    static validation() {
        return {
            "$id": "http://guru.sheenlac.com/schemas/get_trainer_painters.json",
            "type": "object",
            "properties": {
                "page": {
                    "type": "integer",
                    "default": 0
                },
                "size": {
                    "type": "integer",
                    "default": 0
                },
                "searchText": {
                    "type": "string"
                }
            },
            "required": [],
            "additionalProperties": false
        };
    }
}

module.exports = GetTrainerPaintersTask;