const TrainingLocationModel = require('../location.model');
const ApplicationError = require('@src/errors/ApplicationError');

class UpdateTrainingLocationTask {

    static exec(locationId, location) {
        return TrainingLocationModel
            .where('_id').equals(locationId)
            .updateOne({}, {omitUndefoned: true})
            .set(location).exec()
            .then( op_resp => {
                if (op_resp.n < 1 || op_resp.nModified < 1)
                    throw new ApplicationError({name: 'UpdateLocationError', message: 'Location not updated.'});

                return { matched: op_resp.n, updated: op_resp.nModified, message: 'Location updated.' };
            });
    }

    static validation() {
        return {
            "$id": "http://guru.sheenlac.com/schemas/update_training_location.json",
            "type": "object",
            "properties": {
                "name": { "type": "string" },
                "address": { "type": "string" },
                "locality": { "type": "string" },
                "city": { "type": "string" },
                "state": { "type": "string" },
                "pincode": { "type": "string" },
                "country": { "type": "string", "enum": ["India"] },
                "landmark": { "type": "string" }
            },
            "additionalProperties": false
        };
    }

}

module.exports = UpdateTrainingLocationTask;