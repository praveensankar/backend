const UserModel = require("../../users/user.model");
const _ = require('lodash');
const ApplicationError = require('@src/errors/ApplicationError');
const UserStatusModel = require('../../users/user_status.model');
class CreatePainterProfileTask {

    static exec(document) {
        console.log(document);
        const FETCH_USER = UserModel.where('mobile_number').equals(document.mobile_number)
            .findOne()
            .exec();
        
        return Promise.all([FETCH_USER])
            .then(([user]) => {
                console.log(user)
                if (user)
                    throw new ApplicationError({
                        name: 'CreatePainterError',
                        message: 'Painter already registered.'
                    });
                
                document = _.defaultsDeep(_.cloneDeep(document), {
                    status: "PROFILE_ADDED",
                    sp_id: document.empId,
                    sp_verified: true,
                    profile_added: true,
                    addedBy: document.addedById,
                    sp_verifiedAt: new Date().toISOString(),
                    profile_addedAt: new Date().toISOString(),
                    profile_updatedAt: new Date().toISOString(),
                    updatedAt: new Date().toISOString()
                });

                return new UserModel(document)
                    .save()
                    .then(op_resp => {
                        if (op_resp.n < 1 || op_resp.nModified < 1)
                            throw new ApplicationError({
                                name: "CreateProfileError",
                                message: "Profile was not created."
                            });
                            const FETCH_USER = UserModel.where('mobile_number').equals(document.mobile_number)
                                .findOne()
                                .exec();
                            return Promise.all([FETCH_USER])
                                .then(([user]) => {
                                    new UserStatusModel({
                                        status: "PROFILE_ADDED",
                                        remarks: 'ADMIN ADDED RECORD',
                                        user: user._id,
                                        admin: document.addedById
                                    }).save();
                                    return {
                                        user : user,    
                                        message: "Profile created."
                                    };
                                })
                    });
            });
    }

    static validation() {
        return {
            "$id": "http://guru.sheenlac.com/schemas/trainings/create_painter_profile.json",
            "type": "object",
            "properties": {
                "sp_id": {
                    "type" : "string"
                },
                "photo": {
                    "type": "string"
                },
                "first_name": {
                    "type": "string"
                },
                "last_name": {
                    "type": "string"
                },
                "mobile_number": {
                    "type": "string"
                },
                "alternate_mobile_number": {
                    "type": "string"
                },
                "email": {
                    "type": "string",
                    "format": "email"
                },
                "gender": {
                    "type": "string",
                    "enum": ["MALE", "FEMALE"]
                },
                "birth_date": {
                    "type": "string",
                    "format": "date"
                },
                "pincode": {
                    "type": "string",
                    "minLength": 6
                },
                "dealer_code": {
                    "type": "string"
                },
                "registration_type": {
                    "type": "string",
                    "enum": ['PAINTER', 'DEALER', 'CONTRACTOR']
                },
                "proof_type": {
                    "type": "string",
                    "enum": ['AADHAAR', 'PAN CARD']
                },
                "proof": {
                    "type": "array",
                    "minItems": 2,
                    "uniqueItems": true,
                    "items": {
                        "type": "string"
                    }
                }
            },
            "required": ["photo", "first_name","mobile_number","last_name", "gender", "birth_date", "pincode", "registration_type", "proof_type", "proof"],
            "additionalProperties": false
        };
    }
}

module.exports = CreatePainterProfileTask;