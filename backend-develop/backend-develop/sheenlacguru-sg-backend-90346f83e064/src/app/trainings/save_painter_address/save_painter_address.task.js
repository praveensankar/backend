const UserModel = require("../../users/user.model");
const _ = require("lodash");
const ApplicationError = require('@src/errors/ApplicationError');
const UserStatusModel = require('../../users/user_status.model');
class SavePainterAddress {

    static exec(document = {}) {
        const FETCH_USER = UserModel.where('_id').equals(document.user)
            .findOne()
            .exec();

        return Promise.all([FETCH_USER])
            .then(([user]) => {
                let isAdded = false;
                if (!user)
                    throw new ApplicationError({
                        name: 'PainterError',
                        message: 'Painter not registered.'
                    });

                document = _.defaultsDeep(_.cloneDeep(document), {
                    address_updatedAt: new Date().toISOString(),
                    updatedAt: new Date().toISOString()
                });

                if (!_.hasIn(user, 'address')) {
                    isAdded = true;
                    document = _.defaultsDeep(_.cloneDeep(document), {
                        status: "ADDRESS_ADDED",
                        address_added: true,
                        address_addedAt: new Date().toISOString(),
                        address_updatedAt: new Date().toISOString(),
                        updatedAt: new Date().toISOString()
                    });
                    new UserStatusModel({
                        status: "ADDRESS_ADDED",
                        remarks: 'ADMIN ADDED RECORD',
                        user: document.user,
                        admin: document.addedById
                    }).save();
                }

                if (!user.address_added) {
                    // document = _.omit(document, ["status", "under_reviewAt"]);
                    isAdded = true;
                    document = _.defaultsDeep(_.cloneDeep(document), {
                        status: "ADDRESS_ADDED",
                        address_added: true,
                        address_addedAt: new Date().toISOString(),
                        address_updatedAt: new Date().toISOString(),
                        updatedAt: new Date().toISOString()
                    });
                    new UserStatusModel({
                        status: "ADDRESS_ADDED",
                        remarks: 'ADMIN ADDED RECORD',
                        user: document.user,
                        admin: document.addedById
                    }).save();
                }

                const UPDATE_ADDRESS = UserModel.updateOne()
                    .where("_id").equals(document.user)
                    .set(document)
                    .exec();

                return Promise.all([
                    UPDATE_ADDRESS
                ]).then(([update]) => {
                    if (update.n < 1 || update.nModified < 1)
                        throw new ApplicationError({
                            name: 'AddressUpdateError',
                            message: 'Address not updated.'
                        });
                    return {
                        matched: update.n,
                        updated: update.nModified,
                        isAdded: isAdded,
                        message: 'Address updated.'
                    };
                });
            });
    }

    static validation() {
        return {
            "$id": "http://guru.sheenlac.com/schemas/training/save_painter_address.json",
            "type": "object",
            "properties": {
                "line1": {
                    "type": "string"
                },
                "line2": {
                    "type": "string"
                },
                "city": {
                    "type": "string"
                },
                "state": {
                    "type": "string"
                },
                "pincode": {
                    "type": "string"
                },
                "country": {
                    "type": "string",
                    "enum": ["India"]
                },
                "landmark": {
                    "type": "string",
                }
            },
            "required": ["line1", "line2", "city", "state", "pincode", "country"],
            "additionalProperties": false
        };
    }
}

module.exports = SavePainterAddress;