const Ajv = require('ajv');
const _ = require('lodash');

const ApplicationError = require('@src/errors/ApplicationError');
const validations = require('@app/validations');
const ajv = new Ajv(validations.ajv_options);

const GetTrainerPaintersTask = require('./get_trainer_painters/get_trainer_painters.task');
const CreatePainterProfileTask = require('./create_painter_profile/create_painter_profile.task');
const SavePainterAddress = require('./save_painter_address/save_painter_address.task');
const SavePainterBankDetails = require('./save_painter_bank_details/save_painter_bank_details.task');
const GetTrainerPainterTask = require('./get_trainer_painter/get_trainer_painter.task');
const UpdatePainterTask = require('./update_painter/update_painter.task');
const CheckMobileNumber = require('../users/check_mobile_number/check_mobile_number.task');

class TrainerController {
    
    static getTrainerPainters(req, res, next) {
        if (!ajv.validate(GetTrainerPaintersTask.validation(), req.query)) {
            let errors = ajv.errors;
            return next(
                new ApplicationError({status: 400, name: "ValidationError", message: "Bad request parameters.", info: errors})
            );
        }

        return GetTrainerPaintersTask.exec(req.locals.empId, req.query)
            .then(data => {
                return res.status(200).json(
                    new res.app.locals.Success({ data: data })
                );
            })
            .catch(err => next(err));
    }

    static getAdminPainters(req, res, next) {
         if (!ajv.validate(GetTrainerPaintersTask.validation(), req.query)) {
             let errors = ajv.errors;
             return next(
                 new ApplicationError({
                     status: 400,
                     name: "ValidationError",
                     message: "Bad request parameters.",
                     info: errors
                 })
             );
         }

         return GetTrainerPaintersTask.getPainterByAdmin(req.locals.user_id, req.query)
             .then(data => {
                 return res.status(200).json(
                     new res.app.locals.Success({
                         data: data
                     })
                 );
             })
             .catch(err => next(err));
     }
    
    static getTrainerPainter(req, res, next) {
        return GetTrainerPainterTask.exec(req.params.id)
            .then( user =>{
                return res.status(200).json(
                    new res.app.locals.Success({data: { user }})
                );
            })
            .catch( err => next(err));
    }

    static getAdminPainter(req, res, next) {
        return GetTrainerPainterTask.getAdminPainter(req.params.id)
            .then(user => {
                return res.status(200).json(
                    new res.app.locals.Success({
                        data: {
                            user
                        }
                    })
                );
            })
            .catch(err => next(err));
    }



    static createPainterProfile(req, res, next) {
        const photos = req.files['photo'];
        const proofs = req.files['proof'];
        if (Array.isArray(photos) && photos.length > 0) req.body['photo'] = photos[0].key;
        if (Array.isArray(proofs) && proofs.length > 0) req.body['proof'] = proofs.map(proof => proof.key);
        if (!ajv.validate(CreatePainterProfileTask.validation(), req.body)) {
            let errors = ajv.errors;
            return next(
                new ApplicationError({status: 400, name: "ValidationError", message: "Bad request parameters.", info: errors})
            );
        }
        let document = _.defaultsDeep(_.cloneDeep(req.body), {
            empId: req.route.path === '/trainer/painter' ? req.locals.empId : req.body.sp_id,
            addedById: req.locals.user_id
        });
        return CreatePainterProfileTask.exec(document)
            .then(data => {
                return res.status(200).json(
                    new res.app.locals.Success({ data: data })
                );
            })
            .catch(err => next(err));
    }

    static updatePainter(req, res, next) {
        const photos = req.files ? req.files['photo'] : "";
        const proofs = req.files ? req.files['proof'] : "";
        if (Array.isArray(photos) && photos.length > 0) req.body['photo'] = photos[0].key;
        if (Array.isArray(proofs) && proofs.length > 0) req.body["proof"] = proofs.map(file => file.key);
        if (!ajv.validate(UpdatePainterTask.validation(), req.body)) {
            let errors = ajv.errors;
            return next(
                new ApplicationError({
                    status: 400,
                    name: 'ValidationError',
                    message: 'Bad request parameters.',
                    info: errors
                })
            );
        }
        let document = _.defaultsDeep(_.cloneDeep(req.body), {
            empId: req.route.path === '/trainer/painter/:id' ? req.locals.empId : req.body.sp_id,
            addedById: req.locals.user_id
        });

        let userId = req.params["id"];
        return UpdatePainterTask.exec(userId, document)
            .then(resp => {
                req.locals.isAdded = resp.isAdded;
                return next();
            })
            .catch(err => next(err));
    }
    
    static savePainterAddress(req, res, next) {
        if(!ajv.validate(SavePainterAddress.validation(), req.body)) {
            let errors = ajv.errors;
            return next(
                new ApplicationError({status: 400, name: 'ValidationError', message: 'Bad request parameters.', info: errors})
            );
        }

        let userId = req.params["id"];
        let document = _.defaultsDeep({
            address: _.cloneDeep(req.body)
        }, {
            user: userId,
            empId: req.locals.empId,
            addedById: req.locals.user_id
        });
        return SavePainterAddress.exec(document)
            .then(resp => {
                req.locals.isAdded = resp.isAdded;
                return next();
            })
            .catch(err => next(err));
    }

    static savePainterBankDetails(req, res, next) {
        if (req.file) req.body["passbookPhoto"] = req.file.key;
        if(!ajv.validate(SavePainterBankDetails.validation(), req.body)) {
            let errors = ajv.errors;
            return next(
                new ApplicationError({status: 400, name: 'ValidationError', message: 'Bad request parameters.', info: errors})
            );
        }

        let userId = req.params["id"]; 
        let document = _.defaultsDeep({
            bank: _.cloneDeep(req.body)
        }, {
            user: userId,
            empId: req.locals.empId,
            addedById: req.locals.user_id
        });
        return SavePainterBankDetails.exec(document)
            .then(resp => {
                req.locals.isAdded = resp.isAdded;
                return next();
            })
            .catch(err => next(err));
    }

    static makeUnderReview(req, res, next) {

        console.log(req.locals.isAdded)
        let userId = req.params["id"];
        let document = _.defaultsDeep({}, {
            user: userId,
            empId: req.locals.empId,
            isAdded: req.locals.isAdded,
            addedById: req.locals.user_id
        });
        return SavePainterBankDetails.underReviewStatus(document)
            .then( resp => {
                return res.status(200).json(
                    new res.app.locals.Success({ data: resp })
                );
            })
            .catch(err => next(err));
    }

    static check_mobile_number(req, res, next) {

        return CheckMobileNumber.exec(req.params)
            .then(resp => {
                return res.status(200).json(
                    new res.app.locals.Success({
                        data: resp
                    })
                );
            })
            .catch(err => next(err));
    }
}

module.exports = TrainerController;