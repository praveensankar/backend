const TrainingLocationModel = require('../location.model');

class GetTrainingLocationTask {

    static exec(locationId) {
        return TrainingLocationModel
            .where('_id').equals(locationId)
            .select('-__v')
            .findOne().exec();
    }

}

module.exports = GetTrainingLocationTask;