const Promise = require('bluebird');
const TrainingCalendarModel = require('../calendar.model');

class GetTrainingCalendarsTask {

    static exec(query = {}, options = {}) {
        const skip = options.page > 0 ? (options.page - 1) * options.size : 0;
        let GetTrainingCalendarsQuery = TrainingCalendarModel.find(query).select('-__v').populate('location').skip(skip);
        GetTrainingCalendarsQuery = (options.size > 0) ? GetTrainingCalendarsQuery.limit(options.size) : GetTrainingCalendarsQuery;
        GetTrainingCalendarsQuery = (options.location) ? GetTrainingCalendarsQuery.where('location').equals(options.location) : GetTrainingCalendarsQuery;
        return Promise.all([
            GetTrainingCalendarsQuery.exec(),
            TrainingCalendarModel.estimatedDocumentCount().exec()
        ]).then(([calendars, count]) => {
            return {
                items: calendars,
                totalCount: count
            };
        });
    }

    static validation() {
        return {
            "$id": "http://guru.sheenlac.com/schemas/list_trainings_query_params.json",
            "$async": true,
            "type": "object",
            "properties": {
                "page": { "type": "integer", "default": 0 },
                "size": { "type": "integer", "default": 0 },
                "location": { "type": "string" }
            },
            "required": [],
            "additionalProperties": false
        };
    }

}

module.exports = GetTrainingCalendarsTask;