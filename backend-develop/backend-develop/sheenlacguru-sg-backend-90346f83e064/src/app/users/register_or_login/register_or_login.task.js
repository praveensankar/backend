const _ = require('lodash');

const UserModel = require('../user.model');
const LocationModel = require('../location.model');
const UserStatusModel = require('../user_status.model');
class RegisterOrLoginTask {

    static exec(document) {
        return UserModel.where('mobile_number').equals(document.mobile_number)
            .findOne()
            .exec()
            .then(user => {
                const tmpUser = user;
                if (user) {
                    return Object.assign(tmpUser, {
                        isNew : false  
                    });
                } else {
                    return new UserModel(document).save();
                }
            })
            .then(user => {
                if (!user.isNew) {
                    new UserStatusModel({
                        status: "NEW",
                        remarks: 'PAINTER ADDED RECORD',
                        user: user._id
                    }).save();
                }
                let location = _.cloneDeep(_.get(document, 'location', {}))
                location['capture'] = 'LOGIN';
                location['user'] = user._id;
                new LocationModel(location).save();
                
                return user;
            });
    }

    static validation() {
        return {
            "$id": "http://guru.sheenlac.com/schemas/users/auth/register_or_login.json",
            "type": "object",
            "properties": {
                "mobile_number": { "type": "string", "maxLength": 10, "minLength": 10/* , "pattern": "/^[6,7,8,9]\d{9}$/" */},
                "registration_type": { "type": "string", "enum": ['PAINTER', 'CONTRACTOR'], "default": "PAINTER" },
                "location": {
                    "type": "object",
                    "properties": {
                        "type": { "type": "string", "enum": ["Point"] },
                        "latitude": {
                            "type": "string"
                        },
                        "longitude": {
                            "type": "string"
                        },
                        "coordinates": { "type": "array", "minItems": 2, "items": { "type": "number" } }
                    },
                    "required": ["type"],
                    "additionalProperties": false
                }
            },
            "required": ["mobile_number"],
            "additionalProperties": false
        };
    }

}

module.exports = RegisterOrLoginTask;