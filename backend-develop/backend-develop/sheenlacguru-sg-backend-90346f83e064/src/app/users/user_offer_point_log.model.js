const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const userOfferPointLogSchema = new Schema({
    remark: {
        type: String,
        trim: true
    },
    admin: {
        type: Schema.Types.ObjectId,
        ref: 'Admin'
    }
}, 
{ 
    timestamps: true 
});

userOfferPointLogSchema.virtual('id').get(function() {
    return this._id.toHexString();
});

userOfferPointLogSchema.virtual('createdBy', {
    ref: 'Admin',
    localField: 'admin',
    foreignField: '_id',
    justOne: true
});

userOfferPointLogSchema.set('toJSON', { getters: true, virtuals: true });

module.exports = mongoose.model("UserOfferPointLog", userOfferPointLogSchema);