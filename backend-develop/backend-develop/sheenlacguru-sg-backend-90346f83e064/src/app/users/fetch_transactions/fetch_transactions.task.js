const Promise = require('bluebird');
const _ = require('lodash');
const ApplicationError = require('@src/errors/ApplicationError');
const FetchUserTask = require('../fetch_user/fetch_user.task');
const FetchTransactionsTask = require('@src/app/transactions/fetch_transactions/fetch_transactions.task');

class FetchUserTransactionsTask {

    static exec(userId) {

        const USER_LEDGER_DETAILS = FetchUserTask.exec(userId)
            .then(user => {
                if (!user)
                    throw new ApplicationError({ name: 'UnknownUser', status: 204, message: 'User not found.' });
                user = _.defaultsDeep(user, { total: 0, used: 0, balance: 0 });
                return _.pick(user, ['total', 'used', 'balance']);
            });

        const USER_TRANSACTIONS = FetchTransactionsTask.exec({ user: userId });

        return Promise.all([
            USER_LEDGER_DETAILS,
            USER_TRANSACTIONS
        ]).spread((ledger, transactions ) => {
            return { ledger, transactions };
        });
    }

}

module.exports = FetchUserTransactionsTask;