const _ = require('lodash');
const PINCODES = require('@src/PinCodes');
const ApplicationError = require('@src/errors/ApplicationError');
const UserModel = require('../user.model');

class GetApprovedUser {

    static exec(document) {
        return UserModel.findById(document.user)
            .where("status").equals("APPROVED")
            .exec().then(user => {
                if (!user)
                    throw new ApplicationError({ name: "InvalidPainterStatus", message: "Painter is not approved." });
                return _.defaultsDeep(_.cloneDeep(user.toJSON()), { state: PINCODES[user.pincode].state });
            });
    }

}

module.exports = GetApprovedUser;