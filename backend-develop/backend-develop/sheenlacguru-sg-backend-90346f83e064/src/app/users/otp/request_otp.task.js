const moment = require("moment");
const OneTimePassword = require("../../../libraries/otp");
const UserOtpModel = require("../user_otp.model");
const SendOTPFlow = require("../../../configs/SendOTPFlowsConfig.json");

class RequestOTP {
    static exec(options = {}) {
        console.log(options)
        return UserOtpModel.where('mobile_number').equals(options.receiver_mobile_number).where('otp_expires_at').gte(moment.utc())
            .findOne().sort({
                createdAt: -1
            })
            .exec()
            .then(user_otp => {
                console.log(user_otp);
                const generated_otp = OneTimePassword.generate(4);
                if (user_otp && user_otp.otp_expires_at >= moment.utc() && user_otp.is_verified === false) {
                    return OneTimePassword.sendByMSG91(RequestOTP.generate_otp_payload(options, user_otp.otp));
                } else {
                    return new UserOtpModel({
                        mobile_number: options.receiver_mobile_number,
                        otp: generated_otp,
                        is_verified: false,
                        otp_expires_at: moment.utc().add(1, 'm')
                    }).save().then(saved => {
                        return OneTimePassword.sendByMSG91(RequestOTP.generate_otp_payload(options, generated_otp));
                    });
                }
            });
    }

    static generate_otp_payload(args, otp) {
        const flow = SendOTPFlow[args.flow_id];
        const extra_params = {}
        flow.is_extra_params && flow.extra_params.map((params, index) => {
            console.log(params, index);
            const value_reference = flow.value_reference[index];
            const value = args[value_reference];
            return RequestOTP.set_extra_params(extra_params, params, value);
        });

        const payload = {
            template_no: args.flow_id,
            mobile_number: args.receiver_mobile_number,
            otp: otp,
            is_extra_params: flow.is_extra_params,
            extra_params: extra_params
        };
        return payload;
    }

    static set_extra_params(obj, prop, value) {
        obj[prop] = value;
    }

    static validation() {
        return {
            "$id": "http://guru.sheenlac.com/schemas/users/auth/request_otp.json",
            "type": "object",
            "properties": {
                "receiver_mobile_number": {
                    "type": "string",
                    "maxLength": 10,
                    "minLength": 10 /* , "pattern": "/^[6,7,8,9]\d{9}$/" */
                },
                "verification_mobile_number": {
                    "type": "string",
                    "maxLength": 10,
                    "minLength": 10
                },
                "flow_id": {
                    "type": "integer",
                    "default": 1
                },
                "attempt_count": {
                    "type": "integer",
                    "default": 1
                }
            },
            "required": ["receiver_mobile_number", "flow_id", "attempt_count"],
            "additionalProperties": false
        };
    }
}

module.exports = RequestOTP;