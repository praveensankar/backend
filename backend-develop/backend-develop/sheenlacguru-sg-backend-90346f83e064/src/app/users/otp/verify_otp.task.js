const moment = require("moment");
const OneTimePassword = require("../../../libraries/otp");
const UserOtpModel = require("../user_otp.model");
const ApplicationError = require('@src/errors/ApplicationError');

class VerifyOTP {
    static exec(options = {}) {
        return UserOtpModel.where('mobile_number').equals(options.receiver_mobile_number).where('otp').equals(options.otp).where('is_verified').equals(false).where('otp_expires_at').gte(moment.utc())
            .findOne()
            .exec()
            .then(fetched_otp => {
                console.log(fetched_otp)
                if (!fetched_otp) {
                   throw new ApplicationError({
                       name: "InvalidOTPError",
                       message: "Invalid or Expired OTP."
                   });
                }
                
                if (fetched_otp && fetched_otp.otp_expires_at >= moment.utc()) {
                    return UserOtpModel.where('mobile_number').equals(options.receiver_mobile_number).where('otp').equals(options.otp).where('is_verified').equals(false).updateOne({}, {
                        omitUndefined: true
                    }).set({
                        is_verified: true
                    }).exec().then(updated => {
                       return "OTP Verifed Successfully"; 
                    })
                    
                }
            });
    }

    static validation() {
        return {
            "$id": "http://guru.sheenlac.com/schemas/users/auth/verify_otp.json",
            "type": "object",
            "properties": {
                "receiver_mobile_number": {
                    "type": "string",
                    "maxLength": 10,
                    "minLength": 10 /* , "pattern": "/^[6,7,8,9]\d{9}$/" */
                },
                "otp": {
                    "type": "string",
                    "maxLength": 6,
                    "minLength": 4
                },
                "flow_id": {
                    "type": "integer",
                    "default": 1
                }
            },
            "required": ["receiver_mobile_number", "otp", "flow_id"],
            "additionalProperties": false
        };
    }
}

module.exports = VerifyOTP;