const express = require('express');
const nconf = require('nconf');
const router = express.Router();

const Token = require('@src/middleware/token');
const Authorization = require('@src/middleware/authorization');
const Uploader = require('@src/middleware/uploader.middleware');
const UserController = require('./user.controller');

router.use('/auth', require('./authv2.routes'));

router.use(Token.verify({ for: 'user' }));

/* Routes for currently logged in user (user_id from access_token). */
router.post('/sp_verification/:sales_rep_id', Authorization.authorize(["painter", "contractor"]), UserController.userSalesRepVerification);
router.post('/current', Authorization.authorize(["painter", "contractor"]), Uploader({dest: nconf.get("STORAGE:KYC_DOCS")}).fields([{name: 'photo', maxCount: 1}, {name: 'proof', maxCount: 5}]), UserController.create);
router.post('/update', Authorization.authorize(["painter", "contractor"]), Uploader({dest: nconf.get("STORAGE:KYC_DOCS")}).fields([{name: 'photo', maxCount: 1}, {name: 'proof', maxCount: 5}]), UserController.update,UserController.makeUnderReview);
router.get('/current', Authorization.authorize(["painter", "contractor"]), UserController.current);
router.get('/transactions', Authorization.authorize(["painter", "contractor"]), UserController.user_transactions);
router.get('/transactions_v2', Authorization.authorize(["painter", "contractor"]), UserController.user_transactions_v2);
router.post('/address', Authorization.authorize(["painter", "contractor"]), UserController.save_address, UserController.makeUnderReview);
router.post('/bank_details', Authorization.authorize(["painter", "contractor"]), Uploader({dest: nconf.get("STORAGE:KYC_DOCS")}).single('passbookPhoto'), UserController.save_bank_details,UserController.makeUnderReview);

/* Routes for operations admin to manage users (painters/contractors). */
router.get('/', Authorization.authorize(["painterRegistrationManagement", "dropoffPainterManagement"]), UserController.index);
router.get('/:id', Authorization.authorize(["painterRegistrationManagement"]), UserController.show);
router.post('/:id', Authorization.authorize([ "painterRegistrationManagement"]), Uploader({
    dest: nconf.get("STORAGE:KYC_DOCS")
}).fields([{
    name: 'photo',
    maxCount: 1
}, {
    name: 'proof',
    maxCount: 5
}]), UserController.update, UserController.makeUnderReview);
router.get('/:id/transactions', Authorization.authorize(["painterRegistrationManagement"]), UserController.user_transactions);
router.post('/:id/address', Authorization.authorize([ "painterRegistrationManagement"]), UserController.save_address, UserController.makeUnderReview);
router.post('/:id/bank_details', Authorization.authorize(["painterRegistrationManagement"]), Uploader({dest: nconf.get("STORAGE:KYC_DOCS")}).single('passbookPhoto'), UserController.save_bank_details,UserController.makeUnderReview);
router.post('/:id/bp_number', Authorization.authorize(["painterRegistrationManagement"]), UserController.generate_bp_number);
router.post('/:id/status', Authorization.authorize(["painterRegistrationManagement"]), UserController.update_status);
router.get('/ledger/list', Authorization.authorize(["painterLedger"]), UserController.listPainterLedger);
router.get('/ledger/:id', Authorization.authorize(["painterLedger"]), UserController.user_transactions_v2);
router.patch(
  '/changePainterMobileNumber', Authorization.authorize(["canChangePainterMobileNumber"]),UserController.changePainterMobileNumber
);
router.get(
  '/getUserSpecialPointLogs/logs', Authorization.authorize(["canRemoveSpecialPoints"]),UserController.userSpecialPointLogs
);
router.patch(
  '/updateUserSpecialPoints', Authorization.authorize(["canRemoveSpecialPoints"]),UserController.updateSpecialPoints
);


module.exports = router;