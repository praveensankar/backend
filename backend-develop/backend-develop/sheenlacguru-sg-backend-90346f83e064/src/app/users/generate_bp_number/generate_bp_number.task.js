const uniqid = require('uniqid');

const ApplicationError = require('@src/errors/ApplicationError');

const UserModel = require('../user.model');

class GenerateBusinessPartnerNumberTask {

    static exec(user_id) {
        let bp_number = uniqid('SH-');
        return UserModel.where('_id').equals(user_id)
        .updateOne({}, {omitUndefined: true})
        .set({ bp_number }).exec()
        .then( update_resp => {
            if (update_resp.n < 1 || update_resp.nModified < 1)
                throw new ApplicationError({name: 'GenerateBPNumberError', message: 'Business Partner number was not created.'});

            return { matched: update_resp.n, updated: update_resp.nModified, bp_number: bp_number, message: 'Business partner number created.' };
        });
    }

}

module.exports = GenerateBusinessPartnerNumberTask;