const UserModel = require("../user.model");
const ApplicationError = require('@src/errors/ApplicationError');

class CheckMobileNumber {
    static exec(options = {}) {
        return UserModel.find().where('mobile_number').equals(options.mobile_number).populate('sp_user').findOne()
            .exec().then(mobile_number => {
                if (!mobile_number) {
                     throw new ApplicationError({
                         name: 'InvalidMobileNumberError',
                         message: "Mobile Number doesn't exists."
                     });
                }
                return mobile_number;
            });
    }
}

module.exports = CheckMobileNumber;