const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const user_otp_schema = new Schema({
    mobile_number: {
        type: String,
        trim: true,
        required: [true, 'Please enter the mobile number.'],
        match: [/^[6,7,8,9]\d{9}$/, 'Mobile number is invalid.']
    },
    otp: {
        type: String,
        trim: true,
        match: [/^\d{4,10}$/, '{VALUE} does not comply with {PATH} format'],
        required: [true, '{PATH} is required']
    },
    is_verified: {
        type: Boolean,
        default: false
    },
    otp_expires_at: {
        type: Date,
        required: [true, '{PATH} is required']
    }
}, {
    timestamps: true
});

module.exports = mongoose.model("userotp", user_otp_schema);