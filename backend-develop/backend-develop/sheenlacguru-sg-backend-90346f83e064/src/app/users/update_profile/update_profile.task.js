const ApplicationError = require('@src/errors/ApplicationError');
const _ = require("lodash");
const UserModel = require('../user.model');
const UserStatusModel = require('../user_status.model');
const UserOfferPointLogModel = require('../user_offer_point_log.model');

const mongoose = require('mongoose');
class UpdateProfileTask {

    static exec(user_id, document) {
        const FETCH_USER = UserModel.where('_id').equals(user_id)
            .findOne()
            .exec();

        return Promise.all([FETCH_USER])
            .then(([user]) => {
                 if (!user)
                     throw new ApplicationError({
                         name: 'PainterError',
                         message: 'Painter not registered.'
                     });
                let isAdded = false;
                if (!user.profile_added) {
                    isAdded = true;
                    document = _.defaultsDeep(_.cloneDeep(document), {
                        status: user.status,
                        profile_added: true,
                        profile_addedAt: new Date().toISOString(),
                        profile_updatedAt: new Date().toISOString(),
                        updatedAt: new Date().toISOString()
                    });
                    new UserStatusModel({
                        status: "PROFILE_ADDED",
                        remarks: document.admin ? 'ADMIN ADDED RECORD' : 'PAINTER ADDED RECORD',
                        user: user_id,
                        admin: document.admin
                    }).save();
                } else {
                    document = _.defaultsDeep(_.cloneDeep(document), {
                        status: user.status,
                        profile_updatedAt: new Date().toISOString(),
                        updatedAt: new Date().toISOString()
                    });
                }

                return UserModel.where('_id').equals(user_id)
                    .updateOne({}, {
                        omitUndefined: true
                    })
                    .set(document)
                    .exec()
                    .then(op_resp => {
                        if (op_resp.n < 1 || op_resp.nModified < 1)
                            throw new ApplicationError({
                                name: "UpdateProfileError",
                                message: "Profile was not updated."
                            });
                        return {
                            matched: op_resp.n,
                            updated: op_resp.nModified,
                            isAdded: isAdded,
                            message: "Profile updated."
                        };
                    });
            });
    }

    static async updateSpecialPointsFunction(document) {
         // start mongoose transaction
        const session = await mongoose.startSession();
        session.startTransaction({
            readConcern: {
                level: "snapshot"
            },
            writeConcern: {
                w: 1
            }
        });
        try {
            const updated = await UserModel.updateMany(
                { offerPoints: { $gt: 0 } },
                {
                    $set: {
                        "offerPoints": 0
                    }
                }
            ).session(session).exec();
            await UserOfferPointLogModel.create({
                remark: document.remark || "User Offer Points updated",
                admin: document.admin
            });
            await session.commitTransaction();
            // return
            return {
                message: `Special Points updated.`
            };
        } catch (error) {
            console.log(error)
            await session.abortTransaction();
            throw new ApplicationError({
                name: error.name,
                message: error.message
            });
        } finally {
            session.endSession();
        }
    }
    static validation() {
        return {
            "$id": "http://guru.sheenlac.com/schemas/users/update_profile.json",
            "type": "object",
            "properties": {
                "photo": {
                    "type": "string"
                },
                "first_name": {
                    "type": "string"
                },
                "last_name": {
                    "type": "string"
                },
                "alternate_mobile_number": {
                    "type": "string"
                },
                "email": {
                    "type": "string",
                    "format": "email"
                },
                "gender": {
                    "type": "string",
                    "enum": ["MALE", "FEMALE"]
                },
                "birth_date": {
                    "type": "string"
                },
                "pincode": {
                    "type": "string",
                    "minLength": 6
                },
                "dealer_code": {
                    "type": "string"
                },
                "registration_type": {
                    "type": "string",
                    "enum": ['PAINTER', 'DEALER', 'CONTRACTOR']
                },
                "proof_type": {
                    "type": "string",
                    "enum": ['AADHAAR', 'PAN CARD']
                },
                "proof": {
                    "type": "array",
                    "minItems": 2,
                    "uniqueItems": true,
                    "items": {
                        "type": "string"
                    }
                },
                "status": {
                    "type": "string",
                    "enum": ['PROFILE_ADDED', 'UNDER_REVIEW', 'ON_HOLD', 'APPROVED', 'REJECTED', 'BLACKLISTED']
                },
                "painter_access": {
                    "type": "string",
                    "enum": ["No_Scan_Access", "Painter_Under_Contractor", "Individual_Painter", "Contractor"],
                }
            },
            "additionalProperties": false
        };
    }

}

module.exports = UpdateProfileTask;