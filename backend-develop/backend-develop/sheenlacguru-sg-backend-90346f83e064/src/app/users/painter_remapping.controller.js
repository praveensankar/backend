const Ajv = require('ajv');
const _ = require('lodash');

const ApplicationError = require('@src/errors/ApplicationError');
const validations = require('@app/validations');
const PainterRemappingTask = require('./painter_remapping/list_sales_advisor.task');
const ajv = new Ajv(validations.ajv_options);

class PainterRemappingController {
    static listSalesAdvisor(req, res, next) {
        if (!ajv.validate(PainterRemappingTask.listSalesAdvisorsValidation(), req.query)) {
            let errors = ajv.errors;
            return next(
                new ApplicationError({
                    status: 400,
                    name: 'ValidationError',
                    message: 'Bad request parameters.',
                    info: errors
                })
            );
        }
       return PainterRemappingTask.listSalesAdvisorFunction(req.query)
           .then(data => {
               return res.status(200).json(
                   new res.app.locals.Success({
                       data: data
                   })
               );
           })
           .catch(err => next(err));
    }

    static listSalesAdvisorPainters(req, res, next) {
        if (!ajv.validate(PainterRemappingTask.listSalesAdvisorPaintersValidation(), req.query)) {
            let errors = ajv.errors;
            return next(
                new ApplicationError({
                    status: 400,
                    name: 'ValidationError',
                    message: 'Bad request parameters.',
                    info: errors
                })
            );
        }
        return PainterRemappingTask.listSalesAdvisorPaintersFunction(req.query)
            .then(data => {
                return res.status(200).json(
                    new res.app.locals.Success({
                        data: data
                    })
                );
            })
            .catch(err => next(err));
    }

    static remapPainters(req, res, next) {
        if (!ajv.validate(PainterRemappingTask.remapPaintersValidation(), req.body)) {
            let errors = ajv.errors;
            return next(
                new ApplicationError({
                    status: 400,
                    name: 'ValidationError',
                    message: 'Bad request parameters.',
                    info: errors
                })
            );
        }
        let document = _.defaultsDeep(_.cloneDeep(req.body), {
            admin: req.locals.user_id
        });
        return PainterRemappingTask.remapPaintersFunction(document)
            .then(data => {
                return res.status(200).json(
                    new res.app.locals.Success({
                        data: data
                    })
                );
            })
            .catch(err => next(err));
    }
}

module.exports = PainterRemappingController;