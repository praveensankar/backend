const express = require('express');
const nconf = require('nconf');
const router = express.Router();

const Token = require('@src/middleware/token');
const Authorization = require('@src/middleware/authorization');
const PainterRemappingController = require('./painter_remapping.controller');

router.use(Token.verify({
    for: 'admin'
}));

router.get('/listSalesAdvisor', Authorization.authorize(["painterRemapping"]), PainterRemappingController.listSalesAdvisor);
router.get('/listSalesAdvisorPainters', Authorization.authorize(["painterRemapping"]), PainterRemappingController.listSalesAdvisorPainters);
router.post('/remapPainters', Authorization.authorize(["painterRemapping"]), PainterRemappingController.remapPainters);

module.exports = router;