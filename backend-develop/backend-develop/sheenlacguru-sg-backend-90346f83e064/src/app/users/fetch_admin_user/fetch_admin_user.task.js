const UserModel = require('../user.model');
const mongoose = require('mongoose');
const nconf = require('nconf');
const {
    has
} = require('lodash');
const SERVER_HOST = nconf.get('APPLICATION:S3');

class FetchAdminUserTask {
    static async exec(user_id) {
        const user = await UserModel.aggregate([{
            $match: {
                "_id": mongoose.Types.ObjectId(user_id)
            }
        }, {
            $lookup: {
                from: 'admins',
                localField: 'sp_id',
                foreignField: 'empId',
                as: 'sp_user'
            }
            },
        {
            $lookup: {
                from: 'userstatuses',
                pipeline: [{
                    $match: {
                        user: mongoose.Types.ObjectId(user_id)
                    }
                }, {
                    $sort: {
                        createdAt: -1
                    }
                }],
                as: "userstatus"
            }
        }, {
            $unwind: '$sp_user'
        }, {
            $limit: 1
        }, {
            $addFields: {
                photo: "$photo",
                id: "$_id"
            }
            }]);
        user.map(user => {
            Object.assign(user, {
                photo: `${SERVER_HOST}${user.photo}`,
                proof: user.proof.length > 0 ? user.proof.map(proof => `${SERVER_HOST}${proof}`) : [],
                bank: {
                    ...user.bank,
                    passbookPhoto: has(user.bank, 'passbookPhoto') && user.bank.passbookPhoto.length > 0 ? `${SERVER_HOST}${user.bank.passbookPhoto}` : ''
                },
                sp_user: {
                    _id: user.sp_user._id,
                    id: user.sp_user._id,
                    sp_name: `${user.sp_user.first_name} ${user.sp_user.last_name}`,
                    sp_id: user.sp_user.empId,
                    sp_designation: user.sp_user.designation ? user.sp_user.designation : '',
                    sp_location: user.sp_user.location ? user.sp_user.location : '',
                    sp_mobile: user.sp_user.mobile_no ? user.sp_user.mobile_no : ''
                }
            });
            return user;
        });
        return user;
    }
}
module.exports = FetchAdminUserTask;