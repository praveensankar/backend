const ApplicationError = require('@src/errors/ApplicationError');
const _ = require("lodash");
const UserModel = require('../user.model');
const UserStatusModel = require('../user_status.model');
class CreateProfileTask {

    static exec(user_id, document) {

        document = _.defaultsDeep(_.cloneDeep(document), {
            profile_added: true,
            addedBy: user_id,
            profile_addedAt: new Date().toISOString(),
            profile_updatedAt: new Date().toISOString(),
            updatedAt: new Date().toISOString()
        });

        return UserModel.where('_id').equals(user_id)
            .where('status').equals('SP_VERIFIED')
            .updateOne({}, {
                omitUndefined: true
            })
            .set(document)
            .exec()
            .then(op_resp => {
                if (op_resp.n < 1 || op_resp.nModified < 1)
                    throw new ApplicationError({
                        name: "CreateProfileError",
                        message: "Profile was not created."
                    });
                new UserStatusModel({
                    status: "PROFILE_ADDED",
                    remarks: 'PAINTER ADDED RECORD',
                    user: user_id
                }).save();
                return {
                    matched: op_resp.n,
                    updated: op_resp.nModified,
                    message: "Profile created."
                };
            });
    }

    static validation() {
        return {
            "$id": "http://guru.sheenlac.com/schemas/users/create_profile.json",
            "type": "object",
            "properties": {
                "photo": {
                    "type": "string"
                },
                "first_name": {
                    "type": "string"
                },
                "last_name": {
                    "type": "string"
                },
                "alternate_mobile_number": {
                    "type": "string"
                },
                "email": {
                    "type": "string",
                    "format": "email"
                },
                "gender": {
                    "type": "string",
                    "enum": ["MALE", "FEMALE"]
                },
                "birth_date": {
                    "type": "string",
                    "format": "date"
                },
                "pincode": {
                    "type": "string",
                    "minLength": 6
                },
                "dealer_code": {
                    "type": "string"
                },
                "registration_type": {
                    "type": "string",
                    "enum": ['PAINTER', 'DEALER', 'CONTRACTOR']
                },
                "proof_type": {
                    "type": "string",
                    "enum": ['AADHAAR', 'PAN CARD']
                },
                "proof": {
                    "type": "array",
                    "minItems": 2,
                    "uniqueItems": true,
                    "items": {
                        "type": "string"
                    }
                },
                "status": {
                    "type": "string",
                    "enum": ['PROFILE_ADDED', 'UNDER_REVIEW', 'ON_HOLD', 'APPROVED', 'REJECTED', 'BLACKLISTED'],
                    "default": "PROFILE_ADDED"
                }
            },
            "required": ["photo", "first_name", "last_name", "gender", "birth_date", "pincode", "registration_type", "proof_type", "proof"],
            "additionalProperties": false
        };
    }

}

module.exports = CreateProfileTask;