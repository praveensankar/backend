const Promise = require('bluebird');
const ApplicationError = require('@src/errors/ApplicationError');
const _ = require("lodash");
const UserModel = require('../user.model');
const UserStatusModel = require('../user_status.model');
class SaveBankDetailsTask {

    static exec(document = {}) {

        let bankDocument = {};
        const FETCH_USER = UserModel.where('_id').equals(document.user)
            .findOne()
            .exec();

        Object.keys(document.bank).forEach(key => {
            bankDocument[`bank.${key}`] = document.bank[key];
        });

        return Promise.all([FETCH_USER])
            .then(([user]) => {
                let isAdded = false;

                bankDocument = _.defaultsDeep(_.cloneDeep(bankDocument), {
                    bank_info_updatedAt: new Date().toISOString(),
                    updatedAt: new Date().toISOString()
                });

                if (!_.hasIn(user, 'bank')) {
                    isAdded = true;
                    document = _.defaultsDeep(_.cloneDeep(document), {
                        status: "BANK_INFO_ADDED",
                        bank_info_added: true,
                        bank_info_addedAt: new Date().toISOString(),
                        bank_info_updatedAt: new Date().toISOString(),
                        updatedAt: new Date().toISOString()
                    });
                    new UserStatusModel({
                        status: "BANK_INFO_ADDED",
                        remarks: document.admin ? 'ADMIN ADDED RECORD' : 'PAINTER ADDED RECORD',
                        user: document.user,
                        admin: document.admin
                    }).save();
                }

                if (!user.bank_info_added) {
                    isAdded = true;
                    bankDocument = _.defaultsDeep(_.cloneDeep(bankDocument), {
                        status: "BANK_INFO_ADDED",
                        bank_info_added: true,
                        bank_info_addedAt: new Date().toISOString(),
                        bank_info_updatedAt: new Date().toISOString(),
                        updatedAt: new Date().toISOString()
                    });
                    new UserStatusModel({
                        status: "BANK_INFO_ADDED",
                        remarks: document.admin ? 'ADMIN ADDED RECORD' : 'PAINTER ADDED RECORD',
                        user: document.user,
                        admin: document.admin
                    }).save();
                }

                const UPDATE_BANK_DETAIL = UserModel.updateOne({}, {
                        omitUndefined: true
                    })
                    .where('_id').equals(document.user)
                    .set(bankDocument)
                    .exec()

                return Promise.all([
                    UPDATE_BANK_DETAIL
                ]).then(([update]) => {
                    if (update.n < 1 || update.nModified < 1)
                        throw new ApplicationError({
                            name: 'BankDetailsError',
                            message: 'Bank details not updated.'
                        });

                    return {
                        matched: update.n,
                        updated: update.nModified,
                        isAdded: isAdded,
                        message: 'Bank details updated.'
                    };
                });
            });
    }

    static underReviewStatus(document = {}) {
        const FETCH_USER = UserModel.where('_id').equals(document.user)
            .findOne()
            .exec();

        return Promise.all([
            FETCH_USER
        ]).then(([user]) => {
            if (!user)
                throw new ApplicationError({
                    name: 'PainterError',
                    message: 'Painter not registered.'
                });

            if (user.sp_verified && user.profile_added && user.address_added && user.bank_info_added) {
                let data = {};
                if (document.isAdded) {
                    data = _.defaultsDeep(_.cloneDeep(data), {
                        status: "UNDER_REVIEW",
                        milestoneUpdatedAt: _.has(user, 'milestoneUpdatedAt') ? user.milestoneUpdatedAt : new Date().toISOString(),
                        registeredAt: _.has(user, 'registeredAt') ? user.registeredAt : new Date().toISOString(),
                        under_reviewAt: new Date().toISOString(),
                        updatedAt: new Date().toISOString()
                    });
                    new UserStatusModel({
                        status: "UNDER_REVIEW",
                        remarks: document.admin ? 'ADMIN ADDED RECORD' : 'PAINTER FIRST REVIEW',
                        user: document.user,
                        admin: document.admin
                    }).save();
                } else {
                    data = _.defaultsDeep(_.cloneDeep(data), {
                        status: "UNDER_REVIEW",
                        under_reviewAt: new Date().toISOString(),
                        updatedAt: new Date().toISOString()
                    });
                    new UserStatusModel({
                        status: "UNDER_REVIEW",
                        remarks: document.admin ? `ADMIN AUTO REVIEW - FOR ${user.status}` : `PAINTER AUTO REVIEW - FOR ${user.status}`,
                        user: document.user,
                        admin: document.admin
                    }).save();
                }
                const UPDATE_STATUS = UserModel.updateOne({}, {
                        omitUndefined: true
                    })
                    .where('_id').equals(document.user)
                    .set(data)
                    .exec();
                return Promise.all([UPDATE_STATUS]).then(([update]) => {
                    if (!update)
                        throw new ApplicationError({
                            name: 'StatusUpdateError',
                            message: 'Status not updated.'
                        });

                    return {
                        matched: update.n,
                        updated: update.nModified,
                        message: 'Profile is Under Review.'
                    };
                });
            } else {
                return {
                    message: 'Painter details added, but profile is not under review.'
                };
            }

        });
    }

    static validation() {
        return {
            "$id": "http://guru.sheenlac.com/schemas/users/create_bank_details.json",
            "type": "object",
            "properties": {
                "name": {
                    "type": "string"
                },
                "account": {
                    "type": "string"
                },
                "accountHolder": {
                    "type": "string"
                },
                "passbookPhoto": {
                    "type": "string"
                },
                "ifsc": {
                    "type": "string"
                },
                "branch": {
                    "type": "string"
                }
            },
            "required": ["name", "account", "ifsc", "branch"],
            "additionalProperties": false
        };
    }

}

module.exports = SaveBankDetailsTask;