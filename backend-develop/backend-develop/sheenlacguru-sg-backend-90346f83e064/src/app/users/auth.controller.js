const Ajv = require('ajv');
const nconf = require('nconf');

const ApplicationError = require("@src/errors/ApplicationError");
const validations = require("@app/validations");
const ajv = new Ajv(validations.ajv_options);

const RegisterOrLoginTask = require('./register_or_login/register_or_login.task');
const RequestOTP = require('./otp/request_otp.task');
const VerifyOTP = require('./otp/verify_otp.task');
const ResendOTP = require('./otp/resend_otp.task');
const VerifyMissedCall = require('./verify_missedcall/verify_missedcall.task');

const APPLICATION_SECRET = nconf.get("APPLICATION:SECRET");

class AuthController {

    static register_or_login(req, res, next) {
        if (!(req.body.secret === APPLICATION_SECRET && ajv.validate(RegisterOrLoginTask.validation(), req.body))) {
            let errors = ajv.errors;
            return next(
                new ApplicationError({
                    status: "400",
                    name: "ValidationError",
                    message: "Bad request parameters.",
                    info: errors
                })
            );
        }
        if (req.body.location && req.body.location.coordinates && req.body.location.coordinates.length > 0) {
            req.body = Object.assign(req.body, {
                location: req.body.location ? {
                    type: req.body.location.type,
                    latitude: req.body.location.coordinates[0],
                    longitude: req.body.location.coordinates[1],
                } : {
                    type: 'Point',
                    latitude: "",
                    longitude: ""
                }
            });
        }
        console.log(req.body);
        return RegisterOrLoginTask.exec(req.body)
            .then((user) => {
                req.locals = req.locals || {};
                req.locals.claims = {
                    id: user._id,
                    role: user.registration_type ? user.registration_type.toLowerCase() : ''
                }
                req.locals.user_status = user.status;
                req.locals.sp_verified = user.sp_verified;
                return next();
            })
            .catch((err) => {
                return next(err);
            });
    }

    static request_otp(req, res, next) {
        if (!ajv.validate(RequestOTP.validation(), req.query)) {
            let errors = ajv.errors;
            return next(
                new ApplicationError({
                    status: "400",
                    name: "ValidationError",
                    message: "Bad request parameters.",
                    info: errors
                })
            );
        }
        return RequestOTP.exec(req.query)
            .then((otp) => {
                return res.status(200).json(
                    new res.app.locals.Success({
                        data: otp
                    })
                );
            })
            .catch((err) => {
                return next(err);
            });
    }

    static verify_otp(req, res, next) {
        if (!ajv.validate(VerifyOTP.validation(), req.query)) {
            let errors = ajv.errors;
            return next(
                new ApplicationError({
                    status: "400",
                    name: "ValidationError",
                    message: "Bad request parameters.",
                    info: errors
                })
            );
        }
        return VerifyOTP.exec(req.query)
            .then((otp) => {
                return res.status(200).json(
                    new res.app.locals.Success({
                        data: otp
                    })
                );
            })
            .catch((err) => {
                return next(err);
            });
    }

    static resend_otp(req, res, next) {
        if (!ajv.validate(ResendOTP.validation(), req.query)) {
            let errors = ajv.errors;
            return next(
                new ApplicationError({
                    status: "400",
                    name: "ValidationError",
                    message: "Bad request parameters.",
                    info: errors
                })
            );
        }
        return ResendOTP.exec(req.query)
            .then((otp) => {
                return res.status(200).json(
                    new res.app.locals.Success({
                        data: otp
                    })
                );
            })
            .catch((err) => {
                return next(err);
            });
    }

    static verify_missedcall(req, res, next) {
        if (!ajv.validate(VerifyMissedCall.validation(), req.query)) {
            let errors = ajv.errors;
            return next(
                new ApplicationError({
                    status: "400",
                    name: "ValidationError",
                    message: "Bad request parameters.",
                    info: errors
                })
            );
        }
        return VerifyMissedCall.exec(req.query)
            .then((otp) => {
                return res.status(200).json(
                    new res.app.locals.Success({
                        data: otp
                    })
                );
            })
            .catch((err) => {
                return next(err);
            });
    }

}

module.exports = AuthController;