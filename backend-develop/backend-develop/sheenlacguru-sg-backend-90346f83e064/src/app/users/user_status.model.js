const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const userStatusSchema = new Schema({
    status: {
        type: String,
        enum: ['NEW', 'SP_VERIFIED', 'PROFILE_ADDED', 'ADDRESS_ADDED', 'BANK_INFO_ADDED', 'UNDER_REVIEW', 'ON_HOLD', 'APPROVED', 'REJECTED', 'BLACKLISTED','SP_REMAPPED','MOBILE_NUMBER_CHANGED'],
        required: [true, 'User status is required.']
    },
    previous_sp_id: {
        type: String
    },
    latest_sp_id: {
        type: String
    },
    remarks: {
        type: String,
        trim: true
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    admin: {
        type: Schema.Types.ObjectId,
        ref: 'Admin'
    }
}, 
{ 
    timestamps: true 
});

userStatusSchema.virtual('id').get(function() {
    return this._id.toHexString();
});

userStatusSchema.set('toJSON', { getters: true, virtuals: true });

module.exports = mongoose.model("UserStatus", userStatusSchema);