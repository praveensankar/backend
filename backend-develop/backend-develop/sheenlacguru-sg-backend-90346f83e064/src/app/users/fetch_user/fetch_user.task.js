const UserModel = require('../user.model');
const UserOfferPointLogModel = require('../user_offer_point_log.model');
const OfferModel = require('../../offers/offers.model');
const TransactionModel = require('../../transactions/transaction.model');
class FetchUserTask {
    static exec(user_id) {
    const THRESHOLD_DATE = '2022-12-02T00:00:00.000'
        return UserModel.where('_id').equals(user_id)
            .populate('sp_user')
            .select('-__v')
            .findOne()
            .exec().then(async(user) => {
                // let offer_id = '6389a432b2022a7ffd817e95'; // Hardcoded for SWARNA VARSHA
                // const offer = await OfferModel.findById(offer_id).select('-__v').lean().exec();
                let userQueryData = {
                    _id: user_id,
                    // offerPoints: { $gte: 0 },
                    bp_number: { $ne: "" }
                }
                const offerPointsResult = await UserModel.find(userQueryData);  
                let getPainterBonusPoint = await TransactionModel.find({
                        bp_number : offerPointsResult[0].bp_number,
                        // createdAt: {
                        //     $lte: new Date(THRESHOLD_DATE)
                        // },
                        createdAt: {
                            $gte: new Date(THRESHOLD_DATE)
                        },
                        "$or": [{
                            mode : "BONUS-CREDIT"
                            },{
                            mode : "SPECIALOFFER-DEBIT"
                            },{
                            mode : "CREDIT"
                            }
                        ]
                    });
        
                    let totalPoints = 0;
                    let usedPoints = 0;
        
                    getPainterBonusPoint.map((data,index)=> {
                        if((data['mode'] == 'BONUS-CREDIT')){
                            totalPoints +=  data['points']
                        }
        
                        if(data['mode'] == 'SPECIALOFFER-DEBIT'){
                            if((data['status'] == 'APPROVED') || (data['status'] == 'NEW')){
                                usedPoints +=  data['points']
                            }
                        }
        
                        if(data['mode'] == 'CREDIT'){
                            totalPoints +=  data['points']
                        }
                    })
                    user['total'] = totalPoints
                    user['used'] = usedPoints
                    user['balance'] = totalPoints - usedPoints
                return Object.assign(user, {
                    sp_user: {
                        _id: user.sp_user._id,
                        id: user.sp_user._id,
                        sp_name: `${user.sp_user.first_name} ${user.sp_user.last_name}`,
                        sp_id: user.sp_user.empId,
                        sp_designation: user.sp_user.designation ? user.sp_user.designation : '',
                        sp_location: user.sp_user.location ? user.sp_user.location : '',
                        sp_mobile: user.sp_user.mobile_no ? user.sp_user.mobile_no : ''
                    }
                });
            })
    }

    static async getSpecialPointLogsFunction() {
        try {
            const fetched = await UserOfferPointLogModel.find({}).populate('createdBy','first_name last_name email empId').sort({createdAt:-1}).exec();
            console.log(fetched);
            return Promise.all([
                fetched
            ]).then(([logs]) => {
                if (!logs)
                throw new ApplicationError({
                    name: "FetchError",
                    message: "Special Points Log not fetched."
                });
                return { message: `Special points logs fetched.`,
                logs };
            });
        } catch (error) {
            console.log(error);
            throw new ApplicationError({
                name: error.name,
                message: error.message
            });
        }
    }
}

module.exports = FetchUserTask;