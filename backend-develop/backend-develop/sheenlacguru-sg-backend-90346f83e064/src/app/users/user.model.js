const mongoose = require('mongoose');
const nconf = require('nconf');

const Schema = mongoose.Schema;

const SERVER_HOST = nconf.get('APPLICATION:S3');

const LocationSchema = new Schema({
    type: {
        type: String,
        enum: ['Point']
    },
    latitude: {
        type: String
    },
    longitude: {
        type: String
    },
}, {
    _id: false,
    autoIndex: false
});

const BankVerificationStatus = new Schema({
    accountExists : {
        type : String,
        trim : true
    },
    message : {
        type : String,
        trim : true
    },
    bankAccount : {
        type : String,
        trim : true
    },
    ifsc : {
        type : String,
        trim : true
    }
},
{
    _id: false,
    autoIndex: false,
    toJSON: { getters: true }
});

const BankDetailsSchema = new Schema({
    name: {
        type: String,
        trim: true,
        required: [true, 'Bank name is required.']
    },
    account: {
        type: String,
        trim: true,
        required: [true, 'Bank account is required.']
    },
    accountHolder: {
        type: String,
        trim: true
    },
    passbookPhoto: {
        type: String,
        trim: true,
        get: (passbookPhoto) => {
            if (!passbookPhoto) return;
            return `${SERVER_HOST}${passbookPhoto}`;
        }
    },
    ifsc: {
        type: String,
        trim: true,
        required: [true, 'Bank ifsc code is required.']
    },
    branch: {
        type: String,
        trim: true,
        required: [true, 'Bank branch name is required.']
    },
    bvRefId: {
        type: String,
        trim: true
    },
    beneId: {
        type: String,
        trim: true
    },
    bankVerificationStatus: {
        type: BankVerificationStatus
    },
    isBankDetailVerified : {
        type : Boolean,
        default : false
    }
},
    {
        _id: false,
        autoIndex: false,
        toJSON: { getters: true }
    });

const AddressSchema = new Schema({
    line1: {
        type: String,
        trim: true,
        required: [true, 'Address line 1 is required.']
    },
    line2: {
        type: String,
        trim: true,
        required: [true, 'Address line 2 is required.']
    },
    city: {
        type: String,
        trim: true,
        required: [true, 'City is required.']
    },
    state: {
        type: String,
        trim: true,
        required: [true, 'State is required.']
    },
    pincode: {
        type: String,
        trim: true,
        required: [true, 'Pincode is required.']
    },
    country: {
        type: String,
        trim: true,
        required: [true, 'Country is required.']
    },
    landmark: {
        type: String,
        trim: true
    }
},
    {
        _id: false,
        autoIndex: false
    });

const userSchema = new Schema({
    photo: {
        type: String,
        get: (photo) => {
            if (!photo) return;
            return `${SERVER_HOST}${photo}`;
        }
    },
    mobile_number: {
        type: String,
        trim: true,
        unique: true,
        sparse: true,
        required: [true, 'Please enter the mobile number.']
    },
    bp_number: {
        type: String,
        trim: true
    },
    first_name: {
        type: String,
        trim: true
    },
    last_name: {
        type: String,
        trim: true
    },
    alternate_mobile_number: {
        type: String,
        trim: true
    },
    email: {
        type: String,
        trim: true,
        lowercase: true
    },
    gender: {
        type: String,
        enum: ["MALE", "FEMALE"]
    },
    birth_date: {
        type: Date
    },
    pincode: {
        type: String,
        trim: true
    },
    dealer_code: {
        type: String,
        trim: true
    },
    registration_type: {
        type: String,
        enum: ['PAINTER', 'CONTRACTOR']
    },
    proof_type: {
        type: String,
        enum: ['AADHAAR', 'PAN CARD']
    },
    proof: {
        type: [String],
        get: (proofs) => {
            if (!Array.isArray(proofs)) return;
            return proofs.map(proof => `${SERVER_HOST}${proof}`);
        }
    },
    address: {
        type: AddressSchema
    },
    bank: {
        type: BankDetailsSchema
    },
    location: {
        type: LocationSchema
    },
    total: {
        type: Number,
        default: 0
    },
    used: {
        type: Number,
        default: 0
    },
    balance: {
        type: Number,
        default: 0
    },
    underReview: {
        type: Number,
        default: 0
    },
    offerPoints: {
        type: Number,
        default : 0
    },
    status: {
        type: String,
        enum: ['NEW', 'SP_VERIFIED', 'PROFILE_ADDED', 'ADDRESS_ADDED', 'BANK_INFO_ADDED', 'UNDER_REVIEW', 'ON_HOLD', 'APPROVED', 'REJECTED', 'BLACKLISTED'],
        default: 'NEW'
    },
    painter_access: {
        type: String,
        enum: ["No_Scan_Access", "Painter_Under_Contractor", "Individual_Painter", "Contractor"],
        default: "No_Scan_Access"
    },
    added_to_dialer: {
        type: Boolean,
        default: false
    },
    sp_verified: {
        type: Boolean,
        default: false
    },
    sp_id: {
        type: String
    },
    remarks: {
        type: String,
        trim: true
    },
    address_added: {
        type: Boolean,
        default: false
    },
    bank_info_added: {
        type: Boolean,
        default: false
    },
    profile_added: {
        type: Boolean,
        default: false
    },
    sp_verifiedAt: {
        type: Date
    },
    profile_addedAt: {
        type: Date
    },
    profile_updatedAt: {
        type: Date
    },
    address_addedAt: {
        type: Date
    },
    address_updatedAt: {
        type: Date
    },
    bank_info_addedAt: {
        type: Date
    },
    bank_info_updatedAt: {
        type: Date
    },
    registeredAt: {
        type: Date
    },
    under_reviewAt: {
        type: Date
    },
    first_approvedAt: {
        type: Date
    },
    approvedAt: {
        type: Date
    },
    on_holdAt: {
        type: Date
    },
    rejectedAt: {
        type: Date
    },
    blacklistedAt: {
        type: Date
    },
    addedBy: {
        type: Schema.Types.ObjectId
    },
    milestone: {
        type: String
    },
    milestoneUpdatedAt: {
        type: Date
    }
},
    {
        timestamps: true
    });

userSchema.virtual('id').get(function () {
    return this._id.toHexString();
});

userSchema.pre('save', function (next) {
    if (!this.isModified('milestone')) return next();

    return bcrypt.hash(this.password, PASSWORD_SALT_FACTOR).then((hash) => this.password = hash);
});

userSchema.virtual('sp_user', {
    ref: 'Admin',
    localField: 'sp_id',
    foreignField: 'empId',
    justOne: true
});

userSchema.set('toJSON', { virtuals: true, getters: true });

module.exports = mongoose.model("User", userSchema);