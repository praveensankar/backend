const _ = require('lodash');
const ApplicationError = require('@src/errors/ApplicationError');
const UserModel = require('../user.model');

class ListPainterLedger {

    static async exec(query = {}, options = {}) {
        console.log(options)
        const skip = options.page > 0 ? (options.page - 1) * options.size : 0;
        let users = [];
        let userCount = 0;
        // search when search key exist in options
        if (options.searchText !== "") {
            /* Start of Search */
            const listUsers = [];
            /* ------- Search with exact match using text based search */
            if (options.searchText.split("-")[0] === "SH") options.searchText = options.searchText.split("-")[1];
            const usersWithExactMatch = await UserModel.find({
                $text: {
                    $search: options.searchText
                }
            }).where("status").equals("APPROVED")
            // insert list of searched users
            listUsers.push(...usersWithExactMatch);
            /* -------- Search with start (first_name,last_name,mobile_number,bp_number) */
            const rgxForStartMatch = (pattern) => new RegExp(`^${pattern}`); // pattern for matching start of searchText
            const searchTextForStartMatch = rgxForStartMatch(options.searchText);
            const usersWithStartMatch = await UserModel.find({
                $or: [{
                    first_name: {
                        $regex: searchTextForStartMatch,
                        $options: 'i'
                    }
                },
                {
                    last_name: {
                        $regex: searchTextForStartMatch,
                        $options: 'i'
                    }
                },
                {
                    mobile_number: {
                        $regex: searchTextForStartMatch,
                        $options: 'i'
                    }
                },
                {
                    bp_number: {
                        $regex: searchTextForStartMatch,
                        $options: 'i'
                    }
                }
                ]
            }).where("status").equals("APPROVED")
            // insert list of searched users
            listUsers.push(...usersWithStartMatch);

            /* -------- Fuzzy search with (first_name,last_name,mobile_number,bp_number) */
            const rgxForFuzzyMatch = (pattern) => new RegExp(`${pattern}`);
            const searchTextForFuzzyMatch = rgxForFuzzyMatch(options.searchText);
            const usersWithFuzzyMatch = await UserModel.find({
                $or: [{
                    first_name: {
                        $regex: searchTextForFuzzyMatch,
                        $options: 'i'
                    }
                },
                {
                    last_name: {
                        $regex: searchTextForFuzzyMatch,
                        $options: 'i'
                    }
                },
                {
                    mobile_number: {
                        $regex: searchTextForFuzzyMatch,
                        $options: 'i'
                    }
                },
                {
                    bp_number: {
                        $regex: searchTextForFuzzyMatch,
                        $options: 'i'
                    }
                }
                ]
            }).where("status").equals("APPROVED")
            // insert list of searched user
            listUsers.push(...usersWithFuzzyMatch);
            /* generate list of distinct users if users got repeated */
            const map = new Map();
            for (const item of listUsers) {
                if (!map.has(item.id)) {
                    map.set(item.id, true);
                    users.push(item);
                }
            }
            userCount = users.length;
            users = users.slice(skip).slice(0, options.size)

            /* End of Search */
        } else {
            /* fetch users if no search key is present on options */
            users = await UserModel.find(query).where('status').equals("APPROVED").skip(skip).limit(options.size).sort({
                createdAt: -1
            }).exec();
            userCount = await UserModel.where('status').equals("APPROVED").countDocuments(query).exec();
        }

        return Promise.all([
            users,
            userCount
        ]).then(([users, count]) => {
            return {
                items: users,
                totalCount: count
            };
        });

    }
     static validation() {
         return {
             "$id": "http://guru.sheenlac.com/schemas/users/list_painter_ledger.json",
             "$async": true,
             "type": "object",
             "properties": {
                 "page": {
                     "type": "integer",
                     "default": 1
                 },
                 "size": {
                     "type": "integer",
                     "default": 10
                 },
                 "searchText": {
                     "type": "string",
                     "default": ""
                 }
             },
             "required": [],
             "additionalProperties": false
         };
     }
}

module.exports = ListPainterLedger;