const ApplicationError = require('@src/errors/ApplicationError');
const { Promise } = require('bluebird');
const AdminModel = require('../../admins/admin.model');
const FetchSalesRepTask = require('../../salesrep/fetch_salesrep/fetch_salesrep.task');
const UserModel = require('../user.model');
const UserStatusModel = require('../user_status.model');
const _ = require('lodash');

class PainterRemappingTask {

    static async listSalesAdvisorFunction(options = {}) {
        const skip = options.page > 0 ? (options.page - 1) * options.size : 0;
        let salesAdvisor = [];
        let salesAdvisorCount = 0;

        // search when search key exist in options
        if (options.searchText !== "") {
            /* Start of Search */

            /* ------- Search with exact match using text based search */
            const salesAdvisorWithExactMatch = await AdminModel.find({
                $text: {
                    $search: options.searchText
                }
            }).populate('role_id');
            /* -------- Search with start (first_name,last_name,mobile_number,email,empId) */
            const rgxForStartMatch = (pattern) => new RegExp(`^${pattern}`); // pattern for matching start of searchText
            const searchTextForStartMatch = rgxForStartMatch(options.searchText);
            const salesAdvisorWithStartMatch = await AdminModel.find({
                $or: [{
                        first_name: {
                            $regex: searchTextForStartMatch,
                            $options: 'i'
                        }
                    },
                    {
                        last_name: {
                            $regex: searchTextForStartMatch,
                            $options: 'i'
                        }
                    },
                    {
                        mobile_number: {
                            $regex: searchTextForStartMatch,
                            $options: 'i'
                        }
                    },
                    {
                        email: {
                            $regex: searchTextForStartMatch,
                            $options: 'i'
                        }
                    },
                    {
                        empId: {
                            $regex: searchTextForStartMatch,
                            $options: 'i'
                        }
                    }
                ]
            }).populate('role_id');

            /* -------- Fuzzy search with (first_name,last_name,mobile_number,email) */
            const rgxForFuzzyMatch = (pattern) => new RegExp(`${pattern}`);
            const searchTextForFuzzyMatch = rgxForFuzzyMatch(options.searchText);
            const salesAdvisorWithFuzzyMatch = await AdminModel.find({
                $or: [{
                        first_name: {
                            $regex: searchTextForFuzzyMatch,
                            $options: 'i'
                        }
                    },
                    {
                        last_name: {
                            $regex: searchTextForFuzzyMatch,
                            $options: 'i'
                        }
                    },
                    {
                        mobile_number: {
                            $regex: searchTextForFuzzyMatch,
                            $options: 'i'
                        }
                    },
                    {
                        email: {
                            $regex: searchTextForFuzzyMatch,
                            $options: 'i'
                        }
                    },
                    {
                        empId: {
                            $regex: searchTextForStartMatch,
                            $options: 'i'
                        }
                    }
                ]
            }).populate('role_id');

            const adminlist = _.concat(salesAdvisorWithExactMatch, salesAdvisorWithStartMatch, salesAdvisorWithFuzzyMatch);

            /* generate list of distinct salesAdvisor if salesAdvisor got repeated */
            salesAdvisor = _.concat([], _.uniqBy(adminlist, 'id'));
            salesAdvisorCount = salesAdvisor.length;
            salesAdvisor = salesAdvisor.slice(skip).slice(0, options.size)

            /* End of Search */
        } else {
            /* fetch salesAdvisor if no search key is present on options */
            salesAdvisor = await AdminModel.find().select('-password').populate('role_id').exec();
            salesAdvisorCount = await AdminModel.countDocuments().exec();
        }

        return Promise.all([
            salesAdvisor,
            salesAdvisorCount
        ]).then(([salesAdvisors, count]) => {
            if (!salesAdvisors)
                throw new ApplicationError({
                    status: 404,
                    name: "FetchSalesAdvisorError",
                    message: "Sales Advisor not found."
                });
            const salesAdvisorResult = [];
            salesAdvisors.map(salesAdvisor => {
                if (salesAdvisor.role_id && salesAdvisor.role_id.role_access.generalAccessPreviledges.find(ele => ele === 'canVerifyPainter')) {
                    salesAdvisorResult.push({
                        _id: salesAdvisor._id,
                        id: salesAdvisor._id,
                        sp_name: `${salesAdvisor.first_name} ${salesAdvisor.last_name}`,
                        sp_id: salesAdvisor.empId,
                        sp_status: salesAdvisor.status ? salesAdvisor.status : '',
                        sp_designation: salesAdvisor.designation ? salesAdvisor.designation : '',
                        sp_location: salesAdvisor.location ? salesAdvisor.location : '',
                        sp_mobile: salesAdvisor.mobile_no ? salesAdvisor.mobile_no : ''
                    });
                }
            });
            return {
                items: salesAdvisorResult.slice(skip).slice(0, options.size),
                totalCount: salesAdvisorResult.length
            };
        });
    }

    static async listSalesAdvisorPaintersFunction(document) {
        const skip = document.page > 0 ? (document.page - 1) * document.size : 0;
        let salesAdvisorPainters = [];
        let salesAdvisorPaintersCount = 0;

        // search when search key exist in document
        if (document.searchText !== "") {
            /* Start of Search */

            /* ------- Search with exact match using text based search */
            const salesAdvisorPaintersWithExactMatch = await UserModel.find({
                $text: {
                    $search: document.searchText
                }
            }).where('sp_id').equals(document.sp_id).where('sp_verified').equals(true);
            /* -------- Search with start (first_name,last_name,mobile_number,email,empId) */
            const rgxForStartMatch = (pattern) => new RegExp(`^${pattern}`); // pattern for matching start of searchText
            const searchTextForStartMatch = rgxForStartMatch(document.searchText);
            const salesAdvisorPaintersWithStartMatch = await UserModel.find({
                $or: [{
                        first_name: {
                            $regex: searchTextForStartMatch,
                            $options: 'i'
                        }
                    },
                    {
                        last_name: {
                            $regex: searchTextForStartMatch,
                            $options: 'i'
                        }
                    },
                    {
                        mobile_number: {
                            $regex: searchTextForStartMatch,
                            $options: 'i'
                        }
                    },
                    {
                        email: {
                            $regex: searchTextForStartMatch,
                            $options: 'i'
                        }
                    },
                ]
            }).where('sp_id').equals(document.sp_id).where('sp_verified').equals(true);

            /* -------- Fuzzy search with (first_name,last_name,mobile_number,email) */
            const rgxForFuzzyMatch = (pattern) => new RegExp(`${pattern}`);
            const searchTextForFuzzyMatch = rgxForFuzzyMatch(document.searchText);
            const salesAdvisorPaintersWithFuzzyMatch = await UserModel.find({
                $or: [{
                        first_name: {
                            $regex: searchTextForFuzzyMatch,
                            $options: 'i'
                        }
                    },
                    {
                        last_name: {
                            $regex: searchTextForFuzzyMatch,
                            $options: 'i'
                        }
                    },
                    {
                        mobile_number: {
                            $regex: searchTextForFuzzyMatch,
                            $options: 'i'
                        }
                    },
                    {
                        email: {
                            $regex: searchTextForFuzzyMatch,
                            $options: 'i'
                        }
                    }
                ]
            }).where('sp_id').equals(document.sp_id).where('sp_verified').equals(true);

            const painterlist = _.concat(salesAdvisorPaintersWithExactMatch, salesAdvisorPaintersWithStartMatch, salesAdvisorPaintersWithFuzzyMatch);

            /* generate list of distinct salesAdvisorPainters if salesAdvisorPainters got repeated */
            salesAdvisorPainters = _.concat([], _.uniqBy(painterlist, 'id'));
            salesAdvisorPaintersCount = salesAdvisorPainters.length;
            salesAdvisorPainters = salesAdvisorPainters.slice(skip).slice(0, document.size)

            /* End of Search */
        } else {
            /* fetch salesAdvisorPainters if no search key is present on options */
            salesAdvisorPainters = await UserModel.where('sp_id').equals(document.sp_id).where('sp_verified').equals(true).skip(skip).limit(document.size).exec();
            salesAdvisorPaintersCount = await UserModel.where('sp_id').equals(document.sp_id).where('sp_verified').equals(true).countDocuments().exec();
        }

        return Promise.all([
            salesAdvisorPainters,
            salesAdvisorPaintersCount
        ]).then(([salesAdvisorPainters, count]) => {
            if (!salesAdvisorPainters)
                throw new ApplicationError({
                    status: 404,
                    name: "FetchSalesAdvisorError",
                    message: "Sales Advisor not found."
                });
            return {
                items: salesAdvisorPainters,
                totalCount: count
            };
        });
    }

    static async remapPaintersFunction(document) {
        const salesAdvisor = await FetchSalesRepTask.exec(document.sp_id);
        return UserModel.updateMany({
            _id: { $in: document.users },
            sp_verified: true
        }, {
            $set: {
                sp_id: salesAdvisor.sp_id
            }
        }).exec().then(update_resp => {
            if (update_resp.n < 1 || update_resp.nModified < 1)
                throw new ApplicationError({
                    name: "StatusUpdateError",
                    message: "Painters not remapped."
                });
            for (let index = 0; index < document.users.length; index++) {
                new UserStatusModel({
                    status: "SP_REMAPPED",
                    remarks: `Painter Remapped from sp_id - ${document.previous_sp_id} to ${document.sp_id}`,
                    previous_sp_id: document.previous_sp_id,
                    latest_sp_id: document.sp_id,
                    user: document.users[index],
                    admin: document.admin
                }).save();
            }
            return {
                message : "Remapped the painters"
            };
        });
    }

    static listSalesAdvisorsValidation() {
        return {
            "$id": "http://guru.sheenlac.com/schemas/painterRemapping/list_salesAdvisors.json",
            "$async": true,
            "type": "object",
            "properties": {
                "page": {
                    "type": "integer",
                    "default": 1
                },
                "size": {
                    "type": "integer",
                    "default": 10
                },
                "searchText": {
                    "type": "string",
                    "default": ""
                }
            },
            "required": [],
            "additionalProperties": false
        };
    }

    static listSalesAdvisorPaintersValidation() {
        return {
            "$id": "http://guru.sheenlac.com/schemas/painterRemapping/list_salesAdvisor_painters.json",
            "$async": true,
            "type": "object",
            "properties": {
                "sp_id": {
                    "type" : "string"
                },
                "page": {
                    "type": "integer",
                    "default": 1
                },
                "size": {
                    "type": "integer",
                    "default": 10
                },
                "searchText": {
                    "type": "string",
                    "default": ""
                }
            },
            "required": ["sp_id"],
            "additionalProperties": false
        };
    }

        static remapPaintersValidation() {
            return {
                "$id": "http://guru.sheenlac.com/schemas/painterRemapping/remap_painters_validation.json",
                "$async": true,
                "type": "object",
                "properties": {
                    "sp_id": {
                        "type": "string"
                    },
                    "previous_sp_id": {
                        "type": "string"
                    },
                    "users": {
                        "type": "array",
                        "minItems": 1,
                        "uniqueItems": true,
                        "items": {
                            "type": "string"
                        }
                    },
                },
                "required": ["sp_id", "users", "previous_sp_id"],
                "additionalProperties": false
            };
        }
    
}

module.exports = PainterRemappingTask;