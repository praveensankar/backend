const ApplicationError = require("@src/errors/ApplicationError");
const UserModel = require('../user.model');

class UpdateLedgerTask {

    static credit(id, points = 0, session) {
        return UserModel
            .updateOne({
                "_id": id
            }, {
                $inc: {
                    total: points,
                    balance: points,
                    underReview: -points
                }
            }).session(session)
            .exec().then((update) => {
                if (update.n < 1 || update.nModified < 1)
                    throw new ApplicationError({
                        name: 'LedgerUpdateError',
                        message: 'Ledger not updated.'
                    });
                return update;
            });
    }

    static debit(id, points = 0) {
        return UserModel
            .updateOne({
                "_id": id
            }, {
                $inc: {
                    used: points,
                    balance: -points
                }
            })
            .exec().then(update => {
                if (update.n < 1 || update.nModified < 1)
                    throw new ApplicationError({
                        name: 'LedgerUpdateError',
                        message: 'Ledger not updated.'
                    });
                return update;
            });
    }


    static creditUnderReview(id, points = 0, session) {
        return UserModel
            .updateOne({
                "_id": id
            }, {
                $inc: {
                    underReview: points
                }
            }).session(session)
            .exec().then((update) => {
                if (update.n < 1 || update.nModified < 1)
                    throw new ApplicationError({
                        name: 'LedgerUpdateError',
                        message: 'Ledger not updated.'
                    });
                return update;
            });
    }

    static creditReverseTransaction(id, points = 0) {
        return UserModel
            .updateOne({
                "_id": id
            }, {
                $inc: {
                    balance: points,
                    used: -points
                }
            })
            .exec().then((update) => {
                if (update.n < 1 || update.nModified < 1)
                    throw new ApplicationError({
                        name: 'LedgerUpdateError',
                        message: 'Ledger not updated.'
                    });
                return update;
            });
    }

    static debitReverseTransaction(id, points = 0, session) {
        return UserModel
            .updateOne({
                "_id": id
            }, {
                $inc: {
                    total: -points,
                    balance: -points
                }
            }).session(session)
            .exec().then((update) => {
                if (update.n < 1 || update.nModified < 1)
                    throw new ApplicationError({
                        name: 'LedgerUpdateError',
                        message: 'Ledger not updated.'
                    });
                return update;
            });
    }

    static creditBonusPoints(id, points = 0, session) {
        return UserModel
            .updateOne({
                "_id": id
            }, {
                $inc: {
                    total: points,
                    balance: points,
                }
            }).session(session)
            .exec().then((update) => {
                if (update.n < 1 || update.nModified < 1)
                    throw new ApplicationError({
                        name: 'LedgerUpdateError',
                        message: 'Ledger not updated.'
                    });
                return update;
            });
    }

    static creditOfferPoints(id, points = 0, session) {
        return UserModel
            .updateOne({
                "_id": id
            }, {
                $inc: {
                    offerPoints: points
                }
            }).session(session)
            .exec().then((update) => {
                if (update.n < 1 || update.nModified < 1)
                    throw new ApplicationError({
                        name: 'LedgerUpdateError',
                        message: 'Ledger not updated.'
                    });
                return update;
            });
    }
    static debitOfferPoints(id, points = 0, session) {
        return UserModel
            .updateOne({
                "_id": id
            }, {
                $inc: {
                    offerPoints: -points
                }
            }).session(session)
            .exec().then((update) => {
                if (update.n < 1 || update.nModified < 1)
                    throw new ApplicationError({
                        name: 'LedgerUpdateError',
                        message: 'Ledger not updated.'
                    });
                return update;
            });
    }
}

module.exports = UpdateLedgerTask;