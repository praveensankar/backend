const express = require('express');
const router = express.Router();

const Token = require('@src/middleware/token');
const AuthController = require('./auth.controller');

router.post('/', AuthController.register_or_login, Token.generate({ for: 'user' }));

module.exports = router;