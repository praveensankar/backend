const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const pointSchema = new Schema({
    type: {
        type: String,
        enum: ['Point'],
    },
    latitude: {
        type: String,
    },
    longitude: {
        type: String,
    },
    capture: {
        type: String,
        enum: ['LOGIN'],
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
    }
}, 
{
    timestamps: true
});

module.exports = mongoose.model("UserLocation", pointSchema);