const Ajv = require('ajv');
const _ = require('lodash');

const ApplicationError = require('@src/errors/ApplicationError');
const validations = require('@app/validations');
const ajv = new Ajv(validations.ajv_options);

const ListUserTask = require('./list_user/list_user.task');
const FetchUserTask = require('./fetch_user/fetch_user.task');
const CreateProfileTask = require('./create_profile/create_profile.task');
const UpdateProfileTask = require('./update_profile/update_profile.task');
const SaveAddressTask = require('./save_address/save_address.task');
const SaveBankDetailsTask = require('./save_bank_details/save_bank_details.task');
const GenerateBusinessPartnerNumberTask = require('./generate_bp_number/generate_bp_number.task');
const UpdateStatusTask = require('./update_status/update_status.task');
const FetchUserTransactionsTask = require('./fetch_transactions/fetch_transactions.task');
const UserSalesRepVerificationTask = require('./user_salesrep_verification/user_salesrep_verification.task');
const FetchUserTransactionsV2Task = require('./fetch_transactions_v2/fetch_transactions_v2.task');
const ListPainterLedger = require('./list_painter_ledger/list_painter_ledger.task');
const FetchAdminUserTask = require('./fetch_admin_user/fetch_admin_user.task');
const ChangePainterMobileNumber = require("./change_painter_mobile_number/change_painter_mobile_number.task.js");
class UserController {
  static index(req, res, next) {
    if (!ajv.validate(ListUserTask.validation(), req.query)) {
      let errors = ajv.errors;
      return next(
        new ApplicationError({
          status: 400,
          name: "ValidationError",
          message: "Bad request parameters.",
          info: errors,
        })
      );
    }

    let query = _.pick(req.query, ["status"]);
    let options = _.omit(req.query, ["status"]);
    options.search = _.pick(req.query, ["searchText"]);
    return ListUserTask.exec(query, options)
      .then((data) => {
        return res.status(200).json(new res.app.locals.Success({ data: data }));
      })
      .catch((err) => next(err));
  }

  static async show(req, res, next) {
    return FetchAdminUserTask.exec(req.params.id)
      .then((user) => {
        return res
          .status(200)
          .json(new res.app.locals.Success({ data: { user: user[0] } }));
      })
      .catch((err) => next(err));
  }

  static current(req, res, next) {
    return FetchUserTask.exec(req.locals.user_id)
      .then((user) => {
        return res
          .status(200)
          .json(new res.app.locals.Success({ data: { user } }));
      })
      .catch((err) => next(err));
  }

  static userSalesRepVerification(req, res, next) {
    return UserSalesRepVerificationTask.exec(
      req.locals.user_id,
      req.params.sales_rep_id
    )
      .then((op_resp) => {
        return res
          .status(200)
          .json(new res.app.locals.Success({ data: op_resp }));
      })
      .catch((err) => next(err));
  }

  static create(req, res, next) {
    const photos = req.files["photo"];
    const proofs = req.files["proof"];
    if (Array.isArray(photos) && photos.length > 0)
      req.body["photo"] = photos[0].key;
    if (Array.isArray(proofs) && proofs.length > 0)
      req.body["proof"] = proofs.map((proof) => proof.key);
    if (!ajv.validate(CreateProfileTask.validation(), req.body)) {
      let errors = ajv.errors;
      return next(
        new ApplicationError({
          status: 400,
          name: "ValidationError",
          message: "Bad request parameters.",
          info: errors,
        })
      );
    }

    return CreateProfileTask.exec(req.locals.user_id, req.body)
      .then((resp) => {
        return res.status(200).json(new res.app.locals.Success({ data: resp }));
      })
      .catch((err) => next(err));
  }

  static update(req, res, next) {
    const photos = req.files ? req.files["photo"] : "";
    const proofs = req.files ? req.files["proof"] : "";
    if (Array.isArray(photos) && photos.length > 0)
      req.body["photo"] = photos[0].key;
    if (Array.isArray(proofs) && proofs.length > 0)
      req.body["proof"] = proofs.map((file) => file.key);
    if (!ajv.validate(UpdateProfileTask.validation(), req.body)) {
      let errors = ajv.errors;
      return next(
        new ApplicationError({
          status: 400,
          name: "ValidationError",
          message: "Bad request parameters.",
          info: errors,
        })
      );
    }

    let userId =
      req.params && req.params["id"] ? req.params["id"] : req.locals["user_id"];
    let adminId = req.params && req.params["id"] ? req.locals["user_id"] : null;
    let document = _.defaultsDeep(_.cloneDeep(req.body), {
      admin: adminId,
    });
    return UpdateProfileTask.exec(userId, document)
      .then((resp) => {
        req.locals.isAdded = resp.isAdded;
        return next();
      })
      .catch((err) => next(err));
  }

  static user_transactions(req, res, next) {
    let userId =
      req.params && req.params["id"] ? req.params["id"] : req.locals["user_id"];
    return FetchUserTransactionsTask.exec(userId)
      .then((resp) => {
        return res.status(200).json(new res.app.locals.Success({ data: resp }));
      })
      .catch((err) => next(err));
  }

  static user_transactions_v2(req, res, next) {
    if (!ajv.validate(FetchUserTransactionsV2Task.validation(), req.query)) {
      let errors = ajv.errors;
      return next(
        new ApplicationError({
          status: 400,
          name: "ValidationError",
          message: "Bad request parameters.",
          info: errors,
        })
      );
    }
    let userId =
      req.params && req.params["id"] ? req.params["id"] : req.locals["user_id"];
    return FetchUserTransactionsV2Task.exec(userId, req.query)
      .then((resp) => {
        return res.status(200).json(new res.app.locals.Success({ data: resp }));
      })
      .catch((err) => next(err));
  }

  static save_address(req, res, next) {
    if (!ajv.validate(SaveAddressTask.validation(), req.body)) {
      let errors = ajv.errors;
      return next(
        new ApplicationError({
          status: 400,
          name: "ValidationError",
          message: "Bad request parameters.",
          info: errors,
        })
      );
    }

    let userId =
      req.params && req.params["id"] ? req.params["id"] : req.locals["user_id"];
    let adminId = req.params && req.params["id"] ? req.locals["user_id"] : null;
    let document = _.defaultsDeep(
      { address: _.cloneDeep(req.body) },
      { user: userId, admin: adminId }
    );
    return SaveAddressTask.exec(document)
      .then((resp) => {
        req.locals.isAdded = resp.isAdded;
        return next();
      })
      .catch((err) => next(err));
  }

  static save_bank_details(req, res, next) {
    if (req.file) req.body["passbookPhoto"] = req.file.key;
    if (!ajv.validate(SaveBankDetailsTask.validation(), req.body)) {
      let errors = ajv.errors;
      return next(
        new ApplicationError({
          status: 400,
          name: "ValidationError",
          message: "Bad request parameters.",
          info: errors,
        })
      );
    }

    let userId =
      req.params && req.params["id"] ? req.params["id"] : req.locals["user_id"];
    let adminId = req.params && req.params["id"] ? req.locals["user_id"] : null;
    let document = _.defaultsDeep(
      {
        bank: _.cloneDeep(req.body),
      },
      {
        user: userId,
        admin: adminId,
      }
    );
    return SaveBankDetailsTask.exec(document)
      .then((resp) => {
        req.locals.isAdded = resp.isAdded;
        return next();
      })
      .catch((err) => next(err));
  }

  static makeUnderReview(req, res, next) {
    let userId =
      req.params && req.params["id"] ? req.params["id"] : req.locals["user_id"];
    let adminId = req.params && req.params["id"] ? req.locals["user_id"] : null;
    let document = _.defaultsDeep(
      {},
      {
        user: userId,
        empId: req.locals.empId,
        isAdded: req.locals.isAdded,
        admin: adminId,
      }
    );
    return SaveBankDetailsTask.underReviewStatus(document)
      .then((resp) => {
        return res.status(200).json(new res.app.locals.Success({ data: resp }));
      })
      .catch((err) => next(err));
  }

  static generate_bp_number(req, res, next) {
    return GenerateBusinessPartnerNumberTask.exec(req.params.id)
      .then((resp) => {
        return res.status(200).json(new res.app.locals.Success({ data: resp }));
      })
      .catch((err) => next(err));
  }

  static update_status(req, res, next) {
    if (!ajv.validate(UpdateStatusTask.validation(), req.body)) {
      let errors = ajv.errors;
      return next(
        new ApplicationError({
          status: 400,
          name: "ValidationError",
          message: "Bad request parameters.",
          info: errors,
        })
      );
    }

    let document = _.defaultsDeep(_.cloneDeep(req.body), {
      admin: req.locals.user_id,
      user: req.params.id,
    });
    return UpdateStatusTask.exec(document)
      .then((resp) => {
        return res.status(200).json(new res.app.locals.Success({ data: resp }));
      })
      .catch((err) => next(err));
  }

  static listPainterLedger(req, res, next) {
    if (!ajv.validate(ListPainterLedger.validation(), req.query)) {
      let errors = ajv.errors;
      return next(
        new ApplicationError({
          status: 400,
          name: "ValidationError",
          message: "Bad request parameters.",
          info: errors,
        })
      );
    }
    return ListPainterLedger.exec({}, req.query)
      .then((resp) => {
        return res.status(200).json(
          new res.app.locals.Success({
            data: resp,
          })
        );
      })
      .catch((err) => next(err));
  }

  static changePainterMobileNumber(req, res, next) {
    if (!ajv.validate(ChangePainterMobileNumber.validation(), req.body)) {
      let errors = ajv.errors;
      return next(
        new ApplicationError({
          status: 400,
          name: "ValidationError",
          message: "Bad request parameters.",
          info: errors,
        })
      );
    }
    let document = _.defaultsDeep(_.cloneDeep(req.body), {
      admin: req.locals.user_id
    });
    console.log(document);
    return ChangePainterMobileNumber.exec(document)
      .then((resp) => {
        return res.status(200).json(
          new res.app.locals.Success({
            data: resp,
          })
        );
      })
      .catch((err) => {
        next(err);
      });
  }

  static updateSpecialPoints(req, res, next) {
  // if (!ajv.validate(User.validation(), req.body)) {
  //   let errors = ajv.errors;
  //   return next(
  //     new ApplicationError({
  //       status: 400,
  //       name: "ValidationError",
  //       message: "Bad request parameters.",
  //       info: errors,
  //     })
  //   );
  // }
  let document = _.defaultsDeep(_.cloneDeep(req.body), {
    admin: req.locals.user_id
  });
  console.log(document);
  return UpdateProfileTask.updateSpecialPointsFunction(document)
    .then((resp) => {
      return res.status(200).json(
        new res.app.locals.Success({
          data: resp,
        })
      );
    })
    .catch((err) => {
      next(err);
    });
  }

  static userSpecialPointLogs(req, res, next) {
    console.log("controller");
    return FetchUserTask.getSpecialPointLogsFunction()
      .then((resp) => {
        return res.status(200).json(
          new res.app.locals.Success({
            data: resp
          })
        );
      })
      .catch((err) => {
        console.log(err);
        next(err);
      });
  }
}

module.exports = UserController;