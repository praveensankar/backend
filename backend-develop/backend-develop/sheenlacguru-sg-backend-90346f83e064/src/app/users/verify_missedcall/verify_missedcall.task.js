const Axios = require('axios');
const ApplicationError = require('@src/errors/ApplicationError');
const moment = require("moment");

class VerifyMissedCall {

    static exec(options = {}) {
        console.log(options);
        return Axios.get(`http://1.6.65.149/Dropcall.php?phoneno=${options.mobile_number}`, {
                headers: {
                    'Accept': 'application/json',
                    "Content-Type": 'application/json'
                }
            })
            .then(response => {
                if (response.data === 0) {
                    throw new ApplicationError({
                        name: 'MissedCallVerificationError',
                        message: 'Mobile number verification failed.'
                    });
                } else {
                    const resData = response.data.split('\t');
                    const res_status = resData[0];
                    const res_mobile_number = resData[1].slice(13);
                    const res_call_date = resData[2].slice(9)
                    const current_date = moment().add(2, 'm');
                    const server_date = moment(res_call_date);
                    const buffer = current_date.diff(server_date);
                    console.log('current', current_date);
                    console.log(moment(res_call_date));
                    console.log(parseInt(buffer / 1000) + " sec");
                    if (!(parseInt(buffer / 1000) <= 300)) {
                        throw new ApplicationError({
                            name: 'MissedCallVerificationError',
                            message: 'Mobile number verification failed.'
                        });
                    }

                    return {
                        "status": res_status,
                        "mobile_number": res_mobile_number,
                        "call_date": res_call_date
                    }
                }

            });
    }

    static validation() {
        return {
            "$id": "http://guru.sheenlac.com/schemas/users/verify_missedcall.json",
            "type": "object",
            "properties": {
                "mobile_number": {
                    "type": "string"
                }
            },
            "required": ["mobile_number"],
            "additionalProperties": false
        };
    }
}

module.exports = VerifyMissedCall;