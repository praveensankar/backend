const _ = require('lodash');
const ApplicationError = require('@src/errors/ApplicationError');

const UserModel = require('../user.model');
const UserStatusModel = require('../user_status.model');
const FetchSalesRepTask = require('@src/app/salesrep/fetch_salesrep/fetch_salesrep.task');

class UserSalesRepVerificationTask {

    static exec(user_id, sp_id) {
        return FetchSalesRepTask.exec(sp_id)
            .then(sales_rep => {
                console.log(sales_rep)
                return UserModel.where('_id').equals(user_id)
                    .where('status').equals('NEW')
                    .where('sp_verified').equals(false)
                    .updateOne({}, {omitUndefined: true})
                    .set({sp_verified: true, sp_id: sp_id,status: "SP_VERIFIED", sp_verifiedAt : new Date().toISOString(), updatedAt : new Date().toISOString() })
                    .exec();
            })
            .then(update_resp => {
                if (update_resp.n < 1 || update_resp.nModified < 1)
                    throw new ApplicationError({name: 'SPUSERVerificationFailed', message: 'Sales person user verification failed.'});
                new UserStatusModel({
                    status: "SP_VERIFIED",
                    remarks: 'PAINTER ADDED RECORD',
                    user: user_id
                }).save();
                return update_resp;
            });
    }

}

module.exports = UserSalesRepVerificationTask;