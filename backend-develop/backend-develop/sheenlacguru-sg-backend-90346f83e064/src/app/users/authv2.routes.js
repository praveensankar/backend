const express = require('express');
const router = express.Router();

const Token = require('@src/middleware/token');
const AuthController = require('./auth.controller');

router.post('/', AuthController.register_or_login, Token.generate({
    for: 'user'
}));
router.get('/request_otp', AuthController.request_otp);
router.post('/verify_otp', AuthController.verify_otp);
router.post('/resend_otp', AuthController.resend_otp);
router.post('/verify_missedcall', AuthController.verify_missedcall);

module.exports = router;