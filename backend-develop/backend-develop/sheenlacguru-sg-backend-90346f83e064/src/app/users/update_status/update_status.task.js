const Promise = require('bluebird');
const Axios = require('axios');
const _ = require('lodash');

const UserModel = require('../user.model');
const UserStatusModel = require('../user_status.model');

const ApplicationError = require('@src/errors/ApplicationError');

class UpdateStatusTask {

    static exec(document = {}) {
        return UserModel.where('_id').equals(document.user).findOne().exec()
            .then(user => {
                if (!user)
                    throw new ApplicationError({ name: 'UserNotFoundError', message: 'User doest not exists.' });
                
                document = UpdateStatusTask.getStatus(user,document);

                console.log(document.isStatusDifferent)
                document.isStatusDifferent && new UserStatusModel({
                    status: document.status,
                    remarks: document.admin ? 'ADMIN ADDED RECORD' : 'PAINTER ADDED RECORD',
                    user: document.user,
                    admin: document.admin
                }).save();
                return Promise.all([
                    UserModel.where('_id').equals(document.user)
                        .updateOne({}, { omitUndefined: true }).set(document).exec()
                ]);
            }).then(([update_resp]) => {
                if (update_resp.n < 1 || update_resp.nModified < 1)
                    throw new ApplicationError({ name: "StatusUpdateError", message: "User status not updated." });

                UpdateStatusTask.sendDataToDialer(document);

                return { matched: update_resp.n, updated: update_resp.nModified, message: 'User status updated.' }
            });
    }

    static getStatus(user,document){
        const modifyDate = _.toLower(document.status) + 'At';
        document["updatedAt"] = new Date().toISOString();
        if (user.status === document.status) {
            return {
                ...document,
                isStatusDifferent: false
            }
        } else {
            if(document.status !== "APPROVED"){
                document[modifyDate] = new Date().toISOString();
                return {
                    ...document,
                    isStatusDifferent: true
                }
            }else{
                if (document.status === 'APPROVED' && !user.bp_number)
                throw new ApplicationError({ name: 'BPNumberError', message: 'Please generate bp_number.' });
                
                if(!(_.hasIn(user,"first_approvedAt")) && document.status === "APPROVED" && user.bp_number){
                    document = _.defaultsDeep(_.cloneDeep(document), { painter_access : "Individual_Painter", first_approvedAt: new Date().toISOString(), approvedAt: new Date().toISOString(), updatedAt : new Date().toISOString() });
    
                    return {
                        ...document,
                        isStatusDifferent: true
                    }
                }
                if(document.status === "APPROVED" && user.bp_number){
                    document = _.defaultsDeep(_.cloneDeep(document), { painter_access : "Individual_Painter", approvedAt: new Date().toISOString(), updatedAt : new Date().toISOString() });
                    
                    return {
                        ...document,
                        isStatusDifferent: true
                    }
                }
            }
        }
    }

    static sendDataToDialer(document) {
        if (document.status !== 'APPROVED') return;
        UserModel
            .where('_id').equals(document.user)
            .where('status').equals('APPROVED')
            .where('bp_number').exists(true)
            .where('added_to_dialer').equals(false)
            .findOne().exec()
            .then(user => {
                if (!user)
                    throw new ApplicationError({ name: 'UserNotFoundError', message: 'User doest not exists.' });

                const dialer_data = `ListID=${encodeURIComponent("123456")}&PhoneNumber=${encodeURIComponent(user.mobile_number)}&ID=${encodeURIComponent(user.bp_number)}&DealerName=${encodeURIComponent(user.first_name + " " + user.last_name)}&CustomerCode=${encodeURIComponent(user.bp_number)}&PostCode=${encodeURIComponent(user.pincode)}`;

                return Axios.post('http://1.6.65.149/vicidial/LeadUpload.php', dialer_data, { headers: { 'content-type': 'application/x-www-form-urlencoded' } })
                    .then(response => {
                        return UserModel.where('_id').equals(user._id).set('added_to_dialer', true).updateOne().exec();
                    });
            })
            .catch(err => {
                console.error(err);
            });
    }

    static validation() {
        return {
            "$id": "http://guru.sheenlac.com/schemas/users/update_status.json",
            "type": "object",
            "properties": {
                "status": {
                    "type": "string",
                    "enum": ['UNDER_REVIEW','ON_HOLD', 'APPROVED', 'REJECTED', 'BLACKLISTED']
                },
                "remarks": { "type": "string" }
            },
            "required": ["status"],
            "additionalProperties": false
        };
    }

}

module.exports = UpdateStatusTask;