const Promise = require('bluebird');
const _ = require('lodash');
const ApplicationError = require('@src/errors/ApplicationError');
const FetchUserTask = require('../fetch_user/fetch_user.task');
const TransactionModel = require('../../transactions/transaction.model');
const UserModel = require('../../users/user.model');
const OfferModel = require('../../offers/offers.model');
// const PurchaseModel = require('../../purchases/purchase.model');

class FetchUserTransactionsV2Task {

    static async exec(userId, options = {}) {
        const skip = options.page > 0 ? (options.page - 1) * options.size : 0;
        const USER_LEDGER_DETAILS = FetchUserTask.exec(userId)
            .then(user => {
                if (!user)
                    throw new ApplicationError({
                        name: 'UnknownUser',
                        status: 204,
                        message: 'User not found.'
                    });
                user = _.defaultsDeep(user, {
                    total: 0,
                    used: 0,
                    balance: 0
                });
                return _.pick(user, ['total', 'used', 'balance', 'underReview']);
            });

        const TransactionQuery = TransactionModel.find({
            painter: {
                $eq: userId
            }
        });
        if (options.size > 0) TransactionQuery.limit(options.size);
        TransactionQuery.populate('ofGift').populate('ofProduct').populate('ofPurchase').skip(skip);
        const TransactionCountQuery = TransactionModel.find({
            painter: {
                $eq: userId
            }
        }).countDocuments();
        // TransactionQuery.skip(skip);
        // const PurchasesQuery = PurchaseModel.find({ painter : { $eq : userId }});
        // PurchasesQuery.select('status updatedAt').populate({
        //     path:"purchase_transactions",
        //     model : "Transaction",
        //     populate : {
        //         path : 'ofProduct',
        //         model : 'Product'
        //     }
        // });
        let offer_id = '6389a432b2022a7ffd817e95'; // Hardcoded for SWARNA VARSHA
        const offer = await OfferModel.findById(offer_id).select('-__v').lean().exec();
        const offerPointsResult = await UserModel.findOne({ _id: userId,bp_number: { $ne : "" } }).select('-id').exec();
            let getPainterBonusPoint = await TransactionModel.find({
                bp_number : offerPointsResult.bp_number,
                createdAt: {
                    $lte: new Date(offer.end)
                },
                createdAt: {
                    $gte: new Date(offer.start)
                },
                "$or": [{
                    mode : "BONUS-CREDIT"
                    },{
                    mode : "SPECIALOFFER-DEBIT"
                    },{
                    mode : "CREDIT"
                    }
                ]
            });

            let totalPoints = 0;
            let usedPoints = 0;

            getPainterBonusPoint.map((data,index)=> {
                if((data['mode'] == 'BONUS-CREDIT')){
                    totalPoints +=  data['points']
                }

                if(data['mode'] == 'SPECIALOFFER-DEBIT'){
                    if((data['status'] == 'APPROVED') || (data['status'] == 'NEW')){
                        usedPoints +=  data['points']
                    }
                }

                if(data['mode'] == 'CREDIT'){
                    totalPoints +=  data['offerPoints']
                }
            })

        return Promise.all([
            USER_LEDGER_DETAILS,
            TransactionQuery.lean().exec(),
            TransactionCountQuery.exec()
        ]).spread((ledger, fetch_transactions, totalCount) => {
            function pointsComment(mode) {
                switch (mode) {
                    case 'DEBIT':
                        return 'Redeemed Points'
                    case 'CREDIT':
                        return 'Credited Points'
                    case 'REVERSE-CREDIT':
                        return 'Reverse-Credited Points'
                    case 'BONUS-CREDIT':
                        return 'Bonus-Credited Points'
                    default:
                        break;
                }

            }
            // modified transactions
            const debitTxn = fetch_transactions.map(txn => {
                const transaction = _.pick(txn, ['_id', 'points', 'mode', 'status', 'txnId', 'updatedAt', 'ofGift', 'ofProduct', 'ofPurchase']);
                const temp = Object.assign({}, {
                    ...transaction,
                    productName: FetchUserTransactionsV2Task.setProductName(transaction),
                    comment: pointsComment(transaction.mode),
                    purchaseId: FetchUserTransactionsV2Task.setPurchaseId(transaction)
                });
                delete temp.ofPurchase;
                delete temp.ofGift;
                delete temp.ofProduct;
                return temp;
            });
            // modified purchases  -  implement later
            // const modifiedPurchase = purchases.map(purchase => {
            //     const tempTxn = purchase.purchase_transactions.map(txn => {
            //         const transaction = _.pick(txn,['_id','points','status','mode','txnId','updatedAt','ofProduct']);
            //         const temp = Object.assign({},{
            //             ...transaction,
            //             productName : transaction.ofProduct.name,
            //             comment : transaction.mode === 'REVERSE-CREDIT' ? 'Reverse-Credit Points' : 'Credited Points'
            //         });
            //         delete temp.ofProduct;
            //         return temp;
            //     });
            //     const temp = Object.assign({},{
            //         ...purchase,
            //         txnId : null,
            //         modeType : "UUID",
            //         purchase_transactions : tempTxn
            //     });
            //     delete temp.id;    // remove id from purchase
            //     return temp;
            // });
            // const modifiedTransactions = _.concat(modifiedPurchase,...debitTxn);
            const transactions = _.orderBy(debitTxn, ['createdAt', 'updatedAt', 'approvedAt'], ['desc', 'desc', 'desc'])
            ledger['total'] = totalPoints
            ledger['used'] = usedPoints
            ledger['balance'] = totalPoints - usedPoints
            return {
                _id: userId,
                ledger,
                transactions,
                totalCount
            };
        });
    }

    static setProductName(transaction) {
        if (transaction.ofProduct === null) {
            return "-"
        } else {
            if (transaction.mode !== 'DEBIT') {
                return transaction.ofProduct.name;
            } else {
                return transaction.ofGift.name;
            }
        }
    }

    static setPurchaseId(transaction) {
        if (transaction.ofProduct === null) {
            return "-"
        } else {
            if (transaction.mode !== 'DEBIT') {
                return transaction.ofPurchase !== null ? transaction.ofPurchase._id : "";
            } else {
                return null
            }
        }
    }
    static validation() {
        return {
            "$id": "http://guru.sheenlac.com/schemas/fetch_transactions_v2.json",
            "$async": true,
            "type": "object",
            "properties": {
                "page": {
                    "type": "integer",
                    "default": 0
                },
                "size": {
                    "type": "integer",
                    "default": 0
                }
            },
            "required": [],
            "additionalProperties": false
        };
    }
}

module.exports = FetchUserTransactionsV2Task;