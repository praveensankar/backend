const UserModel = require("../user.model");
const ApplicationError = require("@src/errors/ApplicationError");
const UserStatusModel = require("../user_status.model");

class ChangePainterMobileNumber {
  static async exec(doc) {
      const valid = await UserModel.findOne({
        mobile_number: doc.new_mobile_number
      });
      console.log(valid);
      if (valid) {
        throw new ApplicationError({
          name: "AlreadyUsedMobileNumberError",
          message: "User Mobile Number already used.",
        });
      }
      return UserModel.where('_id').equals(doc.userId).where('mobile_number').equals(doc.mobile_number)
        .updateOne({}, {
          omitUndefined: true
        }).set({ mobile_number: doc.new_mobile_number }).exec()
        .then((update_resp) => {
          if (update_resp.n < 1 || update_resp.nModified < 1)
            throw new ApplicationError({
              name: "MobileNumberUpdateError",
              message: "User Mobile Number not updated."
            });
            new UserStatusModel({
              status: "MOBILE_NUMBER_CHANGED",
              remarks: `Changed from ${doc.mobile_number} to ${doc.new_mobile_number}`,
              user: doc.userId,
              admin: doc.admin,
            }).save();
          
          return 'Updated Mobile Number';
        });
  }

  static validation() {
        return {
            "$id": "http://guru.sheenlac.com/schemas/users/changePainterMobileNumber.json",
            "$async": true,
            "type": "object",
            "properties": {
                "mobile_number": {
                    "type": "string",
                    "min" : 10
                },
                "new_mobile_number": {
                    "type": "string",
                    "min" : 10
                },
                "userId": {
                    "type": "string"
                }
            },
            "required": ['mobile_number','userId'],
            "additionalProperties": false
        };
    }
}


module.exports = ChangePainterMobileNumber;