var express = require('express');
var router = express.Router();

const auth_routes = require('@app/admins/auth.routes');
const admin_routes = require('@app/admins/admin.routes');
const milestones_routes = require('@app/milestones/milestones.routes');
const gifts_routes = require('@app/gifts/gifts.routes');
const offers_routes = require('@app/offers/offers.routes');
const products_routes = require('@app/products/products.routes');
const coupons_routes = require('@app/coupons/coupons.routes');
const transaction_routes = require('@app/transactions/transaction.routes');
const purchase_routes = require('@app/purchases/purchase.routes');
const payouts_routes = require('@app/payouts/payouts.routes');

const user_routes = require('@app/users/user.routes');
const trainings_routes = require('@app/trainings/training.routes');
const salesrep_routes = require('@app/salesrep/salesrep.routes');

router.use('/auth', auth_routes);
router.use('/admins', admin_routes);
router.use('/milestones', milestones_routes);
router.use('/gifts', gifts_routes);
router.use('/offers', offers_routes);
router.use('/products', products_routes);
router.use('/coupons', coupons_routes);
router.use('/transactions', transaction_routes);
router.use('/purchases', purchase_routes);
router.use('/payouts', payouts_routes);

router.use('/users', user_routes);
router.use('/trainings', trainings_routes);
router.use('/salesreps', salesrep_routes);

module.exports = router;