var express = require('express');
var router = express.Router();

const auth_routes = require('@app/admins/authv2.routes');
const admin_routes = require('@app/admins/adminv2.routes');
const admin_roles_routes = require('@app/admin_roles/admin_rolesv2.routes');
const milestones_routes = require('@app/milestones/milestonesv2.routes');
const gifts_routes = require('@app/gifts/giftsv2.routes');
const offers_routes = require('@app/offers/offersv2.routes');
const products_routes = require('@app/products/productsv2.routes');
const coupons_routes = require('@app/coupons/couponsv2.routes');
const transaction_routes = require('@app/transactions/transactionv2.routes');
const purchase_routes = require('@app/purchases/purchasev2.routes');

const user_routes = require('@app/users/userv2.routes');
const user_portfolio_routes = require('@app/user_portfolio/user_portfolio.routes');
const trainings_routes = require('@app/trainings/trainingv2.routes');
const salesrep_routes = require('@app/salesrep/salesrepv2.routes');
const payouts_routes = require('@app/payouts/payoutsv2.routes');
const app_update_routes = require('@app/app_update/app_update.routes');
const painter_remapping_routes = require("@app/users/painter_remapping.routes");

router.use('/auth', auth_routes);
router.use('/admins', admin_routes);
router.use('/adminRoles',admin_roles_routes);
router.use('/milestones', milestones_routes);
router.use('/gifts', gifts_routes);
router.use('/offers', offers_routes);
router.use('/products', products_routes);
router.use('/coupons', coupons_routes);
router.use('/transactions', transaction_routes);
router.use('/purchases', purchase_routes);

router.use('/users', user_routes);
router.use('/userPortfolio', user_portfolio_routes);
router.use('/trainings', trainings_routes);
router.use('/salesreps', salesrep_routes);
router.use('/payouts', payouts_routes);
router.use('/appUpdate', app_update_routes);
router.use('/painterRemapping', painter_remapping_routes);

module.exports = router;