const express = require('express');
const router = express.Router();

const Token = require('@src/middleware/token');
const Authorization = require('@src/middleware/authorization');

const MilestoneController = require('./milestones.controller');

router.use(Token.verify({ for: 'admin' }));

router.post('/', Authorization.authorize(["milestonesAndLeaderboardManagement"]), MilestoneController.create);
router.get('/', Authorization.authorize(["milestonesAndLeaderboardManagement", "catalogs"]), MilestoneController.index);
router.get('/:id', Authorization.authorize(["milestonesAndLeaderboardManagement"]), MilestoneController.show);
router.post('/:id', Authorization.authorize(["milestonesAndLeaderboardManagement"]), MilestoneController.update);
router.delete('/:id', Authorization.authorize(["milestonesAndLeaderboardManagement"]), MilestoneController.delete);

module.exports = router;