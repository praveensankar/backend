const uniqid = require('uniqid');
const ApplicationError = require('@src/errors/ApplicationError');

const MilestoneModel = require('../milestones.model');


class CreateMilestoneTask {

    static exec(document) {
        /*  Generate unique id for milestone. */
        document["id"] = uniqid();
        return new MilestoneModel(document).save()
            .then( (milestone) => {
                if (!milestone)
                    throw new ApplicationError({name: "CreateMilestoneError", message: "Milestone was not created."});
                return milestone;
            });
    }

    static validation() {
        return {
            "$id": "http://guru.sheenlac.com/schemas/create_milestone.json",
            "type": "object",
            "properties": {
                "name": { "type": "string", "minLength": 2 },
                "start": { "type": "string", "format": "date-time"},
                "end": { "type": "string", "format": "date-time" },
                "forSpecialGift": { "type": "boolean", "default": false },
                "isActive": {
                    "type": "boolean",
                    "default": false
                },
                "milestoneType": {
                    "type": "string",
                    "enum": ["BONUS-MILESTONE", "SPECIAL-MILESTONE", "CASH-MILESTONE", "GIFT-MILESTONE"]
                },
                "points": { 
                    "type": "array", 
                    "minItems": 1, 
                    "uniqueItems": true, 
                    "items": {
                        "name": { "type": "string", "minLength": 2 },
                        "points": { "type": "integer", "minimum": 0 }
                    } 
                },
                "bonusPoint": {
                    "type": "string"
                },
                "min": {
                    "type": "string"
                },
                "max": {
                    "type": "string"
                },

            },
            "required": ["name", "points"],
            "additionalProperties": false
        };
    }

}

module.exports = CreateMilestoneTask;