const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const StatePointSchema = new Schema({
    name: {
        type: String,
        required: [true, 'State name is required.'],
        trim: true
    },
    points: {
        type: Number,
        required: [true, 'State points are required.'],
        min: 0
    }
}, 
{
    _id: false,
    autoIndex: false
});

const MilestoneSchema = new Schema({
    id: {
        type: String,
        required: [true, 'Milestone ID is required.'],
        select: true,
        index: true,
        unique: true,
        trim: true
    },
    name: {
        type: String,
        required: [true, 'Milestone Name is required.'],
        select: true,
        trim: true
    },
    start: {
        type: Date
    },
    end: {
        type: Date
    },
    isActive: {
        type: Boolean,
        default: false
    },
    milestoneType: {
        type: String,
        trim: true,
        enum: ['BONUS-MILESTONE', 'SPECIAL-MILESTONE', 'CASH-MILESTONE', 'GIFT-MILESTONE']
    },
    points: {
        type: [StatePointSchema],
        required: [true, 'Milestone Points are required.']
    },
    bonusPoint: {
        type: Number,
        default: 0
    },
    min: {
        type: Number
    },
    max: {
        type: Number
    },
    forSpecialGift: {
        type: Boolean,
        default: false,
    },
    created_by: {
        type: Schema.Types.ObjectId,
        ref: 'Admin',
        required: [true, 'Milestone created by is required.']
    },
    updated_by: {
        type: Schema.Types.ObjectId,
        ref: 'Admin',
        required: [true, 'Milestone updated by is required.']
    }
},
{
    timestamps: true
});

module.exports = mongoose.model('Milestone', MilestoneSchema);