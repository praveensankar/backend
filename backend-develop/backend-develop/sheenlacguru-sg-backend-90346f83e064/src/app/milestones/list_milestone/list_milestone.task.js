const Promise = require('bluebird');

const MilestoneModel = require('../milestones.model');

class ListMilestoneTask {

    static exec(query = {}, options = {}) {
        const skip = options.page > 0 ? (options.page - 1) * options.size : 0;
        return Promise.all([
            MilestoneModel.find(query)
                .populate('created_by', 'first_name last_name')
                .populate('updated_by', 'first_name last_name')
                .select('-__v').skip(skip).limit(options.size).sort({updatedAt: -1}).exec(),
            MilestoneModel
                .estimatedDocumentCount().exec()
        ]).then( ([milestones, count]) => {
            return {
                items: milestones,
                totalCount: count
            };
        })
        .catch( (err) => {
            return err;
        });
    }

}

module.exports = ListMilestoneTask;