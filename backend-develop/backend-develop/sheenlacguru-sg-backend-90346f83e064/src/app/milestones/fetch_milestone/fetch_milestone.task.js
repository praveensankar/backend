const MilestoneModel = require('../milestones.model');

class FetchMilestoneTask {

    static exec(milestone_id) {
        return MilestoneModel.where('id').equals(milestone_id)
            .select('-__v')
            .findOne()
            .populate('created_by', 'first_name last_name')
            .populate('updated_by', 'first_name last_name')
            .exec();
    }

     static getByName(name) {
         return MilestoneModel.where('name').equals(name)
             .select('-__v')
             .findOne()
             .populate('created_by', 'first_name last_name')
             .populate('updated_by', 'first_name last_name')
             .exec();
     }

}

module.exports = FetchMilestoneTask;