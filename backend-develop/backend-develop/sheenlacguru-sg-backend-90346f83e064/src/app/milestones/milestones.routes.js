const express = require('express');
const router = express.Router();

const Token = require('@src/middleware/token');
const Authorization = require('@src/middleware/authorization');

const MilestoneController = require('./milestones.controller');

router.use(Token.verify({ for: 'admin' }));

router.post('/', Authorization.authorize(["operations", "operations-l2"]), MilestoneController.create);
router.get('/', Authorization.authorize(["operations", "operations-l2", "catalogs"]), MilestoneController.index);
router.get('/:id', Authorization.authorize(["operations", "operations-l2"]), MilestoneController.show);
router.post('/:id', Authorization.authorize(["operations", "operations-l2"]), MilestoneController.update);
router.delete('/:id', Authorization.authorize(["operations", "operations-l2"]), MilestoneController.delete);

module.exports = router;