const Ajv = require('ajv');
const _ = require('lodash');

const ApplicationError = require('@src/errors/ApplicationError');
const validations =  require('@app/validations');
const ajv = new Ajv(validations.ajv_options);

const CreateMilestoneTask = require('./create_milestone/create_milestone.task');
const ListMilestoneTask = require('./list_milestone/list_milestone.task');
const FetchMilestoneTask = require('./fetch_milestone/fetch_milestone.task');
const UpdateMilestoneTask = require('./update_milestone/update_milestone.task');
const DeleteMilestoneTask = require('./delete_milestone/delete_milestone.task');


class MilestonesController {

    static index(req, res, next) {
        if (!ajv.validate(validations.list_options, req.query)) {
            let errors = ajv.errors;
            return next(
                new ApplicationError({status: 400, name: "ValidationError", message: "Bad request parameters.", info: errors})
            );
        }

        return ListMilestoneTask.exec({}, req.query)
                    .then( (data) => {
                        return res.status(200).json(
                            new res.app.locals.Success({data: data})
                        );
                    })
                    .catch( (err) => {
                       return next(err);
                    });
    }

    static show(req, res, next) {
        return FetchMilestoneTask.exec(req.params.id)
                    .then( (milestone) => {
                        return res.status(200).json(
                            new res.app.locals.Success({data: { milestone } })
                        );
                    })
                    .catch( (err) => {
                        return next(err);
                    });
    }

    static create(req, res, next) {
        if (!ajv.validate(CreateMilestoneTask.validation(), req.body)) {
            let errors = ajv.errors;
            return next(
                new ApplicationError({status: 400, name: "ValidationError", message: "Bad request parameters.", info: errors})
            );
        }
        let user_id = req.locals.user_id;
        let document = _.defaultsDeep(_.cloneDeep(req.body), { created_by: user_id, updated_by: user_id });
        return CreateMilestoneTask.exec(document)
                    .then( (_) => {
                        return res.status(200).json(
                            new res.app.locals.Success({ data: {message: "Milestone created."}})
                        );
                    })
                    .catch( (err) => {
                        return next(err);
                    });
    }

    static update(req, res, next) {
        if (!ajv.validate(UpdateMilestoneTask.validation(), req.body)) {
            let errors = ajv.errors;
            return next(
                new ApplicationError({status: 400, name: "ValidationError", message: "Bad request parameters.", info: errors})
            );
        }
        let document = _.defaultsDeep(_.cloneDeep(req.body), { updated_by: req.locals.user_id });
        return UpdateMilestoneTask.exec(req.params.id, document)
                    .then( (op_resp) => {
                        return res.status(200).json(
                            new res.app.locals.Success({data: op_resp})
                        );
                    })
                    .catch( (err) => {
                        return next(err);
                    });
    }

    static delete(req, res, next) {
        return DeleteMilestoneTask.exec(req.params.id)
                    .then( (op_resp) => {
                        return res.status(200).json(
                            new res.app.locals.Success({data: op_resp})
                        );
                    })
                    .catch( (err) => {
                        return next(err);
                    });
    }

}

module.exports = MilestonesController;