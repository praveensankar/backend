const ApplicationError = require('@src/errors/ApplicationError');

const MilestoneModel = require('../milestones.model');

class UpdateMilestoneTask {

    static exec(milestone_id, document) {
        return MilestoneModel.where('id').equals(milestone_id)
            .updateOne({}, {
                omitUndefined: true
            })
            .set(document)
            .exec()
            .then((update_resp) => {
                if (update_resp.n < 1 || update_resp.nModified < 1)
                    throw new ApplicationError({
                        name: "UpdateMilestoneError",
                        message: "Milestone is not updated."
                    });

                return {
                    matched: update_resp.n,
                    updated: update_resp.nModified,
                    message: "Milestone updated."
                };
            });
    }

    static validation() {
        return {
            "$id": "http://guru.sheenlac.com/schemas/update_milestone.json",
            "type": "object",
            "properties": {
                "name": {
                    "type": "string",
                    "minLength": 2
                },
                "start": {
                    "type": "string",
                    "format": "date-time"
                },
                "end": {
                    "type": "string",
                    "format": "date-time"
                },
                "points": {
                    "type": "array",
                    "minItems": 1,
                    "uniqueItems": true,
                    "items": {
                        "name": {
                            "type": "string",
                            "minLength": 2
                        },
                        "points": {
                            "type": "integer",
                            "minimum": 0
                        }
                    }
                },
                "forSpecialGift": {
                    "type": "boolean",
                    "default": false
                },
                "isActive": {
                    "type": "boolean",
                    "default": false
                },
                "milestoneType": {
                    "type": "string",
                    "enum": ["BONUS-MILESTONE", "SPECIAL-MILESTONE", "CASH-MILESTONE", "GIFT-MILESTONE"]
                },
                "bonusPoint": {
                    "type": "string"
                },
                "min": {
                    "type": "string"
                },
                "max": {
                    "type": "string"
                },
            },
            "additionalProperties": false
        };
    }

}

module.exports = UpdateMilestoneTask;