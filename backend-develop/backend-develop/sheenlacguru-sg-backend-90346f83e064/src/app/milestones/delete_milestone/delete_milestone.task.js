const ApplicationError = require('@src/errors/ApplicationError');

const MilestoneModel = require('../milestones.model');

class DeleteMilestoneTask {

    static exec(milestone_id) {
        return MilestoneModel.deleteOne().where('id').equals(milestone_id)
                .exec()
                .then( (op_resp) => {
                    if (op_resp.ok != 1 || op_resp.deletedCount < 1)
                        return ApplicationError({name: "DeleteMilestoneError", message: 'Milestone not deleted.'});

                    return { deleted: op_resp.deletedCount, message: 'Milestone deleted.' };
                });
    }

}

module.exports = DeleteMilestoneTask;