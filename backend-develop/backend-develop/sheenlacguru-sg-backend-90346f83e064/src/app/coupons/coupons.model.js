const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const couponSchema = new Schema({
    id: { 
        type: Number,
        alias : 'ID'
    },
    uuid: {
        type: String,
        required: [true, 'Coupon uid is required.'],
        uppercase: true,
        unique: true,
        trim: true,
        alias : 'UID'
    },
    batch_number_printed: {
        type: String,
        alias: 'BATCH_NUMBER_PRINTED'
    },
    system_name: {
        type: String,
        alias: 'SYSTEMNAME'
    },
    plant: {
        type: String,
        alias: 'PLANT'
    },
    size: {
        type: String,
        alias: 'SIZE'
    },
    product: {
        type: String,
        trim: true,
        required: [true, 'Coupon product is required.'],
        select: true,
        alias : 'PARTNO'
    },
    date_time: {
        type: String,
        alias : 'DATE_TIME'
    },
    created_on: {
        type: String,
        alias: 'CREATEDON'
    }
}, 
{ 
    timestamps: true 
});

couponSchema.set('toJSON', { virtuals: true, getters: true });

couponSchema.virtual('prod', {
    ref: 'Product',
    localField: 'product',
    foreignField: 'id',
    justOne: true
});

module.exports = mongoose.model("Coupon", couponSchema);