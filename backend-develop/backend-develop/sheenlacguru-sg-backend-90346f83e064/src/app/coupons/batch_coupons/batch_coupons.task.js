const Promise = require('bluebird');
const mongoose = require('mongoose');
const _ = require('lodash');
const uniqid = require('uniqid');

const ApplicationError = require('@src/errors/ApplicationError');
const CreatePurchaseTask = require('@src/app/purchases/create_purchase/create_purchase.task');
const GetApprovedUser = require('@src/app/users/get_approved_user/get_approved_user.task');
const ActiveOfferTask = require('@src/app/offers/active_offer/active_offer.task');
const AddTransactionTask = require('@src/app/transactions/add_transaction/add_transaction.task');
const UpdateLedgerTask = require('@src/app/users/update_ledger/update_ledger.task');
const CouponModel = require('../coupons.model');
const TransactionModel = require('../../transactions/transaction.model');

class BatchCouponTask {

    /**
     * Claim multiple coupons 
     * @param {object} document document object 
     * @return return 
     */
    static async exec(document = {}) {

        // Initialize mongoose transaction
        const session = await mongoose.startSession();
        session.startTransaction({
            readConcern: {
                level: "snapshot"
            },
            writeConcern: {
                w: 1
            }
        });

        try {

            // Check transction collection for used uuid's
            let uuids = [];
            let transactionUsedId = [];
            const usedIds = await TransactionModel.where("uuid").in(document.uuid).where("mode").equals("CREDIT").where("status").in(["NEW", "APPROVED"]).select('uuid -_id').session(session).exec();
            if (usedIds.length >= 1) {
                usedIds.map(id => {
                    transactionUsedId.push(id.uuid);
                });
                uuids.push(...BatchCouponTask.usedCouponResponse(transactionUsedId));
            }
            console.log(document.uuid)
            console.log(uuids);
            const filteredIds = _.without(document.uuid, ...transactionUsedId);
            console.log(filteredIds)
            // validate coupons
            let validCouponsId = [];
            const validCoupons = await CouponModel
                .where("uuid").in(filteredIds)
                .populate("prod", "id points")
                .select("uuid product").session(session).exec();
            console.log(validCoupons)
            if (validCoupons.length >= 1) {
                validCoupons.map(coupon => {
                    validCouponsId.push(coupon.uuid);
                });
                const filteredCoupons = _.without(filteredIds, ...validCouponsId);
                console.log("filteredIds", filteredCoupons)
                uuids.push(...BatchCouponTask.invalidCouponResponse(filteredCoupons));
            } else {
                validCoupons.map(coupon => {
                    validCouponsId.push(coupon.uuid);
                });
                const filteredCoupons = _.without(filteredIds, ...validCouponsId);
                console.log("filteredIds", filteredCoupons)
                uuids.push(...BatchCouponTask.invalidCouponResponse(filteredCoupons));
            }
            if (uuids.length > 0) {
                if (validCoupons < 1) {
                    console.log("invalidCoupons", validCoupons);
                    await session.commitTransaction();
                    return _.uniqBy(uuids, "uuid");
                }
                uuids = _.uniqBy(uuids, "uuid");
                console.log(uuids)
            }
            
            // create purchase request
            const create_purchase_request = await CreatePurchaseTask.exec({
                bills: _.get(document, 'bills', undefined),
                painter: _.get(document, 'user', undefined),
                location: _.get(document, 'location', undefined)
            }, session);
            const purchase = await create_purchase_request[0];
            if (!purchase) {
                throw new ApplicationError({
                    status: 400,
                    name: 'InvalidPurchaseError',
                    message: 'Purchase not created'
                });
            }
            // Get approved painter
            const painter = await GetApprovedUser.exec(document, session).then(painter => {
                return _.pick(painter, ['_id', 'bp_number', 'pincode', 'address']);
            });
            if (!painter) {
                throw new ApplicationError({
                    status: 400,
                    name: 'PainterError',
                    message: 'Invalid Painter.'
                });
            }
            
            // return;
            // get active offers
            const activeOffer = await ActiveOfferTask.exec(session);
            
            // create transactions
            let transactions = BatchCouponTask.createTransactions(painter, validCoupons, activeOffer, purchase, document);

            // calculate totalPoints
            let totalPoints = BatchCouponTask.calculateTotalPoints(transactions);

            // Update user ledger
            const updateLedger = await UpdateLedgerTask.creditUnderReview(painter._id, totalPoints, session);
            console.log(updateLedger);
            if (!updateLedger) {
                throw new ApplicationError({
                    status: 400,
                    name: 'PainterError',
                    message: 'Inactive Offer.'
                });
            }

            // Create new transactions
            const addTransaction = await AddTransactionTask.many(transactions, session);
            if (!addTransaction) {
                throw new ApplicationError({
                    status: 400,
                    name: 'TransactionError',
                    message: 'Add Transaction Error'
                });
            }
 
            // // Update coupons status
            // const UPDATE_COUPONS_STATUS = await CouponModel
            //     .updateMany()
            //     .where("uuid").in(validCouponsId)
            //     .where("status").equals("UNUSED")
            //     .set("status", "USED").session(session).exec();

            // if (!UPDATE_COUPONS_STATUS) {
            //     throw new ApplicationError({
            //         status: 400,
            //         name: 'PainterError',
            //         message: 'Inactive Offer.'
            //     });
            // }

            // create response
            let response = BatchCouponTask.createResponse(uuids, transactions);
            // commit transaction
            await session.commitTransaction();
            return response;

        } catch (error) {
            console.log(error)
            await session.abortTransaction();
            throw new ApplicationError({
                name: error.name,
                message: error.message
            });
        } finally {
            session.endSession();
        }
    }

    /**
     * Get UUID Response
     * @param {array} uuids uuids array 
     * @return return uuids status array 
     */
    static invalidCouponResponse(uuids = []) {
        return uuids.map(uuid => {
            return {
                uuid: uuid,
                points: 0,
                status: "INVALID"
            };
        });
    }

     /**
     * Get Used UUID Response
     * @param {array} uuids uuids array 
     * @return return uuids status array 
     */
      static usedCouponResponse(uuids = []) {
        return uuids.map(uuid => {
            return {
                uuid: uuid,
                points: 0,
                status: "ALREADY USED"
            };
        });
    }

    /**
     * Get total points
     * @param {array} transactions transactions array 
     * @return return points
     */
    static calculateTotalPoints(transactions = []) {
        return transactions.reduce((acc, cVal) => {
            acc += _.isFinite(cVal["points"]) ? cVal["points"] : 0;
            return acc;
        }, 0);
    }

    /**
     * Create transctions
     * @param {object} painter painter object
     * @param {array} coupons coupons array
     * @param {object} activeOffer active offer object
     * @param {object} purchase purchase object
     * @param {object} document document object
     * @return returns transactions
     */
    static createTransactions(painter, coupons = [], activeOffer, purchase, document) {
        console.log(painter);
        if (!Array.isArray(coupons) || coupons.length < 1) return [];
        return coupons.map(coupon => {
            let transaction = {
                mode: "CREDIT",
                scan_type: document.scan_type,
                painter: painter._id,
                bp_number: painter.bp_number,
                pincode: painter.pincode,
                purchase: _.get(purchase, '_id', undefined),
                product: coupon.product,
                uuid: coupon.uuid,
                offer: activeOffer ? _.get(activeOffer, 'id', undefined) : '',
                multiplier: 1,
                actual: 0,
                points: 0,
                status: "NEW",
                createdBy: _.get(document, 'user', undefined),
                purchasedOn: new Date().toISOString()
            };
            transaction["txnId"] = uniqid();

            if (_.hasIn(activeOffer, 'multipliers')) {
                let multipliers = _.get(activeOffer, 'multipliers', []);
                _.each(_.intersectionBy(multipliers, [{
                    name: '*'
                }, {
                    name: painter.address.state.toUpperCase()
                    }], 'name'), (val) => {
                    transaction["multiplier"] = _.isFinite(val["multiplier"]) && val['multiplier'] > 0 ? val["multiplier"] : 1;
                    transaction['offer'] = _.isFinite(val["multiplier"]) && val['multiplier'] > 0 ? transaction['offer'] : '';
                    if (val["name"] === "*") {
                        return false;
                    }
                });
            }
            
            _.each(_.intersectionBy(coupon.prod.points, [{
                name: '*'
            }, {
                name: painter.address.state.toUpperCase()
            }], 'name'), (val) => {
                transaction["actual"] = _.isFinite(val["points"]) ? val["points"] : 0;
                transaction["points"] = _.get(transaction, "actual", 0) * _.get(transaction, "multiplier", 1);
                transaction["points"] = Math.floor(transaction["points"])
                if (val["name"] === "*") {
                    return false;
                }
            });
            return transaction;
        });
    }

    /**
     * Create transction response
     * @param {arrays} uuids uuids array
     * @param {arrays} transactions transactions array
     * @return return response
     */
    static createResponse(uuids = [], transactions = []) {
        // let reducedUuids = BatchCouponTask.invalidCouponResponse(uuids);
        let reducedTransactions = transactions.map(transaction => {
            let res = _.pick(transaction, ["uuid", "points"]);
            res["status"] = "POINTS ASSIGNED";
            return res;
        });
        console.log(reducedTransactions);
        return _.unionBy(reducedTransactions, uuids, 'uuid');
    }

    static validation() {
        return {
            "$id": "http://guru.sheenlac.com/schemas/batch_coupon.json",
            "type": "object",
            "properties": {
                "uuid": {
                    "type": "array",
                    "minItems": 1,
                    "maxItems": 100,
                    "uniqueItems": true,
                    "items": {
                        "type": "string",
                        "minLength": 6,
                        "maxLength": 6
                    }
                },
                "bills": {
                    "type": "array",
                    "minItems": 0,
                    "maxItems": 31,
                    "uniqueItems": true,
                    "items": {
                        "type": "string"
                    }
                },
                "scan_type": {
                    "type": "string",
                    "enum": ['AUTO', 'MANUAL', 'ADMIN-MANUAL']
                },
                "location": {
                    "type": "object",
                    "properties": {
                        "type": {
                            "type": "string",
                            "enum": ["Point"]
                        },
                        "coordinates": {
                            "type": "array",
                            "minItems": 2,
                            "items": {
                                "type": "number"
                            }
                        }
                    },
                    "required": ["type", "coordinates"],
                    "additionalProperties": false
                }
            },
            "required": ["uuid"],
            "additionalProperties": false
        };
    }

}

module.exports = BatchCouponTask;