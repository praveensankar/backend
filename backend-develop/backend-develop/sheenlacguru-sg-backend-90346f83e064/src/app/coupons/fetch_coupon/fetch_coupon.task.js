const _ = require('lodash');
const ProductModel = require('../../products/products.model');
const TransactionModel = require('../../transactions/transaction.model');
const Promise = require('bluebird');
const FetchProductTask = require('../../products/fetch_product/fetch_product.task');
const axiosInstance = require('../../../libraries/axiosInstance');
const ProductsModel = require('../../products/products.model');

class FetchTask {

    static async exec(id, type) {
        if (type === 'product') {
            return Promise.all([ProductModel.where('id').equals(id)
                .select('-__v')
                .findOne()
                .exec()]).then(([product]) => {
                    if (product) {
                        return product;
                    } else {
                        return {
                            id: id,
                            points: [],
                            status: "INVALID"
                        };
                    }
                });
        } else {
            return Promise.all([TransactionModel.where("uuid").equals(id).where("mode").equals("CREDIT").where("status").in(["NEW", "APPROVED"]).findOne().populate('purchase').exec()]).then(async ([usedId]) => {
                if (usedId) {
                    return FetchProductTask.exec(usedId.product).then(product => {
                        console.log("ppp",product);
                        if (product) {
                            return {
                                uuid: id,
                                bp_number: usedId.bp_number,
                                product: usedId.product,
                                name: product.name,
                                createdAt: usedId.createdAt,
                                request_id: usedId.purchase.id,
                                points: product.points,
                                status: "ALREADY USED"
                            }
                        }
                    });
                } else {
                    const uuidRegex = /\b[A-Z0-9]{6,6}\b/
                    const valid = uuidRegex.test(id);
                    if (!valid) {
                        return {
                            uuid: id,
                            points: [],
                            status: "INVALID"
                        };
                    }
                    
                    const res = await axiosInstance.post('/uidvalidation', [{
                        "UID": id
                    }]);
                    const validateUuid = _.groupBy(res.data, (ele) => ele.Status);
                    console.log(validateUuid);
                    if (validateUuid.hasOwnProperty('Valid')) {
                        const validProductId = validateUuid.Valid.map(ele => ele.PARTNO);
                        const product = await ProductsModel.where('id').in(validProductId)
                            .select('id name description image points').findOne()
                            .exec();
                        if (!product)
                            throw new ApplicationError({
                                status: 201,
                                name: "FetchProductError",
                                message: "Product not found"
                            });
                        return {
                            uuid: id,
                            product: product.id,
                            name: product.name,
                            points: product.points,
                            status: 'UNUSED'
                        };
                    }
                    if (validateUuid.hasOwnProperty('InValid')) {
                        return {
                            uuid: id,
                            points: [],
                            status: "INVALID"
                        };
                    }
                }
            });
        }
    }
}

module.exports = FetchTask;