const express = require('express');
const nconf = require('nconf');
const router = express.Router();

const Token = require('@src/middleware/token');
const Authorization = require('@src/middleware/authorization');
const Uploader = require('@src/middleware/uploader.middleware');
const CouponController = require('./coupons.controller');

router.use(Token.verify({ for: 'admin' }));

/* Routes for operations admin to manage coupons. */
router.get('/', Authorization.authorize(["operations", "operations-l2"]), CouponController.index);
router.post('/', Authorization.authorize(["operations", "operations-l2"]), CouponController.create);
router.patch('/:uuid/purchases/:purchase', Authorization.authorize(["operations", "operations-l2"]), CouponController.claim);
router.get('/:id', Authorization.authorize(["operations", "operations-l2"]), CouponController.show);

/* Routes for users (painters & contracters) to validate the scanned coupons. */
router.patch('/', Authorization.authorize(["painter", "contractor"]), Uploader({dest: nconf.get("STORAGE:PURCHASES")}).array('bills', 6), CouponController.batch);

module.exports = router;