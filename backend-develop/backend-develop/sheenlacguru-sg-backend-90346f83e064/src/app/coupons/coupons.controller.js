const Ajv = require('ajv');
const _ = require('lodash');

const ApplicationError = require('@src/errors/ApplicationError');
const validations = require('@app/validations');
const ajv = new Ajv(validations.ajv_options);

const CreateCouponTask = require('./create_coupons/create_coupons.task');
const ListCouponTask = require('./list_coupons/list_coupons.task');
const FetchCouponTask = require('./fetch_coupon/fetch_coupon.task');
const BatchCouponTask = require('./batch_coupons/batch_coupons.task');
const ClaimCouponTask = require('./claim_coupon/claim_coupon.task');
const BatchCouponV2Task = require('./batch_coupons_v2/batch_coupons_v2.task');
const { fetchUuid } = require('./fetch_uuid/fetch_uuid.task');
const { daysInMonth, getSpecificDate } = require('../../cron/uuidSyncCron');
const logger = require('../../configs/winston');

class CouponController {

    /**
     * List Coupons
     * @param {object} req request object 
     * @param {object} res response object
     * @param {function} next next function
     * @return return coupons
     */
    static index(req, res, next) {
        if(!ajv.validate(ListCouponTask.validation(), req.query)) {
            let errors = ajv.errors;
            return next(
                new ApplicationError({status: 400, name: "ValidationError", message: "Bad request parameters.", info: errors})
            );
        }

        let query = _.pick(req.query, ["status"]);
        let options = _.omit(req.query, ["status"]);
        return ListCouponTask.exec(query, options)
            .then( data => {
                return res.status(200).json(
                    new res.app.locals.Success({data: data})
                );
            })
            .catch( err => next(err));
    }

    /**
     * Show single coupon
     * @param {object} req request object 
     * @param {object} res response object
     * @param {function} next next function
     * @return return coupon
     */
    static show(req, res, next) {
        return FetchCouponTask.exec(req.params.id,req.query.type)
            .then(coupon => {
                return res.status(200).json(
                    new res.app.locals.Success({ data: { coupon }})
                );
            })
            .catch( err => next(err));
    }

    /**
     * Create new coupon
     * @param {object} req request object 
     * @param {object} res response object
     * @param {function} next next function
     * @return return
     */
    static create(req, res, next) {
        if(!ajv.validate(CreateCouponTask.validation(), req.body)) {
            let errors = ajv.errors;
            return next(
                new ApplicationError({status: 400, name: "ValidationError", message: "Bad request parameters.", info: errors})
            );
        }

        return CreateCouponTask.exec(req.body)
            .then( _ => {
                return res.status(200).json(
                    new res.app.locals.Success({ data: { message: "Coupon created." }})
                );
            })
            .catch( err => next(err));
    }

    /**
     * Claim multiple coupons
     * @param {object} req request object
     * @param {object} res response object
     * @param {function} next next function
     * @return return
     */
    static batch(req, res, next) {
        const bills = req.files;
        if (Array.isArray(bills) && bills.length > 0) req.body['bills'] = bills.map(bill => bill.key);
        if(!ajv.validate(BatchCouponTask.validation(), req.body)) {
            let errors = ajv.errors;
            return next(
                new ApplicationError({name: 'ValidationError', message: 'Bad request parameters.', status: 400, info: errors})
            );
        }
        let document = _.defaultsDeep(_.cloneDeep(req.body), { user: req.locals.user_id });
        return BatchCouponTask.exec(document)
            .then( coupons => {
                return res.status(200).json(
                    new res.app.locals.Success({ data: { coupons }})
                );
            })
            .catch( err => next(err));
    }

    /**
     * Claim multiple coupons Version 2
     * @param {object} req request object
     * @param {object} res response object
     * @param {function} next next function
     * @return return
     */
    static batchV2(req, res, next) {
        const bills = req.files;
        if (Array.isArray(bills) && bills.length > 0) req.body['bills'] = bills.map(bill => bill.key);
        let document = _.defaultsDeep(_.cloneDeep({}), {
            bills: req.body.bills ? req.body.bills : [],
            user: req.locals.user_id,
            uuid: eval(req.body.uuid),
            location: JSON.parse(req.body.location)
        });
        // console.log("document....",document)
        logger.debug({ message: "Document from Android", doc: document });
        if (!ajv.validate(BatchCouponV2Task.validation(), document)) {
            let errors = ajv.errors;
            return next(
                new ApplicationError({
                    name: 'ValidationError',
                    message: 'Bad request parameters.',
                    status: 400,
                    info: errors
                })
            );
        }
        return BatchCouponV2Task.exec(document)
            .then(coupons => {
                return res.status(200).json(
                    new res.app.locals.Success({
                        data: {
                            coupons
                        }
                    })
                );
            })
            .catch(err => next(err));
    }

    /**
     * Claim single coupon
     * @param {object} req request object
     * @param {*} res response object
     * @param {*} next next function
     * @return return
     */
    static claim(req, res, next) {
        let document = _.defaultsDeep(_.cloneDeep(req.params), { admin: req.locals.user_id, scan_type: req.query.scan_type });
        if(!ajv.validate(ClaimCouponTask.validation(), document)) {
            let errors = ajv.errors;
            return next(
                new ApplicationError({name: 'ValidationError', message: 'Bad request parameters.', status: 400, info: errors})
            );
        }

        return ClaimCouponTask.exec(document)
            .then( response => {
                return res.status(200).json(
                    new res.app.locals.Success({ data: { response } })
                );
            })
            .catch( err => next(err));
    }

    static fetchUUID(req, res, next) {
        return fetchUuid(req.body)
            .then((response) => {
                return res.status(200).json(
                    new res.app.locals.Success({
                        data: response
                    })
                );
            })
            .catch((err) => {
                return next(err);
            });
    }

    static async syncCouponDateWiseScheduler(req, res, next){
        const { month, year, startDate = 1 } = req.body;
        const days = daysInMonth(month, year);
        console.log(days)
        for (let index = startDate; index <= days; index++) {
            const date = getSpecificDate(index, month - 1, year);
            console.log(date);
            const uuids = await fetchUuid({
                fromdate: date,
                todate: date
            });
        }
    }

}

module.exports = CouponController;