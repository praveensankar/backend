const Promise = require('bluebird');

const CouponModel = require('../coupons.model');

class ListCouponModel {
    
    static exec(query = {}, options = {}) {
        const skip = options.page  > 0 ? (options.page - 1) * options.size : 0;
        return Promise.all([
            CouponModel.find(query).select('-__v').skip(skip).limit(options.size).exec(),
            CouponModel.countDocuments(query).exec()
        ]).then( ([coupons, count]) => {
            return { items: coupons, totalCount: count };
        });
    }

    static validation() {
        return {
            "$id": "http://guru.sheenlac.com/schemas/coupons/list_options.json",
            "$async": true,
            "type": "object",
            "properties": {
                "page": { "type": "integer", "default": 1 },
                "size": { "type": "integer", "default": 10 },
                "status": { "type": "string" }
            },
            "required": [],
            "additionalProperties": false
        };
    }

}

module.exports = ListCouponModel;