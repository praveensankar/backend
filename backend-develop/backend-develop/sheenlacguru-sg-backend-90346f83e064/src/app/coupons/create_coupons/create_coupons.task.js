const ApplicationError = require('@src/errors/ApplicationError');

const CouponModel = require('../coupons.model');


class CreateCouponTask {

    static exec(document) {
        return new CouponModel(document).save()
            .then( coupon => {
                if (!coupon)
                    throw new ApplicationError({name: 'CreateCouponError', message: 'Coupon was not created.'});
                return coupon;
            });
    }

    static validation() {
        return {
            "$id": "http://guru.sheenlac.com/schemas/create_coupon.json",
            "type": "object",
            "properties": {
                "uuid": { "type": "string", "minLength": 6, "maxLength": 6 },
                "product": { "type": "string", "minLength": 5, "maxLength": 5 },
                "status": { "type": "string", "enum": ["USED", "UNUSED"], "default": "UNUSED" }
            },
            "required": ["uuid", "product"],
            "additionalProperties": false
        };
    }

}

module.exports = CreateCouponTask;