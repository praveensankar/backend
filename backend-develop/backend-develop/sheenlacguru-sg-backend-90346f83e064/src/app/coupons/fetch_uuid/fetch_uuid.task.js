const logger = require("../../../configs/winston");
const axiosInstance = require("../../../libraries/axiosInstance");
const couponsModel = require("../coupons.model");

class FetchUUIDTask  {
    static async fetchUuid(params) {
        try {
            // logger.info({
            //     message: "Fetching Started",
            //     fromdate: params.fromdate,
            //     todate: params.todate
            // });
            const res = await axiosInstance.post('/uidsync', {
                "fromdate": params.fromdate,
                "todate": params.todate
            });
            // logger.info({
            //     message: "Fetched",
            //     fetchedCount: res.data.length
            // });
            const inserted = await couponsModel.insertMany(res.data, {
                ordered: false
            });
            for (let index = 0; index < inserted.length; index++) {
                // logger.info(inserted[index])
            }
            const response = {
                duplicate_error: false,
                duplicate_count: res.data && res.data.length - inserted.length,
                fetched_count: res.data && res.data.length,
                mongo_inserted_count: inserted.length
            }
            // logger.info("Inserting Finished",response);
            return response;

        } catch (error) {
            const response = {
                duplicate_error: true,
                duplicate_count: error.writeErrors.length,
                mongo_inserted_count: error.insertedDocs.length,
                fetched_count: error.writeErrors.length + error.insertedDocs.length
            }
            // logger.warn(response);
            for (let index = 0; index < error.writeErrors.length; index++) {
                // logger.warn(error.writeErrors[index].err.op);
            }
            return response;
        }
    }
}

module.exports = FetchUUIDTask;