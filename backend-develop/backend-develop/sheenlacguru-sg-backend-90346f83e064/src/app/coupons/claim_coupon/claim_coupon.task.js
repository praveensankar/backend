const Promise = require('bluebird');
const mongoose = require('mongoose');
const _ = require('lodash');
const uniqid = require('uniqid');

const ApplicationError = require('@src/errors/ApplicationError');
const FetchPurchaseTask = require('@src/app/purchases/fetch_purchase/fetch_purchase.task');
const ActiveOfferTask = require('@src/app/offers/active_offer/active_offer.task');
const AddTransactionTask = require('@src/app/transactions/add_transaction/add_transaction.task');
const UpdateLedgerTask = require('@src/app/users/update_ledger/update_ledger.task');
const TransactionModel = require('../../transactions/transaction.model');
const axiosInstance = require('../../../libraries/axiosInstance');
const ProductsModel = require('../../products/products.model');
class ClaimCouponTask {

    /**
     * Claim single coupon
     * @param {object} document document object 
     * @return return 
     */
    static async exec(document = {}) {

        // Initialize mongoose transaction
        const session = await mongoose.startSession();
        session.startTransaction({
            readConcern: {
                level: "snapshot"
            },
            writeConcern: {
                w: 1
            }
        });

        try {

            // Fetch valid purchase
            const fetch_purchase = await FetchPurchaseTask.exec(document.purchase, session).then(purchase => purchase ? purchase.toJSON() : undefined);
            if (!fetch_purchase) {
                throw new ApplicationError({
                    status: 400,
                    name: 'InvalidPurchaseError',
                    message: 'Invalid Purchase.'
                });
            }

            const uuidRegex = /\b[A-Z0-9]{6,6}\b/
            document.uuid = document.uuid.trim();
            const valid = uuidRegex.test(document.uuid);
            if (!valid) {
                throw new ApplicationError({
                    status: 400,
                    name: 'ClaimCouponError',
                    message: 'INVALID COUPON'
                });
            }
            
            // Check transction collection for used uuid's
            const usedId = await TransactionModel.findOne().where("uuid").equals(document.uuid).where("mode").equals("CREDIT").where("status").in(["NEW", "APPROVED"]).select('uuid -_id').session(session).exec();
            if (usedId) {
                throw new ApplicationError({
                    status: 400,
                    name: 'ClaimCouponError',
                    message: 'ALREADY USED COUPON'
                });
            }
            
            const res = await axiosInstance.post('/uidvalidation', [{
                "UID": document.uuid
            }]);
            const validateUuid = _.groupBy(res.data, (ele) => ele.Status);
            console.log(validateUuid);
            if (validateUuid.hasOwnProperty('Valid')) {
                const validProductId = validateUuid.Valid.map(ele => ele.PARTNO);
                const product = await ProductsModel.where('id').in(validProductId)
                    .select('id name description image points').findOne()
                    .exec();
                if (!product)
                    throw new ApplicationError({
                        status: 201,
                        name: "FetchProductError",
                        message: "Product not found"
                    });
                const validCoupon = {
                    uuid: document.uuid,
                    productId: product.id,
                    product: product
                }
                // Get active offer
                const activeOffer = await ActiveOfferTask.exec(session);

                // create transction
                let transaction = ClaimCouponTask.createTransaction(fetch_purchase, validCoupon, activeOffer, document);
                if (!transaction) {
                    throw new ApplicationError({
                        name: 'ClaimCouponError',
                        message: 'Error while creating transaction.'
                    });
                }

                // Update user ledger
                const updateLedger = await UpdateLedgerTask.creditUnderReview(transaction.painter, transaction.points, session);
                if (!updateLedger) {
                    return;
                };

                // Create new transaction
                const addTransaction = await AddTransactionTask.single(transaction, session);
                if (!addTransaction) {
                    throw new ApplicationError({
                        name: 'UpdateCouponError',
                        message: 'Error while updating coupon.'
                    });
                }
            };
            if (validateUuid.hasOwnProperty('InValid')) {
                throw new ApplicationError({
                    status: 400,
                    name: 'ClaimCouponError',
                    message: 'INVALID COUPON'
                });
            }

            // commit transaction
            await session.commitTransaction();
            // return
            return {
                message: `Transaction for ${document.uuid} is created.`
            };
        } catch (error) {
            await session.abortTransaction();
            console.log("error");
            throw new ApplicationError({
                name: error.name,
                message: error.message
            });
        } finally {
            session.endSession();
        }
    }

    /**
     * Create transaction object
     * @param {object} purchase purchase object
     * @param {object} coupon coupon object
     * @param {object} activeOffer active offer object
     * @param {object} document document object
     * @return return transaction
     */
    static createTransaction(purchase, coupon, activeOffer, document) {
        const painter = _.pick(purchase.ofPainter, ['_id', 'bp_number', 'pincode', 'address']);
        let transaction = {
            mode: "CREDIT",
            scan_type: document.scan_type,
            painter: painter._id,
            bp_number: painter.bp_number,
            pincode: painter.pincode,
            purchase: purchase._id,
            product: coupon.productId,
            uuid: coupon.uuid,
            offer: activeOffer ? _.get(activeOffer, 'id', undefined) : "",
            multiplier: 1,
            actual: 0,
            points: 0,
            status: "NEW",
            createdBy: _.get(document, 'admin', undefined),
            purchasedOn: new Date().toISOString()
        };

        transaction["txnId"] = uniqid();

        if (_.hasIn(activeOffer, 'multipliers')) {
            let multipliers = _.get(activeOffer, 'multipliers', []);
            _.each(_.intersectionBy(multipliers, [{
                name: '*'
            }, {
                name: painter.address.state.toUpperCase()
            }], 'name'), (val) => {
                transaction["multiplier"] = _.isFinite(val["multiplier"]) && val['multiplier'] > 0 ? val["multiplier"] : 1;
                transaction['offer'] = _.isFinite(val["multiplier"]) && val['multiplier'] > 0 ? transaction['offer'] : '';
                if (val["name"] === "*") {
                    return false;
                }
            });
        }
        _.each(_.intersectionBy(coupon.product.points, [{
            name: '*'
        }, {
            name: painter.address.state.toUpperCase()
        }], 'name'), (val) => {
            transaction["actual"] = _.isFinite(val["points"]) ? val["points"] : 0;
            transaction["points"] = _.get(transaction, "actual", 0) * _.get(transaction, "multiplier", 1);
            transaction["points"] = Math.floor(transaction["points"])
            if (val["name"] === "*") {
                return false;
            }
        });
        return transaction;
    }
    
    static validation() {
        return {
            "$id": "http://guru.sheenlac.com/schemas/claim_coupon.json",
            "type": "object",
            "properties": {
                "uuid": {
                    "type": "string"
                },
                "purchase": {
                    "type": "string",
                    "minLength": 24,
                    "maxLength": 24
                },
                "admin": {
                    "type": "string",
                    "minLength": 24,
                    "maxLength": 24
                },
                "scan_type": {
                    "type": "string",
                    "enum": ['AUTO', 'MANUAL', 'ADMIN-MANUAL']
                },
                "idType": {
                    "type": "string"
                }
            },
            "required": ["uuid", "purchase", "admin"],
            "additionalProperties": false
        };
    }

}

module.exports = ClaimCouponTask;