const CouponModel = require('../coupons.model');

class GetProductCouponsTask {

    static exec(productId) {
        return CouponModel
            .where('product').equals(productId)
            .select('-__v')
            .find().exec();
    }

}

module.exports = GetProductCouponsTask;