const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const couponLogSchema = new Schema({
    syncDate: {
        type: String,
        unique: true,
        trim: true
    }
}, 
{ 
    timestamps: true 
});

couponLogSchema.set('toJSON', {
    virtuals: true,
    getters: true
});

module.exports = mongoose.model("Coupon", couponLogSchema);