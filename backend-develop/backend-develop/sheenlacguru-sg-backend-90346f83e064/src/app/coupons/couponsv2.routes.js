const express = require('express');
const nconf = require('nconf');
const router = express.Router();

const Token = require('@src/middleware/token');
const Authorization = require('@src/middleware/authorization');
const Uploader = require('@src/middleware/uploader.middleware');
const CouponController = require('./coupons.controller');

router.use(Token.verify({ for: 'admin' }));

/* Routes for operations admin to manage coupons. */
router.get('/', CouponController.index);
router.get('/:id', CouponController.show);
router.post('/', CouponController.create);
// router.post('/sync/fetchWithDate', CouponController.fetchUUID);
// router.post('/sync/fetchWithMonth', CouponController.syncCouponDateWiseScheduler);

router.patch('/:uuid/purchases/:purchase', Authorization.authorize(["purchaseRegistrationManagement"]), CouponController.claim);

/* Routes for users (painters & contracters) to validate the scanned coupons. */
router.patch('/', Authorization.authorize(["painter", "contractor"]), Uploader({ dest: nconf.get("STORAGE:PURCHASES") }).array('bills', 31), CouponController.batch);
router.patch('/batch/v2', Authorization.authorize(["painter", "contractor"]), Uploader({
    dest: nconf.get("STORAGE:PURCHASES")
}).array('bills', 31), CouponController.batchV2);

module.exports = router;