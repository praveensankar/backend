const nconf = require('nconf');
const mongoose = require('mongoose');
const _ = require('lodash');
const uniqid = require('uniqid');
const {
    Client
} = require("@googlemaps/google-maps-services-js");

const ApplicationError = require('@src/errors/ApplicationError');
const CreatePurchaseTask = require('@src/app/purchases/create_purchase/create_purchase.task');
const GetApprovedUser = require('@src/app/users/get_approved_user/get_approved_user.task');
const ActiveOfferTask = require('@src/app/offers/active_offer/active_offer.task');
const AddTransactionTask = require('@src/app/transactions/add_transaction/add_transaction.task');
const UpdateLedgerTask = require('@src/app/users/update_ledger/update_ledger.task');
const TransactionModel = require('../../transactions/transaction.model');
const axiosInstance = require('../../../libraries/axiosInstance');
const ProductsModel = require('../../products/products.model');
const logger = require('../../../configs/winston');

class BatchCouponV2Task {

    /**
     * Claim multiple coupons 
     * @param {object} document document object 
     * @return return 
     */
    static async exec(document = {}) {

        // Initialize mongoose transaction
        const session = await mongoose.startSession();
        session.startTransaction({
            readConcern: {
                level: "snapshot"
            },
            writeConcern: {
                w: 1
            }
        });

        try {

            console.log("naf..1...",document)
            // Check transction collection for used uuid's
            document.scan_type = document.uuid;  //Check 1
            document.uuid = _.map(document.uuid, 'uuid');

            // Reverse Geocoding Google API
            const client = new Client({});
            if (document.location && document.location.latitude && document.location.longitude) {
                const reverseGeocodeResponse = await client.reverseGeocode({
                    params: {
                        latlng: `${document.location.latitude},${document.location.longitude}`,
                        key: nconf.get("GOOGLEAPI:GEOCODINGAPIKEY")
                    },
                    timeout: 1000, // milliseconds
                });
                console.log(reverseGeocodeResponse.data.status);
                const reverseGeocode = reverseGeocodeResponse.data.results
                if (reverseGeocodeResponse.data.status === 'OK') {
                    document.location = _.defaultsDeep(_.cloneDeep(document.location), {
                        is_reverse_geocoded: true,
                        formatted_address: reverseGeocode[0].formatted_address,
                        reverseGeocode: reverseGeocode
                    })
                }
            }

            let uuids = [];
            let transactionUsedId = [];
            const regexValidUuid = BatchCouponV2Task.checkUuidFormat(document.uuid);
            console.log(regexValidUuid);
            if (regexValidUuid.hasOwnProperty('validRegexUuid')) {
                document.uuid = regexValidUuid.validRegexUuid;
            }
            if (regexValidUuid.hasOwnProperty('invalidRegexUuid')) {
                uuids.push(...BatchCouponV2Task.invalidCouponResponse(_.uniq(regexValidUuid.invalidRegexUuid)));
            }
            const usedIds = await TransactionModel.where("uuid").in(document.uuid).where("mode").equals("CREDIT").where("status").in(['NEW','APPROVED']).select('uuid -_id').session(session).exec();
            console.log("naf...2..",usedIds)
            if (usedIds.length >= 1) {
                usedIds.map(id => {
                    transactionUsedId.push(id.uuid);
                });
                uuids.push(...BatchCouponV2Task.usedCouponResponse(_.uniq(transactionUsedId)));
            }
            logger.debug({ message: "user send uuid:", uuids: document.uuid });
            const filteredIds = _.without(document.uuid, ...transactionUsedId);
            logger.debug({ message: "filtered uuid after transaction scan:", filteredIds: filteredIds });
            // validate coupons
            const apiUuid = _.map(filteredIds, (ele) => {
                return {
                    "UID": ele
                }
            });
            const res = await axiosInstance.post('/uidvalidation', apiUuid);
            const validateUuid = _.groupBy(res.data, (ele) => ele.Status);
            let validCoupons = [];
            logger.debug({ message: "Validate Ids", uuids: validateUuid });
            if (validateUuid.hasOwnProperty('Valid')) {
                const validProductId = validateUuid.Valid.map(ele => ele.PARTNO);
                const products = await ProductsModel.where('id').in(validProductId)
                    .select('id name description image points')
                    .exec();
                if (!products)
                    throw new ApplicationError({
                        status: 201,
                        name: "FetchProductError",
                        message: "Product not found"
                    });
                validCoupons = validateUuid.Valid.map(validUuid => {
                    const product = products.find(ele => ele.id === validUuid.PARTNO)
                    return {
                        uuid: validUuid.UID,
                        productId: product.id,
                        product: product
                    }
                })
            };
            if (validateUuid.hasOwnProperty('InValid')) {
                uuids.push(...BatchCouponV2Task.invalidCouponResponse(_.uniq(validateUuid.InValid)));
            }
            logger.debug({ message: "Valid UUID", uuid: validCoupons });
            logger.debug({ message: "response uuids:", uuids: uuids });
            // create purchase request
            const create_purchase_request = await CreatePurchaseTask.exec({
                bills: _.get(document, 'bills', undefined),
                painter: _.get(document, 'user', undefined),
                location: _.get(document, 'location', undefined),
            }, session);
            const purchase = await create_purchase_request[0];
            if (!purchase) {
                throw new ApplicationError({
                    status: 400,
                    name: 'InvalidPurchaseError',
                    message: 'Purchase not created'
                });
            }
            // Get approved painter
            const painter = await GetApprovedUser.exec(document, session).then(painter => {
                return _.pick(painter, ['_id', 'bp_number', 'pincode', 'address']);
            });
            if (!painter) {
                throw new ApplicationError({
                    status: 400,
                    name: 'PainterError',
                    message: 'Invalid Painter.'
                });
            }


            // return;
            // get active offers
            const activeOffer = await ActiveOfferTask.exec(session);

            // create transactions
            let transactions = BatchCouponV2Task.createTransactions(painter, validCoupons, activeOffer, purchase, document);
            logger.debug({ message: "Transaction Created", count: transactions.length });
            if (transactions.length) {
                // calculate totalPoints
                let totalPoints = BatchCouponV2Task.calculateTotalPoints(transactions);
    
                // Update user ledger
                const updateLedger = await UpdateLedgerTask.creditUnderReview(painter._id, totalPoints, session);
                console.log(updateLedger);
                if (!updateLedger) {
                    throw new ApplicationError({
                        status: 400,
                        name: 'PainterError',
                        message: 'Inactive Offer.'
                    });
                }
    
                // Create new transactions
                const addTransaction = await AddTransactionTask.many(transactions, session);
                if (!addTransaction) {
                    throw new ApplicationError({
                        status: 400,
                        name: 'TransactionError',
                        message: 'Add Transaction Error'
                    });
                }
    
            }
            // create response
            let response = BatchCouponV2Task.createResponse(uuids, transactions);
            // commit transaction
            await session.commitTransaction();
            return response;

        } catch (error) {
            console.log(error)
            await session.abortTransaction();
            throw new ApplicationError({
                name: error.name,
                message: error.message
            });
        } finally {
            session.endSession();
        }
    }

    /**
     * Check UUID Regex Format
     * @param {array} uuids uuids array 
     * @return return uuids valid array 
     */
    static checkUuidFormat(uuids = []) {
        const invalidRegexUuid = [];
        const validRegexUuid = [];
        uuids.map(uuid => {
            let trimedUUID = uuid.trim();
            const uuidRegex = /\b[A-Z0-9]{6,6}\b/
            const valid = uuidRegex.test(trimedUUID);
            console.log(valid);
            if (valid) {
                validRegexUuid.push(trimedUUID)
            } else {
                invalidRegexUuid.push({ UID: trimedUUID });
            }
        });
        return {
            invalidRegexUuid,
            validRegexUuid
        }
    }

    /**
     * Get UUID Response
     * @param {array} uuids uuids array 
     * @return return uuids status array 
     */
    static invalidCouponResponse(uuids = []) {
        return uuids.map(uuid => {
            return {
                uuid: uuid.UID,
                points: 0,
                status: "INVALID"
            };
        });
    }

    /**
     * Get Used UUID Response
     * @param {array} uuids uuids array 
     * @return return uuids status array 
     */
    static usedCouponResponse(uuids = []) {
        return uuids.map(uuid => {
            return {
                uuid: uuid,
                points: 0,
                status: "ALREADY USED"
            };
        });
    }

    /**
     * Get total points
     * @param {array} transactions transactions array 
     * @return return points
     */
    static calculateTotalPoints(transactions = []) {
        return transactions.reduce((acc, cVal) => {
            acc += _.isFinite(cVal["points"]) ? cVal["points"] : 0;
            return acc;
        }, 0);
    }

    /**
     * Create transctions
     * @param {object} painter painter object
     * @param {array} coupons coupons array
     * @param {object} activeOffer active offer object
     * @param {object} purchase purchase object
     * @param {object} document document object
     * @return returns transactions
     */
    static createTransactions(painter, coupons = [], activeOffer, purchase, document) {
        console.log(painter);
        if (!Array.isArray(coupons) || coupons.length < 1) return [];
        return coupons.map(coupon => {
            const uuidObject = document.scan_type.find(doc => doc.uuid === coupon.uuid);
            let transaction = {
                mode: "CREDIT",
                scan_type: uuidObject ? uuidObject.scan_type : 'AUTO',
                painter: painter._id,
                bp_number: painter.bp_number,
                pincode: painter.pincode,
                purchase: _.get(purchase, '_id', undefined),
                product: coupon.productId,
                uuid: coupon.uuid,
                offer: activeOffer ? _.get(activeOffer, 'id', undefined) : '',
                multiplier: 1,
                actual: 0,
                points: 0,
                status: "NEW",
                createdBy: _.get(document, 'user', undefined),
                purchasedOn: new Date().toISOString()
            };
            transaction["txnId"] = uniqid();

            if (_.hasIn(activeOffer, 'multipliers')) {
                let multipliers = _.get(activeOffer, 'multipliers', []);
                _.each(_.intersectionBy(multipliers, [{
                    name: '*'
                }, {
                    name: painter.address.state.toUpperCase()
                }], 'name'), (val) => {
                    transaction["multiplier"] = _.isFinite(val["multiplier"]) && val['multiplier'] > 0 ? val["multiplier"] : 1;
                    transaction['offer'] = _.isFinite(val["multiplier"]) && val['multiplier'] > 0 ? transaction['offer'] : '';
                    if (val["name"] === "*") {
                        return false;
                    }
                });
            }

            _.each(_.intersectionBy(coupon.product.points, [{
                name: '*'
            }, {
                name: painter.address.state.toUpperCase()
            }], 'name'), (val) => {
                transaction["actual"] = _.isFinite(val["points"]) ? val["points"] : 0;
                transaction["points"] = _.get(transaction, "actual", 0) * _.get(transaction, "multiplier", 1);
                transaction["points"] = Math.floor(transaction["points"])
                if (val["name"] === "*") {
                    return false;
                }
            });
            return transaction;
        });
    }

    /**
     * Create transction response
     * @param {arrays} uuids uuids array
     * @param {arrays} transactions transactions array
     * @return return response
     */
    static createResponse(uuids = [], transactions = []) {
        // let reducedUuids = BatchCouponTask.invalidCouponResponse(uuids);
        let reducedTransactions = transactions.map(transaction => {
            let res = _.pick(transaction, ["uuid", "points"]);
            res["status"] = "POINTS ASSIGNED";
            return res;
        });
        console.log("response", reducedTransactions);
        return _.unionBy(reducedTransactions, uuids, 'uuid');
    }

    static validation() {
        return {
            "$id": "http://guru.sheenlac.com/schemas/batch_coupon_v2.json",
            "type": "object",
            "properties": {
                "uuid": {
                    "type": "array",
                    "minItems": 1,
                    "maxItems": 100,
                    "uniqueItems": true,
                    "items": {
                        "type": "object",
                        "properties": {
                            "uuid": {
                                "type": "string"
                            },
                            "scan_type": {
                                "type": "string",
                                "enum": ['AUTO', 'MANUAL', 'ADMIN-MANUAL']
                            }
                        }
                    }
                },
                "user": {
                    "type": "string"
                },
                "bills": {
                    "type": "array",
                    "minItems": 0,
                    "maxItems": 31,
                    "uniqueItems": true,
                    "items": {
                        "type": "string"
                    }
                },
                "location": {
                    "type": "object",
                    "properties": {
                        "type": {
                            "type": "string",
                            "enum": ["Point"]
                        },
                        "latitude": {
                            "type": "string"
                        },
                        "longitude": {
                            "type": "string"
                        }
                    },
                    "required": ["type", "latitude", "longitude"],
                    "additionalProperties": false
                }
            },
            "required": ["uuid"],
            "additionalProperties": false
        };
    }

}

module.exports = BatchCouponV2Task;