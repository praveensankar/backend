const ApplicationError = require('@src/errors/ApplicationError');
const AdminModel = require('../../admins/admin.model');
const salesrepModel = require('../salesrep.model');
class FetchSalesRepTask {

    static exec(id) {
        return AdminModel.where('empId').equals(id).where('status').equals('ACTIVE').populate('role_id')
            .findOne().exec()
            .then(admin => {
                if (!admin)
                    throw new ApplicationError({
                        status: 404,
                        name: "FetchSalesAdvisorError",
                        message: "Sales Advisor not found."
                    });
                if (!admin.role_id.role_access.generalAccessPreviledges.find(ele => ele === 'canVerifyPainter')) {
                    throw new ApplicationError({
                        status: 404,
                        name: "FetchSalesAdvisorError",
                        message: "Sales Advisor is not valid."
                    });
                }
                return {
                    _id: admin._id,
                    id: admin._id,
                    sp_name: `${admin.first_name} ${admin.last_name}`,
                    sp_id: admin.empId,
                    sp_designation: admin.designation ? admin.designation : '',
                    sp_location: admin.location ? admin.location : '',
                    sp_mobile: admin.mobile_no ? admin.mobile_no : ''
                };
            });
    }

}

module.exports = FetchSalesRepTask;