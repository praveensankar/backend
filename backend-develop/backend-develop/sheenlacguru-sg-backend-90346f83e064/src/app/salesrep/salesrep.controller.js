const FetchSalesRepTask = require('./fetch_salesrep/fetch_salesrep.task');

class SalesRepController {

    static fetchSalesRep(req, res, next) {
        return FetchSalesRepTask.exec(req.params.id)
            .then(salesRep => {
                return res.status(200).json(
                    new res.app.locals.Success({ data: { salesRep, message : "Valid Sales Representative" } })
                );
            })
            .catch(err => {
                return next(err);
            });
    }

}

module.exports = SalesRepController;