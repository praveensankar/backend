const express = require('express');
const router = express.Router();

const Token = require('@src/middleware/token');
const Authorization = require('@src/middleware/authorization');

const SalesRepController = require('./salesrep.controller');

router.use(Token.verify({ for: 'user' }));

router.get('/:id', Authorization.authorize(["painter", "contractor"]), SalesRepController.fetchSalesRep);

module.exports = router;