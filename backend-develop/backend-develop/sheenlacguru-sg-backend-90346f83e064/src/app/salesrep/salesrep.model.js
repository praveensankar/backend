const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const SalesRepSchema = new Schema({
    sp_id: {
        type: String,
        trim: true,
        required: [true, 'Sales person id is required.']
    },
    sp_name: {
        type: String,
        trim: true,
        required: [true, 'Sales person name is required.']
    },
    sp_designation: {
        type: String,
        trim: true,
        required: [true, 'Sales person designation is required.']
    },
    sp_location: {
        type: String,
        trim: true,
        required: [true, 'Sales person location is required.']
    },
    sp_mobile: {
        type: String,
        trim: true,
        required: [true, 'Sales person mobile number is required.']
    }
}, 
{
    timestamps: true
});

SalesRepSchema.set('toJSON', { virtuals: true, getters: true });

module.exports = mongoose.model('SalesRep', SalesRepSchema);