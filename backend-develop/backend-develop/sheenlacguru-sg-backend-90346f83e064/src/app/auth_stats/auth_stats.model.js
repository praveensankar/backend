const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const AuthStatsSchema = new Schema({
    automated_call_count: {
        type: Number,
        default: 0
    },
    otp_count: {
        type: Number,
        default: 0
    },
    missed_call_count: {
        type: Number,
        default: 0
    },
    otp_generated: {
        type: Number,
        default: 0
    },
    otp_verified: {
        type: Number,
        default: 0
    }
}, {
    timestamps: true
});

AuthStatsSchema.virtual('id').get(function () {
    return this._id.toHexString();
});

AuthStatsSchema.set('toJSON', {
    virtuals: true,
    getters: true
});


module.exports = mongoose.model("authStats", AuthStatsSchema);