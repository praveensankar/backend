const express = require('express');
const nconf = require('nconf');
const router = express.Router();

const Token = require('@src/middleware/token');
const Authorization = require('@src/middleware/authorization');
const Uploader = require('@src/middleware/uploader.middleware');

const GiftsController = require('./gifts.controller');

router.use(Token.verify({ for: 'admin' }));

router.post('/claim', Authorization.authorize(["painter", "contractor"]), GiftsController.claim);
router.get('/adminGifts', Authorization.authorize(["giftAndCashRewardCatelogManagement", "catalogs", "painter", "contractor"]), GiftsController.adminGifts);

router.post('/', Authorization.authorize(["giftAndCashRewardCatelogManagement", "catalogs"]), Uploader({dest: nconf.get("STORAGE:GIFTS")}).single('image'), GiftsController.create);
router.get('/', Authorization.authorize(["giftAndCashRewardCatelogManagement", "catalogs", "painter", "contractor"]), GiftsController.index);
router.get('/:id', Authorization.authorize(["giftAndCashRewardCatelogManagement", "catalogs", "painter", "contractor"]), GiftsController.show);
router.post('/:id', Authorization.authorize(["giftAndCashRewardCatelogManagement", "catalogs"]), Uploader({dest: nconf.get("STORAGE:GIFTS")}).single('image'), GiftsController.update);
router.delete('/:id', Authorization.authorize(["giftAndCashRewardCatelogManagement", "catalogs"]), GiftsController.delete);

module.exports = router;