const mongoose = require('mongoose');
const nconf = require('nconf');

const Schema = mongoose.Schema;

const SERVER_HOST = nconf.get('APPLICATION:S3');

const GiftSchema = new Schema({
    id: {
        type: String,
        required: [true, 'Gift ID is required.'],
        select: true,
        index: true,
        unique: true,
        trim: true
    },
    name: {
        type: String,
        required: [true, 'Gift Name is required.'],
        select: true,
        trim: true
    },
    description: {
        type: String,
        required: [true, 'Gift description is required.'],
        trim: true
    },
    type: {
        type: String,
        required: [true, 'Gift type is required.'],
        trim: true,
        enum: ['cash', 'product']
    },
    start: {
        type: Date,
        required: [true, 'Gift start date is required.']
    },
    end: {
        type: Date,
        required: [true, 'Gift end date is required.']
    },
    image: {
        type: String,
        required: [true, 'Gift image is required.'],
        get: (value) => `${SERVER_HOST}${value}`,
        trim: true
    },
    milestone: {
        type: Schema.Types.ObjectId,
        ref: 'Milestone',
        required: [true, 'Gift Milestone is required.']
    },
    created_by: {
        type: Schema.Types.ObjectId,
        ref: 'Admin',
        required: [true, 'Gift created by is required.']
    },
    updated_by: {
        type: Schema.Types.ObjectId,
        ref: 'Admin',
        required: [true, 'Gift updated by is required.']
    }
},
{
    timestamps: true
});

GiftSchema.set('toJSON', { virtuals: true, getters: true });

GiftSchema.virtual('ofMilestone', {
    ref: 'Milestone',
    localField: 'milestone',
    foreignField: '_id',
    justOne: true
});

module.exports = mongoose.model("Gift", GiftSchema);