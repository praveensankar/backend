const GiftModel = require('../gifts.model');

class FetchGiftTask {

    static exec(gift_id) {
        return GiftModel.where('id').equals(gift_id)
            .select('-__v')
            .findOne()
            .populate('milestone', 'name id')
            .exec();
    }

}

module.exports = FetchGiftTask;