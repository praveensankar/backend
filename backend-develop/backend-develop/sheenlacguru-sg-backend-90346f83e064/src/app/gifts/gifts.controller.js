const Ajv = require('ajv');
const _ = require('lodash');

const ApplicationError = require('@src/errors/ApplicationError');
const validations = require('@app/validations');
const ajv = new Ajv(validations.ajv_options);

const CreateGiftTask = require('./create_gift/create_gift.task');
const ListGiftTask = require('./list_gift/list_gift.task');
const FetchGiftTask = require('./fetch_gift/fetch_gift.task');
const UpdateGiftTask = require('./update_gift/update_gift.task');
const DeleteGiftTask = require('./delete_gift/delete_gift.task');
const ClaimGiftTask = require('./claim_gift/claim_gift.task');
const ListUserGiftTask = require('./fetch_admin_gift/fetch_admin_gift.task');
const ListAdminGiftTask = require('./fetch_admin_gift/fetch_admin_gift.task');


class GiftsController {

    static index(req, res, next) {
        if (!ajv.validate(ListGiftTask.validation(), req.query)) {
            let errors = ajv.errors;
            return next(
                new ApplicationError({ status: 400, name: "ValidationError", message: "Bad request parameters.", info: errors })
            );
        }

        let document = _.defaultsDeep(_.cloneDeep(req.query), {
            user_id: req.locals.user_id
        });
        // document['offer_id'] = req.query.id
        return ListGiftTask.exec({}, document)
            .then((data) => {
                return res.status(200).json(
                    new res.app.locals.Success({ data: data })
                );
            })
            .catch((err) => {
                return next(err);
            });
    }

    static adminGifts(req, res, next) {
        if (!ajv.validate(ListAdminGiftTask.validation(), req.query)) {
            let errors = ajv.errors;
            return next(
                new ApplicationError({
                    status: 400,
                    name: "ValidationError",
                    message: "Bad request parameters.",
                    info: errors
                })
            );
        }
        
        return ListAdminGiftTask.exec({},req.query)
            .then((data) => {
                return res.status(200).json(
                    new res.app.locals.Success({
                        data: data
                    })
                );
            })
            .catch((err) => {
                return next(err);
            });
    }

    static show(req, res, next) {
        return FetchGiftTask.exec(req.params.id)
            .then((gift) => {
                return res.status(200).json(
                    new res.app.locals.Success({ data: { gift } })
                );
            })
            .catch((err) => {
                return next(err);
            });
    }

    static create(req, res, next) {
        if (req.file) req.body["image"] = req.file.key;
        if (!ajv.validate(CreateGiftTask.validation(), req.body)) {
            let errors = ajv.errors;
            return next(
                new ApplicationError({ status: 400, name: "ValidationError", message: "Bad request parameters", info: errors })
            );
        }

        let user_id = req.locals.user_id;
        let document = _.defaultsDeep(_.cloneDeep(req.body), { created_by: user_id, updated_by: user_id });
        return CreateGiftTask.exec(document)
            .then((_) => {
                return res.status(200).json(
                    new res.app.locals.Success({ data: { message: "Gift created." } })
                );
            })
            .catch((err) => {
                return next(err);
            });
    }

    static update(req, res, next) {
        if (req.file) req.body["image"] = req.file.key;
        if (!ajv.validate(UpdateGiftTask.validation(), req.body)) {
            let errors = ajv.errors;
            return next(
                new ApplicationError({ status: 400, name: "ValidationError", message: "Bad request parameters.", info: errors })
            );
        }

        let document = _.defaultsDeep(_.cloneDeep(req.body), { updated_by: req.locals.user_id });
        return UpdateGiftTask.exec(req.params.id, document)
            .then((op_resp) => {
                return res.status(200).json(
                    new res.app.locals.Success({ data: op_resp })
                );
            })
            .catch((err) => {
                return next(err);
            });
    }

    static delete(req, res, next) {
        return DeleteGiftTask.exec(req.params.id)
            .then((op_resp) => {
                return res.status(200).json(
                    new res.app.locals.Success({ data: op_resp })
                );
            })
            .catch((err) => {
                return next(err);
            });
    }

    static claim(req, res, next) {
        if (!ajv.validate(ClaimGiftTask.validation(), req.body)) {
            let errors = ajv.errors;
            return next(
                new ApplicationError({ status: 400, name: "ValidationError", message: "Bad request parameters.", info: errors })
            );
        }

        let document = _.defaultsDeep(_.cloneDeep(req.body), { user: req.locals.user_id });
        return ClaimGiftTask.exec(document)
            .then( data => {
                return res.status(200).json(
                    new req.app.locals.Success({ data: data })
                );
            })
            .catch( err => next(err));
    }

}

module.exports = GiftsController;