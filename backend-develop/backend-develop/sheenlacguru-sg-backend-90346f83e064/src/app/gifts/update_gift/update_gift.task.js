const ApplicationError = require('@src/errors/ApplicationError');

const GiftModel = require('../gifts.model');

class UpdateGiftTask {

    static exec(gift_id, document) {
        return GiftModel.where('id').equals(gift_id)
            .updateOne({}, { omitUndefined: true })
            .set(document)
            .exec()
            .then((op_resp) => {
                if (op_resp.n < 1 || op_resp.nModified < 1)
                    throw new ApplicationError({ name: "UpdateGiftError", message: "Gift was not updated." });
                return { matched: op_resp.n, updated: op_resp.nModified, message: "Gift was updated." };
            });
    }

    static validation() {
        return {
            "$id": "http://guru.sheenlac.com/schemas/update_gift.json",
            "type": "object",
            "properties": {
                "name": { "type": "string", "minLength": 2 },
                "description": { "type": "string", "minLength": 4 },
                "type": { "type": "string", "enum": ['cash', 'product'] },
                "start": { "type": "string", "format": "date-time" },
                "end": { "type": "string", "format": "date-time" },
                "image": { "type": "string" },
                "milestone": { "type": "string", "minLength": 8 }
            },
            "additionalProperties": false
        };
    }

}

module.exports = UpdateGiftTask;