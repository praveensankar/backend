const Promise = require('bluebird');

const GiftModel = require('../gifts.model');
const MilestoneModel = require("../../milestones/milestones.model");
const TransactionModel = require('../../transactions/transaction.model');
const OfferModel = require('../../offers/offers.model');
const GetApprovedUser = require('../../users/get_approved_user/get_approved_user.task');
const _ = require('lodash');
class ListGiftTask {

    static async exec(query = {}, options) {
        console.log(options);
        const skip = options.page > 0 ? (options.page - 1) * options.size : 0;
        const today = new Date();
        const getUserDetails = await GetApprovedUser.exec({
            user: options.user_id
        });
        //Cash Gift removed in Diwali Dhamka 2022
        const allNonSpecialGift = await GiftModel.find({
            type: { $in: ['product']},
            start: {
                $lte: today
            },
            end: {
                $gte: today
            }
        }).skip(skip).limit(options.size).select('-__v').populate('milestone', 'name, points milestoneType').exec();
        const filteredGifts = allNonSpecialGift && allNonSpecialGift.filter((gift) => {
            if ((gift.milestone) && (gift.milestone.milestoneType === "CASH-MILESTONE" ||gift.milestone.milestoneType === "GIFT-MILESTONE") && gift.milestone.points[0].points <= getUserDetails.balance
            ) {
                return gift;
            }
        });
        let offer_id = '6389a432b2022a7ffd817e95'; // Hardcoded for SWARNA VARSHA
            let offer = await OfferModel.findById(offer_id).select('-__v').lean().exec();
            let getPainterBonusPoint = await TransactionModel.find({
                bp_number : getUserDetails.bp_number,
                createdAt: {
                    $lte: new Date(offer.end)
                },
                createdAt: {
                    $gte: new Date(offer.start)
                },
                mode : "BONUS-CREDIT"
            });
            let getPainterRejectedPoint = await TransactionModel.find({
                bp_number : getUserDetails.bp_number,
                createdAt: {
                    $lte: new Date(offer.end)
                },
                createdAt: {
                    $gte: new Date(offer.start)
                },
                mode : "SPECIALOFFER-DEBIT",
                status: "REJECTED"
            });
            let rejectedPoint =  Number(_.sumBy(getPainterRejectedPoint, 'points'))
            let bonusPoint = Number(_.sumBy(getPainterBonusPoint, 'points')) 
            console.log(bonusPoint+".........."+rejectedPoint+"........"+getUserDetails.offerPoints)
            bonusPoint = bonusPoint + rejectedPoint
        if ((getUserDetails.offerPoints) || (bonusPoint)) {
            const userOfferPoints = getUserDetails.offerPoints ? getUserDetails.offerPoints + bonusPoint : 0+bonusPoint;
            const selectedMilestones = await MilestoneModel.find({
              milestoneType: "SPECIAL-MILESTONE",
              min: {
                $lte: userOfferPoints,
              },
              max: {
                $gte: userOfferPoints,
              }
            }).exec();
            const selectedMilestoneIds = selectedMilestones.map(response => {
                return response._id
            });
            const giftWithMilestoneIds = await GiftModel.find({
                milestone: {
                    $in: selectedMilestoneIds
                },
                start: {
                    $lte: today
                },
                end: {
                    $gte: today
                }
            }).select('-__v').populate('milestone', 'name').exec();
            const gifts = [...filteredGifts, ...giftWithMilestoneIds];
            return Promise.resolve({
                items: gifts,
                totalCount: gifts.length
            });
        } else {
            return Promise.resolve({
              items: filteredGifts,
              totalCount: filteredGifts.length,
            });
        }
    }

    static validation() {
        return {
            "$id": "http://guru.sheenlac.com/schemas/gift_list_options.json",
            "$async": true,
            "type": "object",
            "properties": {
                "page": {
                    "type": "integer",
                    "default": 1
                },
                "size": {
                    "type": "integer",
                    "default": 0
                },
                "active": {
                    "type": "integer",
                    "default": 0
                }
            },
            "required": [],
            "additionalProperties": false
        }
    }

}

module.exports = ListGiftTask;