const uniqid = require('uniqid');
const GiftModel = require('../gifts.model');

const ApplicationError = require('@src/errors/ApplicationError');

class CreateGiftTask {

    static exec(document) {
        /* Create unique gift id. */
        document["id"] = uniqid();
        return new GiftModel(document).save()
            .then((gift) => {
                if (!gift)
                    throw new ApplicationError({ name: 'CreateGiftError', message: "Gift was not created." });
                return gift;
            });
    }

    static validation() {
        return {
            "$id": "http://guru.sheenlac.com/schemas/create_gift.json",
            "type": "object",
            "properties": {
                "name": { "type": "string", "minLength": 2 },
                "description": { "type": "string", "minLength": 4 },
                "type": { "type": "string", "enum": ['cash', 'product'] },
                "start": { "type": "string", "format": "date-time" },
                "end": { "type": "string", "format": "date-time" },
                "image": { "type": "string" },
                "milestone": { "type": "string", "minLength": 8 }
            },
            "required": ["name", "description", "type", "start", "end", "image", "milestone"],
            "additionalProperties": false
        };
    }
}

module.exports = CreateGiftTask;