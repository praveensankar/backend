const ApplicationError = require('@src/errors/ApplicationError');

const GiftModel = require('../gifts.model');

class DeleteGiftTask {

    static exec(gift_id) {
        return GiftModel.deleteOne()
            .where('id').equals(gift_id)
            .exec()
            .then((op_resp) => {
                if (op_resp.ok != 1 || op_resp.deletedCount < 1)
                    throw new ApplicationError({ name: "DeleteGiftError", message: "Gift was not deleted." });
                return { deleted: op_resp.deletedCount, message: "Gift was deleted." };
            });
    }

}

module.exports = DeleteGiftTask;