const Promise = require('bluebird');

const GiftModel = require('../gifts.model');

class ListAdminGiftTask {

    static exec(query = {}, options = {}) {
        const skip = options.page > 0 ? (options.page - 1) * options.size : 0;
        let Query = ListAdminGiftTask.buildQuery(query, options).skip(skip);
        if (options.size > 0) Query.limit(options.size);
        return Promise.all([
            Query.select('-__v').populate('milestone', 'name').sort({
                updatedAt: -1
            }).exec(),
            ListAdminGiftTask.buildQuery(query, options).countDocuments().exec()
        ]).then(([gifts, count]) => {
            return {
                items: gifts,
                totalCount: count
            };
        });
    }

    static buildQuery(query, options) {
        const today = new Date();
        let Query = GiftModel.find(query);
        if (options.active > 0) Query.where("start").lte(today).where("end").gte(today);
        return Query;
    }

    static validation() {
        return {
            "$id": "http://guru.sheenlac.com/schemas/gift_amin_list_options.json",
            "$async": true,
            "type": "object",
            "properties": {
                "page": {
                    "type": "integer",
                    "default": 1
                },
                "size": {
                    "type": "integer",
                    "default": 0
                },
                "active": {
                    "type": "integer",
                    "default": 0
                }
            },
            "required": [],
            "additionalProperties": false
        }
    }

}

module.exports = ListAdminGiftTask;