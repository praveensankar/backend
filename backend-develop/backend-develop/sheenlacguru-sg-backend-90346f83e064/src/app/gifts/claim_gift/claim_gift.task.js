const Promise = require('bluebird');
const uniqid = require('uniqid');
const _ = require('lodash');
const mongoose = require('mongoose');

const ApplicationError = require('@src/errors/ApplicationError');
const GetApprovedUser = require('@src/app/users/get_approved_user/get_approved_user.task');
const AddTransactionTask = require('@src/app/transactions/add_transaction/add_transaction.task');
const UpdateLedgerTask = require('@src/app/users/update_ledger/update_ledger.task');
const GiftModel = require('../gifts.model');
const OfferModel = require('../../offers/offers.model');
const TransactionModel = require('../../transactions/transaction.model');

class ClaimGiftTask {

    static async exec(document = {}) {

        // start mongoose transaction
        const session = await mongoose.startSession();
        session.startTransaction({
            readConcern: {
                level: "snapshot"
            },
            writeConcern: {
                w: 1
            }
        });

        const APPROVED_PAINTER = GetApprovedUser.exec(document).then(painter => {
            return _.pick(painter, ['_id', 'bp_number', 'pincode', 'address', 'balance','offerPoints']);
        });

        const SELECTED_GIFT = GiftModel
            .where('id').equals(document.gift)
            .findOne()
            .populate('ofMilestone', 'id points milestoneType')
            .select('id milestone type');



        return Promise.all([APPROVED_PAINTER, SELECTED_GIFT.exec()])
            .then(async([painter, gift]) => {
                if (!gift)
                    throw new ApplicationError({
                        name: 'GiftNotFoundError',
                        message: "No gift found."
                    });
                
                console.log(gift.ofMilestone);
                var isSpecialMilestone = gift && gift.ofMilestone.milestoneType === 'SPECIAL-MILESTONE' ? true : false;
                let offer_id = '6389a432b2022a7ffd817e95'; // Hardcoded for SWARNA VARSHA 6389905e5811ea31c2c83ea1 
                const offer = await OfferModel.findById(offer_id).select('-__v').lean().exec();
                    let getPainterBonusPoint = await TransactionModel.find({
                        bp_number : painter.bp_number,
                        createdAt: {
                            $lte: new Date(offer.end)
                        },
                        createdAt: {
                            $gte: new Date(offer.start)
                        },
                        "$or": [{
                            mode : "BONUS-CREDIT"
                            },{
                            mode : "SPECIALOFFER-DEBIT"
                            },{
                            mode : "CREDIT"
                            }
                        ]
                    });
        
                    let totalPoints = 0;
                    let usedPoints = 0;
        
                    getPainterBonusPoint.map((data,index)=> {
                        if((data['mode'] == 'BONUS-CREDIT')){
                            totalPoints +=  data['points']
                        }
        
                        if(data['mode'] == 'SPECIALOFFER-DEBIT'){
                            if((data['status'] == 'APPROVED') || (data['status'] == 'NEW')){
                                usedPoints +=  data['points']
                            }
                        }
        
                        if(data['mode'] == 'CREDIT'){
                            totalPoints +=  data['offerPoints']
                        }
                    })
    
                    painter.offerPoints = totalPoints - usedPoints
                let transaction = {
                    mode: "DEBIT",
                    painter: painter._id,
                    bp_number: painter.bp_number,
                    pincode: painter.pincode,
                    gift: gift.id,
                    gift_type: gift.type,
                    uuid: gift._id,
                    multiplier : 1,
                    status : "NEW"
                };
                transaction["txnId"] = uniqid();
                
                _.each(_.intersectionBy(gift.ofMilestone.points, [{
                            name: '*'
                        }, {
                            name: painter.address.state.toUpperCase()
                        }], 'name'), (val) => {
                    transaction["actual"] = _.isFinite(val["points"]) ? val["points"] : 0;
                    transaction["points"] = _.get(transaction, "actual", 0) * _.get(transaction, "multiplier", 1);
                    transaction["points"] = Math.floor(transaction["points"])

                    if (val["name"] === "*" && _.isFinite(val["points"])) {
                        return false;
                    }
                });
                if (isSpecialMilestone) {
                    transaction["actual"] = painter.offerPoints;
                    transaction["points"] = painter.offerPoints;
                    transaction['mode'] = 'SPECIALOFFER-DEBIT'
                }

                if (!isSpecialMilestone && painter.balance < gift.ofMilestone.points[0].points) {
                    throw new ApplicationError({ name: 'BalanceError', message: 'Insufficient points.' });
                }

                if (isSpecialMilestone && painter.offerPoints <= 0) {
                    throw new ApplicationError({
                        name: 'InsufficientSpecialPointError',
                        message: 'Insufficient special points.'
                    });
                }
                
                return Promise.all([
                    AddTransactionTask.single(transaction,session),
                    isSpecialMilestone ? UpdateLedgerTask.debitOfferPoints(painter._id, painter.offerPoints, session) : UpdateLedgerTask.debit(painter._id, transaction.points, session)
                ]).spread(async (addedTransaction, userLedgerUpdateStatus) => {
                    if (addedTransaction && userLedgerUpdateStatus) {
                        await session.commitTransaction();
                        return  _.pick(transaction, ['points', 'product', 'painter', 'pincode']);
                    } else {
                        await session.abortTransaction();
                        await session.endSession();
                    }
                });
            });
    }

    static validation() {
        return {
            "$id": "http://guru.sheenlac.com/schemas/claim_gift.json",
            "type": "object",
            "properties": {
                "gift": { "type": "string", "minLength": 2 },
            },
            "required": ["gift"],
            "additionalProperties": false
        };
    }

}

module.exports = ClaimGiftTask;