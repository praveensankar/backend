const Ajv = require('ajv');
const _ = require('lodash');

const ApplicationError = require('@src/errors/ApplicationError');
const validations = require('@app/validations');
const ajv = new Ajv(validations.ajv_options);

const CreateProductTask = require('./create_product/create_product.task');
const ListProductTask = require('./list_product/list_product.task');
const FetchProductTask = require('./fetch_product/fetch_product.task');
const UpdateProductTask = require('./update_product/update_product.task');
const DeleteProductTask = require('./delete_product/delete_product.task');
const GetProductCouponsTask = require('@app/coupons/get_product_coupons/get_product_coupons.task');

class ProductsController {

    static index(req, res, next) {
        if (!ajv.validate(ListProductTask.validation(), req.query)) {
            let errors = ajv.errors;
            return next(
                new ApplicationError({ status: 400, name: "ValidationError", message: "Bad request parameters.", info: errors })
            );
        }

        return ListProductTask.exec({}, req.query)
            .then((data) => {
                return res.status(200).json(
                    new res.app.locals.Success({ data: data })
                );
            })
            .catch((err) => {
                return next(err);
            });
    }

    static show(req, res, next) {
        return FetchProductTask.exec(req.params.id)
            .then((product) => {
                return res.status(200).json(
                    new res.app.locals.Success({ data: { product } })
                );
            })
            .catch((err) => {
                return next(err);
            });
    }

    static create(req, res, next) {
        if (req.file) req.body["image"] = req.file.key;
        if (!ajv.validate(CreateProductTask.validation(), req.body)) {
            let errors = ajv.errors;
            return next(
                new ApplicationError({ status: 400, name: "ValidationError", message: "Bad request parameters.", info: errors })
            );
        }
        let user_id = req.locals.user_id;
        let document = _.defaultsDeep(_.cloneDeep(req.body), { created_by: user_id, updated_by: user_id });
        return CreateProductTask.exec(document)
            .then((_) => {
                return res.status(200).json(
                    new res.app.locals.Success({ data: { message: "Product created." } })
                );
            })
            .catch((err) => {
                return next(err);
            });
    }

    static update(req, res, next) {
        if (req.file) req.body["image"] = req.file.key;
        if (!ajv.validate(UpdateProductTask.validation(), req.body)) {
            let errors = ajv.errors;
            return next(
                new ApplicationError({ status: 400, name: "ValidationError", message: "Bad request parameters.", info: errors })
            );
        }
        let document = _.defaultsDeep(_.cloneDeep(req.body), { updated_by: req.locals.user_id });
        return UpdateProductTask.exec(req.params.id, document)
            .then((op_resp) => {
                return res.status(200).json(
                    new res.app.locals.Success({ data: op_resp })
                );
            })
            .catch((err) => {
                return next(err);
            });
    }

    static delete(req, res, next) {
        return DeleteProductTask.exec(req.params.id)
            .then((op_resp) => {
                return res.status(200).json(
                    new res.app.locals.Success({ data: op_resp })
                );
            })
            .catch((err) => {
                return next(err);
            });
    }

    static productCoupons(req, res, next) {
        return GetProductCouponsTask.exec(req.params.id)
            .then( coupons => {
                return res.status(200).json(
                    new res.app.locals.Success({ data: coupons })
                );
            })
            .catch( err => next(err));
    }

}

module.exports = ProductsController;