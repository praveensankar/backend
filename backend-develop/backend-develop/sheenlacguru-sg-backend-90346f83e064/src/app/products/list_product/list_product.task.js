const Promise = require('bluebird');
const _ = require('lodash');
const ProductModel = require('../products.model');

class ListProductTask {

    static async exec(query = {}, options = {}) {
        const skip = options.page > 0 ? (options.page - 1) * options.size : 0;
        let products = [];
        let productCount = 0;

        // search when search key exist in options
        if (options.searchText !== "") {
            /* Start of Search */

            /* ------- Search with exact match using text based search */
            // const productsWithExactMatch = await ProductModel.find({
            //     $text: {
            //         $search: options.searchText
            //     }
            // }).select('id name description image points');
            /* -------- Search with start (first_name,last_name,mobile_number,email,empId) */
            const rgxForStartMatch = (pattern) => new RegExp(`^${pattern}`); // pattern for matching start of searchText
            const searchTextForStartMatch = rgxForStartMatch(options.searchText);
            const productsWithStartMatch = await ProductModel.find({
                $or: [{
                        id: {
                            $regex: searchTextForStartMatch,
                            $options: 'i'
                        }
                    }
                ]
            }).select('id name description image points');

            /* -------- Fuzzy search with (first_name,last_name,mobile_number,email) */
            const rgxForFuzzyMatch = (pattern) => new RegExp(`${pattern}`);
            const searchTextForFuzzyMatch = rgxForFuzzyMatch(options.searchText);
            const productsWithFuzzyMatch = await ProductModel.find({
                $or: [{
                        id: {
                            $regex: searchTextForFuzzyMatch,
                            $options: 'i'
                        }
                    }
                ]
            }).select('id name description image points');

            const productlist = _.concat(productsWithStartMatch, productsWithFuzzyMatch);

            /* generate list of distinct products if products got repeated */
            products = _.concat([], _.uniqBy(productlist, 'id'));
            productCount = products.length;
            products = products.slice(skip).slice(0, options.size)

            /* End of Search */
        } else {
            /* fetch products if no search key is present on options */
            const FetchProducts = ProductModel.find().select('id name description image points');
            if (options.size > 0) FetchProducts.limit(options.size).skip(skip);
            products = FetchProducts.sort({
                createdAt: -1
            }).exec();

            const CountQuery = ProductModel.find();
            // if (options.size > 0) CountQuery.limit(options.size).skip(skip);
            productCount = CountQuery.countDocuments();
        }

        return Promise.all([
            products,
            productCount
        ]).then(([products, count]) => {
            return {
                items: products,
                totalCount: count
            }
        });
        // const skip = options.page > 0 ? (options.page - 1) * options.size : 0;
        // return Promise.all([
        //     ProductModel.find(query)
        //     .select('id name description image points')
        //     .skip(skip).limit(options.size).sort({
        //         name: 1
        //     }).exec(),
        //     ProductModel.estimatedDocumentCount().exec()
        // ]).then(([products, count]) => {
        //     return {
        //         items: products,
        //         totalCount: count
        //     };
        // }).catch((err) => {
        //     return err;
        // });
    }

    static validation() {
        return {
            "$id": "http://guru.sheenlac.com/schemas/products/list_product.json",
            "$async": true,
            "type": "object",
            "properties": {
                "page": {
                    "type": "integer",
                    "default": 1
                },
                "size": {
                    "type": "integer",
                    "default": 10
                },
                "searchText": {
                    "type": "string",
                    "default": ""
                }
            },
            "additionalProperties": false
        };
    }

}

module.exports = ListProductTask;