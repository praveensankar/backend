const express = require('express');
const nconf = require('nconf');
const router = express.Router();

const Token = require('@src/middleware/token');
const Authorization = require('@src/middleware/authorization');
const Uploader = require('@src/middleware/uploader.middleware');

const ProductController = require('./products.controller');

router.use(Token.verify({ for: 'admin' }), Authorization.authorize(["productAndUIDManagement"]));

router.post('/', Uploader({ dest: nconf.get("STORAGE:PRODUCTS") }).single('image'), ProductController.create);
router.get('/', ProductController.index);
router.get('/:id/coupons', ProductController.productCoupons);
router.get('/:id', ProductController.show);
router.post('/:id', Uploader({ dest: nconf.get("STORAGE:PRODUCTS") }).single('image'), ProductController.update);
router.delete('/:id', ProductController.delete);

module.exports = router;