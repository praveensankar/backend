const ApplicationError = require('@src/errors/ApplicationError');

const ProductModel = require('../products.model');

class DeleteProductTask {

    static exec(id) {
        return ProductModel.deleteOne()
            .where('id').equals(id)
            .exec()
            .then((op_resp) => {
                if (op_resp.ok !== 1 || op_resp.deletedCount < 1)
                    throw new ApplicationError({ name: "DeleteProductError", message: "Product was not deleted." });
                return { deleted: op_resp.deletedCount, message: "Product was deleted." };
            });
    }

}

module.exports = DeleteProductTask;