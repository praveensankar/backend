const ApplicationError = require('@src/errors/ApplicationError');

const ProductModel = require('../products.model');

class UpdateProductTask {

    static exec(id, document) {
        return ProductModel.where('id').equals(id)
            .updateOne({}, { omitUndefined: true })
            .set(document).exec()
            .then((update_resp) => {
                if (update_resp.n < 1 || update_resp.nModified < 1)
                    throw new ApplicationError({ name: "UpdateProductError", message: "Product is not updated." });

                return { matched: update_resp.n, updated: update_resp.nModified, message: 'Product updated.' };
            });
    }

    static validation() {
        return {
            "$id": "http://guru.sheenlac.com/schemas/update_product.json",
            "type": "object",
            "properties": {
                "name": { "type": "string", "minLength": 2 },
                "description": { "type": "string","default":"" },
                "image": { "type": "string" },
                "points": {
                    "type": "array",
                    "minItems": 1,
                    "uniqueItems": true,
                    "items": {
                        "name": { "type": "string", "minLength": 2 },
                        "points": { "type": "integer", "minimum": 0 }
                    }
                }
            },
            "additionalProperties": false
        };
    }
}

module.exports = UpdateProductTask;