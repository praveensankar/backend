const mongoose = require('mongoose');
const nconf = require('nconf');

const Schema = mongoose.Schema;

const SERVER_HOST = nconf.get('APPLICATION:S3');

const StatePointSchema = new Schema({
    name: {
        type: String,
        required: [true, 'State name is required.'],
        trim: true
    },
    points: {
        type: Number,
        required: [true, 'State points are required.'],
        min: 0
    }
},
    {
        _id: false,
        autoIndex: false
    });

const ProductPointSchema = new Schema({
    id: {
        type: String,
        required: [true, 'Product ID is required.'],
        select: true,
        index: true,
        unique: true,
        trim: true
    },
    name: {
        type: String,
        required: [true, 'Product name is required.'],
        select: true,
        trim: true
    },
    description: {
        type: String,
        default: '',
        trim: true
    },
    image: {
        type: String,
        get: (value) => `${SERVER_HOST}${value}`,
        trim: true
    },
    points: {
        type: [StatePointSchema],
        required: [true, 'Product points is required.']
    },
    created_by: {
        type: Schema.Types.ObjectId,
        ref: 'Admin',
        required: [true, 'Product created by is required.']
    },
    updated_by: {
        type: Schema.Types.ObjectId,
        ref: 'Admin',
        required: [true, 'Product updated by is required.']
    }
},
    {
        timestamps: true
    });

ProductPointSchema.set('toJSON', { virtuals: true, getters: true });

module.exports = mongoose.model("Product", ProductPointSchema);