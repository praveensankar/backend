const ApplicationError = require('@src/errors/ApplicationError');

const ProductModel = require('../products.model');

class FetchProductTask {

    static exec(id) {
        return ProductModel.where('id').equals(id)
            .select('id name description image points')
            .findOne().exec()
            .then((product) => {
                if (!product)
                    throw new ApplicationError({ status: 201, name: "FetchProductError", message: "Product not found" });
                return product;
            });
    }

}

module.exports = FetchProductTask;