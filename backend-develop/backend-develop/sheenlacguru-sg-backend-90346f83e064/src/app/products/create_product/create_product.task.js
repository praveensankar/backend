const ApplicationError = require('@src/errors/ApplicationError');

const ProductModel = require('../products.model');


class CreateProductTask {

    static exec(document) {
        return new ProductModel(document).save()
            .then((product) => {
                if (!product)
                    throw new ApplicationError({ name: "CreateProductError", message: "Product was not created." });
                return product;
            });
    }

    static validation() {
        return {
            "$id": "http://guru.sheenlac.com/schemas/create_product.json",
            "type": "object",
            "properties": {
                "id": { "type": "string" },
                "name": { "type": "string" },
                "description": { "type": "string","default":"" },
                "image": { "type": "string" },
                "points": {
                    "type": "array",
                    "minItems": 1,
                    "uniqueItems": true,
                    "items": {
                        "name": { "type": "string" },
                        "points": { "type": "integer", "minimum": 0 }
                    }
                }
            },
            "required": ["id", "name", "points"],
            "additionalProperties": false
        };
    }

}

module.exports = CreateProductTask;