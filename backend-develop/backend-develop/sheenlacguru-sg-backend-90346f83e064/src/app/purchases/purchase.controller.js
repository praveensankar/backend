const Ajv = require('ajv');
const _ = require('lodash');

const ApplicationError = require('@src/errors/ApplicationError');
const validations = require('@app/validations');
const ajv = new Ajv(validations.ajv_options);

const CreatePurchaseTask = require('./create_purchase/create_purchase.task');
const ListPurchaseTask = require('./list_purchase/list_purchase.task');
const FetchPurchaseTask = require('./fetch_purchase/fetch_purchase.task');
const UpdatePurchaseStatusTask = require('./update_purchase_status/update_purchase_status.task');
const ClaimProductTask = require('./claim_product/claim_product.task');

class PurchaseController {

    static index(req, res, next) {
        if (!ajv.validate(ListPurchaseTask.validation(), req.query)) {
            let errors = ajv.errors;
            return next(
                new ApplicationError({ status: 400, name: "ValidationError", message: "Bad request parameters.", info: errors })
            );
        }

        return ListPurchaseTask.exec({}, req.query)
            .then(data => {
                return res.status(200).json(
                    new res.app.locals.Success({ data: data })
                );
            })
            .catch(err => next(err));
    }

    static show(req, res, next) {
        return FetchPurchaseTask.exec(req.params.id)
            .then(purchase => {
                return res.status(200).json(
                    new res.app.locals.Success({ data: { purchase } })
                );
            })
            .catch(err => next(err));
    }

    static create(req, res, next) {
        const bills = req.files;
        if (Array.isArray(bills) && bills.length > 0) req.body['bills'] = bills.map(bill => bill.key);
        req.body['painter'] = req.locals.user_id;
        // if (!ajv.validate(CreatePurchaseTask.validation(), req.body)) {
        //     let errors = ajv.errors;
        //     return next(
        //         new ApplicationError({ status: 400, name: 'ValidationError', message: "Bad request parameters.", info: errors })
        //     );
        // }
        let document = Object.assign(req.body, {
            location: req.body.location && JSON.parse(req.body.location)
        });
        return CreatePurchaseTask.exec(document)
            .then(purchase => {
                return res.status(200).json(
                    new res.app.locals.Success({ data: purchase })
                );
            })
            .catch(err => next(err));
    }

    static updateStatus(req, res, next) {
        return UpdatePurchaseStatusTask.exec(req.params.id,req.body)
            .then(op_resp => {
                return res.status(200).json(
                    new res.app.locals.Success({ data: op_resp })
                );
            })
            .catch(err => next(err));
    }

    static claimProduct(req, res, next) {
        let document = _.defaultsDeep(_.cloneDeep(req.params), { admin: req.locals.user_id, scan_type : req.query.scan_type });
        if(!ajv.validate(ClaimProductTask.validation(), document)) {
            let errors = ajv.errors;
            return next(
                new ApplicationError({name: 'ValidationError', message: 'Bad request parameters.', status: 400, info: errors})
            );
        }

        return ClaimProductTask.exec(document)
            .then( response => {
                return res.status(200).json(
                    new res.app.locals.Success({ data: { response } })
                );
            })
            .catch( err => next(err));
    }

}

module.exports = PurchaseController;