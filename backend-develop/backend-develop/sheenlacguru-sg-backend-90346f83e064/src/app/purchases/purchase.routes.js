const express = require('express');
const nconf = require('nconf');
const router = express.Router();

const Token = require('@src/middleware/token');
const Authorization = require('@src/middleware/authorization');
const Uploader = require('@src/middleware/uploader.middleware');
const PurchaseController = require('./purchase.controller');

router.use(Token.verify({ for: 'user' }));

router.get('/', Authorization.authorize(["operations", "operations-l2"]), PurchaseController.index);
router.get('/:id', Authorization.authorize(["operations", "operations-l2"]), PurchaseController.show);
router.patch('/:id/updateStatus', Authorization.authorize(["operations", "operations-l2"]), PurchaseController.updateStatus);
router.patch('/:id/purchases/:purchase', Authorization.authorize(["operations", "operations-l2"]), PurchaseController.claimProduct);

router.post('/', Authorization.authorize(["painter", "contractor"]), Uploader({ dest: nconf.get("STORAGE:PURCHASES") }).array('bills', 6), PurchaseController.create);

module.exports = router;