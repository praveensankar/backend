const PurchaseModel = require('../purchase.model');

class FetchPurchaseTask {

    /**
     * Get purchase
     * @param {string} purchase_id purchase id
     * @param {session instance} session mongo session instance
     * @return return purchase object
     */
    
    static exec(purchase_id, session) {
        const PurchaseQuery = PurchaseModel.where('_id').equals(purchase_id)
            .populate('ofPainter')
            .populate({
                path: 'purchase_transactions',
                populate: {
                    path: 'ofProduct',
                    model: "Product",
                }
            })
            .populate({
                path: 'purchase_transactions',
                populate: {
                    path: 'createdByAdmin',
                    model: "Admin",
                }
            })
            .populate({
                path: 'purchase_transactions',
                populate: {
                    path: 'createdByPainter',
                    model: "User",
                }
            })
            .select('-__v')
            .findOne()
        if (session) {
            return PurchaseQuery.session(session).exec();
        }
        return PurchaseQuery.exec();
    }
}

module.exports = FetchPurchaseTask;