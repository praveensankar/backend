const PurchaseModel = require('../purchase.model');
const uniqid = require('uniqid');
const ApplicationError = require('@src/errors/ApplicationError');

class CreatePurchaseTask {

    /**
     * Create new purchase
     * @param {object} purchase purchase object to be created
     * @param {session instance} session mongo session instance
     * @return purchase created object
     */
    static exec(purchase, session) {
        /* Create unique purchase request id. */
        purchase["id"] = uniqid();
        if (session) {
            return PurchaseModel.create([purchase], {
                session: session
            }).then(purchase => {
                if (!purchase)
                    throw new ApplicationError({
                        name: 'CreatePurchaseError',
                        message: 'Purchase not created.'
                    });

                return purchase;
            });
        } else {
            return new PurchaseModel(purchase).save()
            .then(purchase => {
                if (!purchase)
                    throw new ApplicationError({
                        name: 'CreatePurchaseError',
                        message: 'Purchase not created.'
                    });

                return purchase;
            });
        }
    }

    static validation() {
        return {
            "$id": "http://guru.sheenlac.com/schemas/create_purchase.json",
            "type": "object",
            "properties": {
                "bills": {
                    "type": "array",
                    "minItems": 1,
                    "maxItems": 31,
                    "uniqueItems": true,
                    "items": {
                        "type": "string"
                    }
                },
                "painter": {
                    "type": "string",
                    "minLength": 24,
                    "maxLength": 24
                },
                // "location": {
                //     "type": "string",
                //     "properties": {
                //         "type": {
                //             "type": "string",
                //             "enum": ["Point"]
                //         },
                //         "latitude": {
                //             "type": "string"
                //         },
                //         "longitude": {
                //             "type": "string"
                //         }
                //     },
                //     "required": ["type", "latitude", "longitude"],
                //     "additionalProperties": false
                // }
            },
            "required": ["painter"],
            "additionalProperties": false
        };
    }

}

module.exports = CreatePurchaseTask;