const express = require('express');
const nconf = require('nconf');
const router = express.Router();

const Token = require('@src/middleware/token');
const Authorization = require('@src/middleware/authorization');
const Uploader = require('@src/middleware/uploader.middleware');
const PurchaseController = require('./purchase.controller');

router.use(Token.verify({ for: 'user' }));

router.get('/', Authorization.authorize(["purchaseRegistrationManagement"]), PurchaseController.index);
router.get('/:id', Authorization.authorize(["purchaseRegistrationManagement"]), PurchaseController.show);
router.patch('/:id/updateStatus', Authorization.authorize(["purchaseRegistrationManagement"]), PurchaseController.updateStatus);
router.patch('/:id/purchases/:purchase', Authorization.authorize(["purchaseRegistrationManagement"]), PurchaseController.claimProduct);

router.post('/', Authorization.authorize(["painter", "contractor"]), Uploader({ dest: nconf.get("STORAGE:PURCHASES") }).array('bills', 31), PurchaseController.create);

module.exports = router;