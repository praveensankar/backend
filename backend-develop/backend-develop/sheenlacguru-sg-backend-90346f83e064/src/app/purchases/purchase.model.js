const mongoose = require('mongoose');
const nconf = require('nconf');

const Schema = mongoose.Schema;

const SERVER_HOST = nconf.get('APPLICATION:S3');

const LocationSchema = new Schema({
    type: {
        type: String,
        enum: ['Point'],
    },
    latitude: {
        type: String
    },
    longitude: {
        type: String
    },
    formatted_address: {
        type: String
    },
    is_reverse_geocoded: {
        type: Boolean,
        default : false
    },
    reverseGeocode: {
        type: Object,
    },
}, {
    _id: false,
    autoIndex: false
});

const PurchaseSchema = new Schema({
    id: {
        type: String,
        required: [true, 'Purchase ID is required.'],
        select: true,
        index: true,
        unique: true,
        trim: true
    },
    bills: {
        type: [String],
        get: (bills) => {
            if (!Array.isArray(bills)) return;
            return bills.map(bill => `${SERVER_HOST}${bill}`);
        }
    },
    location: {
        type : LocationSchema
    },
    painter: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: [true, "Painter who submitted the bills is required."]
    },
    status: {
        type: String,
        enum: ["NEW","APPROVED","UNDER DISPUTE","REJECTED"],
        default: "NEW",
        required: [true, "Purchase request status is required."]
    },
    comment: {
        type: String,
        trim: true
    }
},
    {
        timestamps: true
    });

PurchaseSchema.set('toJSON', { virtuals: true, getters: true });

PurchaseSchema.virtual('ofPainter', {
    ref: 'User',
    localField: 'painter',
    foreignField: '_id',
    justOne: true
});

PurchaseSchema.virtual('purchase_transactions', {
    ref: 'Transaction',
    localField: '_id',
    foreignField: 'purchase',
    justOne: false
})

module.exports = mongoose.model("Purchase", PurchaseSchema);