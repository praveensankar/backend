const Promise = require('bluebird');
const mongoose = require('mongoose');
const _ = require('lodash');
const uniqid = require('uniqid');

const ApplicationError = require('@src/errors/ApplicationError');
const FetchPurchaseTask = require('@src/app/purchases/fetch_purchase/fetch_purchase.task');
const ActiveOfferTask = require('@src/app/offers/active_offer/active_offer.task');
const AddTransactionTask = require('@src/app/transactions/add_transaction/add_transaction.task');
const UpdateLedgerTask = require('@src/app/users/update_ledger/update_ledger.task');
const ProductModel = require('../../products/products.model');

class ClaimProductTask {

    /**
     * Claim Product using product id
     * @param {object} document document object
     * @return 
     */
    static async exec(document = {}) {

        // Start mongoose transaction
        const session = await mongoose.startSession();
        session.startTransaction({
            readConcern: {
                level: "snapshot"
            },
            writeConcern: {
                w: 1
            }
        });
        
        try {
            // Fetch valid Purchase
            const fetch_purchase = await FetchPurchaseTask.exec(document.purchase, session).then(purchase => purchase ? purchase.toJSON() : undefined);
            if (!fetch_purchase)
                throw new ApplicationError({
                    status: 400,
                    name: 'ClaimProductError',
                    message: 'Invalid Purchase.'
                });
                
            // Check for valid product 
            const validProduct = await ProductModel.findOne()
                .where("id").equals(document.id)
                .select("id points")
                .session(session)
                .exec()
                .then(product => product ? product.toJSON() : undefined);
            if (!validProduct)
                throw new ApplicationError({
                    status: 400,
                    name: 'ClaimProductError',
                    message: 'INVALID PRODUCT ID.'
                });
            
            // Check for active offer
            const activeOffer = await ActiveOfferTask.exec(session);
            
            // Generate transaction to be added.
            let transaction = ClaimProductTask.createTransaction(fetch_purchase, validProduct, activeOffer, document);

            // Create transaction
            const addTransaction = await AddTransactionTask.single(transaction, session);
            if (!addTransaction)
                throw new ApplicationError({
                    name: 'ClaimProductError',
                    message: 'Error while creating transaction.'
                });

            // Update Ledger for user.
            const updateLedger = await UpdateLedgerTask.creditUnderReview(transaction.painter, transaction.points, session);
            if (!updateLedger) return;

            // Commit session
            await session.commitTransaction();
            
            // return
            return {
                message: `Transaction for ${document.id} is created.`
            };

        } catch (error) {
            await session.abortTransaction();
            throw new ApplicationError({
                name: error.name,
                message: error.message
            });
        } finally {
            session.endSession();
        }
    }

    /**
     * Create transaction
     * @param {object} purchase purchase object
     * @param {object} product product object
     * @param {object} activeOffer active offer object
     * @param {object} document document object
     * @return {object} return transaction
     */

    static createTransaction(purchase, product, activeOffer, document) {
        const painter = _.pick(purchase.ofPainter, ['_id', 'bp_number', 'pincode', 'address']);
        let transaction = {
            mode: "CREDIT",
            scan_type: document.scan_type,
            painter: painter._id,
            bp_number: painter.bp_number,
            pincode: painter.pincode,
            purchase: purchase._id,
            product: product.id,
            uuid: "******",
            offer: activeOffer ? _.get(activeOffer, 'id', undefined) : '',
            multiplier: 1,
            actual: 0,
            points: 0,
            status: "NEW",
            createdBy: _.get(document, 'admin', undefined),
            purchasedOn: new Date().toISOString()
        };

        transaction["txnId"] = uniqid();

        if (_.hasIn(activeOffer, 'multipliers')) {
            let multipliers = _.get(activeOffer, 'multipliers', []);
            _.each(_.intersectionBy(multipliers, [{
                name: '*'
            }, {
                name: painter.address.state.toUpperCase()
                }], 'name'), (val) => {
                transaction["multiplier"] = _.isFinite(val["multiplier"]) && val['multiplier'] > 0 ? val["multiplier"] : 1;
                transaction['offer'] = _.isFinite(val["multiplier"]) && val['multiplier'] > 0 ? transaction['offer'] : '';
                if (val["name"] === "*") {
                    return false;
                }
            });
        }
        _.each(_.intersectionBy(product.points, [{
            name: '*'
        }, {
            name: painter.address.state.toUpperCase()
        }], 'name'), (val) => {
            transaction["actual"] = _.isFinite(val["points"]) ? val["points"] : 0;
            transaction["points"] = _.get(transaction, "actual", 0) * _.get(transaction, "multiplier", 1);
            transaction["points"] = Math.floor(transaction["points"])
            if (val["name"] === "*") {
                return false;
            }
        });
        return transaction;
    }

    static validation() {
        return {
            "$id": "http://guru.sheenlac.com/schemas/claim_product.json",
            "type": "object",
            "properties": {
                "id": {
                    "type": "string"
                },
                "purchase": {
                    "type": "string",
                    "minLength": 24,
                    "maxLength": 24
                },
                "admin": {
                    "type": "string",
                    "minLength": 24,
                    "maxLength": 24
                },
                "scan_type": {
                    "type": "string",
                    "enum": ['AUTO', 'MANUAL','ADMIN-MANUAL']
                },
            },
            "required": ["id", "purchase", "admin"],
            "additionalProperties": false
        };
    }

}

module.exports = ClaimProductTask;