const Promise = require('bluebird');

const PurchaseModel = require('../purchase.model');
const UserModel     = require("../../users/user.model");

class ListPurchaseTask {

    static async exec(query = {}, options = {}) {
        const skip = options.page  > 0 ? (options.page - 1) * options.size : 0;

        let counts = 0;
        let purchases = [];
        var usersIds = [];

        let PurchasesQuery = PurchaseModel.find(query);

        if (options.searchText !== "") {
          /* Start of Search */
          const listUsers = [];
          /* ------- Search with exact match using text based search */
          if(options.searchText.split("-")[0] === "SH") options.searchText = options.searchText.split("-")[1];
          const usersWithExactMatch = await UserModel.find({ $text : { $search : options.searchText }})
          // insert list of searched users
          listUsers.push(...usersWithExactMatch);
          
          /* -------- Search with start (first_name,last_name,mobile_number,bp_number) */
          const rgxForStartMatch = (pattern) => new RegExp(`^${pattern}`);    // pattern for matching start of searchText
          const searchTextForStartMatch = rgxForStartMatch(options.searchText);  
          const usersWithStartMatch = await UserModel.find({ $or : 
              [{ first_name : { $regex : searchTextForStartMatch, $options : 'i' } },
              { last_name : { $regex : searchTextForStartMatch, $options : 'i' }},
              { mobile_number : { $regex : searchTextForStartMatch, $options : 'i' }},
              { bp_number :{ $regex : searchTextForStartMatch, $options : 'i' }}]})
          // insert list of searched users
          listUsers.push(...usersWithStartMatch);

          /* -------- Fuzzy search with (first_name,last_name,mobile_number,bp_number) */
          const rgxForFuzzyMatch = (pattern) => new RegExp(`${pattern}`);
          const searchTextForFuzzyMatch = rgxForFuzzyMatch(options.searchText);
          const usersWithFuzzyMatch = await UserModel.find({ $or : 
              [{ first_name : { $regex : searchTextForFuzzyMatch, $options : 'i' } },
              { last_name : { $regex : searchTextForFuzzyMatch, $options : 'i' }},
              { mobile_number : { $regex : searchTextForFuzzyMatch, $options : 'i' }},
              { bp_number :{ $regex : searchTextForFuzzyMatch, $options : 'i' }}]})
          // insert list of searched user
          listUsers.push(...usersWithFuzzyMatch);

          /* generate list of distinct users if users got repeated */

          const map = new Map();
          for (const item of listUsers) {
              if(!map.has(item.id)){
                  map.set(item.id, true);
                  usersIds.push(item._id);
              }
          }
            usersIds.length > 0 ? PurchasesQuery.find({ "painter": { $in: usersIds } }) : PurchasesQuery.find({
                $or: [{
                    "id": options.searchText
                }, {
                    "_id": options.searchText
                }]
            })
            if (options.status !== "ALL") PurchasesQuery.where("status").equals(options.status);
            if (options.size > 0) PurchasesQuery.limit(options.size);
            PurchasesQuery.populate('ofPainter').select('-__v').skip(skip);

            // Count Query for total purchases after search with status
            let CountQuery = usersIds.length > 0 ? PurchaseModel.find({
                "painter": {
                    $in: usersIds
                }
            }) : PurchaseModel.find({
                $or: [{
                    "id": options.searchText
                }, {
                    "_id": options.searchText
                }]
            })
            
            if (options.status !== "ALL") CountQuery.where("status").equals(options.status);
            counts = await CountQuery.countDocuments().exec();

            purchases = await PurchasesQuery.sort({createdAt : -1 });
        }else{
            if (options.size > 0) PurchasesQuery.limit(options.size);
            if (options.status !== "ALL") PurchasesQuery.where("status").equals(options.status);
            PurchasesQuery.populate('ofPainter').select('-__v').skip(skip);
            purchases = await PurchasesQuery.sort({createdAt : -1 });

            // Count Query without search with status
            let CountQuery = PurchaseModel.find();
            if(options.status !== "ALL") CountQuery.where("status").equals(options.status);
            counts = await CountQuery.countDocuments().exec();
        }
        
        return Promise.all([
            purchases,
            counts
        ]).then( ([purchases,count]) => {
            return { items: purchases, totalCount: count };
        });
    }

    static validation() {
        return {
            "$id": "http://guru.sheenlac.com/schemas/purchases/list_options.json",
            "$async": true,
            "type": "object",
            "properties": {
                "page": { "type": "integer", "default": 1 },
                "size": { "type": "integer", "default": 10 },
                "status": { "type": "string","default": "ALL"},
                "searchText" : { "type" : "string", "default" : ""}
            },
            "additionalProperties": false
        };
    }

}

module.exports = ListPurchaseTask;