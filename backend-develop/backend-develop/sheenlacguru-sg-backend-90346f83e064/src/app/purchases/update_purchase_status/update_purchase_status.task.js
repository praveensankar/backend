const PurchaseModel = require('../purchase.model');
const ApplicationError = require('@src/errors/ApplicationError');

class UpdatePurchaseStatusTask {

  static exec(purchase_id,purchase) {
    return PurchaseModel.where('_id').equals(purchase_id)
      .updateOne({}, { omitUndefined: true })
      .set(purchase)
      .exec()
      .then((op_resp) => {
        if (op_resp.n < 1 || op_resp.nModified < 1)
          throw new ApplicationError({ name: "UpdatePurchaseError", message: "Purchase request is not updated." });
        return { matched: op_resp.n, updated: op_resp.nModified, message: "Purchase request is updated." };
      });
  }

}

module.exports = UpdatePurchaseStatusTask;