const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const AppUpdateSchema = new Schema({
    appName: {
        type: String,
        trim: true
    },
    appIdentifier: {
        type: String,
        trim: true
    },
    versionName: {
        type: String,
        trim: true
    },
    versionCode: {
        type: Number,
        trim: true
    },
    appDownloadUrl: {
        type: String,
        trim: true
    },
    forceUpgrade: {
        type: Boolean,
        default: false
    }
}, {
    timestamps: true
});

AppUpdateSchema.set('toJSON', {
    virtuals: true
});


module.exports = mongoose.model("AppUpdates", AppUpdateSchema);