const AppUpdateModel = require("../app_update.model");

const ApplicationError = require("@src/errors/ApplicationError");

class ForceUpgradeTask {

    static exec(id) {
        return AppUpdateModel.findOne({}).limit(1).sort({createdAt:-1}).exec().then((appUpdate) => {
            if (!appUpdate)
                throw new ApplicationError({
                    name: "AppUpdateRoleError",
                    message: "App Update is not found."
                });
            return appUpdate;
        });
    }
}

module.exports = ForceUpgradeTask;