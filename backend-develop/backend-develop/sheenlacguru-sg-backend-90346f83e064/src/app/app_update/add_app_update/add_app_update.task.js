const ApplicationError = require("@src/errors/ApplicationError");
const AppUpdateModel = require("../app_update.model");

class AddAppUpdateTask {

    static add(document) {
        return new AppUpdateModel(document).save().then(appUpdate => {
            if (!appUpdate)
                throw new ApplicationError({
                    name: "AddAppUpdateError",
                    message: "App Update is not created."
                });

            return appUpdate;
        }).catch(err => {
            if (err.code === 11000 && err.name === 'MongoError')
                throw new ApplicationError({
                    name: err.name,
                    message: "Admin Role Name should be unique."
                });
        })
    }

    static validation() {
        return {
            "$id": "http://guru.sheenlac.com/schemas/add_app_update.json",
            "type": "object",
            "properties": {
                "appName": {
                    "type": "string"
                },
                "appIdentifier": {
                    "type": "string"
                },
                "versionName": {
                    "type": "string"
                },
                "versionCode": {
                    "type": "integer"
                },
                "appDownloadUrl": {
                    "type": "string"
                },
                "forceUpgrade": {
                    "type": "boolean",
                    "default" : false
                }
            },
            "required": ["appName","appIdentifier","versionName","versionCode","appDownloadUrl","forceUpgrade"],
            "additionalProperties": false
        };
    }
}

module.exports = AddAppUpdateTask;