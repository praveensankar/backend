const AppUpdateModel = require("../app_update.model");

const ApplicationError = require("@src/errors/ApplicationError");

class GetAppUpdateTask {

    static exec(id) {
        return AppUpdateModel.findById(id).exec().then((appUpdate) => {
            if (!appUpdate)
                throw new ApplicationError({
                    name: "AppUpdateRoleError",
                    message: "App Update is not found."
                });
            return appUpdate;
        });
    }
}

module.exports = GetAppUpdateTask;