const express = require('express');
const router = express.Router();

const Token = require('@src/middleware/token');
const Authorization = require('@src/middleware/authorization');
const AppUpdateController = require('./app_update.controller');
router.get('/forceUpgrade',AppUpdateController.forceUpgrade);
router.use(Token.verify({
    for: 'admin'
}));

router.post('/', AppUpdateController.create);
router.get('/', AppUpdateController.list);
router.get('/:id', AppUpdateController.show);
router.patch('/:id', AppUpdateController.update);
router.delete('/:id', AppUpdateController.delete);

module.exports = router;