const Promise = require('bluebird');
const _ = require("lodash");
const AppUpdateModel = require("../app_update.model");

class ListAppUpdatesTask {

    static async exec(query = {}, options = {}) {

        const skip = options.page > 0 ? (options.page - 1) * options.size : 0;

        let appUpdates = [];
        let appUpdatesCount = 0;

        // search when search key exist in options
        if (options.searchText !== "") {
            /* Start of Search */

            /* ------- Search with exact match using text based search */
            const appUpdatesWithExactMatch = await AppUpdateModel.find({
                $text: {
                    $search: options.searchText
                }
            });
            /* -------- Search with start (role_name,_id) */
            const rgxForStartMatch = (pattern) => new RegExp(`^${pattern}`); // pattern for matching start of searchText
            const searchTextForStartMatch = rgxForStartMatch(options.searchText);
            const appUpdatesWithStartMatch = await AppUpdateModel.find({
                $or: [{
                    appName: {
                        $regex: searchTextForStartMatch,
                        $options: 'i'
                    },
                    appIdentifier: {
                        $regex: searchTextForStartMatch,
                        $options: 'i'
                    },
                    versionName: {
                        $regex: searchTextForStartMatch,
                        $options: 'i'
                    },
                    versionCode: {
                        $regex: searchTextForStartMatch,
                        $options: 'i'
                    }
                }]
            });

            /* -------- Fuzzy search with (role_name,_id,mobile_number,email) */
            const rgxForFuzzyMatch = (pattern) => new RegExp(`${pattern}`);
            const searchTextForFuzzyMatch = rgxForFuzzyMatch(options.searchText);
            const appUpdatesWithFuzzyMatch = await AppUpdateModel.find({
                $or: [{
                    appName: {
                        $regex: searchTextForStartMatch,
                        $options: 'i'
                    },
                    appIdentifier: {
                        $regex: searchTextForStartMatch,
                        $options: 'i'
                    },
                    versionName: {
                        $regex: searchTextForStartMatch,
                        $options: 'i'
                    },
                    versionCode: {
                        $regex: searchTextForStartMatch,
                        $options: 'i'
                    }
                }]
            });

            const adminlist = _.concat(appUpdatesWithExactMatch, appUpdatesWithStartMatch, appUpdatesWithFuzzyMatch);

            /* generate list of distinct appUpdates if appUpdates got repeated */
            appUpdates = _.concat([], _.uniqBy(adminlist, '_id'));
            appUpdatesCount = appUpdates.length;
            appUpdates = appUpdates.slice(skip).slice(0, options.size)

            /* End of Search */
        } else {
            /* fetch appUpdates if no search key is present on options */
            const appUpdatesQuery = AppUpdateModel.find(query);
            if (options.status) appUpdatesQuery.where("role_status").equals(options.status);
            appUpdates = await appUpdatesQuery.skip(skip).limit(Number(options.size)).exec();
            appUpdatesCount = await AppUpdateModel.countDocuments().exec();
        }

        return Promise.all([
            appUpdates,
            appUpdatesCount
        ]).then(([appUpdates, count]) => {
            return {
                items: appUpdates,
                totalCount: count
            };
        });
    }
}

module.exports = ListAppUpdatesTask;