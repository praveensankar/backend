const Ajv = require('ajv');
const _ = require("lodash");

const ApplicationError = require("@src/errors/ApplicationError");
const validations = require('@app/validations');
const AddAppUpdateTask = require('./add_app_update/add_app_update.task');
const ListAppUpdatesTask = require('./list_app_update/list_app_update.task');
const GetAppUpdateTask = require('./get_app_update/get_app_update.task');
const UpdateAppUpdateTask = require('./update_app_update/update_app_update.task');
const ForceUpgradeTask = require('./force_upgrade/force_upgrade.task');
const DeleteAppUpdateTask = require('./delete_app_update/delete_app_update.task');
const ajv = new Ajv(validations.ajv_options);

class AppUpdateController {

    static create(req, res, next) {
        if (!ajv.validate(AddAppUpdateTask.validation(), req.body)) {
            let errors = ajv.errors;
            return next(
                new ApplicationError({
                    status: 400,
                    name: "ValidationError",
                    message: "Bad request parameters.",
                    info: errors
                })
            );
        }

        return AddAppUpdateTask.add(req.body)
            .then((response) => {
                return res.status(200).json(
                    new res.app.locals.Success({
                        data: response
                    })
                );
            })
            .catch((err) => {
                return next(err);
            });
    }

    static list(req, res, next) {
        return ListAppUpdatesTask.exec({}, req.query)
            .then((roles) => {
                return res.status(200).json(
                    new res.app.locals.Success({
                        data: roles
                    })
                );
            })
            .catch((err) => {
                return next(err);
            });
    }

    static show(req, res, next) {
        return GetAppUpdateTask.exec(req.params.id)
            .then((appUpdate) => {
                return res.status(200).json(
                    new res.app.locals.Success({
                        data: {
                            appUpdate
                        }
                    })
                );
            })
            .catch((err) => {
                return next(err);
            });
    }

    static update(req, res, next) {
        if (!ajv.validate(UpdateAppUpdateTask.validation(), req.body)) {
            let errors = ajv.errors;
            return next(
                new ApplicationError({
                    status: 400,
                    name: "ValidationError",
                    message: "Bad request parameters",
                    info: errors
                })
            );
        }

        UpdateAppUpdateTask.exec(req.params.id, req.body)
            .then((response) => {
                return res.status(200).json(
                    new res.app.locals.Success({
                        data: response
                    })
                );
            })
            .catch((err) => {
                return next(err);
            });
    }

    static delete(req, res, next) {
        DeleteAppUpdateTask.exec(req.params.id)
            .then((response) => {
                return res.status(200).json(
                    new res.app.locals.Success({
                        data: response
                    })
                );
            })
            .catch((err) => {
                return next(err);
            });
    }

    static forceUpgrade(req,res,next){
        return ForceUpgradeTask.exec()
            .then((appUpdate) => {
                return res.status(200).json(
                    new res.app.locals.Success({
                        data: appUpdate
                    })
                );
            })
            .catch((err) => {
                return next(err);
            });
    }
}

module.exports = AppUpdateController;