const ApplicationError = require("@src/errors/ApplicationError");
const AppUpdateModel = require('../app_update.model');

class DeleteAppUpdateTask {

    static exec(id) {
        return AppUpdateModel.deleteOne().where('_id').equals(id)
            .exec()
            .then((op_resp) => {
                if (op_resp.ok != 1 || op_resp.deletedCount < 1) {
                    throw new ApplicationError({
                        name: "DeleteError",
                        message: "App Update deletion failed."
                    });
                }

                return {
                    deleted: op_resp.deletedCount,
                    message: "App Update deleted."
                };
            });
    }
}

module.exports = DeleteAppUpdateTask;