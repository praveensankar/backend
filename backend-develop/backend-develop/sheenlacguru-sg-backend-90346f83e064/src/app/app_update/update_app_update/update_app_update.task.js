const ApplicationError = require("@src/errors/ApplicationError");
const AppUpdateModel = require('../app_update.model');

class UpdateAppUpdateTask {

    static exec(id, document) {
        return AppUpdateModel.where('_id').equals(id).updateOne({}, {
                omitUndefined: true
            }).set(document)
            .exec()
            .then((update_resp) => {
                if (update_resp.n < 1 || update_resp.nModified < 1)
                    throw new ApplicationError({
                        name: "UpdateError",
                        message: "Updation failed."
                    });

                return {
                    matched: update_resp.n,
                    updated: update_resp.nModified,
                    message: "App Upgrade updated."
                };
            });
    }

    static validation() {
        return {
            "$id": "http://guru.sheenlac.com/schemas/update_app_update.json",
            "type": "object",
            "properties": {
                "appName": {
                    "type": "string"
                },
                "appIdentifier": {
                    "type": "string"
                },
                "versionName": {
                    "type": "string"
                },
                "versionCode": {
                    "type": "integer"
                },
                "appDownloadUrl": {
                    "type": "string"
                },
                "forceUpgrade": {
                    "type": "boolean",
                    "default" : false
                }
            },
            "additionalProperties": false
        };
    }

}

module.exports = UpdateAppUpdateTask;