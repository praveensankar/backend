const UserModel = require("../../users/user.model");
const nconf = require('nconf');
const SERVER_HOST = nconf.get('APPLICATION:S3');

class OfferLeaderboard {
    static async exec() {
        const leaderboard = await UserModel.aggregate([{
                $match: {
                    offerPoints: {
                        $gt: 0
                    }
                }
            },
            {
                $sort: {
                    offerPoints: -1
                }
            }, {
                $project: {
                    _id: 1,
                    offerPoints: 1,
                    first_name: 1,
                    last_name: 1,
                    photo: 1,
                    state: "$address.state"
                }
            }
        ]);
        leaderboard.map(ele => {
            Object.assign(ele, {
                photo: ele.photo ? `${SERVER_HOST}${ele.photo}` : ''
            });
        });
        return leaderboard;
    }
}

module.exports = OfferLeaderboard;