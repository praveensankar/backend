const ApplicationError = require('@src/errors/ApplicationError');

const OfferModel = require('../offers.model');

class UpdateOfferTask {

    static exec(offer_id, document) {
        return OfferModel.where('id').equals(offer_id)
            .updateOne({}, { omitUndefined: true })
            .set(document)
            .exec()
            .then((update_resp) => {
                if (update_resp.n < 1 || update_resp.nModified < 1)
                    throw new ApplicationError({ name: "UpdateOfferError", message: "Offer is not updated." });

                return { matched: update_resp.n, updated: update_resp.nModified, message: "Offer updated." };
            });
    }

    static validation() {
        return {
            "$id": "http://guru.sheenlac.com/schemas/update_offer.json",
            "type": "object",
            "properties": {
                "name": { "type": "string", "minLength": 2 },
                "title": { "type": "string", "minLength": 2 },
                "description": { "type": "string", "minLength": 2 },
                "banner": { "type": "string", "minLength": 2 },
                "start": { "type": "string", "format": "date-time" },
                "end": { "type": "string", "format": "date-time" },
                "is_active": {
                    "type": "boolean"
                },
                "enableOfferLedger": {
                    "type": "boolean"
                },
                "enableLeaderboard": {
                    "type": "boolean"
                },
                "offerBrochure": {
                    "type": "string",
                },
                "pointsValidity": {
                    "type": "string",
                    "format":"date-time"
                },
                "multipliers": {
                    "type": "array",
                    "minItems": 1,
                    "uniqueItems": true,
                    "items": {
                        "name": { "type": "string", "minLength": 2 },
                        "multiplier": { "type": "integer", "minimum": 0 }
                    }
                }
            },
            "additionalProperties": false
        }
    }

}

module.exports = UpdateOfferTask;