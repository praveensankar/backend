const Promise = require('bluebird');

const OfferModel = require('../offers.model');

class ListOfferTask {

    static exec(query = {}, options = {}) {
        const skip = options.page > 0 ? (options.page - 1) * options.size : 0;
        let Query = ListOfferTask.buildQuery(query, options).skip(skip);
        if(options.size > 0) Query.limit(options.size);
        return Promise.all([
            Query.select('-__V').sort({updatedAt: -1}).exec(),
            ListOfferTask.buildQuery(query, options).countDocuments().exec()
        ]).then(([offers, count]) => {
            return {
                items: offers,
                totalCount: count
            };
        })
            .catch((err) => {
                return err;
            });
    }

    static buildQuery(query, options) {
        const today = new Date();
        let Query = OfferModel.find(query);
        if(options.active > 0) Query.where("start").lte(today).where("end").gte(today);
        return Query;
    }

    static validation() {
        return {
            "$id": "http://guru.sheenlac.com/schemas/offer_list_options.json",
            "$async": true,
            "type": "object",
            "properties": {
                "page": { "type": "integer", "default": 1 },
                "size": { "type": "integer", "default": 0 },
                "active": { "type": "integer", "default": 0}
            },
            "required": [],
            "additionalProperties": false
        }
    }

}

module.exports = ListOfferTask;