const Promise = require('bluebird');

const OfferModel = require('../offers.model');

class ActiveOfferTask {

    /**
     * Get active offer
     * @param {session instance} session mongo session instance
     * @return return active offer object
     */
    static exec(session) {
        let today = new Date();
        const OfferQuery = OfferModel
            .where("start").lte(today)
            .where("end").gte(today)
            .where('is_active').equals(true)
            .select("-__v")
            .findOne();
        if (session) {
            return OfferQuery.session(session).exec();
        } else {
            return OfferQuery.exec();
        }
    }

    static activeOffers(session) {
        let today = new Date();
        const OfferQuery = OfferModel
            .where('is_active').equals(true)
            .select("-__v")
        if (session) {
            return OfferQuery.session(session).exec();
        } else {
            return OfferQuery.exec();
        }
    }
}

module.exports = ActiveOfferTask;