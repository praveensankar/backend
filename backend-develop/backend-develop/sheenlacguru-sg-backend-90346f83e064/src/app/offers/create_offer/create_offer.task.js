const uniqid = require('uniqid');
const ApplicationError = require('@src/errors/ApplicationError');

const OfferModel = require('../offers.model');

class CreateOfferTask {

    static exec(document) {
        document["id"] = uniqid();
        return new OfferModel(document).save()
            .then((offer) => {
                if (!offer)
                    throw new ApplicationError({ name: 'CreateOfferError', message: 'Offer not created.' });
                return offer;
            });
    }

    static validation() {
        return {
            "$id": "http://guru.sheenlac.com/schemas/create_offer.json",
            "type": "object",
            "properties": {
                "title": { "type": "string", "minLength": 2 },
                "description": { "type": "string", "minLength": 2 },
                "banner": { "type": "string" },
                "start": { "type": "string", "format": "date-time" },
                "end": { "type": "string", "format": "date-time" },
                "is_active": {
                    "type": "boolean"
                },
                "enableOfferLedger": {
                    "type": "boolean"
                },
                "enableLeaderboard": {
                    "type": "boolean"
                },
                "offerBrochure": {
                    "type": "string"
                },
                "pointsValidity": {
                    "type": "string",
                    "format":"date-time"
                },
                "multipliers": {
                    "type": "array",
                    "minItems": 1,
                    "uniqueItems": true,
                    "items": {
                        "name": { "type": "string", "minLength": 2 },
                        "multiplier": { "type": "integer", "minimum": 0 }
                    }
                }
            },
            "required": ["title", "description", "banner", "is_active", "start", "end", "multipliers"],
            "additionalProperties": false
        };
    }

}

module.exports = CreateOfferTask;