const mongoose  = require("mongoose");
const TransactionModel = require("../../transactions/transaction.model");

class OfferTransactions {
    static async exec(offer_id, user_id) {
        console.log(offer_id, user_id);
        const transactions = await TransactionModel.aggregate([{
            $match: {
                painter: mongoose.Types.ObjectId(user_id),
                offer : offer_id,
                offerPoints: {
                    $gt: 0
                },
                mode: 'CREDIT'
            }
        }, {
            $lookup: {
                from: 'users',
                localField: 'painter',
                foreignField: '_id',
                as: 'painter'
            }
        }, {
            $lookup: {
                from: 'products',
                localField: 'product',
                foreignField: 'id',
                as: 'product'
            }
        }, {
            $unwind: {
                path: '$painter',
            }
        }, {
            $unwind: {
                path: "$product"
            }
        }, {
            $sort: {
                createdAt: -1
            }
        }, {
            $project: {
                _id: 1,
                status: 1,
                txnId: 1,
                offerPoints: 1,
                createdAt: 1,
                updatedAt: 1,
                productName: "$product.name",
                remark: 'Credited Points'
            }
            }]);
        console.log(transactions)
        return transactions;
    }
}

module.exports = OfferTransactions;