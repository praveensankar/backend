const express = require('express');
const nconf = require('nconf');
const router = express.Router();

const Token = require('@src/middleware/token');
const Authorization = require('@src/middleware/authorization');
const Uploader = require('@src/middleware/uploader.middleware');

const OfferController = require('./offers.controller');

router.use(Token.verify({ for: 'admin' }));

router.post('/', Authorization.authorize(["operations", "operations-l2"]), Uploader({ dest: nconf.get("STORAGE:OFFERS") }).single('image'), OfferController.create);
router.get('/', Authorization.authorize(["operations", "operations-l2", "painter", "contractor"]), OfferController.index);
router.get('/:id', Authorization.authorize(["operations", "operations-l2", "painter", "contractor"]), OfferController.show);
router.post('/:id', Authorization.authorize(["operations", "operations-l2"]), Uploader({ dest: nconf.get("STORAGE:OFFERS") }).single('image'), OfferController.update);
router.delete('/:id', Authorization.authorize(["operations", "operations-l2"]), OfferController.delete);

module.exports = router;