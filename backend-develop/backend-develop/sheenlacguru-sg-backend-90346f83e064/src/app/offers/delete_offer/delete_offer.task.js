const ApplicationError = require('@src/errors/ApplicationError');

const OfferModel = require('../offers.model');

class DeleteOfferTask {

    static exec(offer_id) {
        return OfferModel.deleteOne().where('id').equals(offer_id)
            .exec()
            .then((op_resp) => {
                if (op_resp.ok != 1 || op_resp.deletedCount < 1)
                    throw new ApplicationError({ name: 'DeleteOfferError', message: 'Offer not deleted.' });

                return { deleted: op_resp.deletedCount, message: 'Offer deleted.' };
            });
    }

}

module.exports = DeleteOfferTask;