const express = require('express');
const nconf = require('nconf');
const router = express.Router();

const Token = require('@src/middleware/token');
const Authorization = require('@src/middleware/authorization');
const Uploader = require('@src/middleware/uploader.middleware');

const OfferController = require('./offers.controller');

router.use(Token.verify({ for: 'admin' }));

router.post('/admin', Authorization.authorize(["offersSchemesAndPromotionManagement"]), Uploader({
    dest: nconf.get("STORAGE:OFFERS")
}).fields([{
    name: 'banner',
    maxCount: 1
}, {
    name: 'offerBrochure',
    maxCount: 1
}]), OfferController.create);
router.get('/admin', Authorization.authorize(["offersSchemesAndPromotionManagement", "painter", "contractor"]), OfferController.index);
router.get('/admin/:id', Authorization.authorize(["offersSchemesAndPromotionManagement", "painter", "contractor"]), OfferController.show);
router.post('/admin/:id', Authorization.authorize(["offersSchemesAndPromotionManagement"]), Uploader({
    dest: nconf.get("STORAGE:OFFERS")
}).fields([{
    name: 'banner',
    maxCount: 1
}, {
    name: 'offerBrochure',
    maxCount: 1
}]), OfferController.update);
router.delete('/admin/:id', Authorization.authorize(["offersSchemesAndPromotionManagement"]), OfferController.delete);

router.get('/user', Authorization.authorize(["painter", "contractor"]), OfferController.getActiveOffers);
router.get('/user/:id', Authorization.authorize(["painter", "contractor"]), OfferController.getOfferDetailWithPoints);
router.get('/user/:id/leaderboard', Authorization.authorize(["painter", "contractor"]), OfferController.offerLeaderboard);
router.get('/user/:id/transactions', Authorization.authorize(["painter", "contractor"]), OfferController.userOfferTransactions);

module.exports = router;