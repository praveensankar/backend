const UserModel = require('../../users/user.model');
const TransactionModel = require('../../transactions/transaction.model');
const OfferModel = require('../offers.model');
const _ = require('lodash');

class FetchOfferTask {

    static exec(offer_id, session) {
        const OfferQuery = OfferModel
            .where('id').equals(offer_id)
            .select('-__v')
            .findOne();
        if (session) {
            return OfferQuery.session(session).exec();
        } else {
            return OfferQuery.exec();
        }
    }

    static async getOfferWithPoints(offer_id,user_id) {
        const offer = await OfferModel.findById(offer_id).select('-__v').lean().exec();
        if (offer) {
            const offerPointsResult = await UserModel.findOne({ _id: user_id,bp_number: { $ne : "" } }).select('-id').exec();
            let getPainterBonusPoint = await TransactionModel.find({
                bp_number : offerPointsResult.bp_number,
                createdAt: {
                    $lte: new Date(offer.end)
                },
                createdAt: {
                    $gte: new Date(offer.start)
                },
                "$or": [{
                    mode : "BONUS-CREDIT"
                    },{
                    mode : "SPECIALOFFER-DEBIT"
                    },{
                    mode : "CREDIT"
                    }
                ]
            });
            let debitValue = 0;
            let bonusCreditValue = 0;
            let offerPointValue = 0;
            getPainterBonusPoint.map((data,index)=> {
                if(data['mode'] == 'BONUS-CREDIT'){
                    bonusCreditValue +=  data['points']
                }

                if(data['mode'] == 'SPECIALOFFER-DEBIT'){
                    if((data['status'] == 'APPROVED') || (data['status'] == 'NEW'))
                    {
                    debitValue +=  data['points']
                    }
                }

                if(data['mode'] == 'CREDIT'){
                    offerPointValue +=  data['offerPoints']
                }
            })
            if(debitValue > offerPointValue){
                // bonusCreditValue = (offerPointValue+bonusCreditValue) - debitValue
                offerPointValue = (offerPointValue+bonusCreditValue) - debitValue;
                bonusCreditValue = 0;
                
            }else{
                if(bonusCreditValue <= debitValue){
                    debitValue = debitValue - bonusCreditValue;
                    offerPointValue = offerPointValue - debitValue;
                    bonusCreditValue = 0;
                }else{
                    bonusCreditValue = bonusCreditValue - debitValue;
                }
            }
            return {
                ...offer,
                // userOfferPoints: offerPointsResult ? offerPointsResult.offerPoints : 0,
                userOfferPoints : offerPointValue,
                userbonusPoints : bonusCreditValue
            };
        }
    }

    static offerWithSpecficDate(date, session) {
        const OfferQuery = OfferModel
            .where("start").lte(date)
            .where("end").gte(date)
            .select("-__v")
            .findOne();
        if (session) {
            return OfferQuery.session(session).exec();
        } else {
            return OfferQuery.exec();
        }
    }

}

module.exports = FetchOfferTask;