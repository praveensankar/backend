const Ajv = require("ajv");
const _ = require("lodash");

const ApplicationError = require("@src/errors/ApplicationError");
const validations = require("@app/validations");
const ajv = new Ajv(validations.ajv_options);

const CreateOfferTask = require("./create_offer/create_offer.task");
const ListOfferTask = require("./list_offer/list_offer.task");
const FetchOfferTask = require("./fetch_offer/fetch_offer.task");
const UpdateOfferTask = require("./update_offer/update_offer.task");
const DeleteOfferTask = require("./delete_offer/delete_offer.task");
const OfferLeaderboard = require("./offer_leaderboard/offer_leaderboard.task");
const ActiveOfferTask = require("./active_offer/active_offer.task");
const OfferTransactions = require("./offer_transactions/offer_transactions.task");

class OffersController {
  static index(req, res, next) {
    if (!ajv.validate(ListOfferTask.validation(), req.query)) {
      let errors = ajv.errors;
      return next(
        new ApplicationError({
          status: 400,
          name: "ValidationError",
          message: "Bad request parameters.",
          info: errors,
        })
      );
    }

    return ListOfferTask.exec({}, req.query)
      .then((data) => {
        return res.status(200).json(new res.app.locals.Success({ data: data }));
      })
      .catch((err) => {
        return next(err);
      });
  }

  static show(req, res, next) {
    return FetchOfferTask.exec(req.params.id)
      .then((offer) => {
        return res
          .status(200)
          .json(new res.app.locals.Success({ data: { offer } }));
      })
      .catch((err) => {
        return next(err);
      });
  }

  static create(req, res, next) {
    if (req.files.banner && req.files.banner.length) req.body["banner"] = req.files.banner[0].key;
    if (req.files.offerBrochure && req.files.offerBrochure.length) req.body["offerBrochure"] = req.files.offerBrochure[0].key;
    if (!ajv.validate(CreateOfferTask.validation(), req.body)) {
      let errors = ajv.errors;
      return next(
        new ApplicationError({
          status: 400,
          name: "ValidationError",
          message: "Bad request parameters.",
          info: errors,
        })
      );
    }
    let user_id = req.locals.user_id;
    let document = _.defaultsDeep(_.cloneDeep(req.body), {
      created_by: user_id,
      updated_by: user_id,
    });
    // return res.status(200).json(document);
    return CreateOfferTask.exec(document)
      .then((_) => {
        return res
          .status(200)
          .json(
            new res.app.locals.Success({ data: { message: "Offer created." } })
          );
      })
      .catch((err) => {
        return next(err);
      });
  }

  static update(req, res, next) {
    if (req.files.banner && req.files.banner.length) req.body["banner"] = req.files.banner[0].key;
    if (req.files.offerBrochure && req.files.offerBrochure.length) req.body["offerBrochure"] = req.files.offerBrochure[0].key;
    if (!ajv.validate(UpdateOfferTask.validation(), req.body)) {
      let errors = ajv.errors;
      return next(
        new ApplicationError({
          status: 400,
          name: "ValidationError",
          message: "Bad request parameters.",
          info: errors,
        })
      );
    }
    let document = _.defaultsDeep(_.cloneDeep(req.body), {
      updated_by: req.locals.user_id,
    });
    // return res.status(200).json(document);
    return UpdateOfferTask.exec(req.params.id, document)
      .then((op_resp) => {
        return res
          .status(200)
          .json(new res.app.locals.Success({ data: op_resp }));
      })
      .catch((err) => {
        return next(err);
      });
  }

  static delete(req, res, next) {
    return DeleteOfferTask.exec(req.params.id)
      .then((op_resp) => {
        return res
          .status(200)
          .json(new res.app.locals.Success({ data: op_resp }));
      })
      .catch((err) => {
        return next(err);
      });
  }


  /// User Functions
  static getActiveOffers(req, res, next) {
    return ActiveOfferTask.activeOffers()
      .then((data) => {
        return res.status(200).json(new res.app.locals.Success({
          data: data
        }));
      })
      .catch((err) => {
        return next(err);
      });
  }

    static getOfferDetailWithPoints(req, res, next) {
      return FetchOfferTask.getOfferWithPoints(req.params.id, req.locals["user_id"])
        .then((offer) => {
          return res.status(200).json(
            new res.app.locals.Success({
              data: {
                ...offer
              },
            })
          );
        })
        .catch((err) => {
          return next(err);
        });
    }

  static offerLeaderboard(req, res, next) {
    return OfferLeaderboard.exec(req.params.id)
      .then((leaderboard) => {
        return res
          .status(200)
          .json(new res.app.locals.Success({
            data: leaderboard
          }));
      })
      .catch((err) => {
        return next(err);
      });
  }

  static userOfferTransactions(req, res, next) {
    return OfferTransactions.exec(req.params.id,req.locals['user_id'])
      .then((transactions) => {
        return res
          .status(200)
          .json(new res.app.locals.Success({
            data: transactions
          }));
      })
      .catch((err) => {
        return next(err);
      });
  }

}

module.exports = OffersController;
