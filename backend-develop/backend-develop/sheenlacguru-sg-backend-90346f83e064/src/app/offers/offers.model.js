const mongoose = require('mongoose');
const nconf = require('nconf');

const Schema = mongoose.Schema;

const SERVER_HOST = nconf.get('APPLICATION:S3');

const StateMultiplierSchema = new Schema({
    name: {
        type: String,
        required: [true, 'State name is required.'],
        trim: true
    },
    multiplier: {
        type: Number,
        required: [true, 'State multiplier is required.'],
        min: 0
    }
},
    {
        _id: false,
        autoIndex: false
    });

const OfferSchema = new Schema({
    id: {
        type: String,
        required: [true, 'Offer ID is required.'],
        select: true,
        index: true,
        unique: true,
        trim: true
    },
    title: {
        type: String,
        required: [true, 'Offer title is required.'],
        select: true,
        trim: true
    },
    description: {
        type: String,
        required: [true, 'Offer description is required.'],
        trim: true
    },
    banner: {
        type: String,
        get: (value) => `${SERVER_HOST}${value}`,
        required: [true, 'Offer image is required.'],
        trim: true
    },
    start: {
        type: Date,
        required: [true, 'Offer start date is required.']
    },
    end: {
        type: Date,
        required: [true, 'Offer end date is required.']
    },
    is_active: {
        type: Boolean,
        default : false
    },
    enableOfferLedger: {
        type: Boolean,
        default : false
    },
    enableLeaderboard: {
        type: Boolean,
        default : false,
    },
    offerBrochure: {
        type: String,
        get: (value) => value && `${SERVER_HOST}${value}`,
        trim: true
    },
    pointsValidity: {
        type: String
    },
    multipliers: {
        type: [StateMultiplierSchema],
        required: [true, 'Offer multiplication factor is required.']
    },
    created_by: {
        type: Schema.Types.ObjectId,
        ref: 'Admin',
        required: [true, 'Offer created by is required.']
    },
    updated_by: {
        type: Schema.Types.ObjectId,
        ref: 'Admin',
        required: [true, 'Offer updated by is required.']
    }
}, {
        timestamps: true
    });

OfferSchema.set('toJSON', { virtuals: true, getters: true });

module.exports = mongoose.model('Offer', OfferSchema);