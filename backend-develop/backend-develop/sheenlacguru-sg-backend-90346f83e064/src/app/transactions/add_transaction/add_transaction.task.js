const TransactionModel = require('../transaction.model');

class AddTransactionTask {
    
    /**
     * Add multiple transactions
     * @param {array} transactions transactions array
     * @param {session instance} session session instance
     * @return return 
     */
    static many(transactions = [], session) {
        if (session) {
            return TransactionModel.create([...transactions], {
                session: session
            });
        }
        return TransactionModel.insertMany(transactions);
    }

    /**
     *  Add single transaction
     * @param {object} transaction transction object 
     * @param {session instance} session session instance 
     * @return return
     */
    static single(transaction = {},session) {
        if (session) {
            return TransactionModel.create([transaction], {
                session: session
            });
        }
        return new TransactionModel(transaction).save();
    }

}

module.exports = AddTransactionTask;