const express = require('express');
const router = express.Router();

const Token = require('@src/middleware/token');
const Authorization = require('@src/middleware/authorization');

const TransactionController = require('./transaction.controller');

router.use(Token.verify({ for: 'admin' }));

router.get('/reedemed', Authorization.authorize(["painter", "contractor"]), TransactionController.userDebitTransactions);
router.get('/reedemed/:id', Authorization.authorize(["painter", "contractor"]), TransactionController.userDebitTransaction);

router.get('/debit', Authorization.authorize(["cashAndGiftRequestManagement"]), TransactionController.debitTransactions);
router.get('/debit/:id', Authorization.authorize(["cashAndGiftRequestManagement"]), TransactionController.debitTransaction);

router.get('/:id', Authorization.authorize(["purchaseRegistrationManagement"]), TransactionController.transaction);

router.patch('/debit/:id/updateStatus',Authorization.authorize(["purchaseRegistrationManagement"]), TransactionController.updateDebitTransactionStatus);

router.patch('/:id/updateStatus',Authorization.authorize(["purchaseRegistrationManagement"]), TransactionController.updateTransactionStatus);

module.exports = router;