const Promise = require('bluebird');
const uniqid = require('uniqid');
const _ = require('lodash');

const ApplicationError = require('@src/errors/ApplicationError');
const AddTransactionTask = require('@src/app/transactions/add_transaction/add_transaction.task');
const UpdateLedgerTask = require('@src/app/users/update_ledger/update_ledger.task');
const UserModel = require('../../users/user.model');
const FetchOfferTask = require('../../offers/fetch_offer/fetch_offer.task');


class CreateReverseCreditTransactionTask {

    /**
     * Create Reverse Credit Transaction - 
     * @param {object} document document object
     * @param {string} admin_id admin id
     * @param {session instance} session session instance 
     * @returns 
     */
    static async exec(document = {}, admin_id, session) {

        try {
            // Get approved painter
            const APPROVED_PAINTER = await UserModel.findById(document.painter)
                .where("status").equals("APPROVED").session(session)
                .exec();

            const painter = _.pick(APPROVED_PAINTER, ['_id', 'bp_number', 'pincode', 'total', 'milestone',
                'state', 'balance','offerPoints'
            ]);

            // // Update coupon status
            // const UPDATE_COUPON_STATUS = await CouponsModel
            //     .updateOne()
            //     .where("uuid").equals(document.uuid)
            //     .where("status").equals("USED")
            //     .set("status", "UNUSED").session(session).exec();

            let transaction = {
                mode: "REVERSE-CREDIT",
                txnRefId: document.txnId,
                painter: painter._id,
                bp_number: painter.bp_number,
                pincode: painter.pincode,
                uuid: document.uuid,
                purchase: document.purchase,
                product: document.product,
                actual: document.points,
                createdBy: admin_id,
                multiplier: 1
            };

            transaction["txnId"] = uniqid();
            transaction["points"] = transaction.actual * transaction.multiplier;

            // Update user ledger - Debit Reverse Transaction
            const update_ledger = await UpdateLedgerTask.debitReverseTransaction(painter._id, transaction.points, session);
            if (!update_ledger) return;
            const activeOffer = await FetchOfferTask.exec(document.offer, session);
            if (activeOffer && new Date(activeOffer.start).getTime() <= new Date(document.createdAt).getTime() && new Date(activeOffer.end).getTime() >= new Date(document.createdAt).getTime()) {
                if (activeOffer.enableOfferLedger && _.hasIn(document, 'offerPoints') && _.hasIn(painter, 'offerPoints')) {
                    const UPDATE_OFFER_LEDGER = await UpdateLedgerTask.debitOfferPoints(painter._id, document.offerPoints, session);
                    if (!UPDATE_OFFER_LEDGER) return;
                }
            }

            // Add transaction
            const AddedTransaction = await AddTransactionTask.single(transaction, session);
            if (!AddedTransaction) return;

            return _.pick(transaction, ['points', 'product', 'painter', 'pincode']);

        } catch (error) {
            throw new ApplicationError({
                name: error.name,
                message: error.message
            });
        }
    }

    static validation() {
        return {
            "$id": "http://guru.sheenlac.com/schemas/create_reverse_credit_transaction.json",
            "type": "object",
            "additionalProperties": false
        };
    }

}

module.exports = CreateReverseCreditTransactionTask;