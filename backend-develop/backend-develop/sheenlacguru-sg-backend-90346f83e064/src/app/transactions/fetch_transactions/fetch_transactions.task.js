const TransactionModel = require('../transaction.model');

class FetchTransactionsTask {

    static exec(query = {}, options = {}) {
        return TransactionModel.find()
            .where('painter').equals(query.user)
            .populate('ofProduct')
            .populate('ofGift')
            .populate('ofOffer')
            .select('-__v')
            .exec()
            .then((transactions) => {
                return transactions;
            });
    }

}

module.exports = FetchTransactionsTask;