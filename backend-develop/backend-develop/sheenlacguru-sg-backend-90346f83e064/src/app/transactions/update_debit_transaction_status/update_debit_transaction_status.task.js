const _ = require("lodash");
const TransactionModel = require('../transaction.model');
const ApplicationError = require('@src/errors/ApplicationError');
const CreateReverseDebitTransactionTask = require('../create_reverse_debit_transaction/create_reverse_debit_transaction.task');

class UpdateDebitTransactionStatusTask { 

    static exec(transaction_id,transaction){
        console.log(transaction);
        const FETCH_TRANSACTION = TransactionModel.find({
            _id: transaction_id,
            mode: {
                $in: ['DEBIT', 'SPECIALOFFER-DEBIT']
            },
        }).findOne().exec();
 
        if(transaction.status === "REJECTED"){
            return Promise.all([FETCH_TRANSACTION])
                .then(([fetch_transaction]) => {
                    if (!fetch_transaction)
                        throw new ApplicationError({name: 'TransactionError', message: 'Invalid Transaction.'});
                    transaction = _.defaultsDeep(_.cloneDeep(transaction), { updatedBy : transaction.admin });

                    let REVERSE_DEBIT_TRANSACTION;
                    if(fetch_transaction.status !== "REJECTED"){
                        if(fetch_transaction.status === "PROCESSED"){
                            throw new ApplicationError({name: 'TransactionError', message: 'Cannot reject a tranaction while in processed state.'});
                        }else{
                            REVERSE_DEBIT_TRANSACTION = CreateReverseDebitTransactionTask.exec(fetch_transaction,transaction.admin);
                        }
                    }
                    else if(transaction.comment){
                        const UPDATE_TRANSACTION_STATUS = TransactionModel
                        .where('_id').equals(transaction_id)
                        .updateOne({}, { omitUndefined: true })
                        .set(transaction);

                        return Promise.all([
                            UPDATE_TRANSACTION_STATUS.exec()
                        ]).then(([update]) => {
                            if (!update)
                            throw new ApplicationError({status: 400, name: 'StatusUpdateError', message: 'Status not updated.'});

                            return { message: `Transaction for ${fetch_transaction.id} is updated.`};
                        });
                    }
                    else{
                        throw new ApplicationError({name: 'TransactionStatusError', message: 'Invalid Transaction Status.'});
                    }

                    return Promise.all([
                        REVERSE_DEBIT_TRANSACTION
                    ]).then(([reverse_debit_transaction]) => {
                        if(reverse_debit_transaction){
                            const UPDATE_TRANSACTION_STATUS = TransactionModel
                            .where('_id').equals(transaction_id)
                            .updateOne({}, { omitUndefined: true })
                            .set(transaction);

                            return Promise.all([
                                UPDATE_TRANSACTION_STATUS.exec()
                            ]).then(([update]) => {
                                if (!update)
                                throw new ApplicationError({status: 400, name: 'StatusUpdateError', message: 'Status not updated.'});

                                return { message: `Transaction for ${fetch_transaction.id} is updated.`};
                            });
                        }else{
                            console.log(reverse_debit_transaction);
                        }
                    });
                });
        }else{
            return Promise.all([FETCH_TRANSACTION])
            .then(([fetch_transaction]) => {
                if (!fetch_transaction)
                    throw new ApplicationError({name: 'TransactionError', message: 'Invalid Transaction.'});
                if(fetch_transaction.status !== "REJECTED"){
                    if(fetch_transaction.status === "PROCESSED"){
                            transaction = _.omit(transaction, ['status']);
                            transaction = _.defaultsDeep(_.cloneDeep(transaction), { updatedBy : transaction.admin });
                            console.log(transaction)
                            return TransactionModel.where('_id').equals(transaction_id)
                            .updateOne({}, { omitUndefined: true })
                            .set(transaction)
                            .exec()
                            .then((op_resp) => {
                                if (op_resp.n < 1 || op_resp.nModified < 1)
                                throw new ApplicationError({ name: "StatusUpdateError", message: "Transaction status is not updated." });
                                return { matched: op_resp.n, updated: op_resp.nModified, message: 'Cannot change the status of tranaction while in processed state.' };
                            });
                        // throw new ApplicationError({name: 'TransactionError', message: 'Cannot change the status of tranaction while in processed state.'});
                    }
                    else{
                        transaction = _.defaultsDeep(_.cloneDeep(transaction), { updatedBy : transaction.admin });
                        console.log(transaction)
                        return TransactionModel.where('_id').equals(transaction_id)
                        .updateOne({}, { omitUndefined: true })
                        .set(transaction)
                        .exec()
                        .then((op_resp) => {
                            if (op_resp.n < 1 || op_resp.nModified < 1)
                            throw new ApplicationError({ name: "StatusUpdateError", message: "Transaction status is not updated." });
                            return { matched: op_resp.n, updated: op_resp.nModified, message: "Transaction status is updated." };
                        });
                    }
                }else{
                    throw new ApplicationError({name: 'TransactionStatusError', message: 'Invalid Transaction Status.'});
                }
            });
        }
    }   

    static validation() {
        return {
            "$id": "http://guru.sheenlac.com/schemas/transactions/update_debit_transaction_status.json",
            "type": "object",
            "properties": {
                "status": { "type": "string", "enum": ["NEW","APPROVED","PROCESSED","REJECTED"] },
                "tracking_reference_number" : { "type": "string"},
                "comment": { "type": "string" }
            },
            "required": ["status"],
            "additionalProperties": false
        };
    }
}

module.exports = UpdateDebitTransactionStatusTask;
