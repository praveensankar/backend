const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const transactionSchema = new Schema({
    txnId : {
        type: String,
        trim: true
    },
    txnRefId: {
        type: String,
        trim: true
    },
    uuid: {
        type: String,
        required: [true, 'unique id is required.'],
        trim: true
    },
    product: {
        type: String,
        trim: true
    },
    gift: {
        type: String,
        trim: true
    },
    gift_type: {
        type: String,
        trim: true,
        enum: ['cash', 'product']
    },
    offer: {
        type: String,
        trim: true
    },
    multiplier: {
        type: Number,
        default: 1
    },
    actual: {
        type: Number,
        default: 0
    },
    points: {
        type: Number,
        default: 0
    },
    offerPoints: {
        type: Number,
        default: 0
    },
    painter: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: [true, 'Painter is required.']
    },
    bp_number: {
        type: String,
        trim: true,
        required: [true, 'Business partner number is required.']
    },
    pincode: {
        type: String,
        trim: true,
        required: [true, 'Painter location is required.']
    },
    mode: {
        type: String,
        enum: ["CREDIT", "DEBIT", "REVERSE-CREDIT", "REVERSE-DEBIT", "BONUS-CREDIT", "SPECIALOFFER-DEBIT"],
        required: [true, "Transaction mode is required."]
    },
    scan_type: {
        type: String,
        enum: ["AUTO","MANUAL","ADMIN-MANUAL"]
    },
    purchase: {
        type: Schema.Types.ObjectId,
        ref: 'Purchase'
    },
    purchasedOn:{
        type: Date
    },
    createdBy: {
        type: Schema.Types.ObjectId
    },
    approvedBy: {
        type: Schema.Types.ObjectId,
        ref : "Admin"
    },
    approvedAt: {
        type: Date
    },
    updatedBy: {
        type: Schema.Types.ObjectId,
        ref : "Admin"
    },
    status: {
        type: String,
        enum: ["NEW","APPROVED","PROCESSING","PROCESSED","REJECTED"]
    },
    transferStatus : {
        type : Object
    },
    transferLog : {
        type : [Object]
    },
    tracking_reference_number: {
        type: String,
        trim: true
    },
    comment: {
        type: String,
        trim: true
    }
},
{
    timestamps: true
});

transactionSchema.set('toJSON', { virtuals: true, getters: true });

transactionSchema.virtual('ofProduct', {
    ref: 'Product',
    localField: 'product',
    foreignField: 'id',
    justOne: true
});

transactionSchema.virtual('ofGift', {
    ref: 'Gift',
    localField: 'gift',
    foreignField: 'id',
    justOne: true
});

transactionSchema.virtual('ofOffer', {
    ref: 'Offer',
    localField: 'offer',
    foreignField: 'id',
    justOne: true
});

transactionSchema.virtual('ofPurchase', {
    ref: 'Purchase',
    localField: 'purchase',
    foreignField: '_id',
    justOne: true
});

transactionSchema.virtual('createdByAdmin', {
    ref: 'Admin',
    localField: 'createdBy',
    foreignField: '_id',
    justOne: true
});

transactionSchema.virtual('createdByPainter', {
    ref: 'User',
    localField: 'createdBy',
    foreignField: '_id',
    justOne: true
});

module.exports = mongoose.model("Transaction", transactionSchema);