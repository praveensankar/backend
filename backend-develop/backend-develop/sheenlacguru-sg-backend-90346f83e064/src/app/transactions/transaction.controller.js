const Ajv = require('ajv');
const _ = require('lodash');

const ApplicationError = require('@src/errors/ApplicationError');
const validations = require('@app/validations');
const ajv = new Ajv(validations.ajv_options);

const FetchDebitTransactionsTask = require('./fetch_debit_transactions/fetch_debit_transactions.task');
const FetchDebitTransactionTask = require('./fetch_debit_transaction/fetch_debit_transaction.task');
const UpdateDebitTransactionStatusTask = require('./update_debit_transaction_status/update_debit_transaction_status.task');
const FetchTransactionTask = require('./fetch_transaction/fetch_transaction.task');
const UpdateTransactionStatusTask = require('./update_transaction_status/update_transaction_status.task');

class TransactionController {

    static debitTransactions(req, res, next) {
        if (!ajv.validate(FetchDebitTransactionsTask.validation(), req.query)) {
            let errors = ajv.errors;
            return next(
                new ApplicationError({status: 400, name: "ValidationError", message: "Bad request parameters.", info: errors})
            );
        }
        
        return FetchDebitTransactionsTask.exec({}, req.query)
            .then(data => {
                return res.status(200).json(
                    new res.app.locals.Success({ data: data })
                );
            })
            .catch(err => next(err));
    }

    static userDebitTransactions(req, res, next) {
        if(!ajv.validate(FetchDebitTransactionsTask.validation(), req.query)) {
            let errors = ajv.errors;
            return next(
                new ApplicationError({status: 400, name: "ValidationError", message: "Bad request parameters.", info: errors})
            );
        }

        let query = {"painter": req.locals["user_id"]};
        return FetchDebitTransactionsTask.exec(query, req.query)
            .then(data => {
                return res.status(200).json(
                    new res.app.locals.Success({ data: data })
                )
            })
            .catch(err => next(err));
    }

    static debitTransaction(req, res, next) {
        return FetchDebitTransactionTask.exec(req.params.id)
            .then(data => {
                return res.status(200).json(
                    new res.app.locals.Success({ data: data })
                )
            })
            .catch(err => next(err));
    }

    static userDebitTransaction(req, res, next) {
        let query = {"painter": req.locals["user_id"]};
        return FetchDebitTransactionTask.exec(req.params.id, query)
            .then(data => {
                return res.status(200).json(
                    new res.app.locals.Success({ data: data })
                )
            })
            .catch(err => next(err));
    }

    static updateDebitTransactionStatus(req, res, next) {
        if (!ajv.validate(UpdateDebitTransactionStatusTask.validation(), req.body)) {
            let errors = ajv.errors;
            return next(
                new ApplicationError({status: 400, name: 'ValidationError', message: 'Bad request parameters.', info: errors})
            );
        }
        
        req.body = _.defaultsDeep(_.cloneDeep(req.body), { admin: req.locals.user_id });
        return UpdateDebitTransactionStatusTask.exec(req.params.id,req.body)
            .then(op_resp => {
                return res.status(200).json(
                    new res.app.locals.Success({ data: op_resp })
                );
            })
            .catch(err => next(err));
    }

    static transaction(req, res, next) {
        return FetchTransactionTask.exec(req.params.id)
            .then(data => {
                return res.status(200).json(
                    new res.app.locals.Success({ data: data })
                )
            })
            .catch(err => next(err));
    }

    static updateTransactionStatus(req, res, next) {
        if (!ajv.validate(UpdateTransactionStatusTask.validation(), req.body)) {
            let errors = ajv.errors;
            return next(
                new ApplicationError({status: 400, name: 'ValidationError', message: 'Bad request parameters.', info: errors})
            );
        }
        req.body = _.defaultsDeep(_.cloneDeep(req.body), { admin: req.locals.user_id });
        return UpdateTransactionStatusTask.exec(req.params.id,req.body)
            .then(op_resp => {
                return res.status(200).json(
                    new res.app.locals.Success({ data: op_resp })
                );
            })
            .catch(err => next(err));
    }

}

module.exports = TransactionController;