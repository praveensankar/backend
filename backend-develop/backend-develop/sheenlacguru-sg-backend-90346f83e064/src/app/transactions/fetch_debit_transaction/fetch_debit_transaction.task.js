const TransactionModel = require('../transaction.model');

class FetchDebitTransactionTask {

    static exec(transactionId, query = {}) {
        let Query = TransactionModel.findOne({
            mode: {
                $in: ['DEBIT', 'SPECIALOFFER-DEBIT']
            }
        }).where("_id").equals(transactionId);
        if (query.painter) Query.where("painter").equals(query.painter);
        return Query
            .populate("painter")
            .populate("ofGift")
            .select('-__V').exec();
    }

}

module.exports = FetchDebitTransactionTask;