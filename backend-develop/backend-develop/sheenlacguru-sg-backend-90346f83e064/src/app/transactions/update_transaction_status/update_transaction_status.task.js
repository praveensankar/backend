const Promise = require('bluebird');
const _ = require('lodash');
const mongoose = require('mongoose');
const TransactionModel = require('../transaction.model');
const ApplicationError = require('@src/errors/ApplicationError');

const UpdateLedgerTask = require('@src/app/users/update_ledger/update_ledger.task');
const CreateReverseCreditTransactionTask = require('../create_reverse_credit_transaction/create_reverse_credit_transaction.task');
const CreateBonusCreditTransactionTask = require('../create_bonus_credit_transaction/create_bonus_credit_transaction.task');
const FetchOfferTask = require('../../offers/fetch_offer/fetch_offer.task');


class UpdateTransactionStatusTask {

    /**
     * Update Transaction Status
     * @param {string} transaction_id transaction id
     * @param {object} transaction transaction object
     * @return return
     */
    static async exec(transaction_id, transaction) {

        // start mongoose transaction
        const session = await mongoose.startSession();
        session.startTransaction({
            readConcern: {
                level: "snapshot"
            },
            writeConcern: {
                w: 1
            }
        });
        try {

            // Fetch valid transaction
            const fetch_transaction = await TransactionModel.where("_id").equals(transaction_id).findOne().session(session).exec();
            console.log("nafr...2",JSON.stringify(fetch_transaction))
            if (!fetch_transaction)
                throw new ApplicationError({
                    name: 'TransactionError',
                    message: 'Invalid Transaction.'
                });
            
            // Transaction status (NEW)
            if (transaction.status === "NEW") {
                // Painters current transaction status (NEW)
                if (fetch_transaction.status === "NEW") {
                    // Update transaction status
                    const UPDATE_TRANSACTION_STATUS = await TransactionModel
                        .where('_id').equals(transaction_id)
                        .updateOne({}, {
                            omitUndefined: true
                        })
                        .set(transaction).session(session).exec();
                    if (!UPDATE_TRANSACTION_STATUS)
                        throw new ApplicationError({
                            name: 'StatusUpdateError',
                            message: 'Status not updated.'
                        });
                    
                    // commit transaction
                    await session.commitTransaction();
                    return {
                        message: `Transaction is updated.`
                    }
                } else {
                    throw new ApplicationError({
                        name: 'TransactionError',
                        message: 'Invalid Transaction Status.'
                    });
                }
            }

            // Transaction status (APPROVED)
            if (transaction.status === "APPROVED") {
                let UPDATE_LEDGER;
                let UPDATE_OFFER_LEDGER;
                // Invalid operation - Painters current transaction status (REJECTED)
                if (fetch_transaction.status === "REJECTED") {
                    throw new ApplicationError({
                        name: 'TransactionError',
                        message: 'Invalid Transaction Status.'
                    });
                }

                // Approve only transaction with NEW status
                if ((fetch_transaction.status === "NEW") || (fetch_transaction.status === "RE-APPROVED")) {
                    // approve transaction
                    transaction = _.defaultsDeep(_.cloneDeep(transaction), {
                        approvedBy: transaction.admin,
                        approvedAt: new Date().toISOString()
                    });
                    console.log(new Date(fetch_transaction.createdAt).getTime())
                    // update the user ledger, credit the points to user (+balance,+total,-underReview)
                    UPDATE_LEDGER = await UpdateLedgerTask.credit(fetch_transaction.painter, fetch_transaction.points, session);
                    if (!UPDATE_LEDGER) return;
                    
                    const activeOffer = await FetchOfferTask.exec(fetch_transaction.offer, session);
                    if (activeOffer && new Date(activeOffer.start).getTime() <= new Date(fetch_transaction.createdAt).getTime() && new Date(activeOffer.end).getTime() >= new Date(fetch_transaction.createdAt).getTime()) {
                        if (activeOffer.enableOfferLedger) {
                            UPDATE_OFFER_LEDGER = await UpdateLedgerTask.creditOfferPoints(fetch_transaction.painter, fetch_transaction.points, session);
                            if (!UPDATE_OFFER_LEDGER) return;
                            transaction = _.defaultsDeep(_.cloneDeep(transaction), {
                                offerPoints: fetch_transaction.points
                            });
                        }
                    }
                }
                console.log(transaction);
                // updatedBy 
                transaction = _.defaultsDeep(_.cloneDeep(transaction), {
                    updatedBy: transaction.admin
                });

                // Update transaction status
                const UPDATE_TRANSACTION_STATUS = await TransactionModel
                    .where('_id').equals(transaction_id)
                    .updateOne({}, {
                        omitUndefined: true
                    })
                    .set(transaction).session(session).exec();

                if (!UPDATE_TRANSACTION_STATUS)
                    throw new ApplicationError({
                        name: 'StatusUpdateError',
                        message: 'Status not updated.'
                    });
                // if valid update ledger and update transaction status
                if (UPDATE_LEDGER && UPDATE_TRANSACTION_STATUS) {
                    const BONUS_CREDIT_TRANSACTION = await CreateBonusCreditTransactionTask.exec(fetch_transaction, fetch_transaction.painter, transaction.admin, session);
                    console.log(BONUS_CREDIT_TRANSACTION);
                }

                // commit transaction
                await session.commitTransaction();
                // return
                return {
                    message: `Transaction for ${fetch_transaction.uuid} is updated.`
                };
            }

            // When transaction status is REJECTED
            if (transaction.status === "REJECTED") {
                // When current transaction is other than rejected
                if (fetch_transaction.status !== "REJECTED") {
                    // If current transaction status is NEW
                    if (fetch_transaction.status === "NEW") {
                        const update_ledger = await UpdateLedgerTask.credit(fetch_transaction.painter, fetch_transaction.points, session);
                        if (!update_ledger) return;
                    }
                    
                    //When current transaction status is APPROVED
                    const reverse_credit_transaction = await CreateReverseCreditTransactionTask.exec(fetch_transaction, transaction.admin, session);
                    if (!reverse_credit_transaction) return;
                    // On successfull reverse credit transaction, update the status and commit the changes.
                    if (reverse_credit_transaction) {
                        transaction = _.defaultsDeep(_.cloneDeep(transaction), {
                            updatedBy: transaction.admin
                        });

                        // Update transaction status
                        const update_status = await TransactionModel
                            .where('_id').equals(transaction_id)
                            .updateOne({}, {
                                omitUndefined: true
                            })
                            .set(transaction).session(session).exec();
                        if (!update_status)
                            throw new ApplicationError({
                                status: 400,
                                name: 'StatusUpdateError',
                                message: 'Status not updated.'
                            });
                    }
                    // commit transaction
                    await session.commitTransaction();
                    // return
                    return {
                        message: `Transaction for ${fetch_transaction.uuid} is updated.`
                    };

                } else {
                    throw new ApplicationError({
                        name: 'TransactionError',
                        message: 'Invalid Transaction Status.'
                    });
                }
            }
        } catch (error) {
            console.log(error)
            await session.abortTransaction();
            throw new ApplicationError({
                name: error.name,
                message: error.message
            });
        } finally {
            session.endSession();
        }
    }

    static validation() {
        return {
            "$id": "http://guru.sheenlac.com/schemas/transactions/update_transaction_status.json",
            "type": "object",
            "properties": {
                "status": {
                    "type": "string",
                    "enum": ["NEW", "APPROVED", "REJECTED"]
                },
                "comment": {
                    "type": "string"
                }
            },
            "required": ["status"],
            "additionalProperties": false
        };
    }
}

module.exports = UpdateTransactionStatusTask;