const TransactionModel = require('../transaction.model');
const uniqid = require('uniqid');
const ApplicationError = require('@src/errors/ApplicationError');

class CreateDebitTransactionTask {

    /**
     * Create debit transaction
     * @param {object} transaction transaction object
     * @return return transaction
     */
    static exec(transaction) {
        /* Create unique transaction request id. */
        transaction["txnId"] = uniqid();
        return new TransactionModel(transaction).save()
            .then(transaction => {
                if (!transaction)
                    throw new ApplicationError({ name: 'CreateTransactionError', message: 'Transaction not created.' });

                return transaction;
            });
    }

    static validation() {
        return {
            "$id": "http://guru.sheenlac.com/schemas/create_debit_transaction.json",
            "type": "object",
            "properties": {
                "painter": {
                    "type": "string",
                    "minLength": 24,
                    "maxLength": 24
                }
            },
            "required": ["bills", "painter"],
            "additionalProperties": false
        };
    }

}

module.exports = CreateDebitTransactionTask;