const mongoose = require('mongoose');
const TransactionModel = require('../transaction.model');
const UserModel = require("../../users/user.model");

class FetchDebitTranscactionsTask {

    static async exec(query = {}, options = {}) {
        console.log(options);
        const skip = options.page > 0 ? (options.page - 1) * options.size : 0;
        let FetchDebitTransactionsQuery = TransactionModel.find({
            mode: {
                $in: ['DEBIT', 'SPECIALOFFER-DEBIT']
            },
        });
        if (options.type !== "all") FetchDebitTransactionsQuery.where("gift_type").equals(options.type);

        let transactions = [];
        let counts = 0;
        var usersIds = [];

        if(options.searchText !== ""){
            /* Start of Search */
            const listUsers = [];
            /* ------- Search with exact match using text based search */
            if(options.searchText.split("-")[0] === "SH") options.searchText = options.searchText.split("-")[1];
            const usersWithExactMatch = await UserModel.find({ $text : { $search : options.searchText }})
            // insert list of searched users
            listUsers.push(...usersWithExactMatch);
            
            /* -------- Search with start (first_name,last_name,mobile_number,bp_number) */
            const rgxForStartMatch = (pattern) => new RegExp(`^${pattern}`);    // pattern for matching start of searchText
            const searchTextForStartMatch = rgxForStartMatch(options.searchText);  
            const usersWithStartMatch = await UserModel.find({ $or : 
                [{ first_name : { $regex : searchTextForStartMatch, $options : 'i' } },
                { last_name : { $regex : searchTextForStartMatch, $options : 'i' }},
                { mobile_number : { $regex : searchTextForStartMatch, $options : 'i' }},
                { bp_number :{ $regex : searchTextForStartMatch, $options : 'i' }}]})
            // insert list of searched users
            listUsers.push(...usersWithStartMatch);

            /* -------- Fuzzy search with (first_name,last_name,mobile_number,bp_number) */
            const rgxForFuzzyMatch = (pattern) => new RegExp(`${pattern}`);
            const searchTextForFuzzyMatch = rgxForFuzzyMatch(options.searchText);
            const usersWithFuzzyMatch = await UserModel.find({ $or : 
                [{ first_name : { $regex : searchTextForFuzzyMatch, $options : 'i' } },
                { last_name : { $regex : searchTextForFuzzyMatch, $options : 'i' }},
                { mobile_number : { $regex : searchTextForFuzzyMatch, $options : 'i' }},
                { bp_number :{ $regex : searchTextForFuzzyMatch, $options : 'i' }}]})
            // insert list of searched user
            listUsers.push(...usersWithFuzzyMatch);

            /* generate list of distinct users if users got repeated */
 
            const map = new Map();
            for (const item of listUsers) {
                if(!map.has(item.id)){
                    map.set(item.id, true);
                    usersIds.push(item._id);
                }
            }

            usersIds.length > 0 ? FetchDebitTransactionsQuery.find({ "painter" : { $in : usersIds}}) : FetchDebitTransactionsQuery.where("_id").equals(options.searchText);
            
            if (options.size > 0) FetchDebitTransactionsQuery.limit(options.size);
            if (options.status !== "ALL") FetchDebitTransactionsQuery.where("status").equals(options.status);
            FetchDebitTransactionsQuery.select('-__v').populate('painter').populate('ofGift').skip(skip);
            transactions = await FetchDebitTransactionsQuery.sort({createdAt : -1 });

            let CountQuery = usersIds.length > 0 ? TransactionModel.find({ "painter" : { $in : usersIds}}).where("mode").equals("DEBIT") : TransactionModel.where("_id").equals(options.searchText);
            
            if (options.type !== "all") CountQuery.where("gift_type").equals(options.type);
            if (options.status !== "ALL") CountQuery.where("status").equals(options.status);
            counts = await CountQuery.countDocuments().exec();
            // need to be done later
            // FetchDebitTransactionsQuery.where("uuid").equals(options.searchText);

            /* End of Search */
        }else{

            if (options.size > 0) FetchDebitTransactionsQuery.limit(options.size);
            if (options.status !== "ALL") FetchDebitTransactionsQuery.where("status").equals(options.status);
            FetchDebitTransactionsQuery.select('-__v').populate('painter').populate('ofGift').skip(skip);
            transactions = await FetchDebitTransactionsQuery.sort({createdAt : -1 });

            // Count Query for total without search and with status 
            let CountQuery = TransactionModel.find({
                mode: {
                    $in: ['DEBIT', 'SPECIALOFFER-DEBIT']
                },
            });
            if (options.type !== "all") CountQuery.where("gift_type").equals(options.type);
            if (options.status !== "ALL") CountQuery.where("status").equals(options.status);
            counts = await CountQuery.countDocuments().exec();
        }
        
        return Promise.all([
            transactions,
            counts
        ]).then(([transactions, count]) => {
            return {
                items: transactions,
                totalCount: count
            };
        });
    }

    static validation() {
        return {
            "$id": "http://guru.sheenlac.com/schemas/list_trainings_query_params.json",
            "$async": true,
            "type": "object",
            "properties": {
                "page": { "type": "integer", "default": 0 },
                "size": { "type": "integer", "default": 0 },
                "type": { "type": "string", "enum": ['all', 'cash', 'product'],"default": "all" },
                "status": { "type": "string", "enum": ["ALL","NEW","APPROVED","PROCESSED","CANCELLED"],"default": "ALL" },
                "searchText" : { "type" : "string", "default" : ""}
            },
            "required": [],
            "additionalProperties": false
        };
    }

}

module.exports = FetchDebitTranscactionsTask;