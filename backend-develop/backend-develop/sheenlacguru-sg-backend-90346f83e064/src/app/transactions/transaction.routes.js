const express = require('express');
const router = express.Router();

const Token = require('@src/middleware/token');
const Authorization = require('@src/middleware/authorization');

const TransactionController = require('./transaction.controller');

router.use(Token.verify({ for: 'admin' }));

router.get('/reedemed', Authorization.authorize(["painter", "contractor"]), TransactionController.userDebitTransactions);
router.get('/reedemed/:id', Authorization.authorize(["painter", "contractor"]), TransactionController.userDebitTransaction);

router.get('/debit', Authorization.authorize(["operations", "operations-l2"]), TransactionController.debitTransactions);
router.get('/debit/:id', Authorization.authorize(["operations", "operations-l2"]), TransactionController.debitTransaction);

router.get('/:id', Authorization.authorize(["operations", "operations-l2"]), TransactionController.transaction);

router.patch('/debit/:id/updateStatus',Authorization.authorize(["operations", "operations-l2"]), TransactionController.updateDebitTransactionStatus);

router.patch('/:id/updateStatus',Authorization.authorize(["operations", "operations-l2"]), TransactionController.updateTransactionStatus);

module.exports = router;