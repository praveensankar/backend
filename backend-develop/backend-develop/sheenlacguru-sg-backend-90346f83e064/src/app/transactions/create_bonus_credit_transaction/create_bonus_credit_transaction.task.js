const uniqid = require('uniqid');
const _ = require('lodash');
var fs = require('fs')

const ApplicationError = require('@src/errors/ApplicationError');
const AddTransactionTask = require('@src/app/transactions/add_transaction/add_transaction.task');
const UpdateLedgerTask = require('@src/app/users/update_ledger/update_ledger.task');
const FetchMilestoneTask = require('../../milestones/fetch_milestone/fetch_milestone.task');
const UserModel = require('../../users/user.model');
const MilestonesModel = require('../../milestones/milestones.model');
const TransactionModel = require('../../transactions/transaction.model');


class CreateBonusCreditTransactionTask {

    /**
     * Bonus credit transaction - Transaction to credit bonus point to the user.
     * @param {string} painter_id painter id
     * @param {string} admin_id admin id
     * @param {session instance} session session instance 
     * @return return 
     */
    static async exec(transaction, painter_id, admin_id, session) {

        try {
            // Initialize variables
            let milestone = {};
            let prevTransactions = [];
            // console.log("naf...334..transaction",transaction)
            // Get approved painter
            const APPROVED_PAINTER = await UserModel.findById(painter_id)
                .where("status").equals("APPROVED").session(session)
                .exec();
                // console.log("naf12...",APPROVED_PAINTER)
            if (!APPROVED_PAINTER)
                throw new ApplicationError({
                    name: "InvalidPainterStatus",
                    message: "Painter is not approved."
                });
            let painter = _.pick(APPROVED_PAINTER, ['_id', 'bp_number', 'pincode', 'total', 'milestone', 'isHighestMilestone', 'state', 'balance']);
            // Get milestones
            let activeBonusMilestones = await MilestonesModel.find({
                isActive: true,
                milestoneType: "BONUS-MILESTONE"
            });
            // console.log(activeBonusMilestones[0]['start'],".....activeBonusMilestones....",activeBonusMilestones[activeBonusMilestones.length-1]['end'],"...bpNumber...",painter.bp_number);
            
            let calculatedBonusPoint = await TransactionModel.find({
                bp_number : painter.bp_number,
                createdAt: {
                    $lte: new Date(activeBonusMilestones[activeBonusMilestones.length-1]['end'])
                },
                createdAt: {
                    $gte: new Date(activeBonusMilestones[0]['start'])
                },
                "$or": [{
                    mode : "CREDIT"
                    },{
                    mode : "REVERSE-CREDIT"
                    }],
                // "$or": [{
                //     status : "APPROVED"
                //     },{
                //     status : "RE-APPROVED"
                //     }]
                status:"NEW"             
            });

            let debitedBonusPoint = await TransactionModel.find({
                bp_number : painter.bp_number,
                createdAt: {
                    $lte: new Date(activeBonusMilestones[activeBonusMilestones.length-1]['end'])
                },
                createdAt: {
                    $gte: new Date(activeBonusMilestones[0]['start'])
                },
                "$or": [{
                    status : "APPROVED"
                    },{
                    status : "RE-APPROVED"
                    }],
                // status:"NEW",
                "$or": [{
                    mode : "SPECIALOFFER-DEBIT"
                    },{
                    mode : "DEBIT"
                    }]
            });
            let totalDebitedPoints = _.sumBy(debitedBonusPoint, 'points')
            let totalCalculatedPoints = _.sumBy(calculatedBonusPoint, 'points')
            
            painter['total'] = totalCalculatedPoints
            
            // console.log("calculatedBonusPoint....",calculatedBonusPoint)
            // console.log("naf33...painter..",painter)

            var milestones = await MilestonesModel.find({
                isActive: true,
                milestoneType: "BONUS-MILESTONE",
                start: {
                    $lte: transaction.createdAt
                },
                end: {
                    $gte: transaction.createdAt
                },
                "points.points": {
                    $lte: painter.total
                }
            });
            if (milestones && milestones.length) {
                const activeMilestoneIds = milestones.map(milestone => milestone._id.toString());
                // console.log(activeMilestoneIds)
                const bonusTransactions = await TransactionModel.find({
                    painter: painter._id,
                    mode: "BONUS-CREDIT",
                    uuid: {
                        $in: activeMilestoneIds
                    }
                });
                const bonusCreatedMiestoneIds = bonusTransactions.map(milestone => milestone.uuid.toString());
                let milestonesToAdd = []
                milestones.map(milestone => {
                    if (!bonusCreatedMiestoneIds.includes(milestone._id.toString())) {
                        milestonesToAdd.push(milestone)
                    }
                });
                // // Create bonusPoints array with points and milestoneId
                // let bonusPoints = milestones.map(milestone => {
                //     return {
                //         points: milestone.points[0].points,
                //         milestoneId: milestone._id
                //     }
                // });
                // // sort the bonusPoints array
                milestones = milestones.sort((a, b) => a.points[0].points - b.points[0].points);
                // const total = painter.total; // user total points
                // // calculate index where total lies
                // var index = _.sortedIndexBy(bonusPoints, {
                //     "points": total
                // }, 'points');
                // console.log(bonusPoints)
                // console.log(index)
                // const prevIndex = bonusPoints[index - 1];
                // const nextIndex = bonusPoints[index];
                // console.log("prevM", prevIndex);
                // console.log("nextM", nextIndex);

                // // Milestone not present on painters object
                // // if (painter.milestone === undefined) {
                // let lastIndex = index + 1;
                // const prevPoints = _.slice(bonusPoints, 0, lastIndex);
                // const milestoneIds = prevPoints.map(milestone => milestone.milestoneId);
                // const prevMilestones = await MilestonesModel.find({
                //     _id: {
                //         $in: milestoneIds
                //     }
                // });
                milestonesToAdd.map(milestone => {
                    let multiTransaction = {
                        mode: "BONUS-CREDIT",
                        painter: painter._id,
                        bp_number: painter.bp_number,
                        pincode: painter.pincode,
                        uuid: milestone._id,
                        product: `${milestone.name} Bonus Points`,
                        actual: milestone.bonusPoint,
                        createdBy: admin_id,
                        multiplier: 1
                    };
                    multiTransaction["txnId"] = uniqid();
                    multiTransaction["points"] = _.get(multiTransaction, "actual", 0) * _.get(multiTransaction, "multiplier", 1);
                    return prevTransactions.push(multiTransaction);
                });
                // milestone = await MilestonesModel.findById(prevIndex.milestoneId);

                if (prevTransactions && prevTransactions.length) {
                    const totalPoints = prevTransactions.reduce((previous, next) => previous + next.points, 0);
                    let tempData = [];
                    
                    const UpdatedLedger = await UpdateLedgerTask.creditBonusPoints(painter._id, totalPoints, session);
                    if (!UpdatedLedger) return;
                    const AddedTransaction = await AddTransactionTask.many(prevTransactions, session);
                    if (!AddedTransaction) return;
                    // let bonusCreditdata = fs.readFileSync(`${__dirname}/../../../../../bonusCreditNew.json`);
                    // let bonusCreditjson = JSON.parse(bonusCreditdata);
                    // tempData.push({
                    //     painterId: prevTransactions[0]['painter'],
                    //     bonusPoints: _.sumBy(prevTransactions,'points')
                    // })
                    // bonusCreditjson = [...bonusCreditjson,...tempData]
                    // await fs.writeFileSync(`${__dirname}/../../../../../bonusCreditNew.json`, JSON.stringify(bonusCreditjson))
                    
                }
                // const nextMilestone = nextIndex ? await MilestonesModel.findById(nextIndex.milestoneId) : false;
                // Update user milestone
                const UPDATE_USER = await UserModel.updateOne({}, {
                        omitUndefined: true
                    })
                    .where('_id').equals(painter_id)
                    .set({
                        milestone: _.takeRight(milestones)[0].name,
                        milestoneUpdatedAt: new Date().toISOString()
                    }).session(session).exec();
                if (!UPDATE_USER)
                    throw new ApplicationError({
                        name: "MilestoneUpdateError",
                        message: "Milestone not added to user."
                    });
                // return
                // return;
                // }
                // else {
                //     // milestone = await FetchMilestoneTask.getByName(painter.milestone);
                //     const milestone = await MilestonesModel.findOne({
                //         isActive: true,
                //         _id: nextIndex ? nextIndex.milestoneId : prevIndex.milestoneId,
                //         milestoneType: "BONUS-MILESTONE",
                //         start: {
                //             $lte: transaction.createdAt
                //         },
                //         end: {
                //             $gte: transaction.createdAt
                //         }
                //     });
                //     console.log(milestone)
                //     const nextMilestone = nextIndex ? await MilestonesModel.findById(nextIndex.milestoneId) : false;
                //     painter.isHighestMilestone = nextIndex ? true : false;
                //     if (milestone && !painter.isHighestMilestone) {
                //         console.log(prevIndex.milestoneId !== painter.nextMilestone)
                //         // if (prevIndex.milestoneId !== painter.nextMilestone) {

                //             let currentTransactions = {
                //                 mode: "BONUS-CREDIT",
                //                 painter: painter._id,
                //                 bp_number: painter.bp_number,
                //                 pincode: painter.pincode,
                //                 uuid: milestone._id,
                //                 product: `${milestone.name} Bonus Points`,
                //                 actual: milestone.bonusPoint,
                //                 createdBy: admin_id,
                //                 multiplier: 1
                //             }

                //             currentTransactions["txnId"] = uniqid();
                //             currentTransactions["points"] = _.get(currentTransactions, "actual", 0) * _.get(currentTransactions, "multiplier", 1);

                //             // calculate if painter total minus milestone point is less than zero - so change the milestone and create transaction
                //             if ((milestone.points[0].points - painter.total) <= 0) {
                //                 // Update user ledger
                //                 const UpdatedLedger = await UpdateLedgerTask.creditBonusPoints(painter._id, currentTransactions.points, session);
                //                 if (!UpdatedLedger) return;

                //                 // Add transaction
                //                 const AddedTransaction = await AddTransactionTask.single(currentTransactions, session);
                //                 if (!AddedTransaction) return;

                //                 // Update user milestone
                //                 const UPDATE_USER = await UserModel.updateOne({}, {
                //                     omitUndefined: true
                //                 })
                //                     .where('_id').equals(painter_id)
                //                     .set({
                //                         milestone: nextIndex ? nextMilestone.name : milestone.name,
                //                         isHighestMilestone: nextIndex ? false : true,
                //                         milestoneUpdatedAt: new Date().toISOString()
                //                     }).session(session).exec();
                //                 if (!UPDATE_USER)
                //                     throw new ApplicationError({
                //                         name: "MilestoneUpdateError",
                //                         message: "Milestone not added to user."
                //                     });
                //             // }
                //         }
                //         // } else {

                //         //     // Update user milestone
                //         //     const UPDATE_USER = await UserModel.updateOne({}, {
                //         //         omitUndefined: true
                //         //     })
                //         //         .where('_id').equals(painter_id)
                //         //         .set({
                //         //             nextMilestone: nextIndex ? nextMilestone._id : milestone._id,
                //         //             milestoneUpdatedAt: new Date().toISOString()
                //         //         }).session(session).exec();
                //         //     if (!UPDATE_USER)
                //         //         throw new ApplicationError({
                //         //             name: "MilestoneUpdateError",
                //         //             message: "Milestone not added to user."
                //         //         });
                //         //     return;
                //         // }
                //         return;
                //     }
                //     return {
                //         message: 'Bonus Credited'
                //     }
                // }
                return {
                    message: 'Bonuses Credited'
                }
            } else {
                return {
                    message: 'No active milestone.'
                }
            }
        } catch (error) {
            console.log(error.stack)
            throw new ApplicationError({
                name: error.name,
                message: error.message
            });
        }
    }
}

module.exports = CreateBonusCreditTransactionTask;