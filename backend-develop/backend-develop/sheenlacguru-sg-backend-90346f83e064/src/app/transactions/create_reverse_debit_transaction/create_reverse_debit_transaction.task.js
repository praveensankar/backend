const Promise = require('bluebird');
const uniqid = require('uniqid');
const _ = require('lodash');

const ApplicationError = require('@src/errors/ApplicationError');
const GetApprovedUser = require('@src/app/users/get_approved_user/get_approved_user.task');
const AddTransactionTask = require('@src/app/transactions/add_transaction/add_transaction.task');
const UpdateLedgerTask = require('@src/app/users/update_ledger/update_ledger.task');

class CreateReverseDebitTransactionTask {

    /**
     * Create Reverse Debit Transaction
     * @param {object} document document object
     * @param {string} admin_id admin id
     * @return return 
     */
    static exec(document = {},admin_id) {

        // Get approved painter
        const APPROVED_PAINTER = GetApprovedUser.exec({user:document.painter}).then(painter => {
            return _.pick(painter, ['_id', 'bp_number', 'pincode', 'state', 'balance']);
        });

        return Promise.all([APPROVED_PAINTER])
            .then(([painter]) => {
                let transaction = {
                    mode: "REVERSE-DEBIT",
                    txnRefId : document.txnId,
                    painter: painter._id,
                    bp_number: painter.bp_number,
                    pincode: painter.pincode,
                    uuid: document.uuid,
                    purchase : document.purchase,
                    product: document.product,
                    actual : document.points,
                    createdBy : admin_id,
                    multiplier: 1
                };

                transaction["txnId"] = uniqid();
                transaction["points"] = transaction.actual * transaction.multiplier;

                return Promise.all([
                    UpdateLedgerTask.creditReverseTransaction(painter._id, transaction.points),
                    AddTransactionTask.single(transaction)
                ]).spread( (userLedgerUpdateStatus,addedTransaction) => {
                    return  _.pick(transaction, ['points', 'product', 'painter', 'pincode']);
                });
            });
    }

    static validation() {
        return {
            "$id": "http://guru.sheenlac.com/schemas/create_reverse_debit_transaction.json",
            "type": "object",
            "additionalProperties": false
        };
    }

}

module.exports = CreateReverseDebitTransactionTask;