const TransactionModel = require('../transaction.model');

class FetchTransactionTask {

    static exec(transactionId, query = {}) {
        let Query = TransactionModel.findOne().where("_id").equals(transactionId);
        if (query.painter) Query.where("painter").equals(query.painter);
        return Query.populate("painter")
            .populate("ofProduct")
            .populate("ofGift")
            .select('-__V').exec();
    }

}

module.exports = FetchTransactionTask;