const Ajv = require('ajv');
const _ = require('lodash');

const AsyncBankVerificationTask = require('./async-bank-verification/async-bank-verification.task');
const AddBeneficiaryTask = require('./add-beneficiary/add-beneficiary.task');
const BankVerificationStatus = require('./bank-verification-status/bank-verification-status.task');
const CashTransferTask = require('./cash-transfer/cash-transfer.task');
const RemoveBeneficiaryTask = require('./remove-beneficiary/remove-beneficiary.task');
const GetBeneficiaryTask = require('./get-beneficiary/get-beneficiary.task');
const CashTransferStatusTask = require('./cash-transfer-status/cash-transfer-status.task');


class PayoutController{

    static addBeneficiary(req, res, next) {

        return AddBeneficiaryTask.exec(req.params.userId)
            .then( resp => {
                return res.status(200).json(
                    new res.app.locals.Success({ data: resp })
                );
            })
            .catch(err => next(err));
    }

    static getBeneficiary(req, res, next) {

        return GetBeneficiaryTask.exec(req.params.userId)
            .then( resp => {
                return res.status(200).json(
                    new res.app.locals.Success({ data: resp })
                );
            })
            .catch(err => next(err));
    }

    static removeBeneficiary(req, res, next) {

        return RemoveBeneficiaryTask.exec(req.params.userId)
            .then( resp => {
                return res.status(200).json(
                    new res.app.locals.Success({ data: resp })
                );
            })
            .catch(err => next(err));
    }

    static asyncBankVerification(req, res, next) {

        return AsyncBankVerificationTask.exec(req.params.userId)
            .then( resp => {
                return res.status(200).json(
                    new res.app.locals.Success({ data: resp })
                );
            })
            .catch(err => next(err));
    }

    static bankVerificationStatus(req, res, next) {

        return BankVerificationStatus.exec(req.params.userId)
            .then(resp => {
                console.log(resp);
                return res.status(200).json(
                    new res.app.locals.Success({ data: resp })
                );
            })
            .catch(err => next(err));
    }

    static cashTransfer(req, res, next) {

        let document = _.defaultsDeep(_.cloneDeep(req.body), { transactionId: req.params.transactionId });
        return CashTransferTask.exec(document)
            .then( resp => {
                return res.status(200).json(
                    new res.app.locals.Success({ data: resp })
                );
            })
            .catch(err => next(err));
    }

    static cashTransferStatus(req, res, next) {

        let document = _.defaultsDeep(_.cloneDeep(req.body), { transactionId: req.params.transactionId });
        return CashTransferStatusTask.exec(document)
            .then( resp => {
                return res.status(200).json(
                    new res.app.locals.Success({ data: resp })
                );
            })
            .catch(err => next(err));
    }
}

module.exports = PayoutController;