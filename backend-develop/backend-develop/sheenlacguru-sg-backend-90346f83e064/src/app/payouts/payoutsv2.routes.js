const express = require('express');
const nconf = require('nconf');
const router = express.Router();

const Token = require('@src/middleware/token');
const Authorization = require('@src/middleware/authorization');

const PayoutController = require('./payout.controller');

router.use(Token.verify({
    for: 'admin'
}));

router.get('/add_beneficiary/:userId', Authorization.authorize(["painterRegistrationManagement"]), PayoutController.addBeneficiary);
router.get('/get_beneficiary/:userId', Authorization.authorize(["painterRegistrationManagement"]), PayoutController.getBeneficiary);
router.delete('/remove_beneficiary/:userId', Authorization.authorize(["painterRegistrationManagement"]), PayoutController.removeBeneficiary);
router.get('/bank_verification/:userId', PayoutController.asyncBankVerification);
router.get('/bank_verification_status/:userId', Authorization.authorize(["painterRegistrationManagement"]), PayoutController.bankVerificationStatus);
router.post('/cash_transfer/:transactionId', Authorization.authorize(["painterRegistrationManagement"]), PayoutController.cashTransfer);
router.get('/cash_transfer_status/:transactionId', Authorization.authorize(["painterRegistrationManagement"]), PayoutController.cashTransferStatus);

module.exports = router;