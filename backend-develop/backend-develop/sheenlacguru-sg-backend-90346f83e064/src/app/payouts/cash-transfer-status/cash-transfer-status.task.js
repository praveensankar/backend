const axios = require('axios');
const nconf = require('nconf');
const uniqid = require('uniqid');

const ApplicationError = require('@src/errors/ApplicationError');

const AuthorizePayoutsTask = require('../authorize-payouts/authorize-payouts.task');
const UserModel = require('../../users/user.model');
const TransactionModel = require('../../transactions/transaction.model');
const FetchDebitTransactionTask = require('../../transactions/fetch_debit_transaction/fetch_debit_transaction.task');

const payoutsHost = nconf.get("CASHFREE:HOST");
const payoutsAPITimeout = nconf.get("CASHFREE:API_TIMEOUT");

class CashTransferStatusTask {

  static exec(document = {}) {
    console.log(document)
    const FETCH_USER = FetchDebitTransactionTask.exec(document.transactionId,{});

    return Promise.all([FETCH_USER])
      .then(([user]) => {
        // if(!user.bank.bvRefId && !user.bank.isBankDetailVerified){
        //   throw new ApplicationError({
        //     name: 'CashTransferStatusTaskFailed',
        //     message: 'Please verfiy user bank details.'
        //   });
        // }

        // if(!user.bank.beneId){
        //   throw new ApplicationError({
        //     name: 'CashTransferStatusTaskFailed',
        //     message: 'Please add beneficiary details.'
        //   });
        // }

        return AuthorizePayoutsTask.exec().then(tokenDetails => {
          if (!tokenDetails.token)
            throw new ApplicationError({
              name: 'CashTransferStatusAuthorizationError',
              message: 'Error retriving payouts authorization token.'
            }); 

          const config = {
            url: `${payoutsHost}/payout/v1/getTransferStatus?referenceId=${user.tracking_reference_number}`,
            method: 'GET',
            headers: {
              "Content-Type": "application/json",
              "Authorization": `Bearer ${tokenDetails.token}`,
            },
            timeout: payoutsAPITimeout
          };


          return axios.request(config);
        }).then(response => {
          const cashTransferStatusResponse = response.data;

          if (cashTransferStatusResponse.status ==='ERROR' && cashTransferStatusResponse.subCode != "200")
            throw new ApplicationError({
              name: "CashTransferStatusTaskFailed",
              message: cashTransferStatusResponse.message
            });

            const status = {
              "transferStatus.transferId" : cashTransferStatusResponse.data.transfer.transferId,
              "transferStatus.bankAccount" : cashTransferStatusResponse.data.transfer.bankAccount,
              "transferStatus.ifsc" : cashTransferStatusResponse.data.transfer.ifsc,
              "transferStatus.amount" : cashTransferStatusResponse.data.transfer.amount,
              "transferStatus.status" : cashTransferStatusResponse.data.transfer.status,
              "transferStatus.beneId" : cashTransferStatusResponse.data.transfer.beneId,
              "transferStatus.utr" : cashTransferStatusResponse.data.transfer.utr,
              "transferStatus.addedOn" : cashTransferStatusResponse.data.transfer.addedOn,
              "transferStatus.processedOn" : cashTransferStatusResponse.data.transfer.processedOn,
              "transferStatus.transferMode" : cashTransferStatusResponse.data.transfer.transferMode,
              "transferStatus.message" : cashTransferStatusResponse.data.transfer.reason
            };

          return TransactionModel.updateOne({}, {
              omitUndefined: true
            })
            .where('_id').equals(document.transactionId).where("mode").equals("DEBIT")
            .set(status).exec().then(response => {
                if (response.n < 1 || response.nModified < 1)
                throw new ApplicationError({ name: 'TransactionUpdateError', message: 'Transaction not updated.' });

                return FetchDebitTransactionTask.exec(document.transactionId,{});
            });
        }).catch(err => {
          throw new ApplicationError({
            name: err.name,
            message: err.message
          });
        });
      });
  }

  static validation() {
    return {
      "$id": "http://guru.sheenlac.com/schemas/users/cash-transfer-status.json",
      "type": "object",
      "additionalProperties": false
    };
  }

}

module.exports = CashTransferStatusTask;