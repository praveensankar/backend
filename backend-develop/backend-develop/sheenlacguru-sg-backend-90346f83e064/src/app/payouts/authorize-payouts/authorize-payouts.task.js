const Promise = require('bluebird');
const nconf = require('nconf');
const fs = require('fs');
const crypto = require('crypto');
const axios = require('axios');

const ApplicationError = require('@src/errors/ApplicationError');

const payoutsHost = nconf.get("CASHFREE:HOST");
const payoutsClientID = nconf.get("CASHFREE:CLIENT_ID");
const payoutsClientSecret = nconf.get("CASHFREE:CLIENT_SECRET");
const payoutsPublicKeyPath = nconf.get("CASHFREE:PATH_TO_PUBLIC_KEY");
const payoutsAPITimeout = nconf.get("CASHFREE:API_TIMEOUT");

function generatePayoutSignature(clientId, publicKeyPath) {
  const curTimeStamp = Date.now() / 1000;
  const plainText = `${clientId}.${curTimeStamp}`;

  const encrypted = crypto.publicEncrypt(
    {
      key: fs.readFileSync(publicKeyPath),
      padding: crypto.constants.RSA_PKCS1_OAEP_PADDING,
    },
    new Buffer.from(plainText),
  );
  return encrypted.toString('base64');
}

class AuthorizePayouts {

  static exec() {
    const config = {
      url: `${payoutsHost}/payout/v1/authorize`,
      method: 'POST',
      headers: {
        "X-Client-Id": payoutsClientID,
        "X-Client-Secret": payoutsClientSecret,
        "X-Cf-Signature": generatePayoutSignature(payoutsClientID, payoutsPublicKeyPath)
      },
      timeout: payoutsAPITimeout
    }
    return axios.request(config).then(response => {
      const payoutResponse = response.data;
      if (payoutResponse.subCode != "200")
        throw new ApplicationError({ name: "PayoutAuthorizationError", message: payoutResponse.message });

      return payoutResponse.data;
    });
  }
}

module.exports = AuthorizePayouts;