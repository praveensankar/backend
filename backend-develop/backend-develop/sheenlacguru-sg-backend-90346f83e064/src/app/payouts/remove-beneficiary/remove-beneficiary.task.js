const axios = require('axios');
const nconf = require('nconf');

const ApplicationError = require('@src/errors/ApplicationError');

const AuthorizePayoutsTask = require('../authorize-payouts/authorize-payouts.task');
const UserModel = require('../../users/user.model');

const payoutsHost = nconf.get("CASHFREE:HOST");
const payoutsAPITimeout = nconf.get("CASHFREE:API_TIMEOUT");

class RemoveBeneficiaryTask {

  static exec(userId) {

    const FETCH_USER = UserModel.where('_id').equals(userId)
      .findOne()
      .exec();

    return Promise.all([FETCH_USER])
      .then(([user]) => {
        
        if(!user.bank.bvRefId && !user.bank.isBankDetailVerified){
          throw new ApplicationError({
            name: 'RemoveBeneficiaryFailed',
            message: 'Please verfiy user bank details.'
          });
        }

        const removeBeneficiaryDetails = {
          beneId: `${user.bank.beneId}`,
        };

        return AuthorizePayoutsTask.exec().then(tokenDetails => {
          if (!tokenDetails.token)
            throw new ApplicationError({
              name: 'RemoveBeneficiaryAuthorizationError',
              message: 'Error retriving payouts authorization token.'
            });

          const config = {
            url: `${payoutsHost}/payout/v1/removeBeneficiary`,
            method: 'POST',
            headers: {
              "Content-Type": "application/json",
              "Authorization": `Bearer ${tokenDetails.token}`,
            },
            data: removeBeneficiaryDetails,
            timeout: payoutsAPITimeout
          };


          return axios.request(config);
        }).then(response => {
          const addBeneficiaryResponse = response.data;

          if (addBeneficiaryResponse.subCode != "200")
            throw new ApplicationError({
              name: "RemoveBeneficiaryFailed",
              message: addBeneficiaryResponse.message
            });

          const bankDocument = {
            'bank.beneId': ""
          };

          return UserModel.updateOne({}, {
              omitUndefined: true
            })
            .where('_id').equals(user._id)
            .set(bankDocument).exec();
        }).catch(err => {
          throw new ApplicationError({
            name: err.name,
            message: err.message
          });
        });
      });
  }

  static validation() {
    return {
      "$id": "http://guru.sheenlac.com/schemas/users/remove_beneficiary.json",
      "type": "object",
      "additionalProperties": false
    };
  }

}

module.exports = RemoveBeneficiaryTask;