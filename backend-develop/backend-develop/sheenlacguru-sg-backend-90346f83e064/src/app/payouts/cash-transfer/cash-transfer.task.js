const axios = require('axios');
const nconf = require('nconf');
const uniqid = require('uniqid');
const _ = require('lodash');

const ApplicationError = require('@src/errors/ApplicationError');

const AuthorizePayoutsTask = require('../authorize-payouts/authorize-payouts.task');
const UserModel = require('../../users/user.model');
const TransactionModel = require('../../transactions/transaction.model');
const FetchDebitTransactionTask = require('../../transactions/fetch_debit_transaction/fetch_debit_transaction.task');

const payoutsHost = nconf.get("CASHFREE:HOST");
const payoutsAPITimeout = nconf.get("CASHFREE:API_TIMEOUT");

class CashTransferTask {

  static exec(document = {}) {
    console.log(document)
    const FETCH_USER = UserModel.where('_id').equals(document.painter.id)
      .findOne()
      .exec();

    return Promise.all([FETCH_USER])
      .then(([user]) => {

        if (!user.bank.bvRefId && !user.bank.isBankDetailVerified) {
          throw new ApplicationError({
            name: 'CashTransferTaskFailed',
            message: 'Please verfiy user bank details.'
          });
        }

        // if(!user.bank.beneId){
        //   throw new ApplicationError({
        //     name: 'CashTransferTaskFailed',
        //     message: 'Please add beneficiary details.'
        //   });
        // }

        return AuthorizePayoutsTask.exec().then(tokenDetails => {
          if (!tokenDetails.token)
            throw new ApplicationError({
              name: 'CashTransferAuthorizationError',
              message: 'Error retriving payouts authorization token.'
            });

          let transferId = uniqid();

          const transferDetail = {
            beneId : `${user.bank.beneId}`,
            amount: `${document.points}`,
            transferId: `${transferId}`
          };

          const config = {
            url: `${payoutsHost}/payout/v1/requestTransfer`,
            method: 'POST',
            headers: {
              "Content-Type": "application/json",
              "Authorization": `Bearer ${tokenDetails.token}`,
            },
            data: transferDetail,
            timeout: payoutsAPITimeout
          };


          return axios.request(config);
        }).then(response => {
          let cashTransferResponse = response.data;

          if (cashTransferResponse.status === 'ERROR'){
            cashTransferResponse.time = new Date();
            return TransactionModel.updateOne({ _id : document._id, mode : "DEBIT" },{ $push : { transferLog : { ...cashTransferResponse }}})
              .exec().then(response => {
                
                throw new ApplicationError({
                  name: "CashTransferTaskFailed",
                  message: cashTransferResponse.message
                });
              }).catch(err => {
                throw new ApplicationError({
                  name: err.name,
                  message: err.message
                });
              });
          }else {
            const updateTransaction = {
              "status": cashTransferResponse.status === 'PENDING' ? "PROCESSING" : "PROCESSED",
              "tracking_reference_number": cashTransferResponse.data ? cashTransferResponse.data.referenceId : ""
            }
            cashTransferResponse.time = new Date();
            return TransactionModel.updateOne({ _id : document._id, mode : "DEBIT" },{ $push : { transferLog : { ...cashTransferResponse }}, $set : { ...updateTransaction } })
              .exec().then(response => {
                console.log(response);
                return response;
              }).catch(err => {
                throw new ApplicationError({
                  name: err.name,
                  message: err.message
                });
              });
          }
        }).catch(err => {
          throw new ApplicationError({
            name: err.name,
            message: err.message
          });
        });
      });
  }

  static validation() {
    return {
      "$id": "http://guru.sheenlac.com/schemas/users/cash-transfer.json",
      "type": "object",
      "additionalProperties": false
    };
  }

}

module.exports = CashTransferTask;