const axios = require('axios');
const nconf = require('nconf');

const ApplicationError = require('@src/errors/ApplicationError');

const AuthorizePayoutsTask = require('../authorize-payouts/authorize-payouts.task');
const UserModel = require('../../users/user.model');

const payoutsHost = nconf.get("CASHFREE:HOST");
const payoutsAPITimeout = nconf.get("CASHFREE:API_TIMEOUT");

class AddBeneficiaryTask {

  static exec(userId) {

    const FETCH_USER = UserModel.where('_id').equals(userId)
      .findOne()
      .exec();

    return Promise.all([FETCH_USER])
      .then(([user]) => {
        
        if(!user.bank.bvRefId && !user.bank.isBankDetailVerified){
          throw new ApplicationError({
            name: 'AddBeneficiaryFailed',
            message: 'Please verfiy user bank details.'
          });
        }

        const addBeneficiaryDetails = {
          beneId: `${user.mobile_number}_${user.bank.account}`,
          name: `${user.first_name} ${user.last_name}`,
          email: `${user.email}`,
          phone: `${user.mobile_number}`,
          bankAccount: `${user.bank.account}`,
          ifsc: `${user.bank.ifsc}`,
          address1: `${user.address.line1}`,
          address2: `${user.address.line2}`,
          city: `${user.address.city}`,
          state: `${user.address.state}`,
          pincode: Number(user.address.pincode)
        };

        return AuthorizePayoutsTask.exec().then(tokenDetails => {
          if (!tokenDetails.token)
            throw new ApplicationError({
              name: 'AddBeneficiaryAuthorizationError',
              message: 'Error retriving payouts authorization token.'
            });

          const config = {
            url: `${payoutsHost}/payout/v1/addBeneficiary`,
            method: 'POST',
            headers: {
              "Content-Type": "application/json",
              "Authorization": `Bearer ${tokenDetails.token}`,
            },
            data: addBeneficiaryDetails,
            timeout: payoutsAPITimeout
          };


          return axios.request(config);
        }).then(response => {
          const addBeneficiaryResponse = response.data;

          if (addBeneficiaryResponse.subCode != "200")
            throw new ApplicationError({
              name: "AddBeneficiaryFailed",
              message: addBeneficiaryResponse.message
            });

          const bankDocument = {
            'bank.beneId': addBeneficiaryDetails.beneId
          };

          return UserModel.updateOne({}, {
              omitUndefined: true
            })
            .where('_id').equals(user._id)
            .set(bankDocument).exec();
        }).catch(err => {
          throw new ApplicationError({
            name: err.name,
            message: err.message
          });
        });
      });
  }

  static validation() {
    return {
      "$id": "http://guru.sheenlac.com/schemas/users/add_beneficiary.json",
      "type": "object",
      "properties": {
        "name": {
          "type": "string"
        },
        "account": {
          "type": "string"
        },
        "accountHolder": {
          "type": "string"
        },
        "passbookPhoto": {
          "type": "string"
        },
        "ifsc": {
          "type": "string"
        },
        "branch": {
          "type": "string"
        }
      },
      "required": ["name", "account", "ifsc", "branch"],
      "additionalProperties": false
    };
  }

}

module.exports = AddBeneficiaryTask;