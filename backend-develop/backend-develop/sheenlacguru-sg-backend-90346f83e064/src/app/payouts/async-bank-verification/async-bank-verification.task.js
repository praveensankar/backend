const axios = require('axios');
const nconf = require('nconf');

const ApplicationError = require('@src/errors/ApplicationError');

const AuthorizePayoutsTask = require('../authorize-payouts/authorize-payouts.task');
const UserModel = require('../../users/user.model');
const BankVerificationStatus = require('../bank-verification-status/bank-verification-status.task');

const payoutsHost = nconf.get("CASHFREE:HOST");
const payoutsAPITimeout = nconf.get("CASHFREE:API_TIMEOUT");

class AsyncBankVerificationTask {

  static exec(userId) {

    const FETCH_USER = UserModel.where('_id').equals(userId)
      .findOne()
      .exec();

    return Promise.all([FETCH_USER])
      .then(([user]) => {
        const bankVerificationDetails = {
          name: `${user.first_name} ${user.last_name}`,
          phone: `${user.mobile_number}`,
          bankAccount: `${user.bank.account}`,
          ifsc: `${user.bank.ifsc}`
        };

        return AuthorizePayoutsTask.exec().then(tokenDetails => {
          if (!tokenDetails.token)
            throw new ApplicationError({
              name: 'AsyncBankVerificationAuthorizationError',
              message: 'Error retriving payouts authorization token.'
            });

          const config = {
            url: `${payoutsHost}/payout/v1/asyncValidation/bankDetails`,
            method: 'GET',
            headers: {
              "Content-Type": "application/json",
              "Authorization": `Bearer ${tokenDetails.token}`,
            },
            params: bankVerificationDetails,
            timeout: payoutsAPITimeout
          };

          return axios.request(config);
        }).then(response => {
          const asyncBankVerificationResponse = response.data;
          console.log(asyncBankVerificationResponse);
          if (asyncBankVerificationResponse.subCode != "200")
            throw new ApplicationError({
              name: "AsyncBankVerificationFailed",
              message: asyncBankVerificationResponse.message
            });

          const bankDocument = {
              'bank.bvRefId': asyncBankVerificationResponse.data.bvRefId,
              'bank.isBankDetailVerified' : false
          };

          return UserModel.updateOne({}, { omitUndefined: true })
            .where('_id').equals(user._id)
            .set(bankDocument).exec();
        }).catch(err => {
          throw new ApplicationError({
            name: err.name,
            message: err.message
          });
        });
      })
  }
}

module.exports = AsyncBankVerificationTask;