const axios = require('axios');
const nconf = require('nconf');

const ApplicationError = require('@src/errors/ApplicationError');

const AuthorizePayoutsTask = require('../authorize-payouts/authorize-payouts.task');
const UserModel = require('../../users/user.model');

const payoutsHost = nconf.get("CASHFREE:HOST");
const payoutsAPITimeout = nconf.get("CASHFREE:API_TIMEOUT");

class BankVerificationStatus {

  static exec(userId) {

    const FETCH_USER = UserModel.where('_id').equals(userId)
      .findOne()
      .exec();

    return Promise.all([FETCH_USER])
      .then(([user]) => {
        console.log("user2",user.bank.bvRefId);
        return AuthorizePayoutsTask.exec().then(tokenDetails => {
          if (!tokenDetails.token)
            throw new ApplicationError({
              name: 'BankVerificationStatusAuthorizationError',
              message: 'Error retriving payouts authorization token.'
            });
        
          const config = {
            url: `${payoutsHost}/payout/v1/getValidationStatus/bank?bvRefId=${Number(user.bank.bvRefId)}`,
            method: 'GET',
            headers: {  
              "Content-Type": "application/json",
              "Authorization": `Bearer ${tokenDetails.token}`,
            },
            timeout: payoutsAPITimeout
          };


          return axios.request(config);
        }).then(response => {
          const bankVerificationStatus = response.data;
          console.log("status",bankVerificationStatus);
          if (bankVerificationStatus.subCode != "200")
            throw new ApplicationError({
              name: "BankVerificationStatusFailed",
              message: bankVerificationStatus.message
            });
            const bankDocument = {
                'bank.bvRefId': bankVerificationStatus.data.bvRefId,
                'bank.bankVerificationStatus.accountExists' : bankVerificationStatus.data.accountExists,
                'bank.bankVerificationStatus.bankAccount' : bankVerificationStatus.data.bankAccount,
                'bank.bankVerificationStatus.ifsc' : bankVerificationStatus.data.ifsc,
                'bank.bankVerificationStatus.message' : bankVerificationStatus.message,
                'bank.isBankDetailVerified' : bankVerificationStatus.data.accountExists !== "NO" ? true : false
            };

          return UserModel.updateOne({}, {
              omitUndefined: true
            })
            .where('_id').equals(user._id)
            .set(bankDocument).exec().then(response => {
                if (response.n < 1 || response.nModified < 1)
                throw new ApplicationError({ name: 'BankDetailsError', message: 'Bank verification status not updated.' });
                return { message : "Bank Verification Status"}
                // const FETCH_USER = UserModel.where('_id').equals(userId)
                // .findOne()
                // .exec();

                // return Promise.all([FETCH_USER]).then(([user]) => {
                //   return user.bank
                // });
            })
        }).catch(err => {
          throw new ApplicationError({
            name: err.name,
            message: err.message
          });
        });
      });
  }

  static validation() {
    return {
      "$id": "http://guru.sheenlac.com/schemas/users/bank-verification-status.json",
      "type": "object",
      "additionalProperties": false
    };
  }

}

module.exports = BankVerificationStatus;