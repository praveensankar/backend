const axios = require('axios');
const nconf = require('nconf');

const ApplicationError = require('@src/errors/ApplicationError');

const AuthorizePayoutsTask = require('../authorize-payouts/authorize-payouts.task');
const UserModel = require('../../users/user.model');

const payoutsHost = nconf.get("CASHFREE:HOST");
const payoutsAPITimeout = nconf.get("CASHFREE:API_TIMEOUT");

class GetBeneficiaryTask {

  static exec(userId,beneficiaryDetail= false) {

    const FETCH_USER = UserModel.where('_id').equals(userId)
      .findOne()
      .exec();

    return Promise.all([FETCH_USER])
      .then(([user]) => {
        
        // if(!user.bank.beneId){
        //   throw new ApplicationError({
        //     name: 'GetBeneficiaryFailed',
        //     message: 'Please verfiy user bank details and add beneficiary.'
        //   });
        // }

        return AuthorizePayoutsTask.exec().then(tokenDetails => {
          if (!tokenDetails.token)
            throw new ApplicationError({
              name: 'GetBeneficiaryAuthorizationError',
              message: 'Error retriving payouts authorization token.'
            });

          const config = beneficiaryDetail ? {
            url: `${payoutsHost}/payout/v1/getBeneficiary/${user.bank.beneId}`,
            method: 'GET',
            headers: {
              "Content-Type": "application/json",
              "Authorization": `Bearer ${tokenDetails.token}`,
            },
            timeout: payoutsAPITimeout
          } : {
            url: `${payoutsHost}/payout/v1/getBeneId?bankAccount=${user.bank.account}&ifsc=${user.bank.ifsc}`,
            method: 'GET',
            headers: {
              "Content-Type": "application/json",
              "Authorization": `Bearer ${tokenDetails.token}`,
            },
            timeout: payoutsAPITimeout
          };


          return axios.request(config);
        }).then(response => {
          const getBeneficiaryResponse = response.data;

          if (getBeneficiaryResponse.subCode != "200")
            throw new ApplicationError({
              name: "GetBeneficiaryFailed",
              message: getBeneficiaryResponse.message
            });

          return getBeneficiaryResponse;
        }).catch(err => {
          throw new ApplicationError({
            name: err.name,
            message: err.message
          });
        });
      });
  }

  static validation() {
    return {
      "$id": "http://guru.sheenlac.com/schemas/users/get_beneficiary.json",
      "type": "object",
      "additionalProperties": false
    };
  }

}

module.exports = GetBeneficiaryTask;