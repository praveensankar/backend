var list_options_schema = {
    "$id": "http://guru.sheenlac.com/schemas/list_options.json",
    "$async": true,
    "type": "object",
    "properties": {
        "page": { "type": "integer", "default": 1 },
        "size": { "type": "integer", "default": 10 }
    },
    "required": [],
    "additionalProperties": false
}

module.exports = list_options_schema;