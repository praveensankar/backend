module.exports = {
    ajv_options: { "$data": true, "allErrors": true, "removeAdditional": true, "useDefaults": true, "coerceTypes": "array" },
    list_options: require('./list_options')
}