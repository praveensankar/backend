const AdminRolesModel = require("../admin_roles.model");
const AdminAccessConfig = require('../../../configs/AdminAccessConfig.json');
const generalAccessPreviledges = AdminAccessConfig.generalAccessPreviledges.map(access => access.accessPreviledgeSlug);
const superAccessPreviledges = AdminAccessConfig.superAccessPreviledges.map(access => access.accessPreviledgeSlug);

const ApplicationError = require("@src/errors/ApplicationError");

class UpdateAdminRolesTask {

    static exec(id, document) {
        return AdminRolesModel.where('_id').equals(id).updateOne({}, {
                omitUndefined: true
            }).set(document)
            .exec()
            .then((update_resp) => {
                if (update_resp.n < 1 || update_resp.nModified < 1)
                    throw new ApplicationError({
                        name: "UpdateError",
                        message: "Updation of admin role failed."
                    });

                return {
                    matched: update_resp.n,
                    updated: update_resp.nModified,
                    message: "Admin Role updated."
                };
            });
    }

    static validation() {
        return {
            "$id": "http://guru.sheenlac.com/schemas/update_admin_role.json",
            "type": "object",
            "properties": {
                "role_name": {
                    "type": "string",
                    "minLength": 3
                },
                "role_status": {
                    "type": "string",
                    "enum": ["ACTIVE", "INACTIVE"]
                },
                "role_access": {
                    "type" : "object",
                    "properties" : {
                        "superAccessPreviledges" : {
                            "type": "array",
                            "uniqueItems": true,
                            "items": {
                                "type": "string",
                                "enum": superAccessPreviledges
                            }
                        },
                        "generalAccessPreviledges" : {
                            "type": "array",
                            "uniqueItems": true,
                            "items": {
                                "type": "string",
                                "enum": generalAccessPreviledges
                            }
                        }
                    }
                }
            },
            "additionalProperties": false
        };
    }
}

module.exports = UpdateAdminRolesTask;