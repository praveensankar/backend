const Ajv = require('ajv');
const _ = require("lodash");

const ApplicationError = require("@src/errors/ApplicationError");
const validations = require('@app/validations');
const CreateAdminRoleTask = require('./create_admin_role/create_admin_role.task');
const ListAdminRoleTask = require('./list_admin_role/list_admin_role.task');
const GetAdminRoleTask = require('./get_admin_role/get_admin_role.task');
const UpdateAdminRolesTask = require('./update_admin_role/update_admin_role.task');
const DeleteAdminRolesTask = require('./delete_admin_role/delete_admin_role.task');
const ajv = new Ajv(validations.ajv_options);

class AdminRolesController {

    static create(req, res, next) {
        if (!ajv.validate(CreateAdminRoleTask.validation(), req.body)) {
            let errors = ajv.errors;
            return next(
                new ApplicationError({
                    status: 400,
                    name: "ValidationError",
                    message: "Bad request parameters.",
                    info: errors
                })
            );
        }

        return CreateAdminRoleTask.exec(req.body)
            .then((response) => {
                return res.status(200).json(
                    new res.app.locals.Success({
                        data: response
                    })
                );
            })
            .catch((err) => {
                return next(err);
            });
    }

    static list(req, res, next) {
        return ListAdminRoleTask.exec({}, req.query)
            .then((roles) => {
                return res.status(200).json(
                    new res.app.locals.Success({
                        data: roles
                    })
                );
            })
            .catch((err) => {
                return next(err);
            });
    }

    static show(req, res, next) {
        return GetAdminRoleTask.exec(req.params.id)
            .then((role) => {
                console.log(role)
                return res.status(200).json(
                    new res.app.locals.Success({
                        data: { role }
                    })
                );
            })
            .catch((err) => {
                return next(err);
            });
    }

    static update(req, res, next) {
        if (!ajv.validate(UpdateAdminRolesTask.validation(), req.body)) {
            let errors = ajv.errors;
            return next(
                new ApplicationError({
                    status: 400,
                    name: "ValidationError",
                    message: "Bad request parameters",
                    info: errors
                })
            );
        }

        UpdateAdminRolesTask.exec(req.params.id, req.body)
            .then((response) => {
                return res.status(200).json(
                    new res.app.locals.Success({
                        data: response
                    })
                );
            })
            .catch((err) => {
                return next(err);
            });
    }

    static delete(req, res, next) {
        DeleteAdminRolesTask.exec(req.params.id)
            .then((response) => {
                return res.status(200).json(
                    new res.app.locals.Success({
                        data: response
                    })
                );
            })
            .catch((err) => {
                return next(err);
            });
    }
}

module.exports = AdminRolesController;