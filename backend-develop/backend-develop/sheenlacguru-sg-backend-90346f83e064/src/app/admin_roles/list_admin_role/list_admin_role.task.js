const AdminRolesModel = require("../admin_roles.model");
const Promise = require('bluebird');
const _ = require("lodash");
class ListAdminRoleTask {

    static async exec(query = {}, options = {}) {

        const skip = options.page > 0 ? (options.page - 1) * options.size : 0;

        let adminroles = [];
        let adminRolesCount = 0;

        // search when search key exist in options
        if (options.searchText !== "") {
            /* Start of Search */

            /* ------- Search with exact match using text based search */
            const adminrolesWithExactMatch = await AdminRolesModel.find({
                $text: {
                    $search: options.searchText
                }
            });
            /* -------- Search with start (role_name,_id) */
            const rgxForStartMatch = (pattern) => new RegExp(`^${pattern}`); // pattern for matching start of searchText
            const searchTextForStartMatch = rgxForStartMatch(options.searchText);
            const adminrolesWithStartMatch = await AdminRolesModel.find({
                $or: [{
                    role_name: {
                        $regex: searchTextForStartMatch,
                        $options: 'i'
                    }
                }]
            });

            /* -------- Fuzzy search with (role_name,_id,mobile_number,email) */
            const rgxForFuzzyMatch = (pattern) => new RegExp(`${pattern}`);
            const searchTextForFuzzyMatch = rgxForFuzzyMatch(options.searchText);
            const adminrolesWithFuzzyMatch = await AdminRolesModel.find({
                $or: [{
                    role_name: {
                        $regex: searchTextForFuzzyMatch,
                        $options: 'i'
                    }
                }]
            });

            const adminlist = _.concat(adminrolesWithExactMatch, adminrolesWithStartMatch, adminrolesWithFuzzyMatch);

            /* generate list of distinct adminroles if adminroles got repeated */
            adminroles = _.concat([], _.uniqBy(adminlist, 'role_name'));
            adminRolesCount = adminroles.length;
            adminroles = adminroles.slice(skip).slice(0, options.size)

            /* End of Search */
        } else {
            /* fetch adminroles if no search key is present on options */
            const AdminRolesQuery = AdminRolesModel.find(query);
            if (options.status) AdminRolesQuery.where("role_status").equals(options.status);
            adminroles = await AdminRolesQuery.skip(skip).limit(Number(options.size)).exec();
            adminRolesCount = await AdminRolesModel.countDocuments().exec();
        }

        return Promise.all([
            adminroles,
            adminRolesCount
        ]).then(([adminRoles, count]) => {
            return {
                items: adminRoles,
                totalCount: count
            };
        });
    }
}

module.exports = ListAdminRoleTask;