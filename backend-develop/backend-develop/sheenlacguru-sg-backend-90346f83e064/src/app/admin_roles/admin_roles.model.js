const mongoose = require('mongoose');
const AdminAccessConfig = require('../../configs/AdminAccessConfig.json');
const generalAccessPreviledges = AdminAccessConfig.generalAccessPreviledges.map(access => access.accessPreviledgeSlug);
const superAccessPreviledges = AdminAccessConfig.superAccessPreviledges.map(access => access.accessPreviledgeSlug);

const Schema = mongoose.Schema;


const AccessPreviledges = new Schema({
    generalAccessPreviledges : {
        type : [String],
        trim: true,
        enum : generalAccessPreviledges
    },
    superAccessPreviledges : {
        type : [String],
        trim: true,
        enum : superAccessPreviledges
    }
});

const AdminRolesSchema = new Schema({
    role_name: {
        type: String,
        trim: true,
        unique: true
    },
    role_status: {
        type: String,
        enum: ['ACTIVE', 'INACTIVE'],
        default: "INACTIVE"
    },
    role_access: {
        type: AccessPreviledges,
        trim: true
    }
}, {
    timestamps: true
});

AdminRolesSchema.set('toJSON', {
    virtuals: true
});


module.exports = mongoose.model("AdminRoles", AdminRolesSchema);