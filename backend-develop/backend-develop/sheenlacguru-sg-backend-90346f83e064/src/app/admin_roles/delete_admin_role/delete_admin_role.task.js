const AdminRolesModel = require('../admin_roles.model')

const ApplicationError = require("@src/errors/ApplicationError");

class DeleteAdminRolesTask {

    static exec(id) {
        return AdminRolesModel.deleteOne().where('_id').equals(id)
            .exec()
            .then((op_resp) => {
                if (op_resp.ok != 1 || op_resp.deletedCount < 1) {
                    throw new ApplicationError({
                        name: "DeleteError",
                        message: "Admin Role deletion failed."
                    });
                }

                return {
                    deleted: op_resp.deletedCount,
                    message: "Admin Role deleted."
                };
            });
    }

}

module.exports = DeleteAdminRolesTask;