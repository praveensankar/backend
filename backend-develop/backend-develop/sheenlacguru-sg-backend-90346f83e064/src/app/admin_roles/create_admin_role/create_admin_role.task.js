const AdminRolesModel = require("../admin_roles.model");
const AdminAccessConfig = require('../../../configs/AdminAccessConfig.json');
const generalAccessPreviledges = AdminAccessConfig.generalAccessPreviledges.map(access => access.accessPreviledgeSlug);
const superAccessPreviledges = AdminAccessConfig.superAccessPreviledges.map(access => access.accessPreviledgeSlug);

const ApplicationError = require("@src/errors/ApplicationError");

class CreateAdminRoleTask {

    static exec(document) {
        return new AdminRolesModel(document).save().then(adminRole => {
            if (!adminRole)
                throw new ApplicationError({
                    name: "CreateAdminRoleError",
                    message: "Admin Role is not created."
                });

            return adminRole;
        }).catch(err => {
            if (err.code === 11000 && err.name === 'MongoError')
                throw new ApplicationError({
                    name: err.name,
                    message: "Admin Role Name should be unique."
                });
        })
    }

    static validation() {
        return {
            "$id": "http://guru.sheenlac.com/schemas/create_admin_role.json",
            "type": "object",
            "properties": {
                "role_name": {
                    "type": "string",
                    "minLength": 3
                },
                "role_status": {
                    "type": "string",
                    "enum": ["ACTIVE", "INACTIVE"]
                },
                "role_access": {
                    "type" : "object",
                    "properties" : {
                        "superAccessPreviledges" : {
                            "type": "array",
                            "uniqueItems": true,
                            "items": {
                                "type": "string",
                                "enum": superAccessPreviledges
                            }
                        },
                        "generalAccessPreviledges" : {
                            "type": "array",
                            "uniqueItems": true,
                            "items": {
                                "type": "string",
                                "enum": generalAccessPreviledges
                            }
                        }
                    }
                }
            },
            "required": ["role_name"],
            "additionalProperties": false
        };
    }
}

module.exports = CreateAdminRoleTask;