const AdminRolesModel = require("../admin_roles.model");

const ApplicationError = require("@src/errors/ApplicationError");

class GetAdminRoleTask {

    static exec(id) {
        return AdminRolesModel.findById(id).exec().then((admin) => {
            if (!admin)
                throw new ApplicationError({
                    name: "AdminRoleError",
                    message: "Admin Role is not found."
                });
            return admin;
        });
    }
}

module.exports = GetAdminRoleTask;