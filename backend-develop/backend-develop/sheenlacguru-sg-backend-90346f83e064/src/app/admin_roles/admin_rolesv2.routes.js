const express = require('express');
const router = express.Router();

const Token = require('@src/middleware/token');
const Authorization = require('@src/middleware/authorization');
const AdminRolesController = require('./admin_roles.controller');

router.use(Token.verify({
    for: 'admin'
}));

router.get('/', Authorization.authorize(["adminRoleManagement"]), AdminRolesController.list);
router.post('/', Authorization.authorize(["adminRoleManagement"]), AdminRolesController.create);
router.get('/:id', Authorization.authorize(["adminRoleManagement"]), AdminRolesController.show);
router.delete('/:id', Authorization.authorize(["adminRoleManagement"]), AdminRolesController.delete);
router.patch('/:id', Authorization.authorize(["adminRoleManagement"]), AdminRolesController.update);

module.exports = router;