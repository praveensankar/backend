const Ajv = require('ajv');
const _ = require('lodash');

const ApplicationError = require('@src/errors/ApplicationError');
const validations = require('@app/validations');
const ajv = new Ajv(validations.ajv_options);

const AddUserPortfolio = require('./add_user_portfolio/add_user_portfolio.task');
const ListUserPortfolio = require('./list_user_portfolio/list_user_portfolio.task');
const DeleteUserPortfolio = require('./delete_user_portfolio/delete_user_portfolio.task');

class UserPortfolioController {

    static index(req, res, next) {
        if (!ajv.validate(ListUserPortfolio.validation(), req.query)) {
            let errors = ajv.errors;
            return next(
                new ApplicationError({
                    status: 400,
                    name: "ValidationError",
                    message: "Bad request parameters.",
                    info: errors
                })
            );
        }
        let document = _.defaultsDeep(_.cloneDeep(req.query), {
            painter: req.locals.user_id
        });
        return ListUserPortfolio.exec(document)
            .then((data) => {
                return res.status(200).json(
                    new res.app.locals.Success({
                        data: data
                    })
                );
            })
            .catch((err) => {
                return next(err);
            });
    }

    static add(req, res, next) {
        if (req.file) req.body["image"] = req.file.key;
        if (!ajv.validate(AddUserPortfolio.validation(), req.body)) {
            let errors = ajv.errors;
            return next(
                new ApplicationError({
                    status: 400,
                    name: "ValidationError",
                    message: "Bad request parameters.",
                    info: errors
                })
            );
        }

        let document = _.defaultsDeep(_.cloneDeep(req.body), {
            painter: req.locals.user_id
        });
        // return res.status(200).json(document);
        return AddUserPortfolio.exec(document)
            .then((_) => {
                return res.status(200).json(
                    new res.app.locals.Success({
                        data: {
                            message: "Image added to painter portfolio."
                        }
                    })
                );
            })
            .catch((err) => {
                return next(err);
            });
    }

    static delete(req, res, next) {
        return DeleteUserPortfolio.exec(req.params.id,req.locals.user_id)
            .then((op_resp) => {
                return res.status(200).json(
                    new res.app.locals.Success({
                        data: op_resp
                    })
                );
            })
            .catch((err) => {
                return next(err);
            });
    }

}

module.exports = UserPortfolioController;