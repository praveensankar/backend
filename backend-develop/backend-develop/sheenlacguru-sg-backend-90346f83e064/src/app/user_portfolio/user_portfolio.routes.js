const express = require('express');
const nconf = require('nconf');
const router = express.Router();

const Token = require('@src/middleware/token');
const Authorization = require('@src/middleware/authorization');
const Uploader = require('@src/middleware/uploader.middleware');

const UserPortfolioController = require('./user_portfolio.controller');

router.use(Token.verify({
    for: 'admin'
}));

router.post('/', Authorization.authorize(["painter", "contractor"]), Uploader({
    dest: nconf.get("STORAGE:PAINTERPORTFOLIO")
}).single('image'), UserPortfolioController.add);
router.get('/', Authorization.authorize(["painter", "contractor"]), UserPortfolioController.index);
router.delete('/:id', Authorization.authorize(["painter", "contractor"]), UserPortfolioController.delete);

module.exports = router;