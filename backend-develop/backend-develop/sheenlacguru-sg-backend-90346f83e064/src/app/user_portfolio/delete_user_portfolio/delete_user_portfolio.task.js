const ApplicationError = require('@src/errors/ApplicationError');

const UserPortfolioModel = require('../user_portfolio.model');

class DeleteUserPortfolio {

    static exec(portfolioId,painter) {
        return UserPortfolioModel.deleteOne().where('_id').equals(portfolioId).where("painter").equals(painter)
            .exec()
            .then((op_resp) => {
                if (op_resp.ok != 1 || op_resp.deletedCount < 1)
                    throw new ApplicationError({ name: 'DeleteUserPortfolioError', message: 'Image not deleted.' });

                return { deleted: op_resp.deletedCount, message: 'Image deleted.' };
            });
    }

}

module.exports = DeleteUserPortfolio;