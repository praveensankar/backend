const uniqid = require('uniqid');
const ApplicationError = require('@src/errors/ApplicationError');

const UserPortfolioModal = require('../user_portfolio.model');

class AddUserPortfolio {

    static exec(document) {
        return new UserPortfolioModal(document).save()
            .then((portfolio) => {
                if (!portfolio)
                    throw new ApplicationError({
                        name: 'CreateUserPortfolioError',
                        message: 'Image not added'
                    });
                return portfolio;
            });
    }

    static validation() {
        return {
            "$id": "http://guru.sheenlac.com/schemas/add_user_portfolio.json",
            "type": "object",
            "properties": {
                "image": {
                    "type": "string"
                }
            },
            "required": ["image"],
            "additionalProperties": false
        };
    }

}

module.exports = AddUserPortfolio;