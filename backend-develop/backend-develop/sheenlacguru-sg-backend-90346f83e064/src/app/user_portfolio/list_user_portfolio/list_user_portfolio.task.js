const UserPortfolioModel = require('../user_portfolio.model');

class ListUserPortfolio {
    static async exec(options = {}) {
        // const skip = options.page > 0 ? (options.page - 1) * options.size : 0;
        try {
            const images = await UserPortfolioModel.find().where('painter').equals(options.painter);
            if (images.length < 1) {
                throw new ApplicationError({
                    name: 'ListPortfolioError',
                    message: 'No images found'
                });
            }
            return images;
        } catch (error) {
            throw new ApplicationError({
                name: error.name,
                message: error.message
            });
        }
    }

    static validation() {
        return {
            "$id": "http://guru.sheenlac.com/schemas/list_user_portfolio.json",
            "$async": true,
            "type": "object",
            "properties": {
                "page": { "type": "integer", "default": 1 },
                "size": { "type": "integer", "default": 0 }
            },
            "required": [],
            "additionalProperties": false
        }
    }

}

module.exports = ListUserPortfolio;