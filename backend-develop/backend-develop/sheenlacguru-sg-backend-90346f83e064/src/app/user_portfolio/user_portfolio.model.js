const mongoose = require('mongoose');
const nconf = require('nconf');

const Schema = mongoose.Schema;

const SERVER_HOST = nconf.get('APPLICATION:S3');

const UserPortfolioSchema = new Schema({
    painter: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: [true, 'Painter is required.']
    },
    image: {
        type: String,
        get: (value) => `${SERVER_HOST}${value}`,
        required: [true, 'Image is required.'],
        trim: true
    }
}, {
    timestamps: true
});

UserPortfolioSchema.set('toJSON', {
    virtuals: true,
    getters: true
});

module.exports = mongoose.model('UserPortfolio', UserPortfolioSchema);