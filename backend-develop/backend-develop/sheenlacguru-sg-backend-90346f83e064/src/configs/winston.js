const {
    createLogger,
    format,
    transports
} = require('winston');
require('winston-daily-rotate-file');
const {
    prettyPrint
} = format;

// const syncUuidInfoTransport = new transports.DailyRotateFile({
//     json: true,
//     level: 'info',
//     datePattern: 'YYYY-MM-DD',
//     filename: 'sync-uuids-%DATE%',
//     maxSize: '100m',
//     extension: '.log',
// });

// syncUuidInfoTransport.on('new', function (newFilename) {
//     logger.info({ new: newFilename });
// });
// syncUuidInfoTransport.on('rotate', function (oldFilename, newFilename) {
//     logger.info({
//         old: oldFilename,
//         new: newFilename
//     });
// });

// syncUuidInfoTransport.on('logRemoved', function (removedFilename) {
//     logger.info({
//         removed: removedFilename
//     });
// });

// const syncUuidDuplicateTransport = new transports.DailyRotateFile({
//     json: true,
//     level: 'warn',
//     datePattern: 'YYYY-MM-DD',
//     filename: 'sync-uuids-duplicate-%DATE%',
//     maxSize: '100m',
//     extension: '.log',
// });

// syncUuidDuplicateTransport.on('new', function (newFilename) {
//     logger.info({ new: newFilename });
// });
// syncUuidDuplicateTransport.on('rotate', function (oldFilename, newFilename) {
//     logger.info({
//         old: oldFilename,
//         new: newFilename
//     });
// });

// syncUuidDuplicateTransport.on('logRemoved', function (removedFilename) {
//     logger.info({
//         removed: removedFilename
//     });
// });

const debugTransport = new transports.DailyRotateFile({
    json: true,
    level: 'debug',
    datePattern: 'YYYY-MM-DD',
    filename: 'debug-%DATE%',
    maxSize: '100m',
    extension: '.log',
});

debugTransport.on('new', function (newFilename) {
    logger.info({
        new: newFilename
    });
});
debugTransport.on('rotate', function (oldFilename, newFilename) {
    logger.info({
        old: oldFilename,
        new: newFilename
    });
});

debugTransport.on('logRemoved', function (removedFilename) {
    logger.info({
        removed: removedFilename
    });
});

const logger = createLogger({
    format: format.combine(
        format.timestamp({
            format: 'YYYY-MM-DD HH:mm:ss',
        }),
        format.errors({
            stack: true
        }),
        prettyPrint(),
        format.splat(),
        format.json(),
    ),
    exitOnError : false,
    transports: [
        // syncUuidInfoTransport,
        // syncUuidDuplicateTransport,
        debugTransport
    ],
});

//
// If we're not in production then **ALSO** log to the `console`
// with the colorized simple format.
//
// if (process.env.NODE_ENV !== 'production') {
    logger.add(new transports.Console({
        format: format.combine(
            format.colorize(),
            format.simple(),
        ),
    }));
// }

module.exports = logger;